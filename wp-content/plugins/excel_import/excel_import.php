<?php
/**
* Plugin Name: Excel Import
* Plugin URI: 
* Description: Excel Import
* Version: 1.0
* Author: SY
* Author URI: 
**/

// Create table on plugin activation
function myplugin_activate() {
    global $wpdb;
	$table_name = $wpdb->prefix . 'custom_sitemap';

    $sql="CREATE TABLE IF NOT EXISTS $table_name (
		`sitemap_id` int(11) NOT NULL,
		`url` longtext NOT NULL,
		`created_date` datetime NOT NULL DEFAULT current_timestamp()
	  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";
	$wpdb->query($sql);

	$sql="ALTER TABLE  $table_name
	ADD PRIMARY KEY (`sitemap_id`);";
	$wpdb->query($sql);
   
	$sql="ALTER TABLE  $table_name
	MODIFY `sitemap_id` int(11) NOT NULL AUTO_INCREMENT;
  COMMIT;";
	$wpdb->query($sql);

	$sql="ALTER TABLE $table_name CHANGE `sitemap_id` `sitemap_id` INT(11) NOT NULL AUTO_INCREMENT;";
	$wpdb->query($sql);
}
register_activation_hook( __FILE__, 'myplugin_activate' );

// Load CSS in admin panel
add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
  echo "<style>
   input[type='checkbox']:checked::before {
       height: 33px !important;
       width: 21px !important;
   }
  </style>";	
  /*wp_enqueue_style( 'admin_css', plugins_url('bootstrap.min.css', __FILE__) );
  wp_enqueue_style( 'admin_style_css', plugins_url('style.css', __FILE__) );
  wp_enqueue_script( 'my-script', plugins_url('bootstrap-filestyle.min.js', __FILE__));
  wp_enqueue_style( 'alertify_style_css', plugins_url('alert/css/alertify.css', __FILE__) );
  wp_enqueue_script( 'alertify-script', plugins_url('alert/alertify.js', __FILE__));*/
}
// Load JS in admin panel
add_action('admin_footer', 'custom_admin_js');
function custom_admin_js() {
?>
  <script type="text/javascript">
    function addUrl(){
		var excel_sheet=jQuery("#excel_sheet").val();
		if(excel_sheet != ""){
		var myform = document.getElementById("upload_excel");
		var myform= new FormData(myform);
		myform.append("action", "add_url");
		jQuery("#loader_form").show(); 	
		jQuery.ajax({
	  			url: "<?php echo admin_url("admin-ajax.php");?>",
	  			method:"POST",
	  			data:myform,
				contentType:false,  
				processData: false,   
	  			success: function(result){
	  				jQuery("#loader_form").hide(); 	
	  				if(result=="1"){	  
			             //jQuery(".msg").html("Deleted Successfully");
										alertify
						.alert("Added Successfully.", function(){
							window.location.href = '';
						});
			        }else if(result=="2"){
			        	alertify
						.alert("Please Select .csv File.	", function(){
						});
			        }else if(result=="3"){
			        	alertify
						.alert("Records should not greater than 7000", function(){
						});
			        }
	            
		   }});
	  }else{
	  	alertify
				.alert("Please Select .csv File.", function(){
				});
	  }
	}
  	function deleteUrl(){
  		var urlArray = [];
  		jQuery("input:checkbox[name=delete_urls]:checked").each(function(){
           urlArray.push(jQuery(this).val());
		 });
		 if(urlArray != ""){
			jQuery(".loader_class").show(); 
	  		jQuery.ajax({
	  			url: "<?php echo admin_url("admin-ajax.php");?>",
	  			method:"POST",
	  			data:{'action':"delete_url",'urls':urlArray}, 
	  			success: function(result){
					jQuery(".loader_class").hide(); 		  
	             //jQuery(".msg").html("Deleted Successfully");
								alertify
				.alert("Deleted Successfully.", function(){
					window.location.href = '';
				});
	            
		   }});
		}else{
			alertify
				.alert("Please select atleast one checkbox", function(){
				});

		}  
  	}
	
	function copy_url(id){
		var copyText = document.getElementById("copy_url_"+id);
		/* Select the text field */
		copyText.select();
		/* Copy the text inside the text field */
		document.execCommand("copy");
		/* Alert the copied text */
		alert("Copied the Url");
	}
  </script>
<?php    
}

// Delete url using ajax
add_action("wp_ajax_delete_url", "delete_url_fn");
add_action("wp_ajax_nopriv_delete_url", "delete_url_fn");
function delete_url_fn(){
   $newUrls = array();	
   $urls=$_POST["urls"];
   foreach($urls as $url){
	  //array_push($newUrls, array('url' => $url, 'date' => time()));
	  $data=array("sitemap_id"=>$url);
	  global $wpdb;
	  $table = $wpdb->prefix.'custom_sitemap';
	  $wpdb->delete($table,$data);
   }
   //update_option("simple_wp_other_urls",$newUrls);
}
add_action("wp_ajax_add_url", "add_url_fn");
add_action("wp_ajax_nopriv_add_url", "add_url_fn");
function add_url_fn(){
	if(!empty($_POST)){
		$filename=$_FILES["excel_sheet"]["tmp_name"];
		$ext=pathinfo($_FILES["excel_sheet"]["name"],PATHINFO_EXTENSION);
		 if($ext=="csv"){   
		 if($_FILES["excel_sheet"]["size"] > 0)
		 {
			$file = fopen($filename, "r");
			  $i=1;
				$data = file_get_contents($filename);
				$rows = explode("\n",$data);
				$rows = array_map("utf8_encode", $rows);
				$s = array();
				$newUrls = array();
				if(count($rows) > 7000){
					echo "3";
					die;
				}
				foreach($rows as $row) {				
					if(!empty($row)){
                        global $wpdb;
					    $table = $wpdb->prefix.'custom_sitemap';
						$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_NAME);
						$row=str_replace('"', "", $row);
                        $row=str_replace("'", "&apos;", $row);
                        $row=str_replace("(", "&#40;", $row);
                        $row=str_replace(")", "&#41;", $row);
                        $option = $wpdb->get_results( "SELECT * FROM $table where url='".$row."'");
                        if(empty($option)){
							$sql = "INSERT INTO ".$table." (url) VALUES ('".$row."')";
	                        mysqli_query($conn, $sql);
                        }
					}
				}	
		 }
		 echo "1";die;
		}else{
			echo "2";die;
		} 
	}
}
add_action('admin_menu', 'excel_menu_pages');
function excel_menu_pages(){
   add_menu_page('Excel Import', 'Excel Import', 'manage_options', 'excel_import', 'excel_import_fun' );
}
function excel_import_fun(){
?>
  <link rel="stylesheet" type="text/css" href="<?php echo plugins_url('bootstrap.min.css', __FILE__);?>">
  <link rel="stylesheet" type="text/css" href="<?php echo plugins_url('style.css', __FILE__);?>">
  <link rel="stylesheet" type="text/css" href="<?php echo plugins_url('alert/css/alertify.css', __FILE__);?>">
  <script type="text/javascript" src="<?php echo plugins_url('bootstrap-filestyle.min.js', __FILE__);?>"></script>
  <script type="text/javascript" src="<?php echo plugins_url('alert/alertify.js', __FILE__);?>"></script>
<!--Url's uploader-->
<div class="mt-4 white-shadow-box">
<img src="<?php echo plugins_url('loader.gif', __FILE__);?>" class="loader_class" style="display:none;" id="loader_form"/>
<form method="post" name="upload_excel" enctype="multipart/form-data" id="upload_excel">
   <div class="row mb-5">
      <div class="col-md-12">
         <div class="p-4">
            <h5>Upload Url's to sitemap.xml</h5>
            <input type="file" class="filestyle" data-buttonText="Select File" name="excel_sheet" value="" id="excel_sheet">
         </div>
         <div class="py-3 px-4 frm-footer">
            <div class="d-flex justify-content-end">
               <input type="button" name="submit" class="btn btn-blue" value="Upload" onclick="addUrl();">
            </div>
         </div>
      </div>
   </div>
</form>
</div>
<!--Listing of Url's-->
<div class="msg label label-success"></div>
<img src="<?php echo plugins_url('loader.gif', __FILE__);?>" class="loader_class" style="display:none;"/>
<div class="white-shadow-box p-4" id="scroll_div">
<?php
			global $wpdb;
			$table = $wpdb->prefix.'custom_sitemap';
			$option = $wpdb->get_results( "SELECT * FROM $table");
?>
    <div class="total_url">Total Url's: <?php echo count($option);?></div>
	<div class="mb-3  d-flex justify-content-end">
	<button class="btn btn-blue" onclick="deleteUrl();">Delete</button>
</div>
	<table class="table table-striped">
	  <thead>
	    <tr>
	      <th scope="col"><input class="checkbox" type="checkbox" id="select_all"  name="select_all"></th>	
	      <th scope="col">Url</th>
	    </tr>
	  </thead>
	  <tbody>
	  	<?php
		//$option=get_option("simple_wp_other_urls");

          if(!empty($option)){
			$i=1; 
			$site_url=site_url(); 
          	foreach ($option as $value){
	  	?>
	    <tr class="info">
	      <td><input class="checkbox" type="checkbox" name="delete_urls" value="<?php echo $value->sitemap_id;?>"></td>	
	      <td><?php echo $site_url."/".$value->url;?></td>
		  <td>
		  
		  <button  class="btn btn-blue copy-btn_<?php echo $value->sitemap_id;?>" data-type="attribute" data-attr-name="data-clipboard-text" data-model="couponCode" data-clipboard-text="<?php echo $site_url."/".$value->url;?>">Copy Url</button>

		  <script type="text/javascript">
				jQuery(document).ready(function(){
				    jQuery('.copy-btn_<?php echo $value->sitemap_id;?>').on("click", function(){
				        value = jQuery(this).data('clipboard-text'); //Upto this I am getting value
				 
				        var $temp = jQuery("<input>");
				          jQuery("body").append($temp);
				          $temp.val(value).select();
				          document.execCommand("copy");
				          $temp.remove();
				          alert("Copy to clipboard");
				    })
				})	
          </script>

          

		</td>
	    </tr>
	    <?php
	         $i++;
	        }
          }else{
        ?>  
         <tr class="info">
           <td colspan="2" style="font-weight: bold;">No Records Found</td>	
         </tr>	
        <?php  }
	    ?>
	  </tbody>
	</table>
	<script type="text/javascript">
		var select_all = document.getElementById("select_all"); //select all checkbox
	var checkboxes = document.getElementsByClassName("checkbox"); //checkbox items

	//select all checkboxes
	select_all.addEventListener("change", function(e){
		for (i = 0; i < checkboxes.length; i++) { 
			checkboxes[i].checked = select_all.checked;
		}
	});


	for (var i = 0; i < checkboxes.length; i++) {
		checkboxes[i].addEventListener('change', function(e){ //".checkbox" change 
			//uncheck "select all", if one of the listed checkbox item is unchecked
			if(this.checked == false){
				select_all.checked = false;
			}
			//check "select all" if all checkbox items are checked
			if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
				select_all.checked = true;
			}
		});
	}
	</script>	
</div>
<?php 
}
