<?php
/**
* Plugin Name: Mortgage Calculator
* Plugin URI: 
* Description: For Calculate Fixed rate
* Version: 1.0
* Author: Empower
* Author URI: 
**/
add_shortcode('calculator', 'calculator_fn');
function calculator_fn(){
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__);?>css/range.css?e=<?php echo microtime();?>" />
<link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__);?>css/style.css?e=<?php echo microtime();?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />


<section id="calcsection">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12">
                <div id="calc-box">
                    <div class="row">
                        <div class="col" id="calc-box-left">

                            <div id="calc-form">
                                <label class="mb-0">Choose From 2 To 5 Years Fixed Rates</label>
                                <div class="selectdiv">
                                    <select class="form-control" id="fixed-rates">
                                        <option value="1.54">2 year fix at 1.54%</option>
                                        <option value="1.20">5 year fix at 1.20%</option>
                                    </select>
                                </div>
                                <!--<label class="mt-10 mb-0">Length Of Term</label>-->
                                <div class="selectdiv" style="display: none;">
                                    <select class="form-control" id="finance-length-slider">
                                        <option value="15">15 years </option>
                                        <option value="20">20 years </option>
                                        <option value="25">25 years </option>
                                        <option value="30">30 years </option>
                                    </select>
                                </div><br />
                                <label class="mt-10 mb-0">Amount to borrow</label>
                                <div id="amount-contain" class="amount-bx">
                                    <span>&pound;</span><input type="text" value="100,000" name="finance-amount"
                                        id="finance-amount" />
                                </div>
                                <input type="range" name="finance-amount-slider" id="finance-amount-slider" min="25000"
                                    max="700000" step="1000" value="100000"><br />

                            </div>

                        </div>
                        <div class="col" id="calc-box-right">
                            <?php
                    $interest_rate = 1.54;
                    $svr_rate = 4.29;
                    ?>
                            <div id="calc-result">
                                <table>
                                    <tr class="table-border-tr">
                                        <th>Best available fixed rate</th>
                                        <td><strong><span id="fixed_rate_label"><?php echo $interest_rate;?></span>%
                                                APR</strong></td>
                                    </tr>
                                    <tr class="table-border-tr">
                                        <th>Reverts to SVR</th>
                                        <td><strong><?php echo $svr_rate;?>% APR</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th id="calc-number-payments"><span>24 MONTHLY PAYMENTS</span><br />DURING FIXED
                                            RATE PERIOD<br />ON AN INTEREST ONLY BASIS</th>
                                        <td id="calc-monthly-cost"><span class="sign">&pound;</span><span
                                                class="pounds"></span><span class="pence"></span></td>
                                    </tr>
                                </table>
                                <a href="javascript:void(0);"
                                    class="btn btn-primary" onclick="applyNow();">Apply now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<script src="<?php echo plugin_dir_url(__FILE__);?>js/main.php?e=<?php echo microtime();?>"></script>
<script type="text/javascript">
   function applyNow(){
      var fixed_rates= jQuery("#fixed-rates :selected").val();
      var principal = jQuery('#finance-amount-slider').val();
      var ponds = jQuery(".pounds").text();
      var pence = jQuery(".pence").text();
      var monthly= ponds+""+pence;

      window.location.href="<?php echo get_site_url();?>/get-an-easy-quote/"+"?fixed_rates="+fixed_rates+"&principal="+principal+"&monthly="+monthly;

      
   }
</script>




<ul id="wpforms-3556-field_6">
   <li class="choice-1 depth-1 wpforms-selected">
      <div class="stm_input_wrapper stm_input_wrapper_radio active"><input type="radio" id="wpforms-3556-field_6_1" name="wpforms[fields][6]" value="2 Years at 1.51%" checked="checked"></div>
      <label class="wpforms-field-label-inline" for="wpforms-3556-field_6_1">2 Years at 1.51%</label>
   </li>
   <li class="choice-2 depth-1">
      <div class="stm_input_wrapper stm_input_wrapper_radio"><input type="radio" id="wpforms-3556-field_6_2" name="wpforms[fields][6]" value="5 Years at 1.21%"></div>
      <label class="wpforms-field-label-inline" for="wpforms-3556-field_6_2">5 Years at 1.21%</label>
   </li>
</ul>

<?php }