$(document).ready(function () {
    /**
     * Remove Error on validate fields 
     */
    $(document).on('keyup keydown change blur', '.removeError', function () {
        $(this).closest('.form-group').find('.error').remove();
//        $(this).next('.error').remove();
//        $('#verticalError').find('.error').remove();
    });

    /**
     * END OF AREA
     * Remove Error on validate fields 
     */
    /*
     * *****************************
     * AJAX call for get table data*
     * *****************************
     */
    $.ajax({
        type: "POST",
        data: {'action': 'get_country_content'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {
            //console.log("get_country_content  response --->>>", response);
            if (response == 0) {
                    
                    $("#country_add_new_tr").removeClass("hide");
                }
            if (response) {
                var len = response.length;
                //alert(len);
                if (response == 0) {

                    $("#country_add_new_tr").removeClass("hide");
                } else {
                    var tr_str = '';
                    var $heading = '';
                    for (var i = 0; i < len; i++) {
                        var excc = "";
                        var id = response[i].id;
                        var country = response[i].country;
                        if (response[i].description.length > 500) {
                            var description = response[i].description.slice(0, 500);
                            excc = (response[i].description.length > 500) ? "..." : "";
                        } else {
                            var description = response[i].description;
                        }

                        //$heading = change_country_name(country);// Will be replace when api 
                        $heading=response[i].display_country;
                        $country_url=response[i].country_url;

                        var id = response[i].id;
                        var section_id = response[i].section_id;
                        tr_str += "<tr>" +
                                "<td class='nowrap country_class'>" + $heading + "</td>" +
                                "<td>" + description + excc + "</td>" +
                                "<td>" + $country_url + "</td>" +
                                "<td class='nowrap text-center'>" +
                                "<a href='JavaScript:void(0)' class='text-blue font-icon mr-2' onclick='editContent(" + '"country","' + country + '"' + ")'>" +
                                "<i class='icon-edit'></i></a>" +
                                "<a href='JavaScript:void(0)' class='text-red font-icon' onclick='deleteContent(" + '"country","' + country + '"' + ")'><i class='icon-delete'></i></a>" +
                                "</td>" +
                                "</tr>";
                        $("#country_table tbody").html(tr_str);
                    }
                }
            } else {
                $("#list").removeClass("hide");
            }
        }
    });
    /*
     * END OF AJAX    
     * *****************************
     * AJAX call for get table data*
     * *****************************                                             
     */

    /*
     * ***********************************************
     * AJAX call for get sections for popup drop down*
     * ***********************************************
     */
    update_sections('country');
    /*
     * END OF AJAX    
     * ***********************************************
     * AJAX call for get sections for popup drop down*
     * ***********************************************                                             
     */

    $('.editor').summernote({
        placeholder: 'Content Editor ',
        tabsize: 2,
        height: 300, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: true,
        toolbar: [['style', ['bold', 'italic', 'underline', 'clear']],
            ['color', ['color']],
            ['view', ['fullscreen', 'codeview']],
            ['Insert', ['link']]]
    });
//    $(".editor").summernote("enable");
    $('.ddl_select').select2();
    $('.ddl_select').addClass("requiredValue removeError");
    $("#country_add_new_anc").click(function () {
        $(".editor").summernote("enable");
        $('.editor').addClass("requiredValue removeError");
        $('.ddl_select').select2();
        $('.ddl_select').addClass("requiredValue removeError");
        $('.image_preview').show();
        $("#country_list").removeClass('active');
        $("#country_editor").addClass('active');
        $("#country_list_li").removeClass('active');
        $("#country_editor_li").addClass('active');
        $.get("../wp-content/plugins/easybus-contenteditor/views/country_editor.php?v=1", function (data) {
            $("#country_editor").html(data);
            bind_elements();
        });
        update_sections('country');
    });
    $("#city_add_new_anc").click(function () {
        $(".editor").summernote("enable");
        $('.editor').addClass("requiredValue removeError");
        $('.ddl_select').select2();
        $('.ddl_select').addClass("requiredValue removeError");
        $('.image_preview').show();
        $("#city_list").removeClass('active');
        $("#city_editor").addClass('active');
        $("#city_list_li").removeClass('active');
        $("#city_editor_li").addClass('active');
        $.get("../wp-content/plugins/easybus-contenteditor/views/city_editor.php?v=1", function (data) {
            $("#city_editor").html(data);
            bind_elements();
        });
        update_sections('city');
    });
    $("#airport_add_new_anc").click(function () {
        $(".editor").summernote("enable");
        $('.editor').addClass("requiredValue removeError");
        $('.ddl_select').select2();
        $('.ddl_select').addClass("requiredValue removeError");
        $('.image_preview').show();
        $("#airport_list").removeClass('active');
        $("#airport_editor").addClass('active');
        $("#airport_list_li").removeClass('active');
        $("#airport_editor_li").addClass('active');
        $.get("../wp-content/plugins/easybus-contenteditor/views/airport_editor.php?v=1", function (data) {
            $("#airport_editor").html(data);
            bind_elements();
        });
        update_sections('airport');
    });

    $("#city_anch").click(function () {
        get_city_content();
    });

    $("#airport_anch").click(function () {
        get_airport_content();
    });
    

    
    /**
     * **************************************************
     * GET html editor content elements from html files *
     * **************************************************
     */
    /** COUNTRY **/
    $("#country_editor_li").click(function () {
        if ($("#country_editor_li").attr('class') === undefined || $("#country_editor_li").attr('class') === "") {
            $.get("../wp-content/plugins/easybus-contenteditor/views/country_editor.php?v=1", function (data) {
                $("#country_editor").html(data);
                bind_elements();
            });
            update_sections('country');
        }
    });
    /** CITY **/
    $("#city_editor_li").click(function () {
        if ($("#city_editor_li").attr('class') === undefined || $("#city_editor_li").attr('class') === "") {
            $.get("../wp-content/plugins/easybus-contenteditor/views/city_editor.php?v=1", function (data) {
                $("#city_editor").html(data);
                bind_elements();
            });
            update_sections('city');
        }
    });
    /** AIRPORT **/
    $("#airport_editor_li").click(function () {
        if ($("#airport_editor_li").attr('class') === undefined || $("#airport_editor_li").attr('class') === "") {
            $.get("../wp-content/plugins/easybus-contenteditor/views/airport_editor.php?v=1", function (data) {
                $("#airport_editor").html(data);
                bind_elements();
            });
            update_sections('airport');
        }
    });
    /**
     * END OF SECTION
     * **************************************************
     * GET html editor content elements from html files *
     * **************************************************
     */

    $('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

// store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
        var id = $(e.target).attr("href").substr(1);
        href = "#";

        if (id.indexOf("airport") != -1) {
            id = "airport";
        }
        if (id.indexOf("city") != -1) {
            id = "city";
        }
        if (id.indexOf("country") != -1) {
            id = "country";
        }
        window.location.hash = id;
    });

// on load of the page: switch to the currently selected tab
    var hash = window.location.hash;

    if (hash == "#city") {
        get_city_content();
    }
    if (hash == "#airport") {
        get_airport_content();
    }
    if (hash == "#landing_pages") {
        get_landing_pages_content();
    }
    if (hash == "#popular_destinations") {
        get_popular_destinations_content();
    }
    $('#myTab a[href="' + hash + '"]').tab('show');

    $("#add_section_btn").click(function () {
        var radio = $("input[name='sections']:checked").val();
        var radio_name = $("input[name='sections']:checked").next("label").text();
        var section_name = $("#section_name").val();

        if (section_name === '' && radio === undefined) {

            $("#popup_error_div").html("<span class='popuperror'>Please Enter Or Select Section Name</span>");
            return false;
        }
        var type = $('.nav-tabs li.active').data('content');
        var id = radio;
        if (radio === undefined && section_name !== '') {
            $.ajax({
                type: "POST",
                data: {'action': 'add_section', 'section_name': section_name},
                async: false,
                url: ajaxurl,
                success: function (response) {
                    console.log("adds_section response --->>>", response);
                    if (response > 0) {
                        id = response;
                        update_sections_popup();
                        radio_name = section_name;
                        radio = response;
                        var v=Date.now();
                        $.get("../wp-content/plugins/easybus-contenteditor/views/new_section.php?v="+v, function (data) {
                            $("#" + type + "_editor_section").append(data);
                            if (type === 'country') {
                                var lvalue = $("#section_ids").val();
                                $("#section_ids").val(lvalue + "," + id);
                            } else {
                                var lvalue = $("#" + type + "_section_ids").val();
                                $("#" + type + "_section_ids").val(lvalue + "," + id);
                            }


                            $(".new_section_img_txt").attr("id",type + "_content_images_" + radio);
                            $(".new_section_img_btn").attr("onclick", "upload_image_btn('" + type + "_content_images_"+radio+"')");


                            //$(".img_box_new_section").attr("id","preview_"+type + "_content_images_" + radio);
                            //$(".img_box_new_section_preview").attr("id","preview_img_"+type + "_content_images_" + radio);


                            $(".editor-heading").last().html(radio_name);

                            $(".filestyle").last().attr("id", type + "_file_uploader_" + radio);
                            $(".img-box").last().attr("id", type + "_image_" + radio);
                            $(".remove-img").last().attr("onclick", "deleteImage('" + type + "', '" + radio + "')");
                            $(".img_preview").last().attr("id", type + "_preview_" + radio);
                            $(".title_text").last().attr("id", type + "_title_" + radio);
                            $(".editor").last().attr("id", type + "_editor_" + radio);
                            $(".filestyle").last().attr("onchange", "displayImage(this, '" + type + "', " + radio + ")");
                            $('.new-section-editor').modal('toggle');
                            bind_elements();
                        });
                    }else{
                        $("#popup_error_div").html("<span class='popuperror'>Section already exists.</span>");
                    }

                }
            });

        } else {
                
                if (type === 'country') {
                   var lvalue = $("#section_ids").val();
                   if(lvalue.indexOf(id) != -1){
                     alert("Section already exists.");
                     return false;
                   }
                }else{
                   var lvalue = $("#" + type + "_section_ids").val();
                   if(lvalue.indexOf(id) != -1){
                     alert("Section already exists.");
                     return false;
                   }                   
                }

            var v=Date.now();
            $.get("../wp-content/plugins/easybus-contenteditor/views/new_section.php?v="+v, function (data) {
                $("#" + type + "_editor_section").append(data);
                if (type === 'country') {
                    var lvalue = $("#section_ids").val();
                    $("#section_ids").val(lvalue + "," + id);
                } else {
                    var lvalue = $("#" + type + "_section_ids").val();
                    $("#" + type + "_section_ids").val(lvalue + "," + id);
                }
                $(".editor-heading").last().html(radio_name);

                $(".new_section_img_txt").attr("id",type + "_content_images_" + radio);
                $(".new_section_img_btn").attr("onclick", "upload_image_btn('" + type + "_content_images_"+radio+"')");


                //$(".img_box_new_section").attr("id","preview_"+type + "_content_images_" + radio);
                //$(".img_box_new_section_preview").attr("id","preview_img_"+type + "_content_images_" + radio);

                $(".filestyle").last().attr("id", type + "_file_uploader_" + radio);
                $(".img-box").last().attr("id", type + "_image_" + radio);
                $(".remove-img").last().attr("onclick", "deleteImage('" + type + "', '" + radio + "')");
                $(".img_preview").last().attr("id", type + "_preview_" + radio);
                $(".title_text").last().attr("id", type + "_title_" + radio);
                $(".editor").last().attr("id", type + "_editor_" + radio);
                $(".filestyle").last().attr("onchange", "displayImage(this, '" + type + "', " + radio + ")");
                $('.new-section-editor').modal('toggle');
                bind_elements();
            });

        }
    });
});

function bind_elements() {
    $('.ddl_select').select2();
    $('.ddl_select').addClass("requiredValue removeError");
    $('.editor').addClass("requiredValue removeError");
    $('.editor').summernote({
        placeholder: 'Content Editor ',
        tabsize: 2,
        height: 300, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: true,
        toolbar: [['style', ['bold', 'italic', 'underline', 'clear']],
            ['color', ['color']],
            ['Insert', ['link']],
            ['view', ['codeview']]]
    });
    $(":file").filestyle({"buttonText": "Choose an Image"});
//    $(".editor").summernote("disable");
}

function change_country_name(country) {
    return country;
    // alert(country);
    // var $heading = '';
    // if (country === "IND") {
    //     $heading = "India";
    // }
    // if (country === "AUS") {
    //     $heading = "Australia";
    // }
    // if (country === "BRZ") {
    //     $heading = "Brazil";
    // }
    // if (country === "france") {
    //     $heading = "France";
    // }
    // return $heading;
}

function change_city_name(city) {
    return city;
    // var $heading = '';
    // if (city === "DL") {
    //     $heading = "Delhi";
    // }
    // if (city === "MUM") {
    //     $heading = "Mumbai";
    // }
    // if (city === "GOA") {
    //     $heading = "Goa";
    // }
    // if (city === "IND") {
    //     $heading = "Indore";
    // }
    // if (city === "SYD") {
    //     $heading = "Sydney";
    // }
    // if (city === "MEL") {
    //     $heading = "Melbourne";
    // }
    // if (city === "SAL") {
    //     $heading = "Salvador";
    // }
    // if (city === "SAL") {
    //     $heading = "Salvador";
    // }
    // if (city === "FOR") {
    //     $heading = "Fortaleza";
    // }
    // if (city === "paris") {
    //     $heading = "Paris";
    // }
    // return $heading;
}

function change_airport_name(param) {
    return param;
    // var ddloptions = '';
    // switch (param) {
    //     case 'IGIA':
    //         ddloptions = 'Indira Gandhi International Airport';
    //         break;
    //     case 'DDA':
    //         ddloptions = 'Delhi Domestic Airport';
    //         break;
    //     case 'CSIA':
    //         ddloptions = 'Chhatrapati Shivaji International Airport';
    //         break;
    //     case 'PIA':
    //         ddloptions = 'Pune International Airport';
    //         break;
    //     case 'DA':
    //         ddloptions = 'Dabolim Airport';
    //         break
    //     case 'SGA':
    //         ddloptions = 'South Goa Airport';
    //         break;
    //     case 'MA':
    //         ddloptions = 'Mascot Airport';
    //     case 'KSA':
    //         ddloptions = 'Kingsford Smith Airport';
    //         break;
    //     case 'MEA':
    //         ddloptions = 'Melbourne Airport';
    //         break;
    //     case 'AA':
    //         ddloptions = 'Avalon Airport';
    //         break;
    //     case 'BA':
    //         ddloptions = 'Barrillas Airport';
    //         break;
    //     case 'ETA':
    //         ddloptions = 'El Tamarindo Airport';
    //         break;
    //     case 'Paris Charles de Gaulle':
    //         ddloptions = 'Paris Charles de Gaulle';
    //         break;
    //         case 'Paris Beauvais':
    //         ddloptions = 'Paris Beauvais';
    //         break;
    //         case 'Paris Orly':
    //         ddloptions = 'Paris Orly';
    //         break;

    // }
    // return ddloptions;
}

function update_sections(param) {
    $.ajax({
        type: "POST",
        data: {'action': 'get_sections'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {
            //console.log("get_sections response --->>>", response);
            if (response) {
                var len = response.length;
                var i;
                var total_ids = "";
                for (i = 0; i < len; i++) {
                    if (total_ids === "" && response[i].is_default == 1) {
                        total_ids = response[i].id;
                    } else {
                        total_ids = (response[i].is_default == 1) ? total_ids + "," + response[i].id : total_ids;
                    }
                }
                if (param === 'country') {
                    $("#section_ids").val(total_ids);
                }
                $("#" + param + "_section_ids").val(total_ids);
            }
        }
    });
    return false;
}

function update_sections_popup(param) {
    $("#section_name").val("");
    $("#popup_error_div").html("");
    $.ajax({
        type: "POST",
        data: {'action': 'get_sections', 'data_for': 'popup'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {
            //console.log("get_sections response popup --->>>", response);
            if (response) {
                var len = response.length;
                if (len > 0) {
                    var i;
                    var total_ids = "";
                    var html = '';
                    for (i = 0; i < len; i++) {
                        html += '<div><input type="radio" id="check_' + i + '" name="sections" value=' + response[i].id + '>' +
                                '<label for="check_' + i + '">' + response[i].section + '</label></div>';
                    }
                    $("#sections_div").html(html);
                }
            }
        }
    });
    return false;

}

function get_country_content(){
        $.ajax({
        type: "POST",
        data: {'action': 'get_country_content'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {
            //console.log("get_country_content  response --->>>", response);
            if (response == 0) {
                    
                    $("#country_add_new_tr").removeClass("hide");
                }
            if (response) {
                var len = response.length;
                //alert(len);
                if (response == 0) {

                    $("#country_add_new_tr").removeClass("hide");
                } else {
                    var tr_str = '';
                    var $heading = '';
                    for (var i = 0; i < len; i++) {
                        var excc = "";
                        var id = response[i].id;
                        var country = response[i].country;
                        if (response[i].description.length > 500) {
                            var description = response[i].description.slice(0, 500);
                            excc = (response[i].description.length > 500) ? "..." : "";
                        } else {
                            var description = response[i].description;
                        }

                        //$heading = change_country_name(country);// Will be replace when api 
                        $heading=response[i].display_country;
                        $country_url=response[i].country_url;

                        var id = response[i].id;
                        var section_id = response[i].section_id;
                        tr_str += "<tr>" +
                                "<td class='nowrap country_class'>" + $heading + "</td>" +
                                "<td>" + description + excc + "</td>" +
                                "<td>" + $country_url + "</td>" +
                                "<td class='nowrap text-center'>" +
                                "<a href='JavaScript:void(0)' class='text-blue font-icon mr-2' onclick='editContent(" + '"country","' + country + '"' + ")'>" +
                                "<i class='icon-edit'></i></a>" +
                                "<a href='JavaScript:void(0)' class='text-red font-icon' onclick='deleteContent(" + '"country","' + country + '"' + ")'><i class='icon-delete'></i></a>" +
                                "</td>" +
                                "</tr>";
                        $("#country_table tbody").html(tr_str);
                    }
                }
            } else {
                $("#list").removeClass("hide");
            }
        }
    });
}

function get_city_content() {
    /*
     * AJAX call for get sections
     */
    $.ajax({
        type: "POST",
        data: {'action': 'get_city_content'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {

            //console.log("City response", response);
            if (response == 0) {
                    
                    $("#city_add_new_tr").removeClass("hide");
                }
            if (response) {
                var len = response.length;
                if (len == 0) {
                    $("#city_add_new_tr").removeClass("hide");
                }
                var tr_str = '';
                for (var i = 0; i < len; i++) {
                    var id = response[i].id;
                    var country = response[i].country;
                    var city = response[i].city;
                    var description = '';
                    var excc = '';
                    if (response[i].description.length > 500) {
                        description = response[i].description.slice(0, 500);
                        excc = (response[i].description.length > 500) ? "..." : "";
                    } else {
                        description = response[i].description;
                    }

                    var id = response[i].id;
                    var section_id = response[i].section_id;
                    var $heading = '';
                    var $city = '';
                    $heading = response[i].display_country;
                    $city = response[i].display_city;
                    $city_url=response[i].city_url;
                    tr_str = tr_str + "<tr>" +
                            "<td class='nowrap'>" + $heading + "</td>" +
                            "<td class='nowrap'>" + $city + "</td>" +
                            "<td>" + description + excc + "</td>" +
                            "<td>" + $city_url + "</td>" +
                            "<td class='nowrap text-center'>" +
                            "<a href='JavaScript:void(0)' class='text-blue font-icon mr-2' onclick='editContent(" + '"city","' + country + "," + city + '"' + ")'>" +
                            "<i class='icon-edit'></i></a>" +
                            "<a href='JavaScript:void(0)' class='text-red font-icon' onclick='deleteContent(" + '"city","' + city + '"' + ")'><i class='icon-delete'></i></a>" +
                            "</td>" +
                            "</tr>";
                    $("#city_table tbody").html(tr_str);
                }
            } else {
                $("#city_add_new_tr").removeClass("hide");
            }
        }
    });
    /*
     * END OF AJAX                                                 
     */

}

function get_airport_content() {
    /*
     * AJAX call for get sections
     */
    $.ajax({
        type: "POST",
        data: {'action': 'get_airport_content'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {
            if (response == 0) {
                    
                    $("#airport_add_new_tr").removeClass("hide");
                }
            if (response) {
                var len = response.length;
                if (len == 0) {
                    $("#airport_add_new_tr").removeClass("hide");
                }
                var tr_str = "";
                for (var i = 0; i < len; i++) {
                    var id = response[i].id;
                    var country = response[i].country;
                    var city = response[i].city;
                    var airport = response[i].airport;
                    var description = response[i].description.slice(0, 500);
                    var excc = (response[i].description.length > 500) ? "..." : "";
                    var id = response[i].id;
                    var section_id = response[i].section_id;
                    var $heading = '';
                    var $city = '';
                    var $airport = '';
                    $heading = response[i].display_country;
                    $city = response[i].display_city;
                    $airport = response[i].display_airport;
                    $airport_url=response[i].airport_url;
                    tr_str = tr_str + "<tr>" +
                            "<td class='nowrap'>" + $heading + "</td>" +
                            "<td class='nowrap'>" + $city + "</td>" +
                            "<td class='nowrap'>" + $airport + "</td>" +
                            "<td>" + $airport_url + "</td>" +
                            "<td>" + description + excc + "</td>" +
                            "<td class='nowrap text-center'>" +
                            "<a href='JavaScript:void(0)' class='text-blue font-icon mr-2' onclick='editContent(" + '"airport","' + country + "," + city + "," + airport + '"' + ")'>" +
                            "<i class='icon-edit'></i></a>" +
                            "<a href='JavaScript:void(0)' class='text-red font-icon' onclick='deleteContent(" + '"airport","' + airport + '"' + ")'><i class='icon-delete'></i></a>" +
                            "</td>" +
                            "</tr>";
                    $("#airport_table tbody").html(tr_str);
                }
            } else {
                $("#airport_add_new_tr").removeClass("hide");
            }
        }
    });
    /*
     * END OF AJAX                                                 
     */

}


function get_landing_pages_content(){
   /*
     * AJAX call for get sections
     */
    $.ajax({
        type: "POST",
        data: {'action': 'get_landing_pages_content'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {
            if (response == 0) {
                    
                    $("#landing_pages_add_new_tr").removeClass("hide");
                }
            if (response) {
                var len = response.length;
                if (len == 0) {
                    $("#landing_pages_add_new_tr").removeClass("hide");
                }
                var tr_str = "";
                for (var i = 0; i < len; i++) {
                    var id = response[i].id;
                    var country = response[i].country;
                    var city = response[i].city;
                    var airport = response[i].airport;
                    var description = response[i].description.slice(0, 500);
                    var excc = (response[i].description.length > 500) ? "..." : "";
                    var id = response[i].id;
                    var section_id = response[i].section_id;
                    var $heading = '';
                    var $city = '';
                    var $airport = '';
                    $heading = response[i].display_country;
                    $city = response[i].display_city;
                    $airport = response[i].display_airport;
                    $airport_url=response[i].airport_url;
                    tr_str = tr_str + "<tr>" +
                            "<td class='nowrap'>" + $heading + "</td>" +
                            "<td class='nowrap'>" + $city + "</td>" +
                            "<td class='nowrap'>" + $airport + "</td>" +
                            "<td>" + $airport_url + "</td>" +
                            "<td>" + description + excc + "</td>" +
                            "<td class='nowrap text-center'>" +
                            "<a href='JavaScript:void(0)' class='text-blue font-icon mr-2' onclick='editContent(" + '"airport","' + country + "," + city + "," + airport + '"' + ")'>" +
                            "<i class='icon-edit'></i></a>" +
                            "<a href='JavaScript:void(0)' class='text-red font-icon' onclick='deleteContent(" + '"airport","' + airport + '"' + ")'><i class='icon-delete'></i></a>" +
                            "</td>" +
                            "</tr>";
                    $("#landing_pages_table tbody").html(tr_str);
                }
            } else {
                $("#landing_pages_add_new_tr").removeClass("hide");
            }
        }
    });
    /*
     * END OF AJAX                                                 
     */   
}
function get_popular_destinations_content(){
   /*
     * AJAX call for get sections
     */
    $.ajax({
        type: "POST",
        data: {'action': 'get_popular_destinations_content'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {
            if (response == 0) {
                    
                    $("#popular_destinations_add_new_tr").removeClass("hide");
                }
            if (response) {
                var len = response.length;
                if (len == 0) {
                    $("#popular_destinations_add_new_tr").removeClass("hide");
                }
                var tr_str = "";
                for (var i = 0; i < len; i++) {
                    var id = response[i].id;
                    var country = response[i].country;
                    var city = response[i].city;
                    var airport = response[i].airport;
                    var description = response[i].description.slice(0, 500);
                    var excc = (response[i].description.length > 500) ? "..." : "";
                    var id = response[i].id;
                    var section_id = response[i].section_id;
                    var $heading = '';
                    var $city = '';
                    var $airport = '';
                    $heading = response[i].display_country;
                    $city = response[i].display_city;
                    $airport = response[i].display_airport;
                    $airport_url=response[i].airport_url;
                    tr_str = tr_str + "<tr>" +
                            "<td class='nowrap'>" + $heading + "</td>" +
                            "<td class='nowrap'>" + $city + "</td>" +
                            "<td class='nowrap'>" + $airport + "</td>" +
                            "<td>" + $airport_url + "</td>" +
                            "<td>" + description + excc + "</td>" +
                            "<td class='nowrap text-center'>" +
                            "<a href='JavaScript:void(0)' class='text-blue font-icon mr-2' onclick='editContent(" + '"airport","' + country + "," + city + "," + airport + '"' + ")'>" +
                            "<i class='icon-edit'></i></a>" +
                            "<a href='JavaScript:void(0)' class='text-red font-icon' onclick='deleteContent(" + '"airport","' + airport + '"' + ")'><i class='icon-delete'></i></a>" +
                            "</td>" +
                            "</tr>";
                    $("#popular_destinations_table tbody").html(tr_str);
                }
            } else {
                $("#popular_destinations_add_new_tr").removeClass("hide");
            }
        }
    });
    /*
     * END OF AJAX                                                 
     */   
}

/*
 * FORM VALIDATION AREA
 * 
 */
function requiredValidation() {
    var valid = '';
    $(".error").remove();
    $('.requiredValue').each(function () {
        / Get field value /
        var fieldValue = $.trim($(this).val());

        / Get field id /
        var fieldId = $(this).attr('id');
        if (fieldValue == "" && (typeof fieldId != "undefined")) {
            $(this).next('span').remove();
            if (fieldId.indexOf("editor")) {
                fieldId = fieldId.replace("editor", "content_div");
            }
            $("#" + fieldId).after("<span class='error'>This field is required.</span>");
            valid = 1;
        }
        if (fieldValue === "<p><br></p>") {
            if (fieldId.indexOf("editor")) {
                fieldId = fieldId.replace("editor", "content_div");
            }
            $("#" + fieldId).after("<span class='error'>This field is required.</span>");
            valid = 1;
        }

    });

    if (valid == 1) {
        return 1;
    } else {
        return 0;
    }
}
/*
 * END OF 
 * FORM VALIDATION AREA
 * 
 */
