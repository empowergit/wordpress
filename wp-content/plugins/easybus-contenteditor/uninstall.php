<?php 
function on_uninstall(){
	 if( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit();
    global $wpdb;
    $wpdb->query( "DROP TABLE IF EXISTS content" );
    delete_option("easybus-contenteditor_db_version");
}
   
?>