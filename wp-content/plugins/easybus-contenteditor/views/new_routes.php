<?php
require_once("../../../../wp-load.php");
?>
<div class="inner-container">
    <div id="new_routes_editor_section">
        <div class="row">
            <div class="col-md-3">&nbsp;</div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="msg_routes"></div>
                    <form action="" enctype="multipart/form-data" name="new_routes_upload_form"
                        id="new_routes_upload_form">
                        <div class="row">
                            <div class="col-md-9">
                                <label class="control-label">Upload File</label>
                                <input type="file" class="filestyle" data-buttonText="Choose .csv File"
                                    data-placeholder="Choose .csv File" name="new_routes_file">
                            </div>
                            <div class="col-md-3" style="margin-top: 12px;">
                                <button type="button" class="btn btn-blue-small" type="button" name="new_routes_upload"
                                    id="new_routes_upload" value="new_routes_upload" onclick="new_routes_uploads();">
                                    <i class="icon-save-file-option font-icon btn-ico-common"></i> Upload
                                </button>
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-3">&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="new_routes_section-2">OR</div>
                <div class="msg_routes_form"></div>
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <form action="" method="post" name="new_routes_add_form" id="new_routes_add_form">
                        <table class="table custom-tbl" id="new_routes_table">
                            <thead>
                                <tr>
                                    <!--<th>&nbsp;</th>-->
                                    <th>Origin&nbsp;station</th>
                                    <th>Destination&nbsp;station</th>
                                    <th>Landing&nbsp;Page</th>
                                    <th>IsImportant</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <!--<td><span class="dashicons dashicons-plus-alt add_more_rows"
                                                onclick="add_more_rows_routes();"></span></td>-->
                                    <td>
                                        <select class="form-control  ddl_selectn" autocomplete="off"
                                            id="city_country_ddl_newroutes_ddl_1" name="origin_station[]"
                                            placeholder="Pick a Country..."
                                            onchange="changeCityNewRoutes('1_newroutes', this.id)">
                                            <option value="">Select Country</option>
                                            <?php
                                                      global $wpdb;
                                                      $query="Select*from country";
                                                      $result = $wpdb->get_results($query,ARRAY_A);
                                                       
                                                        if(!empty($result)){
                                                           foreach($result as $value){
                                                        ?>
                                            <option value="<?php echo $value["iso"];?>">
                                                <?php echo $value["nicename"];?></option>
                                            <?php   } 
                                                        }
                                                      ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control  ddl_selectn" autocomplete="off" id="1_newroutes_ddl"
                                            name="destination_station[]" placeholder="Pick a City..."
                                            onchange="getBusFromRoute('',this.value,'1','new_routes_city_country_ddl_newroutes_ddl_1');">
                                            <option value="">Select City</option>
                                        </select>
                                    </td>
                                    <td class="new_routes_city_country_ddl_newroutes_ddl_1"></td>
                                    <td><input type="checkbox" name="isImportant[]"
                                            class="form-control new_routes_chk_bx" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <input type="hidden" value="1" id="row_counter" name="row_counter">
                <button type="button" class="btn btn-blue-small" type="button" name="new_routes_saved"
                    id="new_routes_saved" value="new_routes_saved" onclick="new_routes_saved();">
                    <i class="icon-save-file-option font-icon btn-ico-common"></i> Save
                </button>
            </div>
        </div>
    </div>
    <!-- sections here -->
</div>
</div>