<?php
require_once("../../../../wp-load.php");
?>
<div class="inner-container">
    <div id="landing_pages_editor_section">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_select ddl_select_lp" autocomplete="off" id="city_country_ddl"
                        name="city_country_ddl" placeholder="Pick a Country..." onchange="changeCityLandingPages('lp', this.id)">
                        <option value="">Select Country</option>
                        <?php
                      global $wpdb;
                      $query="Select*from country";
                      $result = $wpdb->get_results($query,ARRAY_A);
                       
                        if(!empty($result)){
                           foreach($result as $value){
                        ?>
                        <option value="<?php echo $value["iso"];?>"><?php echo $value["nicename"];?></option>
                        <?php   } 
                        }
                      ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_select ddl_select_lp" autocomplete="off" id="lp_ddl" name="city_ddl"
                        placeholder="Pick a City..." onchange="get_landing_pages(this.value);">
                        <option value="">Select City</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-md-12 margin-top56" id="landing_pages_content_append">
               
           </div> 
        </div>
        <!-- sections here -->
    </div>
    <div class="action-btns">
        <button type="button" class="btn btn-blue action-link" type="button" name="city_save" id="landing_pages_save"
            value="landing_pages_save" onclick="landing_pages_save();">
            <i class="icon-save-file-option font-icon"></i> Save
        </button>
    </div>
</div>