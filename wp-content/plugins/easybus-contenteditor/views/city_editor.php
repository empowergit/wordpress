<?php
require_once("../../../../wp-load.php");
?>
<div class="inner-container">
    <div  id="city_editor_section">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_select" autocomplete="off" 
                            id="city_country_ddl" name="city_country_ddl"
                            placeholder="Pick a Country..." onchange="changeCity('city', this.id)">                                                    
                        <option value="">Select Country</option>
                        <?php
                      global $wpdb;
                      $query="Select*from country";
                      $result = $wpdb->get_results($query,ARRAY_A);
                       
                        if(!empty($result)){
                           foreach($result as $value){
                        ?>
                         <option value="<?php echo $value["iso"];?>"><?php echo $value["nicename"];?></option>
                        <?php   } 
                        }
                      ?>
                        <!-- <option value="IND">India</option>
                        <option value="AUS">Australia</option>
                        <option value="BRZ">Brazil</option>
                        <option value="france">France</option> -->
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_select" autocomplete="off" 
                            id="city_ddl" name="city_ddl"
                            placeholder="Pick a City..." onchange="changeAttr('city', '1')">                                                    
                        <option value="">Select City</option>
                        <!--                                                        <option value="DL">Delhi</option>
                                                                                <option value="MUM">Mumbai</option>
                                                                                <option value="GOA">Goa</option>
                                                                                <option value="IND">Indore</option>
                                                                                <option value="SYD">Sydney</option>
                                                                                <option value="MEL">Melbourne</option>
                                                                                <option value="SAL">Salvador</option>
                                                                                <option value="FOR">Fortaleza</option>-->
                    </select>
                </div>
            </div>
        </div>
        <!-- sections here -->

        <?php 
        $meta_id_key="city";  
        include("meta_tags_section.php");?>

        <div class="content-editor" id="gn_info">
            <h4 class="editor-heading">About Low Cost Bus Transfers <a href="javascript:void(0);" class="text-red font-icon" style="float:right !important;font-size: 20px !important;" onclick="deleteAddSection('gn_info');"><i class="icon-delete"></i></a></h4>
            <div class="editor-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Upload Image</label>
                            <!--<input type="file" class="filestyle"
                                   data-buttonText="Choose an Image" 
                                   name="city_file_uploader_1" 
                                   onChange="displayImage(this, 'city', '1')" 
                                   id="city_file_uploader_1" >-->
                                   <div class="bootstrap-filestyle input-group">
                                <input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="city_content_images_1" id="city_content_images_1"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                        for="country_file_uploader_1" class="btn btn-blue "><span
                                            class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('city_content_images_1');">Choose an
                                            Image</span></label></span></div>
                        </div>
                        <div class="image-info-txt"><?php echo IMAGE_DIM_CITY;?></div>
                        <div class="img-box-outer">
                            <div class="img-box hide" id="preview_city_content_images_1">
                                <a href="javascript:void(0)" class="remove-img" onclick="deleteImage('city', '1')">
                                    <i class="icon-delete"></i>
                                </a>
                                <img src="" id="preview_img_city_content_images_1" name="city_preview_1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><?php echo PAGE_TITLE_MAIN;?></label>
                            <input type="text" name="city_title_1" id="city_title_1" class="form-control" placeholder="<?php echo PAGE_TITLE_MAIN;?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description<span class="re_sign">*</span></label>
                            <div class="city_content_div_1" id="city_content_div_1">                
                                <textarea class="editor" id="city_editor_1" name="city_editor_1"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About -->
        <div class="content-editor" id="about_info">
            <h4 class="editor-heading">About <a href="javascript:void(0);" class="text-red font-icon" style="float:right !important;font-size: 20px !important;" onclick="deleteAddSection('about_info');"><i class="icon-delete"></i></a></h4>
            <div class="editor-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Upload Image</label>
                            <!--<input type="file" class="filestyle"
                                   data-buttonText="Choose an Image" 
                                   name="city_file_uploader_2" 
                                   onChange="displayImage(this, 'city', '2')" 
                                   id="city_file_uploader_2" >-->
                                   <div class="bootstrap-filestyle input-group">
                                <input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="city_content_images_2" id="city_content_images_2"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                        for="country_file_uploader_2" class="btn btn-blue "><span
                                            class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('city_content_images_2');">Choose an
                                            Image</span></label></span></div>
                        </div>
                        <div class="image-info-txt"><?php echo IMAGE_DIM_CITY;?></div>
                        <div class="img-box-outer">
                            <div class="img-box hide" id="preview_city_content_images_2" >
                                <a href="javascript:void(0)" class="remove-img" onclick="deleteImage('city', '2')">
                                    <i class="icon-delete"></i>
                                </a>
                                <img src="" id="preview_img_city_content_images_2" name="city_preview_2">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><?php echo PAGE_TITLE_MAIN;?></label>
                            <input type="text" name="city_title_2" id="city_title_2" class="form-control" placeholder="<?php echo PAGE_TITLE_MAIN;?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description<span class="re_sign">*</span></label>
                            <div class="city_content_div_2" id="city_content_div_2">                
                                <textarea class="editor" id="city_editor_2" name="city_editor_2"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="city_section_ids" id="city_section_ids" value=""/>
        <input type="hidden" name="city_content_rows" id="city_content_rows" value=""/>
        <input type="hidden" name="city_mode" id="city_mode" value="add">
    </div>

    <div class="action-btns">
        <button type="button" class="btn btn-green" data-toggle="modal" data-target=".new-section-editor" onclick="update_sections_popup('city')">
            <i class="icon-add-note font-icon"></i> New Section Editor
        </button>
        <button type="button" class="btn btn-blue action-link" type="button" name="city_save" id="city_save" value="city_save" onclick="saveContent('city')" >
            <i class="icon-save-file-option font-icon"></i> Save
        </button>
    </div>
</div>