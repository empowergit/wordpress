<?php
require_once("../../../../wp-load.php");
if(!empty($_GET["id"])){
  global $wpdb;
  $query="Select*from meta_tags where id='".$_GET["id"]."'";
  $result = $wpdb->get_results($query,ARRAY_A);
  if(!empty($result)){
    foreach($result as $value){
        $type=$value["type"];
        $country=$value["country"];
        $city=$value["city"];
        $meta_title=$value["meta_title"];
        $meta_description=$value["meta_description"];
        $meta_keywords=$value["meta_keywords"];
    }
  }  
}
?>
<form name="meta_tags_form" id="meta_tags_form" method="post">
<div class="inner-container">
    <div  id="city_editor_section">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <?php
                     if(!empty($_GET["id"])){
                    ?>
                    <h4 class="editor-heading"><?php echo @ucfirst($type)." page";?></h4>
                <?php }else{?>
                    <select class="form-control ddl_select_meta_tags" name="type" id="type" onchange="changeType(this.value);">    
                                                
                        <option value="country" <?php echo @$country_sel;?>>Country Page</option>
                        <option value="city" <?php echo @$city_sel;?>>City Page</option>
                    </select>
                <?php }?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    
                    <?php
                      if(!empty($_GET["id"])){

                       $query = "SELECT * FROM country WHERE iso ='".$country."'";
                       $res = $wpdb->get_results($query);
                                           
                       


                    ?>
                    <h4 class="editor-heading"><?php echo @$res[0]->nicename;?></h4>
                    <?php }else{?> 
              

                    <select class="form-control ddl_select_meta_tags" autocomplete="off" 
                            id="meta_tags_city_country_ddl" name="meta_tags_city_country_ddl"
                            placeholder="Pick a Country..." onchange="changeCityMetaTags(this.id)" required="">                                                    
                        <option value="">Select Country</option>
                        <?php
                      global $wpdb;
                      $query="Select*from country";
                      $result = $wpdb->get_results($query,ARRAY_A);
                       
                        if(!empty($result)){
                           foreach($result as $value){
                        ?>
                         <option value="<?php echo $value["iso"];?>"><?php echo $value["nicename"];?></option>
                        <?php   } 
                        }
                      ?>
                    </select>
                <?php }?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?php
                      if(!empty($_GET["id"])){
                      
                       $query2 = "SELECT * FROM city WHERE code ='".$city."'";
                       $res2 = $wpdb->get_results($query2);
                       

                    ?> 
<h4 class="editor-heading"><?php echo @$res2[0]->name;?></h4>
                     <?php }else{
                    ?>
                    <select class="form-control ddl_select_meta_tags" autocomplete="off" 
                            id="meta_tags_city_ddl" name="meta_tags_city_ddl"
                            placeholder="Pick a City...">                                                    
                        <option value="">Select City</option>
                        
                    </select>
                <?php }?>
                </div>
            </div>
        </div>
        <!-- sections here -->
        <div class="row">
           <div class="meta_tags_msg col-md-12"></div> 
           <div class="col-md-4 margin-top56" id="meta_tags_content_append">
               <div class="form-group">
                   <label>Meta Title</label>
                   <input type="text" name="meta_title" class="form-control" id="meta_title" value="<?php echo @$meta_title;?>">
               </div>
               <div class="form-group">
                   <label>Meta Description</label>
                   <textarea name="meta_description" id="meta_description" class="form-control"><?php echo @$meta_description;?></textarea>
               </div>
               <div class="form-group">
                   <label>Meta Keywords</label>
                   <textarea name="meta_keywords" id="meta_keywords" class="form-control"><?php echo @$meta_keywords;?></textarea>
               </div>
           </div> 
        </div>
        
        
    </div>

    <div class="">
        <?php
          if(!empty($_GET["mode"])){
            $mode="edit";
            $btn="Update";
          }else{
            $mode="add";
            $btn="Save"; 
          } 
        ?>
        <input type="hidden" name="mode" value="<?php echo @$mode;?>">
        <?php
         if(!empty($_GET["id"])){
        ?>
        <input type="hidden" name="id" value="<?php echo $_GET["id"];?>">
        <?php }?>
        <button type="button" class="btn btn-blue-small" type="button" name="meta_tags_save" id="meta_tags_save" 
        value="meta_tags_save" onclick="save_tags();" >
            <i class="icon-save-file-option font-icon btn-ico-common"></i> <?php echo $btn;?>
        </button>
    </div>
</div>
</form>