<div class="inner-container">
    <div id="featured_editor_section">
        <div class="content-editor">
          <h4 class="editor-heading">Featured Cities and Airport</h4>
          <div class="editor-body">
            <div class="row featured_loader_div" style="display: none;">
              <div class="col-md-12">
                <div style="width: 23%;margin: 0 auto;">
                  <img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif'/>
                </div>
              </div>
            </div>
            <div class="row featured_content_div">
               <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Cities</label>
                    <div class="featured_cities">
                      <div class="alert alert-danger">No Records Found</div>
                    </div>
                  </div>
                 </div>
                 
               <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Airports</label>
                    <div class="featured_airports">
                      <div class="alert alert-danger">No Records Found</div>
                    </div>
                  </div>
                 </div>
                 </div>
             
                 
                 
          </div>
        </div>
    </div>
</div>      