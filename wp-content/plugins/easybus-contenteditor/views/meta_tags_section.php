<div class="inner-container">
    <div id="meta_tags_editor_section">
        <div class="content-editor">
          <h4 class="editor-heading"><?php echo META_TAGS_SECTION_HEADING;?></h4>
          <div class="editor-body">
          	<div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Page Title</label>
                    <input type="text" class="form-control" name="meta_title" id="meta_title_<?php echo $meta_id_key;?>">
                  </div>
                 </div>
                 </div>
             <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Keywords</label>
                    <input type="text" class="form-control" name="meta_keywords" id="meta_keywords_<?php echo $meta_id_key;?>">
                  </div>
                 </div>
                 </div>
                 <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Description</label>
                    <textarea class="form-control" name="meta_description" id="meta_description_<?php echo $meta_id_key;?>" style="resize: none;"></textarea>

                  </div>
                 </div>
                 </div>
                 
          </div>
        </div>
    </div>
</div>      