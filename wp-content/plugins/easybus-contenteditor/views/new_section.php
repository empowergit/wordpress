<?php
  $v=@$_GET["v"];
?>
<div class="content-editor" id="new_info_<?php echo $v;?>">
    <h4 class="editor-heading">About </h4>
    <a href="javascript:void(0);" class="text-red font-icon" style="float:right !important;font-size: 20px !important;" onclick="deleteAddSection('new_info_<?php echo $v;?>');"><i class="icon-delete" style="margin-right: 13px;"></i></a>
    <div class="editor-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Upload Image</label>
                    <!--<input type="file" class="filestyle"
                           data-buttonText="Choose an Image" 
                           name="airport_file_uploader_2" 
                           onChange="displayImage(this, 'airport', '2')" 
                           id="airport_file_uploader_2" >-->
                           <div class="bootstrap-filestyle input-group">
                                <input type="text" class="form-control new_section_img_txt"
                                    placeholder="Upload Image" disabled="" name="country_content_images_2" id="country_content_images_2"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                         class="btn btn-blue "><span
                                            class="icon-span-filestyle "></span> <span class="buttonText new_section_img_btn" onclick="upload_image_btn('country_content_images_2');">Choose an
                                            Image</span></label></span></div>
                </div>
                <div class="img-box-outer">
                    <div class="img-box hide img_box_new_section" id="preview_airport_content_images_2">
                        <a href="javascript:void(0)" class="remove-img" onclick="deleteImage('airport', '2')">
                            <i class="icon-delete"></i>
                        </a>
                        <img src="" id="preview_img_airport_content_images_2" name="preview_img_airport_content_images_2" class="img_preview img_box_new_section_preview">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label><?php echo PAGE_TITLE_MAIN;?></label>
                    <input type="text" name="airport_title_2" id="airport_title_2" class="form-control title_text" placeholder="<?php echo PAGE_TITLE_MAIN;?>">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Description<span class="re_sign">*</span></label>
                    <div class="airport_content_div_2" id="airport_content_div_2">                
                        <textarea class="editor" id="airport_editor_2" name="airport_editor_2"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>