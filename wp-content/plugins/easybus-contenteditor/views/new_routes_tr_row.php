<?php
require_once("../../../../wp-load.php");
$row_counter=@$_GET["row_counter"];
?>
<tr>
    <td><span class='dashicons dashicons-dismiss delete_rows_routes' onclick='deleteRow(this);'></span></td>
    <td>
        <select class="form-control  ddl_select_nr" autocomplete="off"
            id="city_country_ddl_newroutes_ddl_<?php echo $row_counter;?>" name="origin_station[]"
            placeholder="Pick a Country..."
            onchange="changeCityNewRoutes('<?php echo $row_counter;?>_newroutes', this.id)">
            <option value="">Select Country</option>
            <?php
                                                      global $wpdb;
                                                      $query="Select*from country";
                                                      $result = $wpdb->get_results($query,ARRAY_A);
                                                       
                                                        if(!empty($result)){
                                                           foreach($result as $value){
                                                        ?>
            <option value="<?php echo $value["iso"];?>">
                <?php echo $value["nicename"];?></option>
            <?php   } 
                                                        }
                                                      ?>
        </select>
    </td>
    <td>
        <select class="form-control  ddl_select_nr" autocomplete="off"
            id="<?php echo $row_counter;?>_newroutes_ddl" name="destination_station[]" placeholder="Pick a City..."
            onchange="getBusFromRoute('',this.value,'<?php echo $row_counter;?>','new_routes_city_country_ddl_newroutes_ddl_<?php echo $row_counter;?>');">
            <option value="">Select City</option>
        </select>
    </td>
    <td class="new_routes_city_country_ddl_newroutes_ddl_<?php echo $row_counter;?>"></td>
    <td><input type="checkbox" name="isImportant[]" class="form-control new_routes_chk_bx" /></td>
</tr>