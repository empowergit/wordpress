<?php
require_once("../../../../wp-load.php");
?>
<div class="inner-container">

    <div id="country_editor_section">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_select requiredValue removeError" autocomplete="off"
                        id="country_ddl" name="country_ddl" placeholder="Pick a Country..."
                        onchange="changeAttr('country', '1')">
                        <option value="">Select Country</option>

                        <?php
                      global $wpdb;
                      $query="Select*from country";
                      $result = $wpdb->get_results($query,ARRAY_A);
                       
                        if(!empty($result)){
                           foreach($result as $value){
                        ?>
                        <option value="<?php echo $value["iso"];?>"><?php echo $value["nicename"];?></option>
                        <?php   } 
                        }
                      ?>
                        <!--                         <option value="IND">India</option>
                        <option value="AUS">Australia</option>
                        <option value="BRZ">Brazil</option>
                        <option value="france">France</option> -->
                    </select>
                </div>
            </div>
        </div>
        <!-- sections here -->

        <?php 
        $meta_id_key="country";  
        include("meta_tags_section.php");?>

        <div class="content-editor" id="gn_info">
            <!--<input id="background_image" type="text" name="background_image" value="" />
<input id="upload_image_button" type="button" class="button-primary" value="Insert Image" onclick="upload_image_btn();" />-->
            <h4 class="editor-heading">About Low Cost Bus Transfers <a href="javascript:void(0);"
                    class="text-red font-icon" style="float:right !important;font-size: 20px !important;"
                    onclick="deleteAddSection('gn_info');"><i class="icon-delete"></i></a></h4>
            <div class="editor-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Upload Image</label>
                            <!--<input type="file" class="filestyle"
                                   data-buttonText="Choose an Image" 
                                   name="country_file_uploader_1" 
                                   onChange="displayImage(this, 'country', '1')" 
                                   id="country_file_uploader_1" >-->

                            <div class="bootstrap-filestyle input-group">
                                <input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="country_content_images_1" id="country_content_images_1"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                        for="country_file_uploader_1" class="btn btn-blue "><span
                                            class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('country_content_images_1');">Choose an
                                            Image</span></label></span></div>




                        </div>
                        <div class="image-info-txt"><?php echo IMAGE_DIM_COUNTRY;?></div>
                        <div class="img-box-outer">
                            <div class="img-box hide" id="preview_country_content_images_1">
                                <a href="javascript:void(0)" class="remove-img" onclick="deleteImage('country', '1')">
                                    <i class="icon-delete"></i>
                                </a>
                                <img src="" id="preview_img_country_content_images_1" name="country_preview_1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><?php echo PAGE_TITLE_MAIN;?></label>
                            <input type="text" name="country_title_1" id="country_title_1" class="form-control"
                                placeholder="<?php echo PAGE_TITLE_MAIN;?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description<span class="re_sign">*</span></label>
                            <div class="country_content_div_1" id="country_content_div_1">
                                <textarea class="editor" id="country_editor_1" name="country_editor_1"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About -->
        <div class="content-editor" id="about_info">
            <h4 class="editor-heading">About <a href="javascript:void(0);" class="text-red font-icon"
                    style="float:right !important;font-size: 20px !important;"
                    onclick="deleteAddSection('about_info');"><i class="icon-delete"></i></a></h4>
            <div class="editor-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Upload Image</label>
                            <!--<input type="file" class="filestyle" data-buttonText="Choose an Image"
                                name="country_file_uploader_2" onChange="displayImage(this, 'country', '2')"
                                id="country_file_uploader_2">-->
                                <div class="bootstrap-filestyle input-group">
                                <input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="country_content_images_2" id="country_content_images_2"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                        for="country_file_uploader_1" class="btn btn-blue "><span
                                            class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('country_content_images_2');">Choose an
                                            Image</span></label></span></div>
                        </div>
                        <div class="image-info-txt"><?php echo IMAGE_DIM_COUNTRY;?></div>
                        <div class="img-box-outer">
                            <div class="img-box hide" id="preview_country_content_images_2">
                                <a href="javascript:void(0)" class="remove-img" onclick="deleteImage('country', '2')">
                                    <i class="icon-delete"></i>
                                </a>
                                <img src="" id="preview_img_country_content_images_2" name="country_preview_2">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><?php echo PAGE_TITLE_MAIN;?></label>
                            <input type="text" name="country_title_2" id="country_title_2" class="form-control"
                                placeholder="<?php echo PAGE_TITLE_MAIN;?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description<span class="re_sign">*</span></label>
                            <div class="country_content_div_2" id="country_content_div_2">
                                <textarea class="editor" id="country_editor_2" name="country_editor_2"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <?php include("featured_cities_airport.php");?>

        <input type="hidden" name="section_ids" id="section_ids" value="" />
        <input type="hidden" name="content_rows" id="content_rows" value="" />
        <input type="hidden" name="country_mode" id="country_mode" value="add">
    </div>

    <div class="action-btns">
        <button type="button" class="btn btn-green" data-toggle="modal" data-target=".new-section-editor"
            onclick="update_sections_popup('country')">
            <i class="icon-add-note font-icon"></i> New Section Editor</button>
        <button type="button" class="btn btn-blue action-link" type="button" name="country_save" id="country_save"
            value="country_save" onclick="saveContent('country')">
            <i class="icon-save-file-option font-icon"></i> Save
        </button>
    </div>

</div>