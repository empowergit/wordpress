<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home</title>
          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
        <script src="../wp-content/plugins/easybus-contenteditor/js/bootstrap-filestyle.min.js"></script>
        <script src="../wp-content/plugins/easybus-contenteditor/js/bootstrap.min.js"></script>
        <script src="../wp-content/plugins/easybus-contenteditor/js/custom.js"></script>
        <script src="../wp-content/plugins/easybus-contenteditor/js/jquery.validate.js"></script>
        <!--<script src="../wp-content/plugins/easybus-contenteditor/js/bootstrap-select.min.js"></script>-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
        <link href="../wp-content/plugins/easybus-contenteditor/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../wp-content/plugins/easybus-contenteditor/css/style.css?time=<?php echo time();?>" rel="stylesheet" />
        <!--<link href="../wp-content/plugins/easybus-contenteditor/css/bootstrap-select.min.css" rel="stylesheet" />-->
        <style type="text/css">
            .icon-external-link-symbol:before {
  content: "\e909" !important;
  font-size: 13px !important;
  font-weight: bold !important;
}
        </style>
        
        <script src="../wp-content/plugins/easybus-contenteditor/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../wp-content/plugins/easybus-contenteditor/css/jquery.dataTables.min.css">
    </head>
    <body>
        <div style="padding: 30px;">
            <div id="msg_display"></div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
                <li role="presentation" class="active" data-content="country">
                    <a href="#country" aria-controls="country" role="tab" data-toggle="tab">
                        Country
                    </a>
                </li>
                <li role="presentation" data-content="city" >
                    <a href="#city" aria-controls="city" role="tab" data-toggle="tab" id="city_anch">
                        City
                    </a>
                </li>
                <li role="presentation" data-content="airport">
                    <a href="#airport" aria-controls="airport" role="tab" data-toggle="tab" id="airport_anch">
                        Airport
                    </a>
                </li>
                <li role="presentation" data-content="landing_pages" style="display: none;">
                    <a href="#landing_pages" aria-controls="landing_pages" role="tab" data-toggle="tab" id="landing_pages_anch">
                        Popular Landing Pages
                    </a>
                </li>
                <li role="presentation" data-content="popular_destinations">
                    <a href="javascript:void(0);#popular_destinations" aria-controls="popular_destinations" role="tab" data-toggle="tab" id="popular_destinations_anch">
                        Popular Destinations
                    </a>
                </li>
                <li role="presentation" data-content="new_routes">
                    <a href="#new_routes" aria-controls="new_routes" role="tab" data-toggle="tab" id="new_routes_anch">
                        Add new Routes
                    </a>
                </li>
                <li role="presentation" data-content="conversion_marketing">
                    <a href="#conversion_marketing" aria-controls="conversion_marketing" role="tab" data-toggle="tab" id="conversion_marketing_anch">
                        Marketing Conversion
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <!------------------------------- COUNTRY AREA -------------------------------->
                <div role="tabpanel" class="tab-pane active" id="country" >
                    <form id="form1">
                        <div class="inner-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" id="country_list_li" class="active">
                                    <a href="javascript:void(0);#country_list" aria-controls="list" role="tab" data-toggle="tab">
                                        List
                                    </a>
                                </li>
                                <li role="presentation" id="country_editor_li" data-content="country">
                                    <a href="javascript:void(0);#country_editor" aria-controls="editor" role="tab" data-toggle="tab">
                                        Editor
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content common_loader">
                                <div role="tabpanel" class="tab-pane active" id="country_list">
                                    <div class="inner-container">
                                        <table class="table custom-tbl" id="country_table">
                                            <thead>
                                                <tr>
                                                    <th>Country</th>
                                                    <!--<th>City</th>-->
                                                    <th>Content</th>
                                                    <th width="20%">Landing Page</th>
                                                    <th width="10%" class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="hide" id="country_add_new_tr">
                                                    <td class="text-center" colspan="3">
                                                        <div class="no-record">
                                                            <div class="no-record-text">No record available</div>
                                                            <a href="javascript:void(0)" id="country_add_new_anc" class="btn btn-blue">Add Content</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="country_editor">

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!------------------------------- CITY AREA -------------------------------->
                <div role="tabpane2" class="tab-pane" id="city" >
                    <div class="inner-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" id="city_list_li" class="active">
                                <a href="javascript:void(0);#city_list" aria-controls="city_list" role="tab" data-toggle="tab">
                                    List
                                </a>
                            </li>
                            <li role="presentation" id="city_editor_li" data-content="city">
                                <a href="javascript:void(0);#city_editor" aria-controls="editorcity_editor" role="tab" data-toggle="tab">
                                    Editor
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content common_loader">
                            <div role="tabpane2" class="tab-pane active" id="city_list">
                                <div class="inner-container">
                                    <table class="table custom-tbl" id="city_table">
                                        <thead>
                                            <tr>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>Content</th>
                                                <th width="15%">Landing Page</th>
                                                <th width="10%" class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="hide" id="city_add_new_tr">
                                                <td class="text-center" colspan="4">
                                                    <div class="no-record">
                                                        <div class="no-record-text">No record available</div>
                                                        <a href="javascript:void(0)" id="city_add_new_anc" class="btn btn-blue">Add Content</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpane2" class="tab-pane" id="city_editor">

                            </div>
                        </div>
                    </div>
                </div>
                <!------------------------------- AIRPORT AREA -------------------------------->
                <div role="tabpane3" class="tab-pane" id="airport">
                    <div class="inner-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" id="airport_list_li" class="active">
                                <a href="javascript:void(0);#airport_list" aria-controls="airport_list" role="tab" data-toggle="tab">
                                    List
                                </a>
                            </li>
                            <li role="presentation" id="airport_editor_li"  data-content="airport">
                                <a href="javascript:void(0);#airport_editor" aria-controls="airport_editor" role="tab" data-toggle="tab">
                                    Editor
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content common_loader">
                            <div role="tabpane3" class="tab-pane active" id="airport_list">
                                <div class="inner-container">
                                    <table class="table custom-tbl" id="airport_table">
                                        <thead>
                                            <tr>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>Airport</th>
                                                <th width="15%">Landing Page</th>
                                                <th>Content</th>
                                                <th width="10%" class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="hide" id="airport_add_new_tr">
                                                <td class="text-center" colspan="5">
                                                    <div class="no-record">
                                                        <div class="no-record-text">No record available</div>
                                                        <a href="javascript:void(0)" id="airport_add_new_anc" class="btn btn-blue">Add Content</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpane3" class="tab-pane" id="airport_editor">

                            </div>
                        </div>
                    </div>
                </div>


<!------------------------------- LANDING PAGE AREA -------------------------------->
                <div role="tabpane4" class="tab-pane" id="landing_pages">
                    <div role="tabpane4" class="tab-pane" id="landing_pages_editor">

                    </div>
                </div>

<!------------------------------- POPULAR DESTINATIONS-------------------------------->
                <div role="tabpane5" class="tab-pane" id="popular_destinations">
                    <div role="tabpane5" class="tab-pane" id="popular_destinations_editor">

                    </div>
                </div>

<!------------------------------- Add New Routes-------------------------------->
                <div role="tabpane6" class="tab-pane" id="new_routes">
                    <div role="tabpane6" class="tab-pane" id="new_routes_editor">
                        <?php //require_once('new_routes.php');?>
                    </div>
                </div>   

<!------------------------------- Conversion Marketing-------------------------------->
                <div role="tabpane6" class="tab-pane" id="conversion_marketing">
                    <div role="tabpane6" class="tab-pane" id="conversion_marketing_editor">
                    </div>
                </div> 


                            

            </div>

            <!-- modal -->
            <!-- modal -->

            <div class="modal fade new-section-editor" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">
                                Select or Create Editor
                            </h4>

                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" placeholder="Title" class="form-control" name="section_name" id="section_name">
                                    <div id="popup_error_div"></div>
                                </div>
                                <div class="or"><span>OR</span></div>
                                <div class="col-md-12">
                                    <div class="pre-generated-editor" id="sections_div">
                                        No Record Found!
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-blue" id="add_section_btn" name="add_section_btn">Add Editor</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>

 

            function changeCity(type, id) {
                if(type=="city"){
                   $('#city_ddl').prop( "disabled", true );  
                   $('#city_ddl').next(".select2-container").prop( "disabled", true );  
                }
                if (type === 'airport') {
                   $('#airport_city_ddl').prop( "disabled", true );
                   $('#airport_ddl').prop( "disabled", true );

                   $('#airport_city_ddl').next(".select2-container").prop( "disabled", true );  
                   $('#airport_ddl').next(".select2-container").prop( "disabled", true );                      
                }
                var value = $("#" + id).val();
                var ddloptions = '<option value="">Select City</option>';

                 $.ajax({
                    type: "POST",
                    data: {'action': 'get_city_data','id': value},
                    url: ajaxurl,
                    success: function (response) {
                        if (type === 'city') {
                    //$("#city_ddl").show();         
                    $("#" + type + "_ddl").empty().append(response);
                    $('#city_ddl').prop( "disabled", false );  
                    $('#city_ddl').next(".select2-container").prop( "disabled", false );  
                }
                if (type === 'airport') {
                    //$("#airport_city_ddl").show();   
                    $("#" + type + "_city_ddl").empty().append(response);

                    $('#airport_city_ddl').prop( "disabled", false );
                    $('#airport_ddl').prop( "disabled", false );


                    $('#airport_city_ddl').next(".select2-container").show();
                    $('#airport_ddl').next(".select2-container").prop( "disabled", false );    
                }
                    }})


                /* switch ($value) {
                    case 'IND':
                        ddloptions += '<option value="DL">Delhi</option>' +
                                '<option value="MUM">Mumbai</option>' +
                                '<option value="GOA">Goa</option>' +
                                '<option value="IND">Indore</option>';
                        break;
                    case 'AUS':
                        ddloptions += '<option value="SYD">Sydney</option>' +
                                '<option value="MEL">Melbourne</option>';
                        break;
                    case 'BRZ':
                        ddloptions += '<option value="SAL">Salvador</option>' +
                                '<option value="FOR">Fortaleza</option>';
                        break;
                    case 'france':
                        ddloptions += '<option value="paris">Paris</option>' +
                                '';
                        break;
                }*/

                
            }
            function changeAirport(type, id) {
                $('#airport_ddl').prop( "disabled", true ); 
                $('#airport_ddl').next(".select2-container").prop( "disabled", true );  
                var value = $("#" + id).val();
                var ddloptions = '<option value="">Select Airport</option>';
                $.ajax({
                    type: "POST",
                    data: {'action': 'get_airport_data','id': value},
                    url: ajaxurl,
                    success: function (response) {
                        $("#" + type + "_ddl").empty().append(response);
                        $('#airport_ddl').prop( "disabled", false );
                        $('#airport_ddl').next(".select2-container").prop( "disabled", false );  
                    }})
                /*switch ($value) {
                    case 'DL':
                        ddloptions += '<option value="IGIA">Indira Gandhi International Airport</option>' +
                                '<option value="DDA">Delhi Domestic Airport</option>';

                        break;
                    case 'MUM':
                        ddloptions += '<option value="CSIA">Chhatrapati Shivaji International Airport</option>' +
                                '<option value="PIA">Pune International Airport</option>';
                        break;
                    case 'GOA':
                        ddloptions += '<option value="DA">Dabolim Airport</option>' +
                                '<option value="SGA">South Goa Airport</option>';

                        break;
                    case 'SYD':
                        ddloptions += '<option value="MA">Mascot Airport</option>' +
                                '<option value="KSA">Kingsford Smith Airport</option>';
                        break;
                    case 'MEL':
                        ddloptions += '<option value="MEA">Melbourne Airport</option>' +
                                '<option value="AA">Avalon Airport"</option>';
                        break;
                    case 'SAL':
                        ddloptions += '<option value="BA">Barrillas Airport</option>' +
                                '<option value="ETA">El Tamarindo Airport</option>';
                        break;
                    case 'paris':
                        ddloptions += '<option value="Paris Charles de Gaulle">Paris Charles de Gaulle</option>' +
                                '<option value="Paris Beauvais">Paris Beauvais</option><option value="Paris Orly">Paris Orly</option>';
                        break;    

                }*/
                
            }
            function deleteImage(type, section) {
                var ids = "";
                //$('#' + type + "_image_" + section).hide();
                
                $('#preview_' + type + "_content_images_" + section).hide();
                $("#"+type+"_content_images_"+section).val("");

                if ($('#' + type + "_image_id_" + section).length > 0) {
                    var img_id = $('#' + type + "_image_id_" + section).val();
                    ids = img_id.split(",");
                    $.ajax({
                        type: "POST",
                        data: {'action': 'delete_image', 'image_id': ids[0], 'content_id': ids[1]},
                        url: ajaxurl,
                        success: function (data) {
                            console.log("responce data ------- ", data);
                        }
                    });
                }

            }
            function displayImage(e, type, section) {
                var divname = type + "_image_" + section; //country_image_1
                var preview = type + "_preview_" + section; //country_general_info_preview

                if (e.files[0]) {
                    var ext = e.files[0].name.split('.').pop().toLowerCase();
                    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1)
                    {
                        alert("Invalid Image File");
                        return false;
                    }
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        console.log(preview);
                        $('#' + divname).removeClass("hide");
                        $('#' + divname + " a").attr("href", "javascript:void(0)");
                        $('#' + preview).attr('src', e.target.result);
                        $("#" + divname).show();
                    }
                    reader.readAsDataURL(e.files[0]);
                    var f = e.files[0];
                    var fsize = f.size || f.fileSize;
                    if (fsize > 2000000)
                    {
                        alert("Image File Size is very big");
                        return false;
                    }
                }
            }
            function changeAttr(type, section) {
                switch (type) {
                    case 'country':
                        var divname = type + "_image_" + section; //country_general_info_image
                        var preview = type + "_preview_" + section; //country_general_info_preview

                        var selectedCountry = $(this).children("option:selected").text();


                        var cdata = "";
                        /*
                         * AJAX call for get data of country 
                         */
                        var country_ddl = $('#country_ddl').val();
                        $(".featured_content_div").hide();
                        $(".featured_loader_div").html("<div class='loader_div' style='margin: 0 auto;'><img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif'/></div>").show();
                        

                        $.ajax({
                            type: "POST",
                            data: {'action': 'get_featured_lists', 'country': country_ddl},
                            url: ajaxurl,
                            success: function (result) {
                               var obj=JSON.parse(result);
                               if(obj.status_code=="1"){
                                 $(".featured_cities").html(obj.city_result);
                                 $(".featured_airports").html(obj.airport_result);
                                 jQuery("ul.featured_listing").sortable();
                                 if(obj.city_result == null){
                                    $(".featured_cities").html('<div class="alert alert-danger">No Records Found</div>');
                                 }
                                 if(obj.airport_result == null){
                                    $(".featured_airports").html('<div class="alert alert-danger">No Records Found</div>');
                                 }
                                 $(".featured_loader_div").hide();
                                 $(".featured_content_div").show();
                               } 
                            }
                        });
                        



                        $.ajax({
                            type: "POST",
                            data: {'action': 'get_country_content', 'country_ddl': country_ddl},
                            url: ajaxurl,
                            success: function (data) {
                                console.log("My data ", data);
                                if (data.length > 0) {
                                    $("#country_ddl").next('span').remove();

                                    $("#country_ddl").after("<span class='error'>Content already create for this country.</span>");
                                    $('.ddl_select').select2();
//                                    $('.ddl_select').addClass("requiredValue removeError");
//                                    alert("Already created");
                                    return false;
                                }
                                $("#" + type + "_editor_" + section).summernote("enable"); //country_editor_1
                                $('.editor').addClass("requiredValue removeError");
                                if (data) {
                                    $("#" + type + "_editor_" + section).summernote('code', data);
                                    $('.editor').addClass("requiredValue removeError");
                                    $("#country_save").val("country_update")
                                } else {
                                    $("#" + type + "_editor_" + section).summernote('code', " ");
                                    $('.editor').addClass("requiredValue removeError");
                                    $("#country_save").val("country_save")
                                }
                            }
                        });
                        /*
                         * END Of
                         * AJAX call for get data of country 
                         */
                        break;
                    case 'city':
                        var divname = type + "_image_" + section; //country_general_info_image
                        var preview = type + "_preview_" + section; //country_general_info_preview

                        var selectedCountry = $(this).children("option:selected").text();

                        $("#" + type + "_editor_" + section).summernote("enable"); //country_editor_1
                        var cdata = "";
                        /*
                         * AJAX call for get data of country 
                         */
                        var country_ddl = $('#city_country_ddl').val();
                        var city_ddl = $('#city_ddl').val();
                        $.ajax({
                            type: "POST",
                            data: {'action': 'get_city_content', 'country_ddl': country_ddl, 'city_ddl': city_ddl},
                            url: ajaxurl,
                            success: function (data) {
                                if (data.length > 0) {
                                    alert("Already created");
                                    return false;
                                }
                                if (data && data.length > 0) {
                                    $("#" + type + "_editor_" + section).summernote('code', data);
                                    $("#city_save").val("city_update")
                                } else {
                                    $("#" + type + "_editor_" + section).summernote('code', " ");
                                    $("#city_save").val("city_save")
                                }
                            }
                        });
                        /*
                         * END Of
                         * AJAX call for get data of country 
                         */
                        break;
                    case 'airport':
                        var divname = type + "_image_" + section; //country_general_info_image
                        var preview = type + "_preview_" + section; //country_general_info_preview

                        var selectedCountry = $(this).children("option:selected").text();

                        $("#" + type + "_editor_" + section).summernote("enable"); //country_editor_1
                        var cdata = "";
                        /*
                         * AJAX call for get data of country 
                         */
                        var country_ddl = $('#airport_country_ddl').val();
                        var city_ddl = $('#airport_city_ddl').val();
                        var airport_ddl = $('#airport_ddl').val();
                        $.ajax({
                            type: "POST",
                            data: {'action': 'get_airport_content', 'country_ddl': country_ddl, 'city_ddl': city_ddl, 'airport_ddl': airport_ddl},
                            url: ajaxurl,
                            success: function (data) {
                                if (data.length > 0) {
                                    alert("Already created");
                                    return false;
                                }
                                if (data && data.length > 0) {
                                    $("#" + type + "_editor_" + section).summernote('code', data);
                                    $("#airport_save").val("airport_update")
                                } else {
                                    $("#" + type + "_editor_" + section).summernote('code', " ");
                                    $("#airport_save").val("airport_save")
                                }
                            }
                        });
                        /*
                         * END Of
                         * AJAX call for get data of country 
                         */
                        break;
                }
            }
            function saveContent(type) {
               
                $(".common_loader").prepend("<div class='common_loader_div'><img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif' style='position: fixed;top: 50%;z-index: 999;left: 50%;'/></div>"); 
                if (type === "") {
                    return false;
                }
                $(".error").remove();
                var valid = requiredValidation();
                if (valid) {
                    bind_elements();
                    return false;
                }
                switch (type) {
                    case 'country':
                        
                         
                        var form_data = new FormData();
                        var section_ids = $('#section_ids').val();
                        var sids = section_ids;
                        var sections_idsArr = section_ids.split(',');

                        var $content_rows = $("#content_rows").val();
                        var $content_rowsArr = $content_rows.split(',');

                        //alert(content_images);

                        //form_data.append("content_img", content_images);

                        

                        sections_idsArr.forEach(function (item) {
                            //country_file_uploader_1
                            /*if ($('#' + type + '_file_uploader_' + item).length > 0) {
                                if ($('#' + type + '_file_uploader_' + item)[0].files[0] !== undefined) {
                                    var f = $('#' + type + '_file_uploader_' + item)[0].files[0];//country_file_uploader_1
                                    var fsize = f.size || f.fileSize;
                                    if (fsize > 2000000) {
                                        alert("Image File Size is very big");
                                    } else {
                                        form_data.append("files_in[]", $('#' + type + '_image_id_' + item).val());
                                        form_data.append("files_index[]", item);
                                        form_data.append("file[]", $('#' + type + '_file_uploader_' + item)[0].files[0]);
                                    }
                                }
                            }*/
                            //country_editor_1
                            if ($("#" + type + "_title_" + item).length > 0) {
                                if ($("#" + type + "_title_" + item).val() === '') {
//                                    alert("Please enter a value");
//                                    return false;
                                }
                                form_data.append('title[]', $("#" + type + "_title_" + item).val()); //country_title_1
                            }
                            if ($("#" + type + "_editor_" + item).length > 0) {
                                if ($("#" + type + "_editor_" + item).val() === '') {
//                                    alert("Please enter a value");
//                                    return false;
                                }
                                form_data.append('editor[]', $("#" + type + "_editor_" + item).val());
                            }
                            if ($("#" + type + "_content_images_" + item).length > 0) {
                                if ($("#" + type + "_content_images_" + item).val() === '') {
//                                    alert("Please enter a value");
//                                    return false;
                                }
                                form_data.append('content_images[]', $("#" + type + "_content_images_" + item).val()); //country_title_1
                            }
                            /*if ($("#featured_city_id_"+ item).length > 0) {
                                form_data.append('featured_cities[]', $("#featured_city_id_"+ item).val()); //country_title_1
                            }*/
                        });

                         var fcity=[];
                         $("input:checkbox[name=featured_city_id]:checked").each(function(){
                              fcity.push($(this).val());
                         });
                         form_data.append('featured_cities',fcity);

                         var fairport=[];
                         $("input:checkbox[name=featured_airport_id]:checked").each(function(){
                              fairport.push($(this).val());
                         });
                         form_data.append('featured_airports',fairport);  

                        
                        var meta_title=$("#meta_title_"+type).val();
                        var meta_keywords=$("#meta_keywords_"+type).val();
                        var meta_description=$("#meta_description_"+type).val();
                        form_data.append("meta_title", meta_title);
                        form_data.append("meta_keywords", meta_keywords);
                        form_data.append("meta_description", meta_description);

                        var country_ddl = $("#country_ddl").val();
                        var btn_value = $('#country_save').val();
                        var content_rows = $("#content_rows").val();
                        form_data.append("country_ddl", country_ddl);
                        form_data.append('action', 'save_content');
                        form_data.append("sections", sids);
                        form_data.append("content_rows", content_rows);
                        form_data.append('btn_value', btn_value);
                        $.ajax({
                            url: ajaxurl,
                            method: "POST",
                            data: form_data,
                            contentType: false,
                            cache: false,
                            async: false,
                            processData: false,
                            success: function (data) {
                                var data = data.split(',');
                                if (data.length > 1) {
                                    $("#section_ids").val(data[2].replace("|", ","));
                                    $("#content_rows").val(data[1].replace("|", ","));
                                }
                                $("#msg_display").html(data[0]);
                                $("#msg_display").focus();
                                $("#country_save").val("country_update");
                                $("html, body").animate({scrollTop: 0}, "slow");
                            }
                        });
                        var country_mode=$("#country_mode").val();
                        if(country_mode == "add"){
                            window.setTimeout(function () {
                                location.reload()
                            }, 2000)
                        }else{
                             window.setTimeout(function () {
                                $(".common_loader_div").hide();
                                $("#msg_display").html("");
                                get_country_content();
                            }, 2000)
                        }

                        break;
                    case 'city':
                        var form_data = new FormData();
                        var section_ids = $('#city_section_ids').val();
                        var sids = section_ids;
                        var sections_idsArr = section_ids.split(',');
                        if ($("#city_country_ddl").value === "") {
                            alert("please select country");
                            return false;
                        }
                        if ($("#city_ddl").value === "") {
                            alert("please select city");
                            return false;
                        }

                        var files_in = "";
                        sections_idsArr.forEach(function (item) {
                            //country_file_uploader_1
                            /*if ($('#' + type + '_file_uploader_' + item).length > 0) {
                                if ($('#' + type + '_file_uploader_' + item)[0].files[0] !== undefined) {
                                    var f = $('#' + type + '_file_uploader_' + item)[0].files[0];//country_file_uploader_1
                                    var fsize = f.size || f.fileSize;
                                    if (fsize > 2000000) {
                                        alert("Image File Size is very big");
                                    } else {
                                        form_data.append("files_in[]", $('#' + type + '_image_id_' + item).val());
                                        form_data.append("files_index[]", item);
                                        form_data.append("file[]", $('#' + type + '_file_uploader_' + item)[0].files[0]);
                                    }
                                }

                            }*/
                            //country_editor_1
                            if ($("#" + type + "_title_" + item).length > 0) {
                                form_data.append('title[]', $("#" + type + "_title_" + item).val()); //country_title_1
                            }
                            if ($("#" + type + "_editor_" + item).length > 0) {
                                form_data.append('editor[]', $("#" + type + "_editor_" + item).val());
                            }
                            if ($("#" + type + "_content_images_" + item).length > 0) {
                                if ($("#" + type + "_content_images_" + item).val() === '') {
//                                    alert("Please enter a value");
//                                    return false;
                                }
                                form_data.append('content_images[]', $("#" + type + "_content_images_" + item).val()); //country_title_1
                            }
                        });
                        
                        var meta_title=$("#meta_title_"+type).val();
                        var meta_keywords=$("#meta_keywords_"+type).val();
                        var meta_description=$("#meta_description_"+type).val();
                        form_data.append("meta_title", meta_title);
                        form_data.append("meta_keywords", meta_keywords);
                        form_data.append("meta_description", meta_description);

                        var country_ddl = $("#city_country_ddl").val();
                        var city_ddl = $("#city_ddl").val();
                        var btn_value = $('#city_save').val();
                        var content_rows = $("#city_content_rows").val();
                        form_data.append("country_ddl", country_ddl);
                        form_data.append("city_ddl", city_ddl);
                        form_data.append('action', 'save_content');
                        form_data.append("sections", sids);
                        form_data.append("content_rows", content_rows);
                        form_data.append('btn_value', btn_value);
                        $.ajax({
                            url: ajaxurl,
                            method: "POST",
                            data: form_data,
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (data) {
                                console.log("CITY SAVEING ---->>>>", data);
                                var data = data.split(',');

//                                $("#msg_display").html(data[0]);
//                                $("#city_save").val("city_save");

                                if (data.length > 1) {
                                    $("#city_section_ids").val(data[2].replace("|", ","));
                                    $("#city_content_rows").val(data[1].replace("|", ","));
                                }
                                $("#msg_display").html(data[0]);
                                $("#city_save").val("city_update");
                                $("html, body").animate({scrollTop: 0}, "slow");
                            }
                        });
                        var city_mode=$("#city_mode").val();
                        if(city_mode == "add"){
                            window.setTimeout(function () {
                                location.reload()
                            }, 2000)
                        }else{
                             window.setTimeout(function () {
                                $(".common_loader_div").hide();
                                $("#msg_display").html("");
                                get_city_content();
                            }, 2000)
                        }
                        break;
                    case 'airport':
                        var form_data = new FormData();
                        var section_ids = $('#airport_section_ids').val();
                        var sids = section_ids;
                        var sections_idsArr = section_ids.split(',');
                        if ($("#city_country_ddl").value === "") {
                            alert("please select country");
                            return false;
                        }
                        if ($("#city_ddl").value === "") {
                            alert("please select city");
                            return false;
                        }
                        if ($("#airport_ddl").value === "") {
                            alert("please select airport");
                            return false;
                        }

                        var files_in = "";
                        sections_idsArr.forEach(function (item) {
                            /*if ($('#' + type + '_file_uploader_' + item).length > 0) {
                                if ($('#' + type + '_file_uploader_' + item)[0].files[0] !== undefined) {
                                    var f = $('#' + type + '_file_uploader_' + item)[0].files[0];//country_file_uploader_1
                                    var fsize = f.size || f.fileSize;
                                    if (fsize > 2000000) {
                                        alert("Image File Size is very big");
                                    } else {
                                        form_data.append("files_in[]", $('#' + type + '_image_id_' + item).val());
                                        form_data.append("files_index[]", item);
                                        form_data.append("file[]", $('#' + type + '_file_uploader_' + item)[0].files[0]);
                                    }
                                }
                            }*/
                            //country_editor_1
                            if ($("#" + type + "_title_" + item).length > 0) {
                                form_data.append('title[]', $("#" + type + "_title_" + item).val()); //country_title_1
                            }
                            if ($("#" + type + "_editor_" + item).length > 0) {
                                form_data.append('editor[]', $("#" + type + "_editor_" + item).val());
                            }
                            if ($("#" + type + "_content_images_" + item).length > 0) {
                                if ($("#" + type + "_content_images_" + item).val() === '') {
//                                    alert("Please enter a value");
//                                    return false;
                                }
                                form_data.append('content_images[]', $("#" + type + "_content_images_" + item).val()); //country_title_1
                            }
                        });

                        
                        var min_price=$("#min_price_"+type).val();
                        form_data.append("min_price", min_price);

                        var country_ddl = $("#airport_country_ddl").val();
                        var city_ddl = $("#airport_city_ddl").val();
                        var airport_ddl = $("#airport_ddl").val();
                        var btn_value = $('#airport_save').val();
                        var content_rows = $("#airport_content_rows").val();
                        form_data.append("country_ddl", country_ddl);
                        form_data.append("city_ddl", city_ddl);
                        form_data.append("airport_ddl", airport_ddl);
                        form_data.append('action', 'save_content');
                        form_data.append("sections", sids);
                        form_data.append("content_rows", content_rows);
                        form_data.append('btn_value', btn_value);
                        $.ajax({
                            url: ajaxurl,
                            method: "POST",
                            data: form_data,
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (data) {
                                console.log("airport saving ", data);
                                var data = data.split(',');
                                $("#msg_display").html(data);
                                $("#airport_save").val("airport_save");
                                if (data.length > 1) {
                                    $("#airport_section_ids").val(data[2].replace("|", ","));
                                    $("#airport_content_rows").val(data[1].replace("|", ","));
                                }
                                $("#msg_display").html(data[0]);
                       
                               var min_price=$("#min_price_"+type).val();
                               var dataNT=[];
                               var min_price={"min_price":min_price};
                               dataNT.push(min_price);
                               console.log(JSON.stringify({"Data":dataNT}));
                      
                              $.ajax({
                                url: "",
                                method:"POST",
                                contentType:"application/JSON",
                                data:JSON.stringify({"Data":dataNT}),  
                                success: function(result){
                                     
                                }});
                                $("html, body").animate({scrollTop: 0}, "slow");
                            }
                        });
                        var airport_mode=$("#airport_mode").val();
                        if(airport_mode == "add"){
                            window.setTimeout(function () {
                                location.reload()
                            }, 2000)
                        }else{
                             window.setTimeout(function () {
                                $(".common_loader_div").hide();
                                $("#msg_display").html("");
                                get_airport_content();
                            }, 2000)
                        }
                        break;
                }

            }
            function editContent(type, id) {
                var airport = '';
                var city = '';
                var country = '';
                var new_ids = '';
                var editDiv = '';
                if (type === 'country') {
                    country = id;
                    editDiv = "country_editor";
                }
                if (type === 'city') {
                    new_ids = id.split(',');
                    country = new_ids[0];
                    city = new_ids[1];
                    editDiv = "city_editor";
                }
                if (type === 'airport') {
                    new_ids = id.split(',');
                    country = new_ids[0];
                    city = new_ids[1];
                    airport = new_ids[2];
                    editDiv = "airport_editor";
                }
                /*
                 * AJAX call for get sections for popup drop down
                 */
                $.ajax({
                    type: "POST",
                    data: {'action': 'get_all_sections', 'country_code': country, 'city_code': city, 'airport_code': airport},
                    url: ajaxurl,
                    success: function (response) {
                        //console.log("get_all_sections response ---->>>>>", response);
                        if (response) {
                            $("#" + editDiv).html(response);
                            $('.editor').summernote({
                                placeholder: 'Content Editor ',
                                tabsize: 2,
                                height: 300, // set editor height
                                minHeight: null, // set minimum height of editor
                                maxHeight: null, // set maximum height of editor
                                focus: true,
                                toolbar: [['style', ['bold', 'italic', 'underline', 'clear']],
                                    ['color', ['color']],
                                    ['Insert', ['link']],
                                    ['view', ['codeview']]]
                            });
                            $('.editor').addClass("requiredValue removeError");
//                            $(".editor").summernote("enable");
//                            $('.ddl_select').select2();
                            $('.image_preview').show();
                            $("#" + type + "_list").removeClass('active');
                            $("#" + type + "_editor").addClass('active');
                            $("#" + type + "_list_li").removeClass('active');
                            $("#" + type + "_editor_li").addClass('active');
                            $("#" + type + "_save").val("" + type + "_update");
                            $(":file").filestyle({"buttonText": "Choose an Image"});
                        }

                        $(".editor").summernote("enable");
                        $('.editor').addClass("requiredValue removeError");
                        $('.ddl_select').select2();
                        $('.ddl_select').addClass("requiredValue removeError");
                        $("ul.featured_listing").sortable();
                    }, error: function (error) {
                        // 
                    }
                });
                /*
                 * END OF AJAX                                                 
                 */
            }
            function deleteContent(type, id) {
                $.ajax({
                    type: "POST",
                    data: {'action': 'delete_content', 'type': type, 'id': id},
                    dataType: 'JSON',
                    url: ajaxurl,
                    success: function (response) {
                        if (response > 0) {
                            location.reload();
                        } else {
                            alert("Error in delete");
                        }
                    }})
            }
            function deleteSection(id){
                var r = confirm("Are you sure you want to delete, contents also deleted if press Ok?");
                if (r == true) {
                   $.ajax({
                    type: "POST",
                    data: {'action': 'delete_section_content','id': id},
                    dataType: 'JSON',
                    url: ajaxurl,
                    success: function (response) {
                        if (response > 0) {
                            //location.reload();
                            $("#msg_display").html('<div class="alert alert-success alert-dismissible">Deleted Successfully</div>');
                            $("#cont_id_"+id).remove();
                            $("html, body").animate({scrollTop: 0}, "slow");
                        } else {
                            alert("Error in delete");
                        }
                    }})
                }
            }
            function deleteAddSection(id){
               $("#"+id).remove();
               $("#msg_display").html('<div class="alert alert-success alert-dismissible">Deleted Successfully</div>');
               $("html, body").animate({scrollTop: 0}, "slow");
            }

           // Landing Pages
            $("#landing_pages_anch").click(function () {
                $('.ddl_select_lp').select2();
                $.get("../wp-content/plugins/easybus-contenteditor/views/landing_pages.php?v=<?php echo time();?>", function (data) {
                                $("#landing_pages_editor").html(data);
                                bind_elements();
                                $('.ddl_select_lp').select2();
                            });
            });
            function get_landing_pages(city_id){
                $('#landing_pages_content_append').html("<div class='page-loader'><img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif'/></div>");
                $.ajax({
                    type: "POST",
                    data: {'action': 'get_landing_pages','city_id': city_id},
                    url: ajaxurl,
                    success: function (response) {
                        $('#landing_pages_content_append').html(response);
                    }})
            }
            function changeCityLandingPages(type, id) {
                var value = $("#" + id).val();
                var ddloptions = '<option value="">Select City</option>';

                 $.ajax({
                    type: "POST",
                    data: {'action': 'get_city_data','id': value},
                    url: ajaxurl,
                    success: function (response) {
                        $("#" + type + "_ddl").empty().append(response);
                        var city_ddl=$("#"+type+'_ddl').val();
                        get_landing_pages(city_ddl);
                        $(".error").remove();
                    }})
            }
            function landing_pages_save(){
                $(".error").remove();
                var valid = requiredValidation();
                if (valid) {
                    bind_elements();
                    return false;
                }
                var myform = document.getElementById("landing_pages_form");
                var myform= new FormData(myform);
                myform.append("action", "landing_pages_save");
                $.ajax({
                url: ajaxurl,
                method:"POST",
                data:myform,
                contentType:false,  
                processData: false,   
                success: function(result){
                    
                
                }});

            }

            // Popular Destinations 
            $("#popular_destinations_anch").click(function () {
                $('.ddl_selectpd').select2();
                 $.get("../wp-content/plugins/easybus-contenteditor/views/popular_destinations.php?v=<?php echo time();?>", function (data) {
                                $("#popular_destinations_editor").html(data);
                                bind_elements();
                                $('.ddl_selectpd').select2();
                            });
            });
            function get_popular_destinations(city_id){
                $(".popuplar-destination-msg").html("");
                $('#popular_destinations_content_append').html("<div class='page-loader'><img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif'/></div>");
                $.ajax({
                    type: "POST",
                    data: {'action': 'get_popular_destinations','city_id': city_id},
                    url: ajaxurl,
                    success: function (response) {
                        if((response=="") || (response=="0")) {
                           $('#popular_destinations_content_append').html(""); 
                           $(".popuplar-destination-msg").html("<div class='alert alert-danger alert-dismissible'>No Records Found</div>");
                        }else{
                           $('#popular_destinations_content_append').html(response);
                        }
                    }})
            }
            function changeCityPopularDestinations(type, id) {
                $("#populardestination_ddl").prop( "disabled", true );
                $('#populardestination_ddl').next(".select2-container").prop( "disabled", true ); 
                var value = $("#popuplar_city_country_ddl").val();
                var ddloptions = '<option value="">Select City</option>';
                 $.ajax({
                    type: "POST",
                    data: {'action': 'get_city_data','id': value},
                    url: ajaxurl,
                    success: function (response) {
                            $("#" + type + "_ddl").empty().append(response);
                            var city_ddl=$("#"+type+'_ddl').val();
                            if(response=="<option value=''>No Record found</option>"){
                              $("#populardestination_ddl").prop( "disabled", true);
                              $('#populardestination_ddl').next(".select2-container").prop( "disabled", true );  
                            }else{
                              $("#populardestination_ddl").prop( "disabled", false);
                              $('#populardestination_ddl').next(".select2-container").prop( "disabled", false );  
                            }
                            
                            get_popular_destinations(city_ddl);
                            $(".error").remove();
                    }})
            }
            function popular_destinations_save(){
                var popuplar_city_country_ddl=$("#popuplar_city_country_ddl").val();
                if(popuplar_city_country_ddl==""){
                    $(".popuplar-destination-msg").html("<div class='alert alert-danger alert-dismissible'>Please Select Country</div>");
                    return false;
                }
                $(".popuplar-destination-msg").html("");
                /*$(".error").remove();
                var valid = requiredValidation();
                if (valid) {
                    bind_elements();
                    return false;
                }*/
                var data=[];
                $.each($(".popuplar_checkbox"),function(index,element){
    var station={"Id": $(this).attr("data-id"),"OriginCode": $(this).val(),"IsImportant": $(this).is(":checked"),"SortOrder":($(this).is(":checked")?1:0)};
      data.push(station);
                });
                console.log("{Data:",data,"}");                
                /*var myform = document.getElementById("popular_destinations_form");
                var myform= new FormData(myform);
                myform.append("action", "popular_destinations_save");*/
               
                $('#popular_destinations_content_append').prepend("<div class='page-loader-later'><img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif'/></div>");
                $(".page-loader-later").show();

                $.ajax({
                url: "https://bookings.easybus.com/affiliate/api/popular/set_popular_route",
                method:"POST",
                contentType:"application/JSON",
                data:JSON.stringify({"Data":data}),  
                success: function(result){
                     var code='<?php echo http_response_code();?>';
                     $(".page-loader-later").hide();
                     if(code=="200"){
                        $('#popular_destinations_content_append').prepend("<div class='alert alert-success alert-dismissible'>Saved Successfully</div>");
                     }else{
                        $('#popular_destinations_content_append').prepend("<div class='alert alert-danger alert-dismissible'>"+result+"</div>");
                     }
                     //$('#msg_display').html(result);
                }});

            }
$("#new_routes_anch").click(function () {
                $('.ddl_selectn').select2();
                $.get("../wp-content/plugins/easybus-contenteditor/views/new_routes.php?v=<?php echo time();?>", function (data) {
                                $("#new_routes_editor").html(data);
                                bind_elements();
                                $('.ddl_selectn').select2();
                            });
            });





            function changeCityNewRoutes(type, id) {
                $('#1_newroutes_ddl').prop( "disabled", true );
                $('#1_newroutes_ddl').next(".select2-container").prop( "disabled", true );  
                var value = $("#" + id).val();
                var ddloptions = '<option value="">Select City</option>';
                 $.ajax({
                    type: "POST",
                    data: {'action': 'get_city_data','id': value},
                    url: ajaxurl,
                    success: function (response) {
                            $("#" + type + "_ddl").empty().append(response);
                            var city_ddl=$("#"+type+'_ddl').val();
                            $('#1_newroutes_ddl').next(".select2-container").prop( "disabled", false );
                            $('#1_newroutes_ddl').prop( "disabled", false );  
                            getBusFromRoute(value,city_ddl,id,"new_routes_"+id);
                    }})
            }

            function getBusFromRoute(country,city,id,newr){
                  if(country==""){
                    var country = $("#city_country_ddl_newroutes_ddl_"+id).val();
                  }
                  $.ajax({
                    type: "POST",
                    data: {'action': 'get_bus_from_route','country': country,'city': city},
                    url: ajaxurl,
                    success: function (response) {
                       $("."+newr).html(response);     
                    }})
            }


            function new_routes_uploads(){
                $('.msg_routes').html("<div class='page-loader-later'><img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif'/></div>");
                var myform = document.getElementById("new_routes_upload_form");
                var myform= new FormData(myform);
                myform.append("action", "new_routes_upload");
                jQuery.ajax({
                url: ajaxurl,
                method:"POST",
                data:myform,
                contentType:false,  
                processData: false,   
                success: function(result){
                    console.log(result);
                    if(result=="0"){
                      $(".msg_routes").html("<div class='alert alert-danger alert-dismissible'>Please select a file</div>");   
                    }else if(result=="1"){
                       $(".msg_routes").html("<div class='alert alert-danger alert-dismissible'>Only .csv files allowed</div>");    
                    }else{
                        $.ajax({
                        url: "https://bookings.easybus.com/affiliate/api/popular/create_popular_route",
                        method:"POST",
                        contentType:"application/json",
                        data:JSON.stringify({"data":JSON.parse(result)}),  
                        success: function(result){
                             var code='<?php echo http_response_code();?>';
                             if(code=="200"){
                                $('.msg_routes').html("<div class='alert alert-success alert-dismissible'>Saved Successfully</div>");
                                document.getElementById("new_routes_upload_form").reset();
                                setTimeout(function(){ $('.msg_routes').html(""); }, 2000); 
                             }else{
                                $('.msg_routes').html("<div class='alert alert-danger alert-dismissible'>"+result+"</div>");
                             }
                        }});
                    }
                
                }});
            }
            function new_routes_saved(){
                $('.msg_routes_form').html("<div class='page-loader-later'><img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif'/></div>");
                var myform = document.getElementById("new_routes_add_form");
                var myform= new FormData(myform);
                myform.append("action", "new_routes_saved");
                jQuery.ajax({
                url: ajaxurl,
                method:"POST",
                data:myform,
                contentType:false,  
                processData: false,   
                success: function(result){
                    if(result=="0"){
                      $(".msg_routes_form").html("<div class='alert alert-danger alert-dismissible'>Please select a country</div>");   
                    }else{
                       $.ajax({
                        url: "https://bookings.easybus.com/affiliate/api/popular/create_popular_route",
                        method:"POST",
                        contentType:"application/JSON",
                        data:JSON.stringify({"data":JSON.parse(result)}), 
                        success: function(result){
                             var code='<?php echo http_response_code();?>';
                             if(code=="200"){
                                $('.msg_routes_form').html("<div class='alert alert-success alert-dismissible'>Saved Successfully</div>");
                                //document.getElementById("new_routes_add_form").reset();
                                setTimeout(function(){ $('.msg_routes_form').html(""); }, 2000); 
                             }else{
                                $('.msg_routes_form').html("<div class='alert alert-danger alert-dismissible'>"+result+"</div>");
                             }
                        }});  
                    }
                                   
                }});                
            }
            function add_more_rows_routes(){
              var row_counter;  
              var row_counter=$("#row_counter").val();
              row_counter++;
              $("#row_counter").val(row_counter);

              $('.ddl_select_nr').select2();
                $.get("../wp-content/plugins/easybus-contenteditor/views/new_routes_tr_row.php?row_counter="+row_counter, function (data) {
                                $("#new_routes_table tbody").append(data); 
                                bind_elements();
                                $('.ddl_select_nr').select2();
                            }); 
            }
            function deleteRow(row) {
              var row_counter;  
              var row_counter=$("#row_counter").val();
              row_counter--;
              $("#row_counter").val(row_counter);
              var i = row.parentNode.parentNode.rowIndex;
              document.getElementById('new_routes_table').deleteRow(i);
            }
            function upload_image_btn(type) {
                var mediaUploader;
                if (mediaUploader) {
                  mediaUploader.open();
                  return;
                }
                mediaUploader = wp.media.frames.file_frame = wp.media({
                  title: 'Choose Image',
                  library : {
                                type : 'image',
                            },
                  button: {
                  text: 'Choose Image'
                }, multiple: false });
                mediaUploader.on('select', function() {
                  var attachment = mediaUploader.state().get('selection').first().toJSON();
                     $('#'+type).val(attachment.url);
                     $("#preview_"+type).removeClass("hide");
                     $("#preview_img_"+type).attr("src",attachment.url);
                     $("#preview_"+type).show();
                  
                });
                mediaUploader.open();
            }

            $("#conversion_marketing_anch").click(function () {
                
                $.get("../wp-content/plugins/easybus-contenteditor/views/conversion_marketing_list.php?v=<?php echo time();?>", function (data) {
                                $("#conversion_marketing_editor").html(data);
                                bind_elements();
                                $(document).ready( function () {
                                    $('#conversion_marketing_table').DataTable({
                                        "order": [],
                                        "info":     false,
                                        "searching": false,
                                        "lengthChange":false
                                    });
                                } );                                
                            });
            });
   
    $("#meta_tags_add_new_anc").click(function () {
        
        $('.ddl_select_meta_tags').select2();
        $('.ddl_select_meta_tags').addClass("requiredValue removeError");
        $("#meta_tags_list").removeClass('active');
        $("#meta_tags_editor").addClass('active');
        $("#meta_tags_list_li").removeClass('active');
        $("#meta_tags_editor_li").addClass('active');
        $.get("../wp-content/plugins/easybus-contenteditor/views/meta_tags_form.php?v=<?php echo time();?>", function (data) {
            $("#meta_tags_editor").html(data);
            bind_elements();
            $('.ddl_select_meta_tags').select2();
            $('#meta_tags_city_ddl').prop( "disabled", true );  
            $('#meta_tags_city_ddl').next(".select2-container").prop( "disabled", true ); 
        });
    });

    $("#meta_tags_editor_li").click(function () {
        
        if ($("#meta_tags_editor_li").attr('class') === undefined || $("#meta_tags_editor_li").attr('class') === "") {
            $.get("../wp-content/plugins/easybus-contenteditor/views/meta_tags_form.php?v=<?php echo time();?>", function (data) {
           $("#meta_tags_editor").html(data);
            bind_elements();
            $('.ddl_select_meta_tags').select2();
            $('#meta_tags_city_ddl').prop( "disabled", true );  
            $('#meta_tags_city_ddl').next(".select2-container").prop( "disabled", true ); 
            });
        }
    });

   function save_tags(){
    $(".meta_tags_msg").html("");
    $(".meta_tags_msg").prepend("<div class='common_loader_div'><img src='../wp-content/plugins/easybus-contenteditor/css/ajax-loader.gif' style='position: fixed;top: 50%;z-index: 999;left: 50%;'/></div>"); 
     var myform = document.getElementById("meta_tags_form");
     var myform= new FormData(myform);
     myform.append("action", "meta_tags_save");
     jQuery.ajax({
                url: ajaxurl,
                method:"POST",
                data:myform,
                contentType:false,  
                processData: false,   
                success: function(result){
                    if(result=="1"){
                        $("#msg_display").html("<div class='alert alert-success alert-dismissible'>Saved Successfully</div>");
                                $("#msg_display").focus();
                                $("html, body").animate({scrollTop: 0}, "slow");


                                window.setTimeout(function () {
                            location.reload()
                        }, 2000)
                    }else if(result=="2"){
                         $("#msg_display").html("<div class='alert alert-success alert-dismissible'>Updated Successfully</div>");
                                $("#msg_display").focus();
                                $("html, body").animate({scrollTop: 0}, "slow");
                                window.setTimeout(function () {
                            location.reload()
                        }, 2000)
                    }else if(result=="3"){
                        $(".meta_tags_msg").html("");
                        $(".meta_tags_msg").html("<div class='alert alert-danger alert-dismissible'>Record already exists for this country.</div>");
                    }else if(result=="4"){
                        $(".meta_tags_msg").html("");
                        $(".meta_tags_msg").html("<div class='alert alert-danger alert-dismissible'>Record already exists for this country-city.</div>");
                    }else if(result=="5"){
                        $(".meta_tags_msg").html("");
                        $(".meta_tags_msg").html("<div class='alert alert-danger alert-dismissible'>Please Select Country</div>");
                    }else if(result=="6"){
                        $(".meta_tags_msg").html("");
                        $(".meta_tags_msg").html("<div class='alert alert-danger alert-dismissible'>Please Select City</div>");
                    }
                
           }});
   }

   function editMetaContent(id){
      
      if ($("#meta_tags_editor_li_edit").attr('class') === undefined || $("#meta_tags_editor_li").attr('class') === "") {
            $("#meta_tags_editor_li_edit a").click();
            $.get("../wp-content/plugins/easybus-contenteditor/views/meta_tags_form.php?v=<?php echo time();?>&mode=edit&id="+id, function (data) {
           $("#meta_tags_editor_edit").html(data);
            bind_elements();
            $('.ddl_select_meta_tags').select2();
            $('#meta_tags_city_ddl').prop( "disabled", true );  
            $('#meta_tags_city_ddl').next(".select2-container").prop( "disabled", true ); 
            });
        }
   }

   function changeCityMetaTags(id) {
        $('#meta_tags_city_ddl').prop( "disabled", true );  
        $('#meta_tags_city_ddl').next(".select2-container").prop( "disabled", true ); 
        var value = $("#" + id).val();
        var ddloptions = '<option value="">Select City</option>';

         $.ajax({
            type: "POST",
            data: {'action': 'get_city_data','id': value},
            url: ajaxurl,
            success: function (response) {
                $("#meta_tags_city_ddl").empty().append(response);
                $('#meta_tags_city_ddl').prop( "disabled", false );  
                $('#meta_tags_city_ddl').next(".select2-container").prop( "disabled", false ); 
            }})
        
    }
    function changeType(id){
        if(id=="country"){
          $('#meta_tags_city_ddl').prop( "disabled", true );  
          $('#meta_tags_city_ddl').next(".select2-container").prop( "disabled", true ); 
        }else{
          $('#meta_tags_city_ddl').prop( "disabled", false );  
          $('#meta_tags_city_ddl').next(".select2-container").prop( "disabled", false ); 
        }
    }


    $("#meta_tags_anch").click(function () {
        get_meta_tags_content();
    });
    function get_meta_tags_content() {
    /*
     * AJAX call for get sections
     */
    $.ajax({
        type: "POST",
        data: {'action': 'get_meta_tags_content'},
        dataType: 'JSON',
        url: ajaxurl,
        success: function (response) {
            if (response == 0) {
                    
                    $("#meta_tags_add_new_tr").removeClass("hide");
                }
            if (response) {
                var len = response.length;
                if (len == 0) {
                    $("#meta_tags_add_new_tr").removeClass("hide");
                }
                var tr_str = "";
                for (var i = 0; i < len; i++) {
                    var id = response[i].id;
                    var country = response[i].country;
                    var city = response[i].city;
                    
                    
                    
                    var id = response[i].id;
                    var section_id = response[i].section_id;
                    var $heading = '';
                    var $city = '';
                   
                    $heading = response[i].display_country;
                    $city = response[i].display_city;
                    $meta_title = response[i].meta_title;
                    
                    tr_str = tr_str + "<tr>" +
                            "<td class='nowrap'>" + $heading + "</td>" +
                            "<td class='nowrap'>" + $city + "</td>" +
                            "<td class='nowrap'>" + $meta_title + "</td>" +
                            "<td class='nowrap text-center'>" +
                            "<a href='JavaScript:void(0)' class='text-blue font-icon mr-2' onclick='editMetaContent("+id+")'>" +
                            "<i class='icon-edit'></i></a>" +
                            "<a href='JavaScript:void(0)' class='text-red font-icon' onclick='deleteMetaContent("+id+")'><i class='icon-delete'></i></a>" +
                            "</td>" +
                            "</tr>";
                    $("#meta_tags_table tbody").html(tr_str);
                }
            } else {
                $("#meta_tags_add_new_tr").removeClass("hide");
            }
        }
    });
    /*
     * END OF AJAX                                                 
     */

}


        </script>
        <style type="text/css">
            .landing_pages_listing_bx{
                width: 24px !important;
                height: 22px !important;
                margin-top: -4px !important;
            }
            .landing_pages_listing li span{
                padding: 0 0 0 9px !important;
                font-size:13px !important;
            }
            .margin-top56{
               /* margin-top: 56px !important;*/
            }
            .page-loader{
                text-align: center !important;
            }
            .page-loader-later{
                height: 88px;
                margin-right: 16px;
                margin-left: -90px;
                margin-top: -50px;
                margin-bottom: 10px;
            }
             .landing_pages_listing {
                display: flex;
                flex-wrap: wrap;
            }
            .landing_pages_listing li {
                list-style: none;
                width: 31.33%;
                white-space: nowrap;
                text-overflow: ellipsis;
                margin-bottom: 10px;
                padding: 7px 8px 7px 8px;
                margin: 0 9px 7px 0;
                /*background-color: #80808047;*/
            }
            .btn-blue-small{
                background-color:#0073aa;
                color:#fff;
                font-weight: 500;
                font-size: 13px;
                transition: all 250ms ease;
                margin-top: 12px;
            }
             .btn-blue-small:hover, .btn-blue-small:active, .btn-blue-small:focus {
                background-color: #005d8a;
                color:#fff;
            }
            .my-custom-scrollbar {
                position: relative;
                overflow: auto;
            }
            .table-wrapper-scroll-y {
                display: block;
            }
            .add_more_rows{
                cursor: pointer;
                color: #005d8a !important;
                font-size: 26px;
            }
            .delete_rows_routes{
                cursor: pointer;
                color: #c90d0d;
                font-size: 26px; 
            }
            .new_routes_section-2{
                font-size: 20px;
                font-weight: bold;
                text-align: center;
                margin: 24px 0 48px 0;
            }
            .btn-ico-common{
               margin-right: 3px;
               font-size: 14px;
            }
            .new_routes_chk_bx{
               width: 20px !important;
               height: 20px !important;
             }
             .pd-heading{
                font-size: 20px !important;
                font-weight: bold !important;
                margin-top: 30px !important;
                margin-bottom: 20px !important;
                margin-left: 5px !important;
                text-decoration: underline !important;
             }
             #wpfooter p{
                display: none !important; 
             }
             .image-info-txt{
                font-weight: bold !important;  
                color:green !important; 
             }
             .sorting{
                border-bottom: 0px !important;
             }
             .featured_listing_bx{
                width: 19px !important;
                height: 19px !important;
                margin-top: -4px !important;
            }
            .featured_listing li span{
                padding: 0 0 0 9px !important;
                font-size:13px !important;
            }
            .featured_listing li {
                list-style: none;
                width: auto;
                white-space: nowrap;
                text-overflow: ellipsis;
                margin-bottom: 10px;
                padding: 7px 8px 7px 8px;
                margin: 0 9px 7px 0;
                background-color: #80808047;
                cursor: s-resize;
            }
            .featured_listing_scroll{
                overflow-y: scroll !important;
                height: 398px !important;
            }
        </style>
        
    </body>
</html>
