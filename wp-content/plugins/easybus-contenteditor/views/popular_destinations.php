<?php
require_once("../../../../wp-load.php");
?>
<div class="inner-container">
    <div id="popular_destinations_editor_section">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_selectpd" autocomplete="off" id="popuplar_city_country_ddl"
                        name="city_country_ddl" placeholder="Pick a Country..." onchange="changeCityPopularDestinations('populardestination', this.id)">
                        <option value="">Select Country</option>
                        <?php
                      global $wpdb;
                      $query="Select*from country";
                      $result = $wpdb->get_results($query,ARRAY_A);
                       
                        if(!empty($result)){
                           foreach($result as $value){
                        ?>
                        <option value="<?php echo $value["iso"];?>"><?php echo $value["nicename"];?></option>
                        <?php   } 
                        }
                      ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_selectpd" autocomplete="off" id="populardestination_ddl" name="city_ddl"
                        placeholder="Pick a City..." onchange="get_popular_destinations(this.value);">
                        <option value="">Select City</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="popuplar-destination-msg col-md-12"></div> 
           <div class="col-md-12 margin-top56" id="popular_destinations_content_append">
               
           </div> 
        </div>
        <!-- sections here -->
    </div>
    <div class="" style="min-width: 0 !important;">
        <button type="button" class="btn btn-blue-small" type="button" name="city_save" id="popular_destinations_save"
            value="landing_pages_save" onclick="popular_destinations_save();">
            <i class="icon-save-file-option font-icon btn-ico-common"></i> Save
        </button>
    </div>
</div>