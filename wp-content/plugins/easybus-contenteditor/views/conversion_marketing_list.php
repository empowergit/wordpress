<?php
require_once("../../../../wp-load.php");
?>
<div class="inner-container">
    <table class="table custom-tbl" id="conversion_marketing_table">
        <thead>
            <tr>
                <th>Date</th>
                <th>Customer ID</th>
                <th>Redirect URL</th>
            </tr>
        </thead>
        <tbody>
        <?php
           global $wpdb;
           $query="Select*from marketing_info ORDER BY created_date  DESC";
           $result = $wpdb->get_results($query,ARRAY_A);
           if(!empty($result)){
            foreach($result as $value){
              $url=explode("custmkid=",$value["request_url"]);
        ?>
            <tr>
                <td class="nowrap noborder"><?php echo $value["created_date"];?></td>
                <td class="nowrap noborder"><?php echo $url["1"];?></td>
                <td class="nowrap noborder"><?php echo $url["0"];?></td>
            </tr>
        <?php }}else{?>
            <tr>
              <td colspan="3" class="text-center">No Records Found</td>  
            </tr>
        <?php }?>    
        </tbody>
    </table>
</div>
<?php
  if(!empty($result)){
    if(count($result) < 10){
       echo "<style>#conversion_marketing_table_paginate{
        display:none !important;
       }</style>"; 
    }
  }
?>
