<?php
require_once("../../../../wp-load.php");
?>
<div class="inner-container">
    <div  id="airport_editor_section">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_select" autocomplete="off" 
                            id="airport_country_ddl" name="airport_country_ddl"
                            placeholder="Pick a Country..." onchange="changeCity('airport', this.id)">                                                    
                        <option value="">Select Country</option>
                        <?php
                      global $wpdb;
                      $query="Select*from country";
                      $result = $wpdb->get_results($query,ARRAY_A);
                       
                        if(!empty($result)){
                           foreach($result as $value){
                        ?>
                         <option value="<?php echo $value["iso"];?>"><?php echo $value["nicename"];?></option>
                        <?php   } 
                        }
                      ?>
<!--                         <option value="IND">India</option>
                        <option value="AUS">Australia</option>
                        <option value="BRZ">Brazil</option>
                        <option value="france">France</option> -->
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_select" autocomplete="off" 
                            id="airport_city_ddl" name="airport_city_ddl"
                            placeholder="Pick a City..." onchange="changeAirport('airport', this.id)">
                        <option value="">Select City</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select class="form-control ddl_select" autocomplete="off" 
                            id="airport_ddl" name="airport_ddl"
                            placeholder="Pick a airport..." onchange="changeAttr('airport', '1')">                                                    
                        <option value="">Select Airport</option>

                    </select>
                </div>
            </div>
        </div>
        <!-- sections here -->

        <div class="inner-container">
            <div id="min_price_editor_section">
                <div class="content-editor">
                  <h4 class="editor-heading"><?php echo MIN_PRICE_SECTION_HEADING;?></h4>
                  <div class="editor-body">
                    <div class="row">
                       <div class="col-md-12">
                         <div class="form-group">
                            <label class="control-label">Min. Price</label>
                            <input type="text" class="form-control" name="min_price_airport" id="min_price_airport">
                          </div>
                         </div>
                         </div>
                         
                  </div>
                </div>
            </div>
        </div>  

        <div class="content-editor" id="gn_info">
            <h4 class="editor-heading">About Low Cost Bus Transfers <a href="javascript:void(0);" class="text-red font-icon" style="float:right !important;font-size: 20px !important;" onclick="deleteAddSection('gn_info');"><i class="icon-delete"></i></a></h4>
            <div class="editor-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Upload Image</label>
                            <!--<input type="file" class="filestyle"
                                   data-buttonText="Choose an Image" 
                                   name="airport_file_uploader_1" 
                                   onChange="displayImage(this, 'airport', '1')" 
                                   id="airport_file_uploader_1" >-->
                                   <div class="bootstrap-filestyle input-group">
                                <input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="airport_content_images_1" id="airport_content_images_1"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                        for="country_file_uploader_1" class="btn btn-blue "><span
                                            class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('airport_content_images_1');">Choose an
                                            Image</span></label></span></div>
                        </div>
                        <div class="image-info-txt"><?php echo IMAGE_DIM_AIRPORT;?></div>
                        <div class="img-box-outer">
                            <div class="img-box hide" id="preview_airport_content_images_1">
                                <a href="javascript:void(0)" class="remove-img" onclick="deleteImage('airport', '1')">
                                    <i class="icon-delete"></i>
                                </a>
                                <img src="" id="preview_img_airport_content_images_1" name="airport_preview_1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><?php echo PAGE_TITLE_MAIN;?></label>
                            <input type="text" name="airport_title_1" id="airport_title_1" class="form-control" placeholder="<?php echo PAGE_TITLE_MAIN;?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description<span class="re_sign">*</span></label>
                            <div class="airport_content_div_1" id="airport_content_div_1">                
                                <textarea class="editor" id="airport_editor_1" name="airport_editor_1"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About -->
        <div class="content-editor" id="about_info">
            <h4 class="editor-heading">About <a href="javascript:void(0);" class="text-red font-icon" style="float:right !important;font-size: 20px !important;" onclick="deleteAddSection('about_info');"><i class="icon-delete"></i></a></h4>
            <div class="editor-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Upload Image</label>
                            <!--<input type="file" class="filestyle"
                                   data-buttonText="Choose an Image" 
                                   name="airport_file_uploader_2" 
                                   onChange="displayImage(this, 'airport', '2')" 
                                   id="airport_file_uploader_2" >-->
                                   <div class="bootstrap-filestyle input-group">
                                <input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="airport_content_images_2" id="airport_content_images_2"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                        for="country_file_uploader_2" class="btn btn-blue "><span
                                            class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('airport_content_images_2');">Choose an
                                            Image</span></label></span></div>
                        </div>
                        <div class="image-info-txt"><?php echo IMAGE_DIM_AIRPORT;?></div>
                        <div class="img-box-outer">
                            <div class="img-box hide" id="preview_airport_content_images_2">
                                <a href="javascript:void(0)" class="remove-img" onclick="deleteImage('airport', '2')">
                                    <i class="icon-delete"></i>
                                </a>
                                <img src="" id="preview_img_airport_content_images_2" name="airport_preview_2">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><?php echo PAGE_TITLE_MAIN;?></label>
                            <input type="text" name="airport_title_2" id="airport_title_2" class="form-control" placeholder="<?php echo PAGE_TITLE_MAIN;?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description<span class="re_sign">*</span></label>
                            <div class="airport_content_div_2" id="airport_content_div_2">                
                                <textarea class="editor" id="airport_editor_2" name="airport_editor_2"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="airport_section_ids" id="airport_section_ids" value=""/>
        <input type="hidden" name="airport_content_rows" id="airport_content_rows" value=""/>
        <input type="hidden" name="airport_mode" id="airport_mode" value="add">
    </div>

    <div class="action-btns">
        <button type="button" class="btn btn-green" data-toggle="modal" data-target=".new-section-editor" onclick="update_sections_popup('airport')">
            <i class="icon-add-note font-icon"></i> New Section Editor
        </button>
        <button type="button" class="btn btn-blue action-link" type="button" name="airport_save" id="airport_save" value="airport_save" onclick="saveContent('airport')" >
            <i class="icon-save-file-option font-icon"></i> Save
        </button>
    </div>
</div>