<?php

/**
 * Plugin Name: Easy Bus Content Editor
 * Plugin URI: 
 * Description: Easy bus country, City, Airport Content Editor
 * Version: 1.0
 * Author: EMPOWER
 * Author URI: 
 */
$your_db_name = 'wp_db';
define("IMAGE_DIM_COUNTRY","Image dimension must be 1284 x 370 pixel");
define("IMAGE_DIM_CITY","Image dimension must be 800 x 295 pixels");
define("IMAGE_DIM_AIRPORT","Image dimension must be 401*280 pixel");
define("PAGE_TITLE_MAIN","Page teaser text - use some of your target SEO Keywords here");
define("META_TAGS_SECTION_HEADING","Meta tags and page title");
define("MIN_PRICE_SECTION_HEADING","Minimum Price");

//include_once 'save-content.php';
// function to create the DB / Options / Defaults					
function your_plugin_options_install() {
    global $wpdb;
    global $your_db_name;

    // create the ECPT metabox database table
    if ($wpdb->get_var("show tables like '$your_db_name'") != $your_db_name) {
        $sql = "CREATE TABLE content (
		`id` int(11) NOT NULL AUTO_INCREMENT,
                `section_id` int(11) NOT NULL,
		`country` varchar(256) NOT NULL,
		`city` varchar(256)  NULL,
		`airport` varchar(256)  NULL,
                `title` varchar(256)  NULL,
		`description` text NOT NULL,
		UNIQUE KEY id (id)
		);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        $image_sql = "CREATE TABLE `image` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `content_id` int(11) NOT NULL,
                    `image_path` varchar(255) NOT NULL,
                    `created_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                    UNIQUE KEY id (id)
                    )";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($image_sql);
        $sections_sql = "CREATE TABLE `sections` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `section` varchar(255) NOT NULL,
                    `is_default` int(11) NOT NULL,
                    `created_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                    UNIQUE KEY id (id)
                );";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sections_sql);

        $insert_sections = "INSERT INTO `sections` (`id`, `section`, `is_default`, `created_on`) VALUES
                    (1, 'General Information', 1, '2019-11-15 13:25:20'),
                    (2, 'About', 1, '2019-11-18 11:37:41');";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($insert_sections);
    }
}

// run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'your_plugin_options_install');

define('AJAXCONTENT', plugins_url() . "/" . dirname(plugin_basename(__FILE__)));
// Creating a Top-Level Admin Page
add_action('admin_menu', 'spp_admin_menu');

function spp_admin_menu() {
    add_menu_page('Content Editor', 'Content Editor', 'publish_pages', 'content-editor-admin-page', 'contenteditor_admin_page', 'dashicons-clipboard', 9);
}

function ajaxloadpost_enqueuescripts() {
    //if (isset($_GET['page']) && ($_GET['page'] == "add-question.php" || $_GET['page'] == "cpa-questionnaire-admin-page")) {
    //wp_enqueue_script('ajquery-3.2.1', AJAXCONTENT . '/js/jquery-3.2.1.slim.min.js', array('jquery'));
    //wp_enqueue_script('ajaxcustom', AJAXCONTENT . '/js/bootstrap-filestyle.min.js', array('jquery'));
    //wp_enqueue_script('ajaxcustom', AJAXCONTENT . '/js/custom.js', array('jquery'));
//    wp_enqueue_script('bootstrap-js', AJAXCONTENT . '/js/bootstrap.min.js', array('jquery'));
    wp_localize_script('ajaxloadquestion', 'ajaxloadquestionajax', array('ajaxurl' => admin_url('admin-ajax.php')));

//    wp_enqueue_style('bootstrap-css', AJAXCONTENT . '/css/bootstrap.min.css');
//    wp_enqueue_style('default-css', AJAXCONTENT . '/css/style.css');
//    wp_enqueue_script('bootstrap-select-min', plugins_url('/easybus-contenteditor/js/bootstrap-select.min.js'));
//    wp_enqueue_style('bootstrap-select', plugins_url('/easybus-contenteditor/css/bootstrap-select.min.css'));
    //  wp_enqueue_script('summernote-lite-js', plugins_url('/easybus-contenteditor/js/summernote-lite.js'));
    // wp_enqueue_style('summernote-lite-css', plugins_url('/easybus-contenteditor/css/summernote-lite.css'));
    //}
}

add_action('admin_enqueue_scripts', 'ajaxloadpost_enqueuescripts');

function contenteditor_admin_page() {
    require_once('views/add-content-view.php');
}

add_action('wp_ajax_get_easy_bus_countries', 'get_easy_bus_countries');

function get_easy_bus_countries() {
    $api_url = 'https://restcountries.eu/rest/v2/all';
    $response = wp_remote_get($api_url, array(
        // Set the Content-Type header.
        'headers' => array(
            'Content-Type' => 'application/json',
        ),
        // Set the request payload/body.
        'body' => json_encode($post_data),
    ));
    $res_body = wp_remote_retrieve_body($response);
    echo $res_body;
    wp_die();
//******************************************************************************//
}

add_action('wp_ajax_get_easy_bus_cities', 'get_easy_bus_cities');

function get_easy_bus_cities() {
    $directory = $plugin_dir = ABSPATH . 'wp-content/plugins/easyBusLinkViewer/city.json';
    // Define the URL
    $url = $directory;
    $response = wp_remote_get($url, array(
        // Set the Content-Type header.
        'headers' => array(
            'Content-Type' => 'application/json',
        ),
        // Set the request payload/body.
        'body' => json_encode($post_data),
    ));
    $res_body = wp_remote_retrieve_body($response);
    echo $res_body;
    wp_die();
//******************************************************************************//
}

function easy_bus_enqueue() {
    wp_enqueue_script('ajax-script', get_template_directory_uri() . '/js/ajax-script.js', array('jquery'));
    wp_localize_script('ajax-script', 'my_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}

add_action('wp_easy_bus_enqueue_scripts', 'easy_bus_enqueue');
add_action('wp_ajax_save_content', 'save_content');
add_action('wp_ajax_get_country_content', 'get_country_content');
add_action('wp_ajax_get_city_content', 'get_city_content');
add_action('wp_ajax_get_airport_content', 'get_airport_content');

add_action('wp_ajax_get_meta_tags_content', 'get_meta_tags_content');


add_action('wp_ajax_get_sections', 'get_sections');
add_action('wp_ajax_get_all_sections', 'get_all_sections');
add_action('wp_ajax_delete_image', 'delete_image');
add_action('wp_ajax_delete_content', 'delete_content');
add_action('wp_ajax_add_section', 'add_section');


add_action('wp_ajax_get_featured_lists', 'get_featured_lists');
function get_featured_lists(){
  $country=$_POST["country"];
  global $wpdb;
    $query="Select * from city where country_code='".$country."'";
    $result = $wpdb->get_results($query,ARRAY_A);
    $style_class="";
    if(count($result) > 10){
      $style_class="featured_listing_scroll";
    }
    $i=1;
    if(!empty($result)){
      $html="<ul class='featured_listing ".$style_class."'>";
      foreach($result as $val){
        $html.='<li><input type="checkbox" name="featured_city_id" value="'.$val["code"].'" class="featured_listing_bx" id="featured_city_id_'.$i.'">
                                        <span>'.$val["name"].'</span>
                                    </li>';
      $i++;      
      }
      $html.="</ul>";
    }


    $queryRes2 = "SELECT a.* FROM airport as a LEFT JOIN city as c ON c.code=a.city_code LEFT JOIN country as con ON con.iso=c.country_code  WHERE con.iso ='".$country."'";
      $result2 = $wpdb->get_results($queryRes2,ARRAY_A);
      $style_class2="";
      if(count($result2) > 10){
        $style_class2="featured_listing_scroll";
      }
      $j=1;
      if(!empty($result2)){
      $html_airport="<ul class='featured_listing ".$style_class2."'>";
      foreach($result2 as $value){
        $html_airport.='<li><input type="checkbox" name="featured_airport_id" value="'.$value["code"].'" class="featured_listing_bx" id="featured_airport_id_'.$j.'">
                                        <span>'.$value["name"].'</span>
                                    </li>';
      
      

      $j++;
      }
      $html_airport.="</ul>";

      
      }   
      

    $response=array("status_code"=>'1',"city_result"=>@$html,"airport_result"=>@$html_airport,"message"=>"Success");
    echo json_encode($response);
    die;
}





// Landing pages
add_action('wp_ajax_get_landing_pages_content', 'get_landing_pages_content');
function get_landing_pages_content() {
 echo "0";
 wp_die();
}

add_action('wp_ajax_get_landing_pages', 'get_landing_pages');
function get_landing_pages(){
  $city_id=$_POST["city_id"];
  $html="";
  $html.='<form action="" method="POST" name="landing_pages_form" id="landing_pages_form">  
                                  <ul class="landing_pages_listing">
                                    
                                    <li><input type="checkbox" name="landing_pages_id[]" value="1" class="landing_pages_listing_bx">
                                        <span>Landing Page 1</span>
                                    </li>

                                    <li><input type="checkbox" name="landing_pages_id[]" value="2" class="landing_pages_listing_bx">
                                        <span>Landing Page 2</span>
                                    </li>

                                    <li><input type="checkbox" name="landing_pages_id[]" value="3" class="landing_pages_listing_bx">
                                        <span>Landing Page 3</span>
                                    </li>
                                    <li><input type="checkbox" name="landing_pages_id[]" value="4" class="landing_pages_listing_bx">
                                        <span>Landing Page 4</span>
                                    </li>
                                     
                                  </ul>
                                  
                                
                        </form>';  
  echo $html;
  wp_die();  
}

add_action('wp_ajax_landing_pages_save', 'landing_pages_save');
function landing_pages_save(){
   $landing_pages_id=$_POST["landing_pages_id"];
   echo '<pre/>';
   print_r($landing_pages_id);die; 
}

// Popular destinations
add_action('wp_ajax_get_popular_destinations_content', 'get_popular_destinations_content');
function get_popular_destinations_content() {
 echo "0";
 wp_die();
}

add_action('wp_ajax_get_popular_destinations', 'get_popular_destinations');
function get_popular_destinations(){
  $city_id=$_POST["city_id"];

    global $wpdb;
    $query="Select name from city where code='".$city_id."'";
    $result = $wpdb->get_results($query,ARRAY_A);


$html="";
$route_details = file_get_contents("https://bookings.easybus.com/affiliate/api/popular/connected_routes?stationCode=".$city_id);
if(!empty($route_details)){
    $route_details=json_decode($route_details,true);
    if(!empty($route_details)){
          $html="";
          $html.="<div class='pd-heading'>Routes from ".@$result[0]["name"]."</div>";
          $html.='<form action="" method="POST" name="popular_destinations_form" id="popular_destinations_form">  
                                          <ul class="landing_pages_listing" id="foo">';
                foreach($route_details as $value){
                    $sel="";
                    if(!empty($value["IsImportant"])){
                        if($value["IsImportant"]=="true"){
                            $sel="checked";
                        }else{
                            $sel=""; 
                        }
                    }    
          $html.=' <li><input type="checkbox" data-id="'.$value["Id"].'" name="popular_destinations_id['.$value["DestinationName"].']" value="'.$value["OriginCode"].'" class="landing_pages_listing_bx popuplar_checkbox" '.$sel.'>
                                        <span>'.$value["DestinationName"].'</span>
                                    </li>';         
                }
          $html.='</ul>';
          $html.='<input type="hidden" name="city_name" value="'.@$result[0]["name"].'"/>';
          $html.='<input type="hidden" name="city_code" value="'.$city_id.'"/>';
          $html.='</form>';
          $html.="<script>
  Sortable.create(foo, {
  group: 'foo',
  animation: 100
});


</script>";      
    }
}else{
    echo "0";
    wp_die();
}
  echo $html;
  wp_die();  
}

add_action('wp_ajax_popular_destinations_save', 'popular_destinations_save');
function popular_destinations_save(){
   $popular_destinations_id=$_POST["popular_destinations_id"];
   echo '<pre/>';
   print_r($popular_destinations_id);die;
   if(!empty($popular_destinations_id)){
     foreach($popular_destinations_id as $key=>$val){
       $value["code"]=$val;
       $value["name"]=$key;
       $rows[]=$value;
     }
     $json["destination"]=$rows;
   }

   $city_code=$_POST["city_code"];
   $city_name=$_POST["city_name"];

   $json_response=array();
   $json["origin"]=array("code"=>$city_code,"name"=>$city_name);
   $json_response=json_encode($json,true);


    $url = 'http://localhost:8080/test/index.php';
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);

    //curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $json_response);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute post
    $result = curl_exec($ch);
    //close connection
    curl_close($ch);

    echo "<div class='alert alert-success alert-dismissible'>Saved Successfully</div>";
    wp_die();
}

// New Routes

add_action('wp_ajax_new_routes_upload', 'new_routes_upload');
function new_routes_upload() {
 $filename=$_FILES["new_routes_file"]["tmp_name"];
 if(!empty($filename)){
     $ext=pathinfo($_FILES["new_routes_file"]["name"],PATHINFO_EXTENSION);
     if($ext=="csv"){
        if($_FILES["new_routes_file"]["size"] > 0){
            $file = fopen($filename, "r");
            $data = file_get_contents($filename);
            $rows = explode("\n",$data);

           // $rows = array_map("utf8_encode", $rows);
            $i=0;
            $excel_columns=array();
            $result_rows=array();
            foreach($rows as $val) {

              if(!empty($val)){
                if($i > 0){
                  //$val=utf8_encode($val);  
                  $excel=explode(",",$val);
                  if(!empty($excel)){
                      $excel_columns["distribution_origin"]=@$excel[0];
                      $excel_columns["origin_station"]=@$excel[1];
                      $excel_columns["destination_station"]=@$excel[2];
                      $excel_columns["is_origin_same"]=(int)@$excel[3];
                      $excel_columns["is_destination_same"]=(int)@$excel[4];
                      $excel_columns["generated_link"]=@$excel[5];
                      $excel_columns["crawled_source_link"]=@$excel[6];
                      $excel_columns["is_route_exist"]=(int)@$excel[7];
                      $excel_columns["FromLocationCode"]=@$excel[8];
                      $excel_columns["ToLocationCode"]=@$excel[9];
                      $excel_columns["isImportant"]=(int)@$excel[10];
                      if($excel_columns["isImportant"]==0){
                        $excel_columns["sort_by"]=0;
                      }else{
                        $excel_columns["sort_by"]=1;
                      }
                      $result_rows[]=$excel_columns;
                  }
                }
              }
              $i++;  
            }
            echo json_encode($result_rows);die;
            //print_r($result_rows);die;
        } 
     }else{
        echo "1";die;
     }
 }else{
    echo "0";die;
 }

 wp_die();
}

add_action('wp_ajax_new_routes_saved', 'new_routes_saved');
function new_routes_saved(){
  $origin_station=$_POST["origin_station"];
  if(empty(array_filter($origin_station))){
    echo "0";
    wp_die();
  }
  $columns=array();
  $result_rows=array();
  global $wpdb;
  for($i=0;$i<count($origin_station);$i++){

    
    $query="Select name from city where code='".$_POST["destination_station"][$i]."'";
    $result = $wpdb->get_results($query,ARRAY_A);
    $city=str_replace(" ","-",strtolower($result[0]["name"])); 
    
    $query2="Select nicename from country where iso='".$_POST["origin_station"][$i]."'";
    $result2 = $wpdb->get_results($query2,ARRAY_A);
    $country=str_replace(" ","-",strtolower($result2[0]["nicename"])); 

    
    $columns["origin_station"]=@$result2[0]["nicename"];
    $columns["distribution_origin"]=@$result2[0]["nicename"];
    $columns["destination_station"]=@$result[0]["name"];
    $columns["is_origin_same"]=1;
    $columns["is_destination_same"]=1;
    $columns["generated_link"]=utf8_encode("bus-from-".$country."-to-".$city);
    $columns["crawled_source_link"]="";
    $columns["is_route_exist"]="1";
    $columns["FromLocationCode"]=@$_POST["origin_station"][$i];
    $columns["ToLocationCode"]=@$_POST["destination_station"][$i];
    
    if(!empty(@$_POST["isImportant"][$i])){
      $columns["isImportant"]=1;   
      $columns["sort_by"]=1;
    }else{
      $columns["isImportant"]=0;   
      $columns["sort_by"]=0;
    }
    $result_rows[]=$columns;
  }
          /*echo '<pre/>';
        print_r($result_rows);die;*/
        echo json_encode($result_rows);die;
        die;   
}

add_action('wp_ajax_get_bus_from_route','get_bus_from_route');
function get_bus_from_route(){
    $country=$_POST["country"];
    $city=$_POST["city"];
    global $wpdb;
    $query="Select name from city where code='".$city."'";
    $result = $wpdb->get_results($query,ARRAY_A);
    $city=str_replace(" ","-",strtolower($result[0]["name"])); 

    $query2="Select nicename from country where iso='".$country."'";
    $result2 = $wpdb->get_results($query2,ARRAY_A);
    $country=str_replace(" ","-",strtolower($result2[0]["nicename"])); 
    
    echo utf8_encode("bus-from-".$country."-to-".$city);
    wp_die();

}

add_action('wp_ajax_delete_section_content','delete_section_content');
function delete_section_content(){
    $id=$_POST["id"];
    global $wpdb;
    $success = $wpdb->delete('content', array('id' => $_POST['id']));
    echo "1";
    wp_die();   
}

add_action('wp_ajax_get_city_data','get_city_data');
function get_city_data(){
    $country_code=$_POST["id"];
    global $wpdb;
    $query="Select*from city where country_code='".$country_code."'";
    $result = $wpdb->get_results($query,ARRAY_A);
    $html="";
    if(!empty($result)){
       foreach($result as $value){
         $html.="<option value='".$value["code"]."'>".$value["name"]."</option>";
       } 
    }else{
        $html.="<option value=''>No Record found</option>";
    }
    echo $html;
    wp_die();
}
add_action('wp_ajax_get_airport_data','get_airport_data');
function get_airport_data(){
    $city_code=$_POST["id"];
    global $wpdb;
    $query="Select*from airport where city_code='".$city_code."'";
    $result = $wpdb->get_results($query,ARRAY_A);
    $html="";
    if(!empty($result)){
       foreach($result as $value){
         $html.="<option value='".$value["code"]."'>".$value["name"]."</option>";
       } 
    }else{
        $html.="<option value=''>No Record found</option>";
    }
    echo $html;
    wp_die();
}
add_action('wp_ajax_get_country_full_name', 'get_country_full_name');
function get_country_full_name(){
    $country=$_POST["country"];
    global $wpdb;
    $query="Select*from country where iso='".$country."'";
    $result = $wpdb->get_results($query,ARRAY_A);
    foreach ($result as $value) {
        if ($country === $value["iso"]) {
            echo @$value["nicename"];  
        }
    }
     
}

function add_section() {
    global $wpdb;
    $section_name=trim($_POST["section_name"]);
    $section = $wpdb->get_results("SELECT id FROM sections WHERE section='".$section_name."'");
    if(!empty($section)){
      echo 0;
      wp_die();   
    }

    if (isset($_POST['action']) && isset($_POST['section_name'])) {
        $result = $wpdb->insert("sections", array(
            "section" => $_POST['section_name']
        ));
        echo $wpdb->insert_id;
        wp_die();
    }
    echo 0;
    wp_die();
}

function delete_content() {
    global $wpdb;
    $success = 0;
    if (isset($_POST['action']) && $_POST['type'] != '' && $_POST['id'] != '') {
        $type = $_POST['type'];
        $id = $_POST['id'];

        if ($type === 'country') {
            $query = "SELECT * FROM content WHERE country = '" . $id . "' AND city = '' AND airport = ''";
            $string = $wpdb->get_results($query);
            foreach ($string as $s) {
                $cid = $s->id;
                $query = "SELECT * FROM image WHERE content_id = $cid";
                $string = $wpdb->get_results($query);
                foreach ($string as $s) {
                    $path = $s->image_path;
                }
//                $wpdb->delete('image', array('id' => $_POST['image_id']));
                /*$location = ABSPATH . 'wp-content/plugins/easybus-contenteditor/' . $path;
                if (file_exists($location)) {
                    if (is_file($location))// Delete the given file 
                        unlink($location);
                }*/
                $wpdb->delete('image', array('content_id' => $cid));
            }
            $success = $wpdb->delete('content', array('country' => $_POST['id'], 'city' => '', 'airport' => ''));
        }
        if ($type === 'city') {
            $query = "SELECT * FROM content WHERE 'city' = '" . $id . "' AND airport = ''";
            $string = $wpdb->get_results($query);
            foreach ($string as $s) {
                $cid = $s->id;
                $query = "SELECT * FROM image WHERE content_id = $cid";
                $string = $wpdb->get_results($query);
                foreach ($string as $s) {
                    $path = $s->image_path;
                }
//                $wpdb->delete('image', array('id' => $_POST['image_id']));
                /*$location = ABSPATH . 'wp-content/plugins/easybus-contenteditor/' . $path;
                if (file_exists($location)) {
                    if (is_file($location))// Delete the given file 
                        unlink($location);
                }*/
                $wpdb->delete('image', array('content_id' => $cid));
            }
            $success = $wpdb->delete('content', array('city' => $_POST['id'], 'airport' => ''));
        }
        if ($type === 'airport') {
            $query = "SELECT * FROM content WHERE 'airport' = '" . $id . "'";
            $string = $wpdb->get_results($query);
            foreach ($string as $s) {
                $cid = $s->id;
                $query = "SELECT * FROM image WHERE content_id = $cid";
                $string = $wpdb->get_results($query);
                foreach ($string as $s) {
                    $path = $s->image_path;
                }
//                $wpdb->delete('image', array('id' => $_POST['image_id']));
                /*$location = ABSPATH . 'wp-content/plugins/easybus-contenteditor/' . $path;
                if (file_exists($location)) {
                    if (is_file($location))// Delete the given file 
                        unlink($location);
                }*/
                $wpdb->delete('image', array('content_id' => $cid));
            }
            $success = $wpdb->delete('content', array('airport' => $_POST['id']));
        }
    }
    echo $success;

    wp_die();
}

function delete_image() {
    global $wpdb;
    if (isset($_POST['action']) && $_POST['image_id'] > 0 && $_POST['content_id'] > 0) {
        $id = $_POST['image_id'];
        $query = "SELECT * FROM image WHERE id = $id";
        $string = $wpdb->get_results($query);
        foreach ($string as $s) {
            $path = $s->image_path;
        }
        $wpdb->delete('image', array('id' => $_POST['image_id']));
        //$location = ABSPATH . 'wp-content/plugins/easybus-contenteditor/' . $path;

        /*if (file_exists($location)) {
            if (is_file($location))// Delete the given file 
                unlink($location);
        }*/
        echo 1;
    }
    wp_die();
}

function get_all_sections() {

    global $wpdb;
    //*********************** AIRPORT AREA ***********************//
    if (isset($_POST['action']) && isset($_POST['country_code']) && $_POST['country_code'] != '' && isset($_POST['city_code']) && $_POST['city_code'] != '' && isset($_POST['airport_code']) && $_POST['airport_code'] != '') {
        $query = "SELECT c.*,c.id as cid,s.section,s.id as sid,i.image_path,i.id as image_id FROM content AS c"
                . " LEFT JOIN sections AS s ON s.id = c.section_id"
                . " LEFT JOIN image AS i ON i.content_id = c.id"
                . " WHERE country = '" . $_POST['country_code'] . "' AND city = '" . $_POST['city_code'] . "' AND airport = '" . $_POST['airport_code'] . "' ORDER BY c.id ASC";
        $string = $wpdb->get_results($query);
        $heading = "";
        $heading1 = "";
        $heading2 = "";
        $html = "";
        $sids = "";
        $cids = "";
        foreach ($string as $single) {
            if ($heading === "" && $heading1 === "" && $heading2 === "") {
                /*if ($single->country === "IND") {
                    $heading = "India";
                } elseif ($single->country === "AUS") {
                    $heading = "Australia";
                } elseif ($single->country === "BRZ") {
                    $heading = "Brazil";
                }
                elseif ($single->country === "france") {
                    $heading = "France";
                }
//                $heading1 = $single->city;
                $heading1 = change_city_name($single->city);
                $heading1 = ($heading1 != '' ) ? $heading1 : $single->city;
                $heading2 = change_airport_name($single->airport);
                $heading2 = ($heading2 != '' ) ? $heading2 : $single->airport;*/

                $query = "SELECT * FROM country WHERE iso ='".$single->country."'";
                $res = $wpdb->get_results($query);
                $heading=@$res[0]->nicename;

                $query2 = "SELECT * FROM city WHERE code ='".$single->city."'";
                $res2 = $wpdb->get_results($query2);
                $heading1=@$res2[0]->name;

                $query3 = "SELECT * FROM airport WHERE code ='".$single->airport."'";
                $res3 = $wpdb->get_results($query3);
                $heading2=@$res3[0]->name;


                $html .= '<div class="inner-container">
                            <div  id="country_editor_section">';
                $html .= '<div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <h4 class="editor-heading">' . $heading . '</h4>
                                        <input type="hidden" name="airport_country_ddl" id="airport_country_ddl" value="' . $single->country . '"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <h4 class="editor-heading">' . $heading1 . '</h4>
                                        <input type="hidden" name="airport_city_ddl" id="airport_city_ddl" value="' . $single->city . '"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <h4 class="editor-heading">' . $heading2 . '</h4>
                                        <input type="hidden" name="airport_ddl" id="airport_ddl" value="' . $single->airport . '"/>
                                </div>
                            </div>
                        </div>';

        $sql="SELECT * FROM airport_minimum_price WHERE airport='".$_POST['airport_code']."'";
        $res = $wpdb->get_results($sql,ARRAY_A);
        $min_price=@$res[0]["min_price"];
        $html.='<div class="inner-container">
            <div id="min_price_editor_section">
                <div class="content-editor">
                  <h4 class="editor-heading">'.MIN_PRICE_SECTION_HEADING.'</h4>
                  <div class="editor-body">
                    <div class="row">
                       <div class="col-md-12">
                         <div class="form-group">
                            <label class="control-label">Min. Price</label>
                            <input type="text" class="form-control" name="min_price_airport" id="min_price_airport" value="'.$min_price.'">
                          </div>
                         </div>
                         </div>
                         
                  </div>
                </div>
            </div>
        </div>';


            }

            $html .= '<div class="content-editor" id="cont_id_'.$single->cid.'">
                        <h4 class="editor-heading">' . $single->section . '<a href="javascript:void(0);" class="text-red font-icon" style="float:right !important;font-size: 20px !important;" onclick="deleteSection('.$single->cid.');"><i class="icon-delete"></i></a></h4>
                           <div class="editor-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"><label class="control-label">Upload Image</label>';
            

$image_id="'"."airport_content_images_".$single->sid."'";

                    $html.='<div class="bootstrap-filestyle input-group"><input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="airport_content_images_' . $single->sid . '" id="airport_content_images_' . $single->sid . '" value="'. $single->image_path . '"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                         class="btn btn-blue "><span
    class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('.$image_id.')">Choose an
                                            Image</span></label></span></div>';
            $html.="</div>";
            $html.="<div class='image-info-txt'>".IMAGE_DIM_AIRPORT."</div>"; 
            $class = $single->image_path ? "" : 'hide';
            $html .= '<div class="img-box-outer">
                 <input type="hidden" name="image_id[]" id=airport_image_id_' . $single->sid . ' value="' . $single->image_id . ',' . $single->cid . '"/>
                        <div class="img-box ' . $class . '" id="preview_airport_content_images_' . $single->sid . '">
                            <a href="javascript:void(0)" class="remove-img" onclick="deleteImage(' . "'airport'," . $single->sid . ')">
                                <i class="icon-delete"></i>
                            </a>
                            <img class="image_preview" src="'. $single->image_path . '" id="preview_img_airport_content_images_' . $single->sid . '" name="preview_img_airport_content_images_' . $single->sid . '">
                        </div>
                    </div>
                </div>';
            $html .= '<div class="col-md-12"><div class="form-group">
                        <label>'.PAGE_TITLE_MAIN.'</label>
                        <input type="text" name="airport_title_' . $single->sid . '" id="airport_title_' . $single->sid . '" class="form-control" placeholder="'.PAGE_TITLE_MAIN.'" value="' . $single->title . '">
                    </div>
                    </div>';
            $html .= '<div class="col-md-12"><div class="form-group">
                        <label>Description<span class="re_sign">*</span></label>
                        <div class="airport_content_div_' . $single->sid . '" id="airport_content_div_' . $single->sid . '">
                        <textarea class="editor" id="airport_editor_' . $single->sid . '" name="airport_editor_' . $single->sid . '">' . $single->description . '</textarea>
                      </div></div></div></div></div></div>';
            if ($sids === "") {
                $sids = $single->sid;
            } else {
                $sids = $sids . "," . $single->sid;
            }
            if ($cids === "") {
                $cids = $single->id;
            } else {
                $cids = $cids . "," . $single->id;
            }
        }
        $html .= '<input type="hidden" name="airport_section_ids" id="airport_section_ids" value="' . $sids . '"/>
                      <input type="hidden" name="airport_content_rows" id="airport_content_rows" value="' . $cids . '"/>';
        $html .= '<div class="action-btns">' .
                '<button type="button" class="btn btn-green" data-toggle="modal" data-target=".new-section-editor" onclick="update_sections_popup(' . "'airport'" . ')">
                    <i class="icon-add-note font-icon"></i> New Section Editor</button>
                <button type="button" class="btn btn-blue action-link" type="button" name="airport_save" id="airport_save" value="airport_update" onclick="saveContent(' . "'airport'" . ')" >
                    <i class="icon-save-file-option font-icon"></i> Save
                </button></div></div>';
        echo $html; //json_encode($string);
        wp_die();
    }
    //*********************** CITY AREA ***********************//
    elseif (isset($_POST['action']) && isset($_POST['country_code']) && $_POST['country_code'] != '' && isset($_POST['city_code']) && $_POST['city_code'] != '') {
        $query = "SELECT c.*,c.id as cid,s.section,s.id as sid,i.image_path,i.id as image_id FROM content AS c"
                . " LEFT JOIN sections AS s ON s.id = c.section_id"
                . " LEFT JOIN image AS i ON i.content_id = c.id"
                . " WHERE country = '" . $_POST['country_code'] . "' AND city = '" . $_POST['city_code'] . "' AND airport = '' ORDER BY c.id ASC";
        $string = $wpdb->get_results($query);

        $sql="SELECT * FROM meta_tags WHERE city='".$_POST['city_code']."' AND type='city'";
        $res = $wpdb->get_results($sql,ARRAY_A);
        $meta_title=@$res[0]["meta_title"];
        $meta_keywords=@$res[0]["meta_keywords"];
        $meta_description=@$res[0]["meta_description"];


        $heading = "";
        $heading1 = "";
        $html = "";
        $sids = "";
        $cids = "";
        foreach ($string as $single) {
            if ($heading === "" && $heading1 === "") {
                $heading = $single->country;
                /*if ($single->country === "IND") {
                    $heading = "India";
                } elseif ($single->country === "AUS") {
                    $heading = "Australia";
                } elseif ($single->country === "BRZ") {
                    $heading = "Brazil";
                }*/
                $query = "SELECT * FROM country WHERE iso ='".$single->country."'";
                $res = $wpdb->get_results($query);
                $heading=@$res[0]->nicename;

                $query2 = "SELECT * FROM city WHERE code ='".$single->city."'";
                $res2 = $wpdb->get_results($query2);
                $heading1=@$res2[0]->name;

                /*$heading1 = $single->city;
                $heading1 = change_city_name($single->city);
                $heading1 = ($heading1 != '' ) ? $heading1 : $single->city;*/
                $html .= '<div class="inner-container">
                            <div  id="country_editor_section">';
                $html .= '
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <h4 class="editor-heading">' . $heading . '</h4>
                                        <input type="hidden" name="city_country_ddl" id="city_country_ddl" value="' . $single->country . '"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <h4 class="editor-heading">' . $heading1 . '</h4>
                                        <input type="hidden" name="city_ddl" id="city_ddl" value="' . $single->city . '"/>
                                </div>
                            </div>
                        </div>';
            

            $html.='<div class="inner-container">
    <div id="meta_tags_editor_section">
        <div class="content-editor">
          <h4 class="editor-heading">'.META_TAGS_SECTION_HEADING.'</h4>
          <div class="editor-body">
          <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Page Title</label>
                    <input type="text" class="form-control" name="meta_title" id="meta_title_city" value="'.@$meta_title.'">
                  </div>
                 </div>
                 </div>
             <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Keywords</label>
                    
                    <input type="text" class="form-control" name="meta_keywords" id="meta_keywords_city" value="'.@$meta_keywords.'">
                  </div>
                 </div>
                 </div>
                 <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Description</label>
                    <textarea class="form-control" name="meta_description" id="meta_description_city" style="resize: none;">'.@$meta_description.'</textarea>
                  </div>
                 </div>
                 </div>
                 
          </div>
        </div>
    </div>
</div>';
}
            $html .= '<div class="content-editor" id="cont_id_'.$single->cid.'">
                        <h4 class="editor-heading">' . $single->section . ' <a href="javascript:void(0);" class="text-red font-icon" style="float:right !important;font-size: 20px !important;" onclick="deleteSection('.$single->cid.');"><i class="icon-delete"></i></a></h4>
                           <div class="editor-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"><label class="control-label">Upload Image</label>';
            

            $image_id="'"."city_content_images_".$single->sid."'";

                    $html.='<div class="bootstrap-filestyle input-group"><input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="city_content_images_' . $single->sid . '" id="city_content_images_' . $single->sid . '" value="'. $single->image_path . '"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                         class="btn btn-blue "><span
    class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('.$image_id.')">Choose an
                                            Image</span></label></span></div>';
            $html.="</div>";
            $html.="<div class='image-info-txt'>".IMAGE_DIM_CITY."</div>";

            $class = $single->image_path ? '' : 'hide';
            $html .= '<div class="img-box-outer">
                     <input type="hidden" name="image_id[]" id=city_image_id_' . $single->sid . ' value="' . $single->image_id . ',' . $single->cid . '"/>
                        <div class="img-box ' . $class . '" id="preview_city_content_images_' . $single->sid . '">
                            <a href="javascript:void(0)" class="remove-img" onclick="deleteImage(' . "'city'," . $single->sid . ')">
                                <i class="icon-delete"></i>
                            </a>
                            <img class="image_preview" src="'. $single->image_path . '" id="preview_img_city_content_images_' . $single->sid . '" name="preview_img_city_content_images_' . $single->sid . '">
                        </div>
                    </div>
                </div>';
            $html .= '<div class="col-md-12"><div class="form-group">
                        <label>'.PAGE_TITLE_MAIN.'</label>
                        <input type="text" name="city_title_' . $single->sid . '" id="city_title_' . $single->sid . '" class="form-control" placeholder="'.PAGE_TITLE_MAIN.'" value="' . $single->title . '">
                    </div>
                    </div>';
            $html .= '<div class="col-md-12"><div class="form-group">
                        <label>Description<span class="re_sign">*</span></label>
                        <div class="city_content_div_' . $single->sid . '" id="city_content_div_' . $single->sid . '">
                        <textarea class="editor" id="city_editor_' . $single->sid . '" name="city_editor_' . $single->sid . '">' . $single->description . '</textarea>
                      </div>
                                </div>
                            </div>
                      </div></div></div>';
            if ($sids === "") {
                $sids = $single->sid;
            } else {
                $sids = $sids . "," . $single->sid;
            }
            if ($cids === "") {
                $cids = $single->id;
            } else {
                $cids = $cids . "," . $single->id;
            }
        }
        $html .= '<input type="hidden" name="city_section_ids" id="city_section_ids" value="' . $sids . '"/>
                      <input type="hidden" name="city_content_rows" id="city_content_rows" value="' . $cids . '"/>';
        $html .= '<div class="action-btns">' .
                '<button type="button" class="btn btn-green" data-toggle="modal" data-target=".new-section-editor" onclick="update_sections_popup(' . "'city'" . ')">
                    <i class="icon-add-note font-icon"></i> New Section Editor</button>
                <button type="button" class="btn btn-blue action-link" type="button" name="city_save" id="city_save" value="city_update" onclick="saveContent(' . "'city'" . ')" >
                    <i class="icon-save-file-option font-icon"></i> Save
                </button></div></div>';
        echo $html; //json_encode($string);
        wp_die();
    }
    //*********************** COUNTRY AREA ***********************//
    elseif (isset($_POST['action']) && isset($_POST['country_code'])) {
        $query = "SELECT c.*,c.id as cid,s.section,s.id as sid,i.image_path, i.id as image_id FROM content AS c"
                . " LEFT JOIN sections AS s ON s.id = c.section_id"
                . " LEFT JOIN image AS i ON i.content_id = c.id"
                . " WHERE country = '" . $_POST['country_code'] . "' AND city = '' ORDER BY c.id ASC ";
        $string = $wpdb->get_results($query);

        $sql="SELECT * FROM meta_tags WHERE country='".$_POST['country_code']."' AND type='country'";
        $res = $wpdb->get_results($sql,ARRAY_A);
        $meta_title=@$res[0]["meta_title"];
        $meta_keywords=@$res[0]["meta_keywords"];
        $meta_description=@$res[0]["meta_description"];

        $heading = "";
        $html = "";
        $sids = "";
        $cids = "";
        foreach ($string as $single) {
            if ($heading === "") {
                $heading = $single->country;
                /*if ($single->country === "IND") {
                    $heading = "India";
                } elseif ($single->country === "AUS") {
                    $heading = "Australia";
                } elseif ($single->country === "BRZ") {
                    $heading = "Brazil";
                }*/
                $query = "SELECT * FROM country WHERE iso ='".$single->country."'";
                $res = $wpdb->get_results($query);
                $heading=@$res[0]->nicename;
                $html .= '<div class="inner-container">
                            <div  id="country_editor_section">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h4 class="editor-heading">' . $heading . ' </h4>
                                            <input type="hidden" name="country_ddl" id="country_ddl" value="' . $single->country . '"/>
                                        </div>
                                    </div>
                                </div>';
            
             

            $html.='<div class="inner-container">
    <div id="meta_tags_editor_section">
        <div class="content-editor">
          <h4 class="editor-heading">'.META_TAGS_SECTION_HEADING.'</h4>
          <div class="editor-body">
          <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Page Title</label>
                    <input type="text" class="form-control" name="meta_title" id="meta_title_country" value="'.@$meta_title.'">
                  </div>
                 </div>
                 </div>
             <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Keywords</label>
                    <input type="text" class="form-control" name="meta_keywords" id="meta_keywords_country" value="'.@$meta_keywords.'">
                  </div>
                 </div>
                 </div>
                 <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                    <label class="control-label">Description</label>
                    <textarea class="form-control" name="meta_description" id="meta_description_country" style="resize: none;">'.@$meta_description.'</textarea>
                  </div>
                 </div>
                 </div>
                 
          </div>
        </div>
    </div>
</div>';
}

            $class = $single->image_path ? "" : 'hide';
            $html .= '<div class="content-editor" id="cont_id_'.$single->cid.'"><h4 class="editor-heading">' . $single->section . '<a href="javascript:void(0);" class="text-red font-icon" style="float:right !important;font-size: 20px !important;" onclick="deleteSection('.$single->cid.');"><i class="icon-delete"></i></a></h4>

                           <div class="editor-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Upload Image</label>';
                    
                    $image_id="'"."country_content_images_".$single->sid."'";

                    $html.='<div class="bootstrap-filestyle input-group"><input type="text" class="form-control"
                                    placeholder="Upload Image" disabled="" name="country_content_images_' . $single->sid . '" id="country_content_images_' . $single->sid . '" value="'. $single->image_path . '"> <span
                                    class="group-span-filestyle input-group-btn" tabindex="0"><label
                                         class="btn btn-blue "><span
    class="icon-span-filestyle "></span> <span class="buttonText" onclick="upload_image_btn('.$image_id.')">Choose an
                                            Image</span></label></span></div>';





                    $html.='</div><div class="image-info-txt">'.IMAGE_DIM_COUNTRY.'</div>' .
                    '<div class="img-box-outer">                
                                            <input type="hidden" name="image_id[]" id=country_image_id_' . $single->sid . ' value="' . $single->image_id . ',' . $single->cid . '"/>
                                            <div class="img-box ' . $class . '" id="preview_country_content_images_' . $single->sid . '">
                                                <a href="javascript:void(0)" class="remove-img" onclick="deleteImage(' . "'country'," . $single->sid . ')">
                                                    <i class="icon-delete"></i>
                                                </a>
                                                <img class="image_preview" src="' .$single->image_path . '" id="preview_img_country_content_images_' . $single->sid . '" name="preview_img_country_content_images_' . $single->sid . '">
                                            </div>
                                        </div>
                                    </div>' .
                    '<div class="col-md-12"><div class="form-group">
                                        <label>'.PAGE_TITLE_MAIN.'</label>
                                        <input type="text" name="country_title_' . $single->sid . '" id="country_title_' . $single->sid . '" class="form-control" placeholder="'.PAGE_TITLE_MAIN.'" value="' . $single->title . '">
                                    </div>
                                </div>' .
                    '<div class="col-md-12"><div class="form-group">
                                    <label>Description<span class="re_sign">*</span></label>
                                    <div class="country_content_div_' . $single->sid . '" id="country_content_div_' . $single->sid . '">
                                        <textarea class="editor" id="country_editor_' . $single->sid . '" name="country_editor_' . $single->sid . '">' . $single->description . '</textarea>
                                    </div>
                                </div>
                            </div>
                      </div></div></div>';
            if ($sids === "") {
                $sids = $single->sid;
            } else {
                $sids = $sids . "," . $single->sid;
            }
            if ($cids === "") {
                $cids = $single->id;
            } else {
                $cids = $cids . "," . $single->id;
            }
        }


        $country=$_POST["country_code"];

        $queryF="Select featured_city from featured_cities where country_code='".$country."'";
        $resultF = $wpdb->get_results($queryF,ARRAY_A);
        if(!empty($resultF)){
          foreach($resultF as $val){
            $featured_cities[]=$val["featured_city"];
          }
        }

        $queryFA="Select featured_airport from featured_airport where country_code='".$country."'";
        $resultFA = $wpdb->get_results($queryFA,ARRAY_A);
        if(!empty($resultFA)){
          foreach($resultFA as $val){
            $featured_airport[]=$val["featured_airport"];
          }
        }

  
    $query="Select * from city where country_code='".$country."' ORDER BY sort_order=0,sort_order";
    $result = $wpdb->get_results($query,ARRAY_A);
    $style_class="";
    if(count($result) > 10){
      $style_class="featured_listing_scroll";
    }
    $i=1;
    if(!empty($result)){
      $html_city="<ul class='featured_listing ".$style_class."'>";
      foreach($result as $val){
        if(!empty($featured_cities)){

           $sel="";
          if(in_array($val["code"], $featured_cities)){
            $sel="checked";
          }else{
            $sel="";
          }
        }else{
           $sel="";
        }
        $html_city.='<li><input type="checkbox" name="featured_city_id" value="'.$val["code"].'" class="featured_listing_bx" id="featured_city_id_'.$i.'" '.$sel.'>
                                        <span>'.$val["name"].'</span>
                                    </li>';
      $i++;      
      }
      $html_city.="</ul>";
    }else{
      $html_city='<div class="alert alert-danger">No Records Found</div>';
    }


    $queryRes2 = "SELECT a.* FROM airport as a LEFT JOIN city as c ON c.code=a.city_code LEFT JOIN country as con ON con.iso=c.country_code  WHERE con.iso ='".$country."' ORDER BY a.sort_order=0,a.sort_order";
      $result2 = $wpdb->get_results($queryRes2,ARRAY_A);
      if(count($result2) > 10){
        $style_class2="featured_listing_scroll";
      }
      $j=1;
      if(!empty($result2)){
      $html_airport="<ul class='featured_listing ".$style_class2."'>";
      foreach($result2 as $value){
        if(!empty($featured_airport)){

           $sel="";
          if(in_array($value["code"], $featured_airport)){
            $sel="checked";
          }else{
            $sel="";
          }
        }else{
           $sel="";
        }
        $html_airport.='<li><input type="checkbox" name="featured_airport_id" value="'.$value["code"].'" class="featured_listing_bx" id="featured_airport_id_'.$j.'" '.$sel.'>
                                        <span>'.$value["name"].'</span>
                                    </li>';
      
      

      $j++;
      }
      $html_airport.="</ul>";

      
      }else{
         $html_airport='<div class="alert alert-danger">No Records Found</div>';
      }

        $html.='<div class="inner-container">
    <div id="featured_editor_section">
        <div class="content-editor">
          <h4 class="editor-heading">Featured Cities and Airport</h4>
          <div class="editor-body">
            <div class="row">
               <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Cities</label>
                    <div class="featured_cities">
                      '.$html_city.'
                    </div>
                  </div>
                 </div>
                 
               <div class="col-md-6">
                 <div class="form-group">
                    <label class="control-label">Airports</label>
                    <div class="featured_airports">
                      '.$html_airport.'
                    </div>
                  </div>
                 </div>
                 </div>
             
                 
                 
          </div>
        </div>
    </div>
</div>';


        $html .= '<input type="hidden" name="section_ids" id="section_ids" value="' . $sids . '"/>
                      <input type="hidden" name="content_rows" id="content_rows" value="' . $cids . '"/><div>';
        $html .= '<div class="action-btns">' .
                '<button type="button" class="btn btn-green" data-toggle="modal" data-target=".new-section-editor" onclick="update_sections_popup(' . "'country'" . ')">
                    <i class="icon-add-note font-icon"></i> New Section Editor</button>
                <button type="button" class="btn btn-blue action-link" type="button" name="country_save" id="country_save" value="country_update" onclick="saveContent(' . "'country'" . ')" >
                    <i class="icon-save-file-option font-icon"></i> Save
                </button></div></div>';
        echo $html; //json_encode($string);
        wp_die();
    }
//******************************************************************************//
}

function get_sections() {
    global $wpdb;
    if (isset($_POST['action'])) {

        $query = "SELECT * FROM sections";
//        $query .= $_POST['data_for'] ? " WHERE is_default != 1" : "";
        $string = $wpdb->get_results($query);
        echo json_encode($string);
    }
    wp_die();
}

function get_country_content() {
    global $wpdb;
    if (isset($_POST['action'])) {
        if ($_POST['country_ddl']) {
            $query = "SELECT * FROM content  WHERE country = '" . $_POST['country_ddl'] . "' AND city = ''";
            $string = $wpdb->get_var($query);
            echo $string;
        } else {
            $query = "SELECT * FROM content WHERE city = '' AND airport = '' GROUP BY country ORDER BY `id` DESC";
            $string = $wpdb->get_results($query);
            if(!empty($string)){
               foreach($string as $val){
                    $query = "SELECT * FROM country WHERE iso ='".$val->country."'";
                    $res = $wpdb->get_results($query);
                    $val->display_country=@$res[0]->nicename;
                    $val->display_country=str_replace(" ", "-", $val->display_country);
                    $country_url=get_site_url()."/en/routes/transfers/".strtolower($val->display_country).'/';
                    $val->country_url='<a href="'.$country_url.'" target="_blank"><span class="exter_link">'.$val->display_country.'</span>&nbsp;&nbsp;<img src="../wp-content/plugins/easybus-contenteditor/css/external.png" style="margin-top: -5px;"/></a>';

                    $description=strip_tags($val->description);
                    $val->description=substr($description, 0, 150);

                    $rows[]=$val;
               } 
               echo json_encode($rows);
               wp_die();
            }else{
                echo 0;
            }
        }
        wp_die();
    }
//******************************************************************************//
}

function get_city_content() {


    global $wpdb;
    if (isset($_POST['action'])) {
        if ($_POST['country_ddl'] && $_POST['city_ddl']) {
            $query = "SELECT description FROM content  WHERE country = '" . $_POST['country_ddl'] . "' AND city ='" . $_POST['city_ddl'] . "'  AND airport = ''  ORDER BY `id` DESC";
            $string = $wpdb->get_var($query);
            echo $string;
        } else {
            $query = "SELECT * FROM content WHERE city != '' AND airport = '' group by city,country ORDER BY `id` DESC";
            $string = $wpdb->get_results($query);

            /*echo '<pre/>';
            print_r($string);die;*/

            if(!empty($string)){
               foreach($string as $val){
                    $query = "SELECT * FROM country WHERE iso ='".$val->country."'";
                    $res = $wpdb->get_results($query);
                    $val->display_country=@$res[0]->nicename;
                    $val->display_country=str_replace(" ", "-", $val->display_country);

                    $query2 = "SELECT * FROM city WHERE code ='".$val->city."'";
                    $res2 = $wpdb->get_results($query2);
                    $val->display_city=@$res2[0]->name;
                    
                    $description=strip_tags($val->description);
                    $val->description=substr($description, 0, 150);

                    $url_city=strtolower($val->display_city);
                    $city_url=get_site_url()."/en/routes/transfers/".strtolower($val->display_country).'/'.$url_city.'';
                    $val->city_url='<a href="'.$city_url.'" target="_blank"><span class="exter_link">'.$val->display_city.'</span>&nbsp;&nbsp;<img src="../wp-content/plugins/easybus-contenteditor/css/external.png" style="margin-top: -5px;"/></a>';


                    $rows[]=$val;
               } 
               echo json_encode($rows);
               wp_die();
            }
            echo 0;
        }
        wp_die();
    }
//******************************************************************************//
}

function get_airport_content() {

    global $wpdb;

    if (isset($_POST['action'])) {
        if ($_POST['country_ddl'] && $_POST['city_ddl'] && $_POST['airport_ddl']) {
            $query = "SELECT description FROM content  WHERE country = '" . $_POST['country_ddl'] . "' AND city ='" . $_POST['city_ddl'] . "' AND airport ='" . $_POST['airport_ddl'] . "' ORDER BY `id` DESC";
            $string = $wpdb->get_var($query);
            echo $string;
        } else {
            $query = "SELECT * FROM content WHERE city != '' AND airport != '' group by city,country,airport";
            $string = $wpdb->get_results($query);
            if (sizeof($string) > 0) {
                foreach($string as $val){
                    $query = "SELECT * FROM country WHERE iso ='".$val->country."'";
                    $res = $wpdb->get_results($query);
                    $val->display_country=@$res[0]->nicename;

                    $query2 = "SELECT * FROM city WHERE code ='".$val->city."'";
                    $res2 = $wpdb->get_results($query2);
                    $val->display_city=@$res2[0]->name;
                      

                    $query3 = "SELECT * FROM airport WHERE code ='".$val->airport."'";
                    $res3 = $wpdb->get_results($query3);
                    $val->display_airport=@$res3[0]->name;

                   $description=strip_tags($val->description);
                   $val->description=substr($description, 0, 150);
                   $val->airport_url="NA";  
                   $rows[]=$val; 
                }
                echo json_encode($rows);
                wp_die();
            }
            echo 0;
        }
        wp_die();
    }
//******************************************************************************//
}

function get_meta_tags_content(){

    global $wpdb;
    if (isset($_POST['action'])) {
        if ($_POST['country_ddl'] && $_POST['city_ddl']) {
            $query = "SELECT description FROM content  WHERE country = '" . $_POST['country_ddl'] . "' AND city ='" . $_POST['city_ddl'] . "'  AND airport = ''  ORDER BY `id` DESC";
            $string = $wpdb->get_var($query);
            echo $string;
        } else {
            $query = "SELECT * FROM meta_tags  ORDER BY `id` DESC";
            $string = $wpdb->get_results($query);

            if(!empty($string)){
               foreach($string as $val){
                    $query = "SELECT * FROM country WHERE iso ='".$val->country."'";
                    $res = $wpdb->get_results($query);
                    $val->display_country=@$res[0]->nicename;
                    
                    if($val->type=="city"){
                      $query2 = "SELECT * FROM city WHERE code ='".$val->city."'";
                      $res2 = $wpdb->get_results($query2);
                      $val->display_city=@$res2[0]->name;
                    }else{
                      $val->display_city="--";
                    }

                    

                    $rows[]=$val;
               } 
               echo json_encode($rows);
               wp_die();
            }
            echo 0;
        }
        wp_die();
    }
//******************************************************************************//
}

function save_content() {
    if (isset($_POST['action'])) {
        if ($_POST['country_ddl'] && $_POST['city_ddl'] && $_POST['airport_ddl']) {
            save_data_into_table("airport");
        } elseif ($_POST['country_ddl'] && $_POST['city_ddl']) {
            save_data_into_table("city");
        } elseif ($_POST['country_ddl']) {
            save_data_into_table("country");
        }
    }

//******************************************************************************//
}

function save_data_into_table($param) {
    global $wpdb;
    switch ($param) {
        case "country":
            $country = $_POST['country_ddl'];
            $editor = $_POST['editor'];
            $title = $_POST['title'];

            if (sizeof($_POST['editor']) === 0) {
                echo '<div class="notice notice-error is-dismissible"><p>Error</p></div>';
                wp_die();
                break;
            }
            $sections = explode(",", $_POST['sections']);
            $sections_ = str_replace(",", "|", $_POST['sections']);
            $content = explode(",", $_POST['content_rows']);
            $files_in = $_POST['files_in'];
            $files_index = $_POST['files_index'];
            $values = array();
            $image_values = array();
            $last_ids = '';

            if ($_POST['btn_value'] === 'country_update') {
                /*
                 * *UPDATE**
                 *  */
                for ($i = 0; $i < sizeof($_POST['editor']); $i++) {

                    $success = $wpdb->update('content', array(
                        "section_id" => $sections[$i],
                        "title" => $title[$i],
                        "description" => trim($editor[$i])), array(
                        'country' => trim($country), 'id' => $content[$i]
                    ));

                    if(!empty($_POST["content_images"])){
                            
                            $wpdb->delete("image",array("content_id"=>$content[$i]));

                            if(!empty($_POST["content_images"][$i])){
                                $successa = $wpdb->insert("image", array(
                                        "content_id" => $content[$i],
                                        "image_path" => $_POST["content_images"][$i],
                                    ));
                            }  
                    }
                }

                 $type="country";
                 $country=$_POST['country_ddl'];
                 $meta_description=$_POST['meta_description'];
                 $meta_keywords=$_POST['meta_keywords'];
                 $sql="SELECT * FROM meta_tags WHERE country='".$country."' AND type='".$type."'";  
                 $res = $wpdb->get_results($sql); 
                 if(!empty($res)){

                  $where=array("country"=>$country,"type"=>"country");
                  $update_data["meta_description"]=$_POST["meta_description"];
                  $update_data["meta_keywords"]=$_POST["meta_keywords"];
                  $update_data["meta_title"]=$_POST["meta_title"];
                  $wpdb->update("meta_tags",$update_data,$where);


                 }else{
                     $city="";
                     $meta_description=$_POST['meta_description'];
                     $meta_keywords=$_POST['meta_keywords'];
                     $meta_title=$_POST["meta_title"];
                     $sql = "INSERT INTO meta_tags (type, country, city, meta_title,meta_description,meta_keywords) VALUES ('".$type."','".$country."','".$city."','".$meta_title."','".$meta_description."','".$meta_keywords."')";
                     $wpdb->query($sql);
                 }

                 if(!empty($_POST["featured_cities"])){
                    create_featured_cities($_POST["featured_cities"],$_POST['country_ddl']);
                 }else{
                    $wpdb->delete("featured_cities",array("country_code"=>$_POST['country_ddl']));
                 }

                 if(!empty($_POST["featured_airports"])){
                    create_featured_airport($_POST["featured_airports"],$_POST['country_ddl']);
                 }else{
                    $wpdb->delete("featured_airport",array("country_code"=>$_POST['country_ddl']));
                 }

            } else {
                /*
                 * *INSERT**
                 *  */



                for ($i = 0; $i < sizeof($_POST['editor']); $i++) {
                    $success = $wpdb->insert("content", array(
                        "country" => $country,
                        "city" => '',
                        "airport" => '',
                        "section_id" => $sections[$i],
                        "title" => $title[$i],
                        "description" => $editor[$i],
                    ));
                    if ($success) {
                        $lastid = $wpdb->insert_id;
                         

                         
                        if(!empty($_POST["content_images"])){
                            if(!empty($_POST["content_images"][$i])){
                                $successa = $wpdb->insert("image", array(
                                        "content_id" => $lastid,
                                        "image_path" => $_POST["content_images"][$i],
                                    ));
                            }  
                        }
                    }
                    if ($last_ids === "") {
                        $last_ids = $lastid;
                    } else {
                        $last_ids = $last_ids . "|" . $lastid;
                    }
                }

                 $type="country";
                 $country=$_POST['country_ddl'];
                 $city="";
                 $meta_description=$_POST['meta_description'];
                 $meta_keywords=$_POST['meta_keywords'];
                 $meta_title=$_POST["meta_title"];
                     $sql = "INSERT INTO meta_tags (type, country, city, meta_title,meta_description,meta_keywords) VALUES ('".$type."','".$country."','".$city."','".$meta_title."','".$meta_description."','".$meta_keywords."')";
                 $wpdb->query($sql);

                 if(!empty($_POST["featured_cities"])){
                    create_featured_cities($_POST["featured_cities"],$_POST['country_ddl']);
                 }else{
                    $wpdb->delete("featured_cities",array("country_code"=>$_POST['country_ddl']));
                 }

                 if(!empty($_POST["featured_airports"])){
                    create_featured_airport($_POST["featured_airports"],$_POST['country_ddl']);
                 }else{
                    $wpdb->delete("featured_airport",array("country_code"=>$_POST['country_ddl']));
                 }


            }

            if ($success || $success1) {
                echo '<div class="alert alert-success alert-dismissible"> 
                    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> Saved Successfully</div>,' . $last_ids . "," . $sections_;
            } else {
                //echo '<div class="notice notice-error is-dismissible"><p>No Updates In Database</p></div>';
                echo '<div class="alert alert-success"><p>Update Successfully</p></div>';
            }
            wp_die();
            break;

        case "city":
            $country = $_POST['country_ddl'];
            $city = $_POST['city_ddl'];
            $editor = $_POST['editor'];
            $title = $_POST['title'];
            $files_in = $_POST['files_in'];
            $files_index = $_POST['files_index'];
            if (sizeof($_POST['editor']) === 0) {
                echo '<div class="notice notice-error is-dismissible"><p>Error</p></div>';
                wp_die();
                break;
            }
            $sections = explode(",", $_POST['sections']);
            $content = explode(",", $_POST['content_rows']);
            $values = array();
            $image_values = array();
            $last_ids = '';
            $sections_ = str_replace(",", "|", $_POST['sections']);
            if ($_POST['btn_value'] === 'city_update') {
                /*
                 * *UPDATE**
                 *  */
                for ($i = 0; $i < sizeof($_POST['editor']); $i++) {

                    $success = $wpdb->update('content', array(
                        "section_id" => $sections[$i],
                        "title" => $title[$i],
                        "description" => stripslashes($editor[$i])), array('id' => $content[$i]));
//                    echo $success;

if(!empty($_POST["content_images"])){
                            
                            $wpdb->delete("image",array("content_id"=>$content[$i]));

                            if(!empty($_POST["content_images"][$i])){
                                $successa = $wpdb->insert("image", array(
                                        "content_id" => $content[$i],
                                        "image_path" => $_POST["content_images"][$i],
                                    ));
                            }  
                    }

                }

                 $type="city";
                 $country = $_POST['country_ddl'];
                 $city = $_POST['city_ddl'];
                 $meta_description=$_POST['meta_description'];
                 $meta_keywords=$_POST['meta_keywords'];
                 $sql="SELECT * FROM meta_tags WHERE city='".$city."' AND type='".$type."'";  
                 $res = $wpdb->get_results($sql); 
                 if(!empty($res)){

                  $where=array("city"=>$city,"type"=>"city");
                  $update_data["meta_description"]=$_POST["meta_description"];
                  $update_data["meta_keywords"]=$_POST["meta_keywords"];
                  $update_data["meta_title"]=$_POST["meta_title"];
                  $wpdb->update("meta_tags",$update_data,$where);


                 }else{
                     $meta_description=$_POST['meta_description'];
                     $meta_keywords=$_POST['meta_keywords'];
                      $meta_title=$_POST["meta_title"];
                     $sql = "INSERT INTO meta_tags (type, country, city, meta_title,meta_description,meta_keywords) VALUES ('".$type."','".$country."','".$city."','".$meta_title."','".$meta_description."','".$meta_keywords."')";
                     $wpdb->query($sql);
                 }


            } else {
                /*
                 * *INSERT**
                 *  */
                $lastid = '';
                for ($i = 0; $i < sizeof($_POST['editor']); $i++) {
                    $success = $wpdb->insert("content", array(
                        "country" => $country,
                        "city" => $city,
                        "airport" => '',
                        "section_id" => $sections[$i],
                        "title" => $title[$i],
                        "description" => $editor[$i],
                    ));
                    $lastid = $wpdb->insert_id;
                    if ($success) {

                        if(!empty($_POST["content_images"])){
                            if(!empty($_POST["content_images"][$i])){
                                $successa = $wpdb->insert("image", array(
                                        "content_id" => $lastid,
                                        "image_path" => $_POST["content_images"][$i],
                                    ));
                            }  
                        }
                    }
                    if ($last_ids === "") {
                        $last_ids = $lastid;
                    } else {
                        $last_ids = $last_ids . "|" . $lastid;
                    }
                }

                 $type="city";
                 $country = $_POST['country_ddl'];
                 $city = $_POST['city_ddl'];
                 $meta_description=$_POST['meta_description'];
                 $meta_keywords=$_POST['meta_keywords'];
                  $meta_title=$_POST["meta_title"];
                     $sql = "INSERT INTO meta_tags (type, country, city, meta_title,meta_description,meta_keywords) VALUES ('".$type."','".$country."','".$city."','".$meta_title."','".$meta_description."','".$meta_keywords."')";
                 $wpdb->query($sql);
            }

            if ($success) {
                echo '<div class="alert alert-success alert-dismissible"> 
                    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> Saved Successfully</div>,' . $last_ids . "," . $sections_;
            } else {
                echo '<div class="alert alert-success"><p>Update Successfully</p></div>';
            }
            wp_die();
            break;

        case "airport":
            $country = $_POST['country_ddl'];
            $city = $_POST['city_ddl'];
            $airport = $_POST['airport_ddl'];
            $editor = $_POST['editor'];
            $title = $_POST['title'];
            $files_in = $_POST['files_in'];
            $files_index = $_POST['files_index'];
            if (sizeof($_POST['editor']) === 0) {
                echo '<div class="notice notice-error is-dismissible"><p>Error</p></div>';
                wp_die();
                break;
            }
            $sections = explode(",", $_POST['sections']);
            $content = explode(",", $_POST['content_rows']);
            $values = array();
            $image_values = array();
            $last_ids = '';
            $sections_ = str_replace(",", "|", $_POST['sections']);
            if ($_POST['btn_value'] === 'airport_update') {
                /*
                 * *UPDATE**
                 *  */
                for ($i = 0; $i < sizeof($_POST['editor']); $i++) {

                    $success1 = $wpdb->update('content', array(
                        "section_id" => $sections[$i],
                        "title" => $title[$i],
                        "description" => stripslashes($editor[$i])), array('id' => $content[$i]));

                    if(!empty($_POST["content_images"])){
                            
                            $wpdb->delete("image",array("content_id"=>$content[$i]));

                            if(!empty($_POST["content_images"][$i])){
                                $successa = $wpdb->insert("image", array(
                                        "content_id" => $content[$i],
                                        "image_path" => $_POST["content_images"][$i],
                                    ));
                            }  
                    }
                }
                if(!empty($_POST["min_price"])){
                    $data_insert["country"] = $_POST['country_ddl'];
                    $data_insert["city"] = $_POST['city_ddl'];
                    $data_insert["airport"] = $_POST['airport_ddl'];
                    $data_insert["min_price"] = $_POST['min_price'];
                    $wpdb->delete("airport_minimum_price",array("airport"=>$_POST['airport_ddl']));
                    $wpdb->insert("airport_minimum_price",$data_insert);
                }
            } else {
                /*
                 * *INSERT**
                 *  */

                $sql="SELECT * FROM content WHERE airport='".$airport."'";  
                $res = $wpdb->get_results($sql);
                if(!empty($res)){
                  echo '<div class="notice notice-error is-dismissible"><p>Record Already Exists</p></div>';
                  wp_die();
                  break;
                } 


                for ($i = 0; $i < sizeof($_POST['editor']); $i++) {
                    $success = $wpdb->insert("content", array(
                        "country" => $country,
                        "city" => $city,
                        "airport" => $airport,
                        "section_id" => $sections[$i],
                        "title" => $title[$i],
                        "description" => $editor[$i],
                    ));
                    if ($success) {
                        $lastid = $wpdb->insert_id;
                        if(!empty($_POST["content_images"])){
                            
                            if(!empty($_POST["content_images"][$i])){
                                $successa = $wpdb->insert("image", array(
                                        "content_id" => $lastid,
                                        "image_path" => $_POST["content_images"][$i],
                                    ));
                            }  
                    }
                    }
                    if ($last_ids === "") {
                        $last_ids = $lastid;
                    } else {
                        $last_ids = $last_ids . "|" . $lastid;
                    }
                }
                if(!empty($_POST["min_price"])){
                    $data_insert["country"] = $_POST['country_ddl'];
                    $data_insert["city"] = $_POST['city_ddl'];
                    $data_insert["airport"] = $_POST['airport_ddl'];
                    $data_insert["min_price"] = $_POST['min_price'];
                    $wpdb->delete("airport_minimum_price",array("airport"=>$_POST['airport_ddl']));
                    $wpdb->insert("airport_minimum_price",$data_insert);
                }
            }

            if ($success || $success1) {
                echo '<div class="alert alert-success alert-dismissible"> 
                    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> Saved Successfully</div>,' . $last_ids . "," . $sections_;
            } else {
                echo '<div class="alert alert-success"><p>Update Successfully</p></div>';
            }
            wp_die();
            break;
    }
}

function wpdb_update_in($table, $data, $where, $format = NULL, $where_format = NULL) {

    global $wpdb;

    $table = esc_sql($table);

    if (!is_string($table) || !isset($wpdb->$table)) {
        return FALSE;
    }

    $i = 0;
    $q = "UPDATE content SET ";
    $format = array_values((array) $format);
    $escaped = array();

    foreach ((array) $data as $key => $value) {
        $f = isset($format[$i]) && in_array($format[$i], array('%s', '%d'), TRUE) ? $format[$i] : '%s';
        $escaped[] = esc_sql($key) . " = " . $wpdb->prepare($f, $value);
        $i++;
    }

    $q .= implode($escaped, ', ');
    $where = (array) $where;
    $where_keys = array_keys($where);
    $where_val = (array) array_shift($where);
    $q .= " WHERE " . esc_sql(array_shift($where_keys)) . ' IN (';

    if (!in_array($where_format, array('%s', '%d'), TRUE)) {
        $where_format = '%s';
    }

    $escaped = array();

    foreach ($where_val as $val) {
        $escaped[] = $wpdb->prepare($where_format, $val);
    }

    $q .= implode($escaped, ', ') . ')';

    return $wpdb->query($q);
}

register_deactivation_hook(__FILE__, 'my_plugin_remove_database');

function my_plugin_remove_database() {
    global $wpdb;
    $table_name = 'content,image,sections';

    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query($sql);
    $dir = ABSPATH . 'wp-content/plugins/easybus-contenteditor/img/';
    rmdir_recursive($dir);
    delete_option("my_plugin_db_version");
}

function rmdir_recursive($dir) {
    foreach (scandir($dir) as $file) {
        if ('.' === $file || '..' === $file)
            continue;
        if (is_dir("$dir/$file"))
            rmdir_recursive("$dir/$file");
        else
            unlink("$dir/$file");
    }
    rmdir($dir);
}

function change_airport_name($param) {
    $ddloptions = '';
    switch ($param) {
        case 'IGIA':
            $ddloptions = 'Indira Gandhi International Airport';
            break;
        case 'DDA':
            $ddloptions = 'Delhi Domestic Airport';
            break;
        case 'CSIA':
            $ddloptions = 'Chhatrapati Shivaji International Airport';
            break;
        case 'PIA':
            $ddloptions = 'Pune International Airport';
            break;
        case 'DA':
            $ddloptions = 'Dabolim Airport';
            break;
        case 'SGA':
            $ddloptions = 'South Goa Airport';
            break;
        case 'MA':
            $ddloptions = 'Mascot Airport';
        case 'KSA':
            $ddloptions = 'Kingsford Smith Airport';
            break;
        case 'MEA':
            $ddloptions = 'Melbourne Airport';
            break;
        case 'AA':
            $ddloptions = 'Avalon Airport';
            break;
        case 'BA':
            $ddloptions = 'Barrillas Airport';
            break;
        case 'ETA':
            $ddloptions = 'El Tamarindo Airport';
            break;
    }
    return $ddloptions;
}

function change_city_name($city) {
    $heading = '';
    if ($city === "DL") {
        $heading = "Delhi";
    }
    if ($city === "MUM") {
        $heading = "Mumbai";
    }
    if ($city === "GOA") {
        $heading = "Goa";
    }
    if ($city === "IND") {
        $heading = "Indore";
    }
    if ($city === "SYD") {
        $heading = "Sydney";
    }
    if ($city === "MEL") {
        $heading = "Melbourne";
    }
    if ($city === "SAL") {
        $heading = "Salvador";
    }
    if ($city === "SAL") {
        $heading = "Salvador";
    }
    if ($city === "FOR") {
        $heading = "Fortaleza";
    }
    return $heading;
}
function create_featured_cities($featured_cities,$country_ddl){
      global $wpdb;
      $wpdb->delete("featured_cities",array("country_code"=>$country_ddl));
      $wpdb->update("city",array("sort_order"=>0),array("country_code"=>$country_ddl));
      $featured_cities=explode(",",$featured_cities);
      for($i=0;$i<count($featured_cities);$i++){
         $data_insert["country_code"]=$country_ddl;
         $data_insert["featured_city"]=$featured_cities[$i];
         $wpdb->insert("featured_cities",$data_insert);
         $wpdb->update("city",array("sort_order"=>$i+1),array("code"=>$featured_cities[$i])); 
      }
      return true;
}
function create_featured_airport($featured_airports,$country_ddl){
    global $wpdb;
    $wpdb->delete("featured_airport",array("country_code"=>$country_ddl));
    $queryRes2 = "SELECT a.id FROM airport as a LEFT JOIN city as c ON c.code=a.city_code LEFT JOIN country as con ON con.iso=c.country_code  WHERE con.iso ='".$country_ddl."'";
    $result2 = $wpdb->get_results($queryRes2,ARRAY_A);
    if(!empty($result2)){
      foreach($result2 as $val){
        $wpdb->update("airport",array("sort_order"=>0),array("id"=>$val["id"]));
      }
    }
    $featured_airports=explode(",",$featured_airports);
    for($j=0;$j<count($featured_airports);$j++){
       $data_insert_fa["country_code"]=$country_ddl;
       $data_insert_fa["featured_airport"]=$featured_airports[$j];
       $wpdb->insert("featured_airport",$data_insert_fa);
       $wpdb->update("airport",array("sort_order"=>$j+1),array("code"=>$featured_airports[$j])); 
    }
}
//register_uninstall_hook('uninstall.php', 'on_uninstall'); this hook is registered when we uninstall the plugin
