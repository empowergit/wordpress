<?php

return array(
	'id'          => 'ozy_tracking_meta',
	'types'       => array('ozy_tracking'),
	'title'       => esc_attr__('Tracking Parameters', 'vp_textdomain'),
	'priority'    => 'high',
	'template'    => array(
		array(
			'type'      => 'group',
			'repeating' => true,
			'length'    => 1,
			'name'      => 'movement_group',
			'title'     => esc_attr__('Movements', 'vp_textdomain'),
			'fields'    => array(	
				array(
					'type' => 'select',
					'name' => 'movement_status',
					'label' => esc_attr__('Status', 'vp_textdomain'),
					'items' => array(
						array(
							'value' => 'Departed',
							'label' => 'Departed',
						),						array(							'value' => 'In-Transit',							'label' => 'In Transit',						),	
						array(
							'value' => 'Arrived',
							'label' => 'Arrived',
						),												array(							'value' => 'Delivered',							'label' => 'Delivered',						),							
					),
					'validation' => 'required',
					'default' => array(
						'normal',
					),
				),			
				array(
					'type' => 'textbox',
					'name' => 'movement_location',
					'label' => esc_attr__('Location', 'vp_textdomain'),
				),	
				array(
					'type' => 'textbox',
					'name' => 'movement_message',
					'label' => esc_attr__('Message', 'vp_textdomain'),
				),
				array(
					'type' => 'date',
					'name' => 'movement_date',
					'label' => esc_attr__('Date', 'vp_textdomain'),
					'format' => 'yy-mm-dd',
				),
				array(
					'type' => 'textbox',
					'name' => 'movement_time',
					'label' => esc_attr__('Time', 'vp_textdomain'),
					'description' => esc_attr__('Enter your values like 10:45', 'vp_textdomain'),
					'validation' => 'maxlength[5]'
				),
			)
		),	
		
		array(
			'type'      => 'group',
			'repeating' => false,
			'length'    => 1,
			'name'      => 'sender_group',
			'title'     => esc_attr__('Sender', 'vp_textdomain'),
			'fields'    => array(	
				array(
					'type' => 'textbox',
					'name' => 'tracking_sender_name',
					'label' => esc_attr__('Sender\'s Name', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_sender_company',
					'label' => esc_attr__('Company', 'vp_textdomain'),
				),
				array(
					'type' => 'textarea',
					'name' => 'tracking_sender_address',
					'label' => esc_attr__('Address', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_sender_country',
					'label' => esc_attr__('Country', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_sender_city',
					'label' => esc_attr__('City', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_sender_state',
					'label' => esc_attr__('State/Province', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_sender_postal_code',
					'label' => esc_attr__('Postal Code', 'vp_textdomain'),
				),	
				array(
					'type' => 'textbox',
					'name' => 'tracking_sender_telephone',
					'label' => esc_attr__('Telephone', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_sender_email',
					'label' => esc_attr__('E-Mail', 'vp_textdomain'),
					'validation ' => 'emaiL'
				),
			),
		),		
		
		array(
			'type'      => 'group',
			'repeating' => false,
			'length'    => 1,
			'name'      => 'receiver_group',
			'title'     => esc_attr__('Receiver', 'vp_textdomain'),
			'fields'    => array(	
				array(
					'type' => 'textbox',
					'name' => 'tracking_receiver_name',
					'label' => esc_attr__('Receiver\'s Name', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_receiver_company',
					'label' => esc_attr__('Company', 'vp_textdomain'),
				),
				array(
					'type' => 'textarea',
					'name' => 'tracking_receiver_address',
					'label' => esc_attr__('Address', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_receiver_country',
					'label' => esc_attr__('Country', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_receiver_city',
					'label' => esc_attr__('City', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_receiver_state',
					'label' => esc_attr__('State/Province', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_receiver_postal_code',
					'label' => esc_attr__('Postal Code', 'vp_textdomain'),
				),	
				array(
					'type' => 'textbox',
					'name' => 'tracking_receiver_telephone',
					'label' => esc_attr__('Telephone', 'vp_textdomain'),
				),
				array(
					'type' => 'textbox',
					'name' => 'tracking_receiver_email',
					'label' => esc_attr__('E-Mail', 'vp_textdomain'),
					'validation ' => 'emaiL'
				),
			),
		),
	)
);

/**
 * EOF
 */