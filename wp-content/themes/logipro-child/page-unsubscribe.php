<!DOCTYPE html>
<?php
/*
  Template Name: Page : Country Info Page Layout
 */
//get_header();

global $wp;
$home_page = home_url(add_query_arg(array(), $wp->request));
$current_page_url = str_replace('/' . ICL_LANGUAGE_CODE . '/', '', home_url(add_query_arg(array(), $wp->request)));
$page_title = ucwords($city_name) . ' Airport Transfers';
//echo $plugin_css_path = plugin_dir_path( __FILE__ ) . '/js_composer/assets/css/js_composer.min.css';
$plugin_css_path = plugins_url('js_composer/assets/css/js_composer.min.css');
global $wpdb;
?>
<html <?php language_attributes(); ?>>

<head>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <![endif]-->
    <meta charset="<?php esc_attr(bloginfo('charset')); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="format-detection" content="telephone=no">
    <title>Unsubscribe - easyBus</title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
    <link rel="stylesheet" href="<?php echo $plugin_css_path; ?>" />
    <?php remove_action('wp_head', '_wp_render_title_tag', 1);
    wp_head(); /* this is used by many Wordpress features and for plugins to work properly */ ?>
    <style type="text/css">
        .alert_error{
            color: red;
        }
    .wpforms-submit-container{
        text-align: center;
    }
    .unsubscribe-col .vc_column-inner {
    width: 80% !important;
    margin: 0 auto;
}
    button#wpforms-submit-18854 {
            background:
            black !important;
            border: none !important;
            padding-left: 20px;
            padding-right: 21px;
            border-radius: 3px;
            font-size: 18px;
            font-weight: normal;
            color:
            white;
            padding-top: 12px;
            padding-bottom: 10px;
            margin-top: 20px;
}
button#wpforms-submit-18854:hover {
    background: 
#ff6600 !important;
color:
    white!Important;
}
#email_address{
    width: 65%;

text-align: center;

margin: 0 auto;
}    
    .bottom_border {
        border-bottom: 2px solid #E7E7E7;
    }

    #content p,
    #side-extended-content p,
    #request-a-rate p {
        padding-bottom: 30px !important;
    }

    #content {
        color: #363636 !important;
    }

    .source_links {
        display: flex;
        flex-wrap: wrap;
    }

    .source_links li {
        list-style: none;
        width: 49.33%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }


    @media only screen and (max-width: 600px) {
        .source_links {
            display: block;
            flex-wrap: nowrap;
        }

        .source_links li {
            list-style: none;
            width: 100%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    }
    </style>
</head>

<body <?php body_class(); ?>>
    <?php
    /* Animsition */

    // if (logipro_get_ozy_data('is_animsition_active'))
    //   echo '<div class="animsition">';
    // echo '<div id="body-bg-wrapper">';
    /* Include Primary Menu */
    logipro_ozy_include_primary_menu();
    ?>
    <div class="none">
        <p><a href="#content"><?php esc_attr_e('Skip to Content', 'logipro'); ?></a></p>
    </div>
    <!--.none-->
    <?php
    if (logipro_get_ozy_data('hide_everything_but_content') <= 0):
      $header_slider_class = $footer_slider_class = '';
      logipro_ozy_header_footer_slider_class($header_slider_class, $footer_slider_class);
      ?>

    <div id="main" class="<?php echo esc_attr($footer_slider_class);
      echo esc_attr($header_slider_class); ?>">
        <?php
        logipro_ozy_custom_header();
        ?>
        <?php endif; ?>
        <div class="pre-spin" style="display: none;"><img
                src="<?php echo get_site_url(); ?>/wp-content/uploads/2019/09/loader.gif"></div>

        <div id="page-title-wrapper">
            <div>
                <h1 class="page-title">Customer Support</h1>
                <h4>Mailing List Management</h4>
            </div>
        </div>
        <div id="page-bread-crumbs-share-bar">
            <ul id="page-breadcrumbs" class="content-font">
                <li><a href="<?php echo $home_page; ?>"><i class="oic-home-2"></i></a></li>
                <li><a href="<?php echo $home_page; ?>routes/" title="Unsubscribe">Unsubscribe</a></li>
            </ul>
            <span class="content-font"><?php esc_attr_e('Share', 'logipro') ?></span>
            <ul class="page-share-buttons">
                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="Facebook" class="symbol-facebook"><span
                            class="symbol">&#xe027;</span></a></li>
                <li><a href="https://twitter.com/intent/tweet?text=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="Twitter" class="symbol-twitter"><span class="symbol">&#xe086;</span></a>
                </li>
                <li><a href="https://plus.google.com/share?url=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="Google+" class="symbol-googleplus"><span
                            class="symbol">&#xe039;</span></a></li>
                <li><a href="https://pinterest.com/pin/create/button/?url=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="Pinterest" class="symbol-pinterest"><span
                            class="symbol">&#xe064;</span></a></li>
                <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="LinkedIn" class="symbol-linkedin"><span
                            class="symbol">&#xe052;</span></a></li>
                <li><a href="#" class="page-social-share"><span class="oic-share-outline"></span></a></li>
            </ul>
        </div>
        <!-- footer content code here -->
        <div class="container <?php echo esc_attr(logipro_get_ozy_data('_page_content_css_name')); ?>">
            <div id="content" class="without-sidebar route-page">
                <div class="main-section">
                    <article>
                        <div class="post-content page-content">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <h5 style="color: #ff6600;text-align: left" class="vc_custom_heading"
                                                onclick="showhide();">In
                                                This Section...</h5>
                                            <div id="vmenu" class="vc_wp_custommenu wpb_content_element vmenu">
                                                <div class="widget widget_nav_menu">
                                                    <div class="menu-rome-airport-transfers-container">
                                                        <ul id="menu-customer-support" class="menu">
                                                            <li id="menu-item-2617"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2617">
                                                                <a
                                                                    href="/customer-support/">Customer
                                                                    Support Advice</a></li>
                                                            <li id="menu-item-2616"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2616">
                                                                <a
                                                                    href="/customer-support/customer-support-contact/">Contact
                                                                    Us</a></li>
                                                            <li id="menu-item-2615"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2615">
                                                                <a
                                                                    href="/customer-support/head-office/">easyBus
                                                                    Head Office</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_widgetised_column wpb_content_element">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                        <?php dynamic_sidebar('IBE_Internal'); ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="unsubscribe-col wpb_column vc_column_container vc_col-sm-8 vc_col-has-fill">
                                    <div class="vc_column-inner vc_custom_1577447003042">
                                        <div class="wpb_wrapper">
                                            <h4 style="color: #ff6600;text-align: left" class="vc_custom_heading">Our
                                                Mailing List</h4>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p>To be removed from our mailing list please confirm your email
                                                        address in the field below and click – Remove Me.</p>

                                                </div>
                                            </div>
                                            <div
                                                class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_border_width_2 vc_sep_pos_align_center vc_separator_no_text">
                                                <span class="vc_sep_holder vc_sep_holder_l"><span
                                                        style="border-color:#e7e7e7;"
                                                        class="vc_sep_line"></span></span><span
                                                    class="vc_sep_holder vc_sep_holder_r"><span
                                                        style="border-color:#e7e7e7;" class="vc_sep_line"></span></span>
                                            </div>
                                            <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                                <div class="wpb_wrapper">
                                                    <div class="inner-container">
                                                        <svg id="svg" data-name="Layer 1"
                                                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 590 484.7">
                                                            <g id="blobs">
                                                                <path id="blob-1"
                                                                    d="M545.5,299c0,80.3-28.6,150.4-126.4,139.4-63.2-7.1-109.3-37.3-142.6-37.3-45.7,0-105.4,29.3-146.8,22.2-69-11.7-85.3-66.8-85.3-135.8,0-56.3,25.5-99.9,46.2-140.8,18.3-36.1,55.9-97.8,125.1-100.5,53.3-2.1,97.4,50.5,138.4,74.2,49.9,28.8,98.4-1.8,126,1.3C537.9,127.9,545.5,265.5,545.5,299Z"
                                                                    fill="#ffd1b2"></path>
                                                                <path id="blob-3"
                                                                    d="M55.1,300.7c0,80.3,27.4,150.4,121,139.4,60.5-7.1,104.7-37.3,136.5-37.3,43.8,0,100.9,29.3,140.5,22.2,66-11.7,81.7-66.8,81.7-135.8,0-56.3-16.2-103.6-36.1-144.5-17.6-36.1-54.9-97.4-121.2-100.1-51-2.1-100.1,53.8-139.4,77.5-47.8,28.8-94.3-1.8-120.7,1.3C62.4,129.6,55.1,267.1,55.1,300.7Z"
                                                                    fill="#ffd1b2"></path>
                                                            </g>
                                                            <g id="confetti" class="confetti">
                                                                <rect x="284" y="230.4" width="17.7" height="17.67"
                                                                    rx="4" ry="4" fill="#000000"></rect>
                                                                <rect x="284" y="230.4" width="17.7" height="17.67"
                                                                    rx="4" ry="4" fill="#000000"></rect>
                                                                <rect x="285.4" y="231.7" width="17.7" height="17.67"
                                                                    rx="4" ry="4" fill="#fff"></rect>
                                                                <rect x="285.4" y="231.7" width="17.7" height="17.67"
                                                                    rx="4" ry="4" fill="#fff"></rect>
                                                                <rect x="285.4" y="230.1" width="17.7" height="17.67"
                                                                    rx="4" ry="4" fill="#f1731f"></rect>
                                                                <rect x="285.4" y="230.1" width="17.7" height="17.67"
                                                                    rx="4" ry="4" fill="#f1731f"></rect>
                                                                <rect x="285.4" y="231.7" width="17.7" height="17.67"
                                                                    rx="4" ry="4" fill="#ff6600"></rect>
                                                                <rect x="285.4" y="231.7" width="17.7" height="17.67"
                                                                    rx="4" ry="4" fill="#ff6600"></rect>
                                                                <circle cx="294.1" cy="241.3" r="9.7" fill="#000000">
                                                                </circle>
                                                                <circle cx="294.1" cy="243.6" r="12" fill="none"
                                                                    stroke="#fff" stroke-miterlimit="10"
                                                                    stroke-width="2"></circle>
                                                                <circle cx="294.2" cy="243.6" r="12" fill="#fff">
                                                                </circle>
                                                                <circle cx="294.2" cy="243.6" r="12" fill="none"
                                                                    stroke="#fff" stroke-miterlimit="10"
                                                                    stroke-width="2"></circle>
                                                                <circle cx="294.2" cy="243.6" r="12" fill="none"
                                                                    stroke="#f1731f" stroke-miterlimit="10"
                                                                    stroke-width="2"></circle>
                                                                <circle cx="294.2" cy="243.6" r="12" fill="none"
                                                                    stroke="#f1731f" stroke-miterlimit="10"
                                                                    stroke-width="2"></circle>
                                                                <circle cx="295.9" cy="242.1" r="12" fill="none"
                                                                    stroke="#000000" stroke-miterlimit="10"
                                                                    stroke-width="2"></circle>
                                                                <circle cx="295.9" cy="242.1" r="12" fill="none"
                                                                    stroke="#000000" stroke-miterlimit="10"
                                                                    stroke-width="2"></circle>
                                                                <circle cx="294.1" cy="241.3" r="9.7" fill="#f1731f">
                                                                </circle>
                                                                <circle cx="294.1" cy="241.3" r="9.7" fill="#f1731f">
                                                                </circle>
                                                                <circle cx="292.9" cy="241.3" r="9.7" fill="#fff">
                                                                </circle>
                                                                <circle cx="294.1" cy="241.3" r="9.7" fill="#f1731f">
                                                                </circle>
                                                                <circle cx="294.1" cy="241.3" r="9.7" fill="#000000">
                                                                </circle>
                                                                <circle cx="294.1" cy="241.3" r="9.7" fill="#f1731f">
                                                                </circle>
                                                                <circle cx="294.1" cy="241.3" r="9.7" fill="#000000">
                                                                </circle>
                                                                <circle cx="294.1" cy="241.3" r="9.7" fill="#000000">
                                                                </circle>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#ff6600"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#000000"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#f1731f"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#ff6600"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#fff"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#000000"></path>
                                                                <path
                                                                    d="M300.9,243.1l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.1Z"
                                                                    fill="#f1731f"></path>
                                                                <path
                                                                    d="M300.9,243.1l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.1Z"
                                                                    fill="#ff6600"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#000000"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#f1731f"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#ff6600"></path>
                                                                <path
                                                                    d="M300.9,243.2l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.2Z"
                                                                    fill="#fff"></path>
                                                                <path
                                                                    d="M300.9,243.1l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.1Z"
                                                                    fill="#f1731f"></path>
                                                                <path
                                                                    d="M300.9,243.1l-3-3a1.5,1.5,0,0,1,0-2.1l3-3a2,2,0,0,0,.3-2.5,1.9,1.9,0,0,0-2.9-.3l-3.1,3.1a1.5,1.5,0,0,1-2.1,0l-3-3a2,2,0,0,0-2.5-.3,1.9,1.9,0,0,0-.3,2.9l3.1,3.1a1.5,1.5,0,0,1,0,2.1l-3,3a2,2,0,0,0-.3,2.5,1.9,1.9,0,0,0,2.9.3l3.1-3.1a1.5,1.5,0,0,1,2.1,0l3.1,3.1a1.9,1.9,0,0,0,2.9-.3A2,2,0,0,0,300.9,243.1Z"
                                                                    fill="#ff6600"></path>
                                                            </g>
                                                            <g id="envelope">
                                                                <path id="Background"
                                                                    d="M452.9,376.3a26.1,26.1,0,0,1-25.5,20.8H162.6a26.1,26.1,0,0,1-26-26V193.2a26.1,26.1,0,0,1,26-26H427.4a26.1,26.1,0,0,1,26,26V371.1a25.9,25.9,0,0,1-.5,5.2"
                                                                    fill="#f1731f" stroke="#000000"
                                                                    stroke-miterlimit="10" stroke-width="5"></path>
                                                                <g id="paper-group">
                                                                    <rect id="paper" x="157.3" y="87.6" width="275.3"
                                                                        height="266.33" rx="26" ry="26" fill="#fff"
                                                                        stroke="#000000" stroke-miterlimit="10"
                                                                        stroke-width="5"></rect>
                                                                    <g id="face">
                                                                        <g id="mouth">

                                                                            <path id="mouth-sad"
                                                                                d="M258.8,231.9c3.9-14.5,17.7-25.2,34-25.2s30.3,10.8,34.1,25.4"
                                                                                fill="none" stroke="#000000"
                                                                                stroke-linecap="round"
                                                                                stroke-miterlimit="10" stroke-width="5">
                                                                            </path>



                                                                        </g>
                                                                        <g id="eye-group">
                                                                            <g id="eyes" class="eyes">
                                                                                <ellipse id="eye-right" cx="349"
                                                                                    cy="172.8" rx="11.2" ry="13.8"
                                                                                    fill="#000000"
                                                                                    stroke-linecap="round"
                                                                                    stroke-miterlimit="10"
                                                                                    stroke-width="5"></ellipse>
                                                                                <ellipse id="eye-left" cx="235.5"
                                                                                    cy="172.8" rx="11.2" ry="13.8"
                                                                                    fill="#000000"
                                                                                    stroke-linecap="round"
                                                                                    stroke-miterlimit="10"
                                                                                    stroke-width="5"></ellipse>
                                                                                <path id="eyebrow-sad-right"
                                                                                    d="M341.9,133.7c2.6,5.3,14.8,14.1,24.3,14.7"
                                                                                    fill="none" stroke="#000000"
                                                                                    stroke-linecap="round"
                                                                                    stroke-miterlimit="10"
                                                                                    stroke-width="5"></path>
                                                                                <path id="eyebrow-sad-left"
                                                                                    d="M240.7,133.7c-2.6,5.3-14.8,14.1-24.3,14.7"
                                                                                    fill="none" stroke="#000000"
                                                                                    stroke-linecap="round"
                                                                                    stroke-miterlimit="10"
                                                                                    stroke-width="5"></path>

                                                                            </g>
                                                                            <g id="eyes-laughing">
                                                                                <path id="eye-laughing-right"
                                                                                    d="M332.2,174c0-8.3,7.5-15,16.8-15s16.8,6.7,16.8,15"
                                                                                    fill="none" stroke="#000000"
                                                                                    stroke-linecap="round"
                                                                                    stroke-miterlimit="10"
                                                                                    stroke-width="5"></path>
                                                                                <path id="eye-laughing-left"
                                                                                    d="M218.7,174c0-8.3,7.5-15,16.8-15s16.8,6.7,16.8,15"
                                                                                    fill="none" stroke="#000000"
                                                                                    stroke-linecap="round"
                                                                                    stroke-miterlimit="10"
                                                                                    stroke-width="5"></path>
                                                                            </g>
                                                                            <g id="eyebrows-happy">
                                                                                <path id="eyebrow-happy-right"
                                                                                    d="M366.2,146.3c-2.6-5.3-14.8-14.1-24.3-14.7"
                                                                                    fill="none" stroke="#000000"
                                                                                    stroke-linecap="round"
                                                                                    stroke-miterlimit="10"
                                                                                    stroke-width="5"></path>
                                                                                <path id="eyebrow-happy-left"
                                                                                    d="M216.4,146.3c2.6-5.3,14.8-14.1,24.3-14.7"
                                                                                    fill="none" stroke="#000000"
                                                                                    stroke-linecap="round"
                                                                                    stroke-miterlimit="10"
                                                                                    stroke-width="5"></path>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                                <path id="back"
                                                                    d="M451.9,186.7S322.4,288.2,313.4,294.1s-27,5.8-36.9,0S137.9,186.5,137.9,186.5a23.6,23.6,0,0,0-1.3,6.7V371.1a26.1,26.1,0,0,0,26,26H427.4a26,26,0,0,0,26-26V193.2C453.4,190.7,452.5,188.9,451.9,186.7Z"
                                                                    fill="#ff6600" stroke="#000000"
                                                                    stroke-miterlimit="10" stroke-width="5"></path>
                                                                <g id="shadow">
                                                                    <path id="shadow-3"
                                                                        d="M263.3,279.7s11.3-8.1,13.1-9.3c9.9-6.5,27-5.8,36.9,0,1.7,1,13.5,9.3,13.5,9.3"
                                                                        fill="none" stroke="#ffd1b2"
                                                                        stroke-linejoin="bevel" stroke-width="7"></path>
                                                                    <path id="shadow-2"
                                                                        d="M430.2,193.3L313.4,282.2a26.1,26.1,0,0,1-36.8,0L159.8,193.3V201l116.8,90.6c7.9,5.7,26.9,6.4,37,0l116.6-90.9v-7.4Z"
                                                                        fill="#ffd1b2"></path>
                                                                </g>
                                                                <path id="shadow-1"
                                                                    d="M425.2,381.5h-262c-14.1,0-24.2-11-24.2-24.4v13.2c0,13.4,10.1,24.3,24.2,24.3h262c12.7,1.2,23.9-8.4,25.2-19.5a42.8,42.8,0,0,0,.5-4.9V358.1a14.7,14.7,0,0,1-.5,3.9C448,373.1,437.6,381.5,425.2,381.5Z"
                                                                    fill="#f1731f" opacity="0.5"></path>
                                                                <g id="Front">
                                                                    <path id="Front-2" data-name="Front"
                                                                        d="M139.8,381.9s127.5-99.5,136.5-105.4,27-5.8,36.9,0S449.8,382.1,449.8,382.1"
                                                                        fill="none" stroke="#000000"
                                                                        stroke-miterlimit="10" stroke-width="5"></path>
                                                                    <path id="Front-3" data-name="Front"
                                                                        d="M225.4,315.3s41.9-33,51-38.9,27-5.8,36.9,0S355,307.9,355,307.9"
                                                                        fill="#ff6600" stroke="#000000"
                                                                        stroke-miterlimit="10" stroke-width="5"></path>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 style="color: #352c66;text-align: center" class="vc_custom_heading">Do
                                                you want to unsubscribe?</h3>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p style="text-align: center;">To be removed from our mailing list
                                                        please confirm your email address in the field below and click –
                                                        Remove Me.</p>

                                                </div>
                                            </div>
                                            <div class="wpforms-container " id="wpforms-18854">
                                                <form id="wpforms-form-18854" class="wpforms-validate wpforms-form"
                                                    data-formid="18854" method="post" enctype="multipart/form-data"
                                                    action="#">
                                                    
                                                    <div class="wpforms-field wpforms-field-hp" id="wpform-field-hp">
                                                        <input type="email"
                                                            name="email_address" id="email_address"
                                                            class="wpforms-field-medium" required="" placeholder="Enter Your E-mail"></div>
                                                            <div class="alert_msgs" style="display: none;"></div>
                                                            <div class="email_loader" style="display: none;">
                                                               <img src="../wp-content/themes/logipro-child/ajax-loader.gif" />
                                                            </div>
                                                    <div class="wpforms-submit-container"><input type="hidden"
                                                            name="wpforms[id]" value="18854"><input type="hidden"
                                                            name="wpforms[author]" value="21"><input type="hidden"
                                                            name="wpforms[post_id]" value="18835"><button type="button"
                                                            name="wpforms[submit]" class="wpforms-submit "
                                                            id="wpforms-submit-18854" value="wpforms-submit"
                                                            data-alt-text="Sending..." onclick="removeEmail();">Remove Me</button>

                                                        <input type="hidden" name="custmkid" id="custmkid" value="<?php echo @$_GET["custmkid"];?>">
                                                        </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--.post-content .page-content -->
                    </article>
                    <!-- end page content section -->
                </div>
            </div>
        </div>
        <div>
            <script>
            // toggle collapse
            jQuery('.pre-spin').css('display', 'flex');
            jQuery(document).ready(function() {
                jQuery('.pre-spin').css('display', 'none');
            });
            </script>
        </div>
        <?php if (logipro_get_ozy_data('hide_everything_but_content') < 1): ?>
        <div class="clear"></div>
    </div>
    <!--.container-->
    <div class="footer-tagline-container">
        <div id="custom-footer-tagline" class="no-sidebar has-title custom-footer">
            <?php dynamic_sidebar('Footer_Tagline'); ?>
        </div>
    </div>
    <!-- The Modal -->
    <div id="unsubscribeMessageBox" class="modal">

      <!-- Modal content -->
      <div class="modal-content">
        <div class="modal_content_inner">
          <p>You have successfully unsubscribed</p>
          <a name="ok" class="wpforms-submit " id="ok_btn" href="<?php echo get_home_url();?>">OK</a>
        </div>
      </div>

    </div>
    <?php
      /* footer widget bar and footer */
      include(get_stylesheet_directory() . '/include/footer-bars.php');
      ?>
    <!-- </div> -->
    <!--#main-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ClientJS/0.1.11/client.min.js"></script>
    <script type="text/javascript">
    var width = screen.width;

    function manageUIOnMobile() {
        jQuery(
                '<a href="https://easy.com" id="family-logo" target="_blank"><img id="family-logo-default" src="/wp-content/uploads/2019/01/part_of_the_easy_family_of_brands-300x42.png" alt="logo"></a>'
                )
            .insertAfter("#top-menu #logo");
        var isMobile = '';
        //var client = new ClientJS();
        isMobile = (width < 768); //client.isMobile();
        if (isMobile == true && !jQuery('body').hasClass('home') && !jQuery('body').hasClass('page-id-13394')) {
            //jQuery('h5:contains("In This Section...")').parent().addClass('rightbar-section');
            //jQuery('h5:contains("In This Section...")').addClass('rightbar-toggle');
            jQuery('h5:contains("In this Section...")').addClass('rightbar-toggle');
            jQuery('h4:contains("In This Section...")').addClass('rightbar-toggle');
            jQuery("h5,h4").each(function(index, ele) {
                if (jQuery(this).html() === 'In This Section...') {
                    jQuery(this).addClass('rightbar-toggle');
                }
            });
            jQuery("li .custom-html-widget").closest("ul").parent().parent().parent().addClass('rightbar-section');
            if (jQuery('.rightbar-iframe').length == 0) {
                if (jQuery(".rightbar-toggle").length > 0) {
                    jQuery('<h5 style="color: #ff6600;text-align: left" class="rightbar-iframe">Search and Book</h5>')
                        .insertAfter(jQuery(".rightbar-toggle").next("#vmenu"));
                    //it doesn't exist
                } else {
                    jQuery(".rightbar-section").prepend(
                        '<h5 style="color: #ff6600;text-align: left" class="rightbar-iframe ">Search and Book</h5>');
                }
            }
            jQuery('.rightbar-iframe').next('.wpb_widgetised_column').addClass('bookingiframe');
            setTimeout(function() {
                jQuery('.rightbar-iframe').parent().find('.vc_custom_heading').addClass('rightbar-toggle');
            }, 1000);
            //jQuery( '<h5 style="color: #ff6600;text-align: left">Search and Book</h5>' ).find('.rightbar-section').insertAfter( "#vmenu" );
            //jQuery("li .custom-html-widget #iframeBookingEngine").closest("ul").parent().parent().parent().prepend('<h5 style="color: #ff6600;text-align: left">Search and Book</h5>');
            jQuery('#vmenu').css('display', 'none');
            jQuery('.page-share-buttons').css('display', 'none');
            if (jQuery('#headerShare').length == 0)
                jQuery('#header').append(
                    '<ul class="page-share-buttons" id="headerShare"><li><a href="https://www.facebook.com/sharer/sharer.php?u=https://www.easybus.com/en/" target="_blank" title="Facebook" class="symbol-facebook"><span class="symbol"></span></a></li><li><a href="https://twitter.com/intent/tweet?text=http://easybus.ibooking.com/en/routes/london-gatwick/gatwick-airport-heathrow-airport/" target="_blank" title="Twitter" class="symbol-twitter"><span class="symbol"></span></a></li><li><a href="https://plus.google.com/share?url=https://www.easybus.com/en/" target="_blank" title="Google+" class="symbol-googleplus"><span class="symbol"></span></a></li><li><a href="http://pinterest.com/pin/create/button/?url=http://easybus.ibooking.com/en/routes/london-gatwick/gatwick-airport-heathrow-airport/" target="_blank" title="Pinterest" class="symbol-pinterest"><span class="symbol"></span></a></li><li><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.easybus.com/en/" target="_blank" title="LinkedIn" class="symbol-linkedin"><span class="symbol"></span></a></li><li><a href="#" class="page-social-share"><span class="oic-share-outline"></span></a></li></ul>'
                );
            else
                jQuery('#headerShare').css('display', 'block');
        } else {
            jQuery('.page-share-buttons').css('display', 'block');
            jQuery('#headerShare').css('display', 'none');
            jQuery('#iframeBookingEngine').parents('.wpb_widgetised_column').addClass('bookingiframe1');
            console.log(isMobile + 'mobile');
        }
    }

    jQuery(document).ready(function() {
        jQuery(window).resize(function() {
            width = screen.width;
            console.log("Current Device Width : ", width);
            manageUIOnMobile();
        });
        manageUIOnMobile();
        jQuery('.templatera-footer').parent().addClass('footerhide');
        var originCountryCode = $("#idOrigin").val();
        var destinationCountryCode = $("#idDestination").val();
        var selectedOriginName = $("#OriginName").val();
        var selectedDestinationName = $("#destinationName").val();
        if (originCountryCode && originCountryCode != '') {
            originCountryCode = originCountryCode.substring(0, 2);
        }
        if (destinationCountryCode && destinationCountryCode != '') {
            destinationCountryCode = destinationCountryCode.substring(0, 2);
        }
        var originRouteList = $('#originRouteList');
        $('#originRouteList').html('');
        var departureRouteList = $('#departureRouteList');
        $('#departureRouteList').html('');
    });
    jQuery(function() {
        /*jQuery('.rightbar-toggle').on('click', function () {
          jQuery('#vmenu').toggle();
        });*/
    });
    jQuery(function() {
        /*jQuery('.rightbar-iframe').on('click', function () {
          jQuery('.wpb_widgetised_column').toggle();
        });*/
    });
    </script>
    <?php
    endif;

    if (logipro_ozy_get_theme_mod('back_to_top_button') == '1' && logipro_get_ozy_data('hide_everything_but_content') <= 0) {
      ?>
    <div class="logipro-btt-container">
        <div class="top logipro-btt"><img src="<?php echo esc_url(LOGIPRO_OZY_BASE_URL) ?>images/up-arrow.svg"
                class="svg" alt="" /><img src="<?php echo esc_url(LOGIPRO_OZY_BASE_URL) ?>images/up-arrow-2.svg"
                class="svg" alt="" /></div>
    </div>
    <?php
    }
    wp_footer();
    ?>
    <script async="async" type="text/javascript">
    jQuery(window).load(function() {
        jQuery('.gm-style-iw').parent().parent().addClass('black-caption');
    });
    </script>
    <style>
    body.admin-bar #header {
        margin-top: 0 !important;
    }
    </style>
    <?php
get_footer();
?>
    <script type="text/javascript">
    function showhide() {
        var x = document.getElementById("vmenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    jQuery(window).load(function() {
        jQuery('.rightbar-iframe').on('click', function() {

            console.log("search-bar");

            if ($('.wpb_widgetised_column:visible').length == 0) {
                jQuery('.wpb_widgetised_column').hide();
            } else {
                jQuery('.wpb_widgetised_column').show();
            }

        });
    });
    function removeEmail(){
        var email_address=jQuery("#email_address").val();
        var custmkid=jQuery("#custmkid").val();
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(email_address.match(mailformat)){
            jQuery(".alert_msgs").html("").hide();
            jQuery(".email_loader").show();
        jQuery.ajax({
                url: "<?php echo admin_url("admin-ajax.php");?>",
                method:"POST",
                data:{'action':"remove_email",'email_address':email_address,'custmkid':custmkid}, 
                success: function(result){
                    jQuery("#email_address").val("");
                    jQuery(".alert_msgs").html("").hide();
                    var unsubscribeMessageBox = document.getElementById("unsubscribeMessageBox");
                    unsubscribeMessageBox.style.display = "block";
                    jQuery(".email_loader").hide();
                
           }});
        }else{
           jQuery(".alert_msgs").html("<div class='alert_error'>Please Enter a valid E-mail</div>").show();
       }  
    }
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function() {
       var unsubscribeMessageBox = document.getElementById("unsubscribeMessageBox");  
       unsubscribeMessageBox.style.display = "none";
       window.location.href ="<?php echo get_home_url();?>";
    }
    </script>
    <style type="text/css">
        /* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    top: 45%;
    max-width: 280px;
    position: fixed;
    left: 50%;
}
.modal_content_inner p {
    margin-bottom: 10px;
    display: inline-block;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
#ok_btn {
    background: black !important;
    border: none !important;
    font-size: 11px;
    font-weight: normal;
    color: white;
    margin-top: 20px;
    display: inline-block;
    margin-left: 20px;
    padding: 0px 30px;
    border-radius: 4px;
    line-height: 40px !important;
}
#ok_btn:hover{
background: #ff6600 !important;
color:white!Important;
text-decoration: none;
}
.modal_content_inner{
    text-align:center;
}
.modal_content_inner p{
  margin-bottom: 10px;  
}
.email_loader{
    float: right;
    margin-top: -49px;
}
 input#email_address {
        background: #fff;
        border: 1px solid #919191 !important;
        border-color: #919191 !important;
        font-size: 16px !important;
        text-align: left !important;
}    
#wpform-field-hp>input#email_address:hover {
    border-color: #919191 !important;
}
 .unsubscribe-col .vc_column-inner {
    margin: 0 auto;
    padding-top: 0;
}
.wpb_button, .wpb_content_element, ul.wpb_thumbnails-fluid>li {
    margin-bottom: 0;
}    
    </style>