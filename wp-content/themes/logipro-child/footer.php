		<?php 
			if(logipro_get_ozy_data('hide_everything_but_content') < 1): ?> 
        
        <div class="clear"></div>
        
        </div><!--.container-->  

		<?php 
			logipro_ozy_blog_pagination();
			?>
<div class="custom-footer-container">
	<div id="custom-footer" class="no-sidebar has-title custom-footer">
		<?php if(ICL_LANGUAGE_NAME=="English"){
			dynamic_sidebar( 'Footer_Widget ');
}else{
	dynamic_sidebar( 'Footer_Widget ('.ICL_LANGUAGE_NAME.')' );
}
		?>

	</div>
	</div>
	<div class="footer-tagline-container">		
		<div id="custom-footer-tagline" class="no-sidebar has-title custom-footer">
		<?php dynamic_sidebar( 'Footer_Tagline' ); ?>
			</div>
		</div>
      <?php       /*footer widget bar and footer*/
            include(get_stylesheet_directory() . '/include/footer-bars.php');
        ?>
        
    </div><!--#main-->

	<script src="https://cdnjs.cloudflare.com/ajax/libs/ClientJS/0.1.11/client.min.js"></script>
<script async="async" type="text/javascript">
var width = screen.width;

function manageUIOnMobile(){
	jQuery( '<a href="https://easy.com" id="family-logo" target="_blank"><img id="family-logo-default" src="/wp-content/uploads/2019/01/part_of_the_easy_family_of_brands-300x42.png" alt="logo"></a>' ).insertAfter( "#top-menu #logo" );
	
	var isMobile ='';
	//var client = new ClientJS();
	isMobile = (width < 768);//client.isMobile();
	if(isMobile==true && !jQuery('body').hasClass('home') && !jQuery('body').hasClass('page-id-13394')){
		//jQuery('h5:contains("In This Section...")').parent().addClass('rightbar-section');
		//jQuery('h5:contains("In This Section...")').addClass('rightbar-toggle');
		jQuery('h5:contains("In this Section...")').addClass('rightbar-toggle');
		jQuery('h4:contains("In This Section...")').addClass('rightbar-toggle');
		jQuery("h5,h4").each(function(index, ele){
			if(jQuery(this).html() === 'In This Section...'){
				jQuery(this).addClass('rightbar-toggle');
			}
		});
		jQuery("li .custom-html-widget").closest("ul").parent().parent().parent().addClass('rightbar-section');
		if(jQuery('.rightbar-iframe').length == 0){
			if(jQuery(".rightbar-toggle").length > 0) {
				jQuery('<h5 style="color: #ff6600;text-align: left" class="rightbar-iframe">Search and Book</h5>').insertAfter(jQuery(".rightbar-toggle").next("#vmenu"));
				//it doesn't exist
			}else{
				jQuery(".rightbar-section").prepend('<h5 style="color: #ff6600;text-align: left" class="rightbar-iframe ">Search and Book</h5>');					
			}
		}
		jQuery('.rightbar-iframe').next('.wpb_widgetised_column').addClass('bookingiframe');
		setTimeout(function(){
			jQuery('.rightbar-iframe').parent().find('.vc_custom_heading').addClass('rightbar-toggle');
		},1000);
		//jQuery( '<h5 style="color: #ff6600;text-align: left">Search and Book</h5>' ).find('.rightbar-section').insertAfter( "#vmenu" );
		//jQuery("li .custom-html-widget #iframeBookingEngine").closest("ul").parent().parent().parent().prepend('<h5 style="color: #ff6600;text-align: left">Search and Book</h5>');
		
		jQuery('#vmenu').css('display','none');
		
		jQuery('.page-share-buttons').css('display','none'); 
		if(jQuery('#headerShare').length == 0)
			jQuery('#header').append('<ul class="page-share-buttons" id="headerShare"><li><a href="https://www.facebook.com/sharer/sharer.php?u=https://www.easybus.com/en/" target="_blank" title="Facebook" class="symbol-facebook"><span class="symbol"></span></a></li><li><a href="https://twitter.com/intent/tweet?text=http://easybus.ibooking.com/en/routes/london-gatwick/gatwick-airport-heathrow-airport/" target="_blank" title="Twitter" class="symbol-twitter"><span class="symbol"></span></a></li><li><a href="https://plus.google.com/share?url=https://www.easybus.com/en/" target="_blank" title="Google+" class="symbol-googleplus"><span class="symbol"></span></a></li><li><a href="http://pinterest.com/pin/create/button/?url=http://easybus.ibooking.com/en/routes/london-gatwick/gatwick-airport-heathrow-airport/" target="_blank" title="Pinterest" class="symbol-pinterest"><span class="symbol"></span></a></li><li><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.easybus.com/en/" target="_blank" title="LinkedIn" class="symbol-linkedin"><span class="symbol"></span></a></li><li><a href="#" class="page-social-share"><span class="oic-share-outline"></span></a></li></ul>');
		else
			jQuery('#headerShare').css('display','block');
		
	}else{
		jQuery('.page-share-buttons').css('display','block');
		jQuery('#headerShare').css('display','none');  
		jQuery('#iframeBookingEngine').parents('.wpb_widgetised_column').addClass('bookingiframe1');
		console.log(isMobile+'mobile'); 
	}
}

jQuery(document).ready(function() {
	jQuery( window ).resize(function() {
		width = screen.width;
		console.log("Current Device Width : ",width);
	 	manageUIOnMobile();	
	});
	manageUIOnMobile();

	jQuery('.templatera-footer').parent().addClass('footerhide');
});
jQuery(function(){
    jQuery('.rightbar-toggle').on('click',function(){
        jQuery('#vmenu').toggle();
    });
});
jQuery(function(){
    jQuery('.rightbar-iframe').on('click',function(){
        jQuery('.wpb_widgetised_column').toggle();
    });
});
</script>
    
    <?php
		endif;	
		
		if(logipro_ozy_get_theme_mod('back_to_top_button') == '1' && logipro_get_ozy_data('hide_everything_but_content') <= 0 ) {	
	?>
            <div class="logipro-btt-container">
                <div class="top logipro-btt"><img src="<?php echo esc_url(LOGIPRO_OZY_BASE_URL) ?>images/up-arrow.svg" class="svg" alt=""/><img src="<?php echo esc_url(LOGIPRO_OZY_BASE_URL) ?>images/up-arrow-2.svg" class="svg" alt=""/></div>
</div>

    <?php
		}
		
		if(isset($ozyLogiProHelper->footer_html)) echo $ozyLogiProHelper->footer_html;		
		
		echo '</div>'; //body bg wrapper end		

		if(logipro_get_ozy_data('is_animsition_active')) echo '</div><!--.animsition-->';
		
		wp_footer();		
	?>
<script async="async" type="text/javascript">
	jQuery(window).load(function() {
		jQuery('.gm-style-iw').parent().parent().addClass('black-caption');
		});
</script>

<script type="text/javascript">
	function removeEmailList(){
		wpforms-submit-18832
	}
</script>
</body>
</html>
