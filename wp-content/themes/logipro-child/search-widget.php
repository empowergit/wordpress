<link href="<?php echo get_template_directory_uri(); ?>-child/css/datepicker.css" rel=stylesheet>
<link href="<?php echo get_template_directory_uri(); ?>-child/css/jquery-select-step.css" rel=stylesheet>
<link href="<?php echo get_template_directory_uri(); ?>-child/css/easy-autocomplete.min.css" rel=stylesheet>
<link href="<?php echo get_template_directory_uri(); ?>-child/css/easy-autocomplete.themes.min.css" rel=stylesheet>
<style>
    #main>.container,.container{overflow:visible!important;}
    #content .post-content ul{padding-left: 0px!important;}
</style>
<div class="container <?php echo esc_attr(logipro_get_ozy_data('_page_content_css_name')); ?>">
    <div id="content" class="without-sidebar route-page">
    <div class="search-section">
        <div class="ticket-type-btn">
            <input type="radio" id="oneWay" checked="checked" name="way">
            <label for="oneWay">One-Way</label>

            <input type="radio" id="retrun" name="way">
            <label for="retrun">Return</label>
        </div>
        <div class="search-inner">
            <div class="mb-10">
                <label>From</label>
                <input type="text" id="departFrom" class="custom-input" onfocus="OnFocus(event)" value="<?php echo ucwords($string_from); ?>">
            </div>
            <div class="mb-10">
                <label>To</label>
                <input type="text" id="arrivalTo" class="custom-input" onfocus="OnFocus(event)" value="<?php echo ucwords($string_to); ?>">
            </div>
            <div id="departContainer" class="date-time-box">
                <div class="mb-10">
                    <label>Date</label>
                    <input type="text" id="departDate" class="custom-input" data-toggle="datepicker" name="from_date" readonly>
                </div>
                <div class="mb-10">
                    <label>Depart After</label>
                    <select id="departTime" class="custom-input">
                        <option selected="selected" value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </div>
            </div>
            <div id="returnContainer" class="date-time-box d-none">
                <div class="mb-10">
                    <label>Return</label>
                    <input type="text" id="returnDate" class="custom-input" data-toggle="datepicker" name="to_date" readonly>
                </div>
                <div class="mb-10">
                    <label>Depart After</label>
                    <select id="returnTime" class="custom-input">
                        <option selected="selected" value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </div>
            </div>
            <div class="mb-10 seats">
                <label>Seats</label>
                <div class="position-relative">
                    <div id="searchWidgetTicketTypeContainer">
                        <button id="searchWidgetTicketType" class="ticket-type-dropdown" type="button">
                            <i class="fa fa-user user-icon"></i>
                            <span class="ticket-count" id="ticketCount">1</span><i class="fa fa-angle-down down-arrow-icon"></i>
                        </button>
                        <div id="searchWidgetTicketTypeOptionContainer" class="ticket-type-option d-none">
                            <span>Seats</span>
                            <div class="jquery-select-step">
                                <select name="selectTicketType" id="selectTicketType" class="select-step">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="submit" class="btn btn-black" value="SEARCH" onclick="Search()">

        </div>
    </div>
	<input type="hidden" id="idOrigin" name="idOrigin" value="<?php echo ucwords($source_code); ?>">
    <input type="hidden" id="idDestination" name="idDestination" value="<?php echo ucwords($dest_code); ?>">
    <input type="hidden" id="OriginName" name="OriginName" value="<?php echo ucwords($string_from); ?>">
    <input type="hidden" id="destinationName" name="DestinationName" value="<?php echo ucwords($string_to); ?>">

    <script>
        var $ = jQuery;
        
        var tommorow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
        var day = tommorow.getDate()
        var month = tommorow.getMonth() + 1
        var year = tommorow.getFullYear()
        
        var dayAfterTommorow = new Date(new Date().getTime() + 48 * 60 * 60 * 1000);
        var dayT = dayAfterTommorow.getDate()
        var monthT = dayAfterTommorow.getMonth() + 1
        var yearT = dayAfterTommorow.getFullYear()
        

        var traveldate = year + "-" + month + "-" + day;
        var returndate = '';

        var ignoredList = [];
    </script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/slick.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/datepicker.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-select-step.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.easy-autocomplete.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/SearchWidget.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/country.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/findIndex-pollyfill.js"></script>
    <script>
        function call_url(url) {
            console.log('https://bookings.easybus.com/affiliate/api/redirect/save?redirectURL=' + encodeURIComponent(url) + '&provider=distribusion');
            // call api before link call
            jQuery.ajax({
                url: 'https://bookings.easybus.com/affiliate/api/redirect/save?redirectURL=' + encodeURIComponent(url) + '&provider=distribusion',
                type: 'GET',
                success: function(result) {
                    console.log(result);
                },
                error: function(error) {
                    console.log('error' + error);
                }
            });
            window.open(url);
        }

		function Search(){
			var departureDates = $("#departDate").val();
			var departuretime = $("#departTime").val();
			var st1 = departureDates.split("/");
			var strDepartureDates = st1[2] + "" + st1[1] + "" + st1[0];

			var returnDates = $("#returnDate").val();
			var returntime = $("#returnTime").val();
			var st2 = returnDates.split("/");
			var strReturnDates = st2[2] + "" + st2[1] + "" + st2[0];

			var seats = $("#selectTicketType").val();
			var baseLocation = window.location.protocol+'//'+window.location.host+'/en/routes'
			var url = "";
			url = baseLocation+"/easybus-offer-low-cost-airport-transfers-and-city-to-city-bus-routes-in-the-uk-and-europe/?seats=" + seats + "&idOrigin=" + $("#idOrigin").val()
					+ "&originlocation=" + $("#OriginName").val() + "&idDestination=" + $("#idDestination").val() + "&destinationlocation=" + $("#destinationName").val()
					+ "&departureDates=" + strDepartureDates + "&seatTypes=seat:1|students:0|Seniors:0||&departuretime=" + departuretime + "&id_prov=2&lng=english&isReferedByLanding=1";
			var chk = document.getElementById("retrun");
			if (chk.checked == true) {
				url = url + "&isReturn=1&returnDates=" + strReturnDates + "&returntime=" + returntime;
			}

			if(url !== ""){
				window.location.href = url;
			}
		}

		function OnFocus(event){
			event.target.select();
		}
        // datepicker slider
        jQuery(".slick-slider").slick({
            infinite: true,
            slidesToShow: 7,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        // toggle collapse
        jQuery('.pre-spin').css('display', 'flex');
        jQuery(document).ready(function() {
            jQuery('.pre-spin').css('display', 'none');
            jQuery(".set > a").on("click", function() {
                if (jQuery(this).hasClass("active")) {
                    jQuery(this).removeClass("active");
                    jQuery(this)
                        .siblings(".content")
                        .slideUp(200);
                    jQuery(".set > a >i")
                        .removeClass("fa-minus")
                        .addClass("fa-plus");
                } else {
                    jQuery(".set > a >i")
                        .removeClass("fa-minus")
                        .addClass("fa-plus");
                    jQuery(this)
                        .find("i")
                        .removeClass("fa-plus")
                        .addClass("fa-minus");
                    jQuery(".set > a").removeClass("active");
                    jQuery(this).addClass("active");
                    jQuery(".content").slideUp(200);
                    jQuery(this)
                        .siblings(".content")
                        .slideDown(200);
                }
            });
            // search date picker
            jQuery('#departDate').datepicker();
			jQuery('#departDate').datepicker('reset').datepicker('destroy').datepicker({
                autoHide: true,
                format: 'dd/mm/yyyy'
            });
			
			$('#departDate').datepicker('setDate', new Date(year, month-1, day));
			$('#departDate').datepicker('setStartDate', new Date());
			
            jQuery('#returnDate').datepicker();
			jQuery('#returnDate').datepicker('reset').datepicker('destroy').datepicker({
                autoHide: true,
                format: 'dd/mm/yyyy'
            });
			
			$('#returnDate').datepicker('setDate', new Date(yearT, monthT-1, dayT));
			$('#returnDate').datepicker('setStartDate', new Date());

            // currency selector
            jQuery('.sort__btn,.sort').on('click',function() {
                
				jQuery(".curr").addClass('show');
					jQuery('.curr li a').click(function() {
						jQuery('.curr li a').removeClass('sort_select');
						jQuery(this).addClass('sort_select');
						jQuery(".curr").removeClass('show');
						var get_currency = jQuery(".sort_select").text();
						jQuery(".select-curr").text(get_currency);
						jQuery('.pre-spin').css('display', 'flex');
						window.location.href = "?get_currency=" + get_currency;
					});
            });
			$(document).mouseup(function(e) 
			{
				var container = $(".search-filter");

				// if the target of the click isn't the container nor a descendant of the container
				if (!container.is(e.target) && container.has(e.target).length === 0) 
				{
					$('.curr').removeClass('show');
				}
			});
			jQuery.ajax({
                url: 'https://bookings.easybus.com/affiliate/api/search/ignored_stations',
                type: 'GET',
                success: function(result) {
                    ignoredList = result;
                },
                error: function(error) {
                    ignoredList = null;
                }
            });

			$('input[type=radio][name=way]').change(function() {
				$('#returnContainer').toggleClass('d-none');
			});
        });

		var departOptions = {
			minCharNumber:3,
			url: function(phrase) {
				return "//bookings.easybus.com/affiliate/api/search/station?term=" + phrase;
			},
			ajaxCallback: function(stations){
				console.log("\n------------------------------------\nAutocomplete response data : \n", stations, "\n------------------------------------\n");
				var destinationCode = $("#idDestination").val();
				let ignoredStationCodeIndex = ignoredList.code.findIndex(
					(item, index) => {
						return item === destinationCode;
					}
				);
				if (ignoredStationCodeIndex === -1) {
					stations = stations.filter(function(item) {
						return destinationCode !== item.code;
					});
					return stations;
				} else {
					stations = stations.filter(function(item) {
						return !ignoredList.code.includes(item.code);
					});
					return stations;
				}
			},
			getValue: "name",
			template: {
				type: "custom",
				method: function(value, item) {
					return value + " - <span class=\"autocomplete-description\">"+ AlphaCodeTwoCountryName[item.code.substr(0,2)].Country +"</span>";
				}
			},
			list: {
				match: {
					enabled: true
				},
				onSelectItemEvent: function() {
					var data = $("#departFrom").getSelectedItemData();
					$("#idOrigin").val(data.code);
					$("#OriginName").val(data.name);
				}
			}
		};
		
		var arrivalOptions = {
			minCharNumber:3,
			url: function(phrase) {
				return "//bookings.easybus.com/affiliate/api/search/station?term=" + phrase;
			},
			ajaxCallback: function(stations){
				console.log("\n------------------------------------\nAutocomplete response data : \n", stations, "\n------------------------------------\n");
				var departureCode = $("#idOrigin").val();
					
				let ignoredStationCodeIndex = ignoredList.code.findIndex(
					(item, index) => {
						return item === departureCode;
					}
				);
				if (ignoredStationCodeIndex === -1) {
					stations = stations.filter(function(item) {
						return departureCode !== item.code;
					});
					return stations;
				} 
				else {
					stations = stations.filter(function(item) {
						return !ignoredList.code.includes(item.code);
					});
					return stations;
				}
			},
			getValue: "name",
			template: {
				type: "custom",
				method: function(value, item) {
					return value + " - <span class=\"autocomplete-description\">"+ AlphaCodeTwoCountryName[item.code.substr(0,2)].Country +"</span>";
				}
			},
			list: {
				match: {
					enabled: true
				},
				onSelectItemEvent: function() {
					var data = $("#arrivalTo").getSelectedItemData();
					$("#idDestination").val(data.code);
					$("#destinationName").val(data.name);
				}
			}
		};

		$("#departFrom").easyAutocomplete(departOptions);

		$("#arrivalTo").easyAutocomplete(arrivalOptions);

    </script>
</div>

