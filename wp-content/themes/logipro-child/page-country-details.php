<!DOCTYPE html>
<?php
/*
  Template Name: Page : Country Info Page Layout
 */
//get_header();
$country_name = $city_name = "";
$full_query_param = add_query_arg();
$get_params = explode('transfers', $full_query_param);
$get_params = explode('/', $get_params[1]);
$country_name = $get_params[1];
if (!empty($country_name)) {
  $country_name = str_replace("-", " ", $country_name);
}
$city_name = $get_params[2];
if (!empty($city_name)) {
  $city_name = str_replace("-", " ", $city_name);
}

$q_param = explode('routes/', urldecode($full_query_param));
// for country
$q_param_locate_country = explode('/', $q_param[1]);
$q_param_country = $q_param_locate_country[0];
// for location
$q_param_location = explode('-airport', $q_param_locate_country[1]);
$q_param_palce = $q_param_location[0];
// for double word location
$argu = '';
if (strpos($q_param_palce, '-') !== false) {
  $q_param_locate_place = explode('-', $q_param_palce);
  for ($i = 0; $i < count($q_param_locate_place); $i++) {
    $argu .= " " . $q_param_locate_place[$i];
  }
} else {
  $argu = $q_param_palce;
}
// Read JSON file
$json = file_get_contents(get_template_directory_uri() . '-child/currencyCodes.json');
//Decode JSON
$json_data = json_decode($json, true);
$currency_symbol = '';
foreach ($json_data as $curr_data) {
  if (strpos($currency, $curr_data['code']) !== FALSE) {
    $currency_symbol = $curr_data['symbol'];
  }
}
global $wp;
$home_page = home_url(add_query_arg(array(), $wp->request));
$current_page_url = str_replace('/' . ICL_LANGUAGE_CODE . '/', '', home_url(add_query_arg(array(), $wp->request)));
$page_title = ucwords($city_name) . ' Airport Transfers';
//echo $plugin_css_path = plugin_dir_path( __FILE__ ) . '/js_composer/assets/css/js_composer.min.css';
$plugin_css_path = plugins_url('js_composer/assets/css/js_composer.min.css');
global $wpdb;

$uri=$_SERVER["REQUEST_URI"];
$uri_segment=explode("/", $uri);
if(!empty($uri_segment)){
  $country_code=!empty($uri_segment[4])?$uri_segment[4]:"Estonia";
  $country_code=str_replace("%20", " ", $country_code);
                $country_code=str_replace("-", " ", $country_code);      
                $query = "SELECT * FROM country WHERE nicename ='".$country_code."'";
                $res = $wpdb->get_results($query);
                $country_code=@$res[0]->iso;
                $country_code_iso=@$res[0]->iso;
  $sqlTags="SELECT * FROM meta_tags WHERE country='".$country_code."' AND type='country'";  
  $resTags = $wpdb->get_results($sqlTags,ARRAY_A);
  if(!empty($resTags)){
    $meta_title=@$resTags[0]["meta_title"];
    $meta_keywords=@$resTags[0]["meta_keywords"];
    $meta_description=@$resTags[0]["meta_description"];
  }
}                      
?>
<html <?php language_attributes(); ?>>
<head>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <![endif]-->
    <meta charset="<?php esc_attr(bloginfo('charset')); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="format-detection" content="telephone=no">
    
    <?php
      if(!empty($meta_title)){
    ?>
    <title><?php echo $meta_title;?></title>
    <?php }else{?>
    <title>Low-Cost Bus Transfers in <?php echo ucwords($country_name); ?></title>
    <?php }?> 
 
    <?php
      if(!empty($meta_description)){
    ?>
    <meta name="description" content="<?php echo $meta_description;?>">
    <?php }else{?>
    <meta name="description"
        content="easyBus.com provides access to cheap bus tickets from <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?>. Fares from as low as <?php echo $currency_symbol . min($min_price_array); ?>.">
    <?php }?> 

    <?php
      if(!empty($meta_keywords)){
    ?>
    <meta name="keywords" content="<?php echo $meta_keywords; ?>">
    <?php }else{?>
    <meta name="keywords"
        content="bus <?php echo $string_from; ?> to <?php echo $string_to; ?>, <?php echo $string_from; ?>, <?php echo $string_to; ?>, bus times <?php echo $string_from; ?> to <?php echo ucwords($string_to); ?>"> 
    <?php }?>
    
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />

    <link rel="stylesheet" href="<?php echo $plugin_css_path; ?>" />
    <?php remove_action('wp_head', '_wp_render_title_tag', 1);
    wp_head(); /* this is used by many Wordpress features and for plugins to work properly */ ?>
    <style type="text/css">
    .bottom_border {
        border-bottom: 2px solid #E7E7E7;
    }
    #content p,
    #side-extended-content p,
    #request-a-rate p {
        padding-bottom: 30px !important;
    }

    #content {
        color: #363636 !important;
    }
    .source_links {
        display: flex;
        flex-wrap: wrap;
    }
    .source_links li {
        list-style: none;
        width: 49.33%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    @media only screen and (max-width: 600px) {
        .source_links {
            display: block;
            flex-wrap: nowrap;
        }
        .source_links li {
            list-style: none;
            width: 100%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    }
    </style>
</head>
<body <?php body_class(); ?>>
    <?php
    /* Animsition */

    // if (logipro_get_ozy_data('is_animsition_active'))
    //   echo '<div class="animsition">';
    // echo '<div id="body-bg-wrapper">';
    /* Include Primary Menu */
    logipro_ozy_include_primary_menu();
    ?>
    <div class="none">
        <p><a href="#content"><?php esc_attr_e('Skip to Content', 'logipro'); ?></a></p>
    </div>
    <!--.none-->
    <?php
    if (logipro_get_ozy_data('hide_everything_but_content') <= 0):
      $header_slider_class = $footer_slider_class = '';
      logipro_ozy_header_footer_slider_class($header_slider_class, $footer_slider_class);
      ?>

    <div id="main" class="<?php echo esc_attr($footer_slider_class);
      echo esc_attr($header_slider_class); ?>">
        <?php
        logipro_ozy_custom_header();
        ?>
        <?php endif; ?>
        <div class="pre-spin" style="display: none;"><img
                src="<?php echo get_site_url(); ?>/wp-content/uploads/2019/09/loader.gif"></div>

        <div id="page-title-wrapper">
            <div>
                <h1 class="page-title">Our Routes -
                    <?php echo ucwords(!empty($country_name)?$country_name:"Estonia"); ?></h1>
                <h4>Low-Cost Bus Transfers in <?php echo ucwords($country_name); ?></h4>
            </div>
        </div>
        <div id="page-bread-crumbs-share-bar">
            <ul id="page-breadcrumbs" class="content-font">
                <li><a href="<?php echo $home_page; ?>"><i class="oic-home-2"></i></a></li>
                <li><a href="<?php echo $home_page; ?>routes/" title="Our Routes">Our Routes</a></li>
                <li><?php echo ucwords($country_name); ?></li>
            </ul>
            <span class="content-font"><?php esc_attr_e('Share', 'logipro') ?></span>
            <ul class="page-share-buttons">
                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="Facebook" class="symbol-facebook"><span
                            class="symbol">&#xe027;</span></a></li>
                <li><a href="https://twitter.com/intent/tweet?text=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="Twitter" class="symbol-twitter"><span class="symbol">&#xe086;</span></a>
                </li>
                <li><a href="https://plus.google.com/share?url=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="Google+" class="symbol-googleplus"><span
                            class="symbol">&#xe039;</span></a></li>
                <li><a href="https://pinterest.com/pin/create/button/?url=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="Pinterest" class="symbol-pinterest"><span
                            class="symbol">&#xe064;</span></a></li>
                <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $current_page_url . $full_query_param ?>"
                        target="_blank" title="LinkedIn" class="symbol-linkedin"><span
                            class="symbol">&#xe052;</span></a></li>
                <li><a href="#" class="page-social-share"><span class="oic-share-outline"></span></a></li>
            </ul>
        </div>
        <?php
                   $uri=$_SERVER["REQUEST_URI"];
                   $uri_segment=explode("/", $uri);
                   if(!empty($uri_segment)){
                      $country_code=!empty($uri_segment[4])?$uri_segment[4]:"Estonia";
                      $city_code=!empty($uri_segment[5])?$uri_segment[5]:"paris";
                      $city_actual_name=!empty($uri_segment[5])?$uri_segment[5]:"paris";

                $country_code=str_replace("%20", " ", $country_code);
                $country_code=str_replace("-", " ", $country_code);      
                $query = "SELECT * FROM country WHERE nicename ='".$country_code."'";
                $res = $wpdb->get_results($query);
                $country_code=@$res[0]->iso;
                $country_code_iso=@$res[0]->iso;


                $queryRes = "SELECT * FROM city WHERE country_code ='".$country_code."'";
                $result = $wpdb->get_results($queryRes);

$query = "SELECT  a.* FROM featured_airport AS fa"
                . " LEFT JOIN airport AS a ON a.code = fa.featured_airport"
                . " WHERE  fa.country_code='" . $country_code . "' ORDER BY a.sort_order=0,a.sort_order";
$airport_result = $wpdb->get_results($query);
                   }
                 ?>
        <!-- footer content code here -->
        <div class="container <?php echo esc_attr(logipro_get_ozy_data('_page_content_css_name')); ?>">
            <div id="content" class="without-sidebar route-page">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <h4>You can now book low-cost bus travel in
                                            <?php echo ucwords(!empty($country_name)?$country_name:"Estonia"); ?>.
                                            easyBus provides low-cost bus transfers between airports and city centres,
                                            as well as connections between hundreds of cities.</h4>
                                    </div>
                                </div>
                                <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_border_width_2 vc_sep_pos_align_center vc_separator_no_text"
                                    style="margin-top: -14px;"><span class="vc_sep_holder vc_sep_holder_l"><span
                                            style="border-color:#e7e7e7;" class="vc_sep_line"></span></span><span
                                        class="vc_sep_holder vc_sep_holder_r"><span style="border-color:#e7e7e7;"
                                            class="vc_sep_line"></span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-section">
                    <article>
                        <div class="post-content page-content">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <h5 style="color: #ff6600;text-align: left" class="vc_custom_heading" onclick="showhide();">In
                                                This Section...</h5>
                                            <div id="vmenu" class="vc_wp_custommenu wpb_content_element vmenu">
                                                <div class="widget widget_nav_menu">
                                                    <div class="menu-rome-airport-transfers-container">
                                                        <ul id="menu-rome-airport-transfers" class="menu">
                                                           <?php
                                                             if(!empty($airport_result)){
                                                                foreach($airport_result as $values){
                                                                    $airport_name=@$values->name;
                                                                    $airport_name_url=str_replace(" ", "-", $airport_name);
                                                                    $airport_url="/routes/bus-from-".$airport_name_url."-to-".$result[0]->name;
                                                            ?>
                                                           <li id="menu-item-18567" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-18567">
                                                               <a href="<?php echo $airport_url;?>"><?php echo $values->name;?></a>
                                                           </li>                 
                                                            <?php            
                                                                    }
                                                                }
                                                           ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_widgetised_column wpb_content_element">
                                                <div class="wpb_wrapper">
                                                    <ul>
                                                        <?php dynamic_sidebar('IBE_Internal'); ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-bold-black wpb_column vc_column_container vc_col-sm-8">
                                    <?php 
                $query = "SELECT c.*,c.id as cid,s.section,s.id as sid,i.image_path, i.id as image_id FROM content AS c"
                . " LEFT JOIN sections AS s ON s.id = c.section_id"
                . " LEFT JOIN image AS i ON i.content_id = c.id"
                . " WHERE country = '" . $country_code . "' AND city = '' ORDER BY c.id ASC ";
                      $result = $wpdb->get_results($query);
                       if(!empty($result)){?>
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <?php
                               foreach($result as $value){
                                if(!empty($value->image_path)){ 
                             ?>
                                            <div class="wpb_single_image wpb_content_element vc_align_left">
                                                <figure class="wpb_wrapper vc_figure">
                                                    <div
                                                        class="vc_single_image-wrapper vc_box_rounded  vc_box_border_grey">
                                                        <img src="<?php echo $value->image_path;?>"
                                                            alt="<?php echo $value->title;?>"
                                                            sizes="(max-width: 1284px) 100vw, 1284px" data-id="2173"
                                                            width="1284" height="370">
                                                    </div>
                                                </figure>
                                            </div>
                                            <?php }?>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper bottom_border">
                                                    <?php if(!empty($value->title)){?>
                                                    <h4 style="text-align: left" class="vc_custom_heading">
                                                        <?php echo $value->title;?></h4>
                                                    <?php }?>
                                                    <?php if(!empty($value->description)){?>
                                                    <p><?php echo $value->description;?></p>
                                                    <?php }?>
                                                </div>
                                            </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <?php  } ?>
                                    <?php
                          


$query = "SELECT  c.* FROM featured_cities AS fc"
                . " LEFT JOIN city AS c ON c.code = fc.featured_city"
                . " WHERE  fc.country_code='" . $country_code . "' ORDER BY c.sort_order=0,c.sort_order";
$featured_city_result = $wpdb->get_results($query);
                          if(!empty($featured_city_result)){
                            foreach($featured_city_result as $value){
                              $route_details = file_get_contents("https://bookings.easybus.com/affiliate/api/search/landing_page_links?countryCode=".$country_code_iso."&originStationName=".$value->name);
                              if(!empty($route_details)){
                                $route_details=json_decode($route_details,true);
                                  if(!empty($route_details)){
                            ?>

                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p><strong class="popular-colour"
                                                                            style="color: #363636 !important;">Popular
                                                                            Routes departing from
                                                                            <?php echo $value->name;?></strong>
                                                                    </p>
                                                                    <ul class="source_links">
                                                                        <?php
                      foreach($route_details as $val){
                    ?>
                                                                        <li>
                                                                            <a
                                                                                href="/routes/<?php echo $val["LandingPageUrl"];?>">
                                                                                <?php echo $val["Origin"];?> to
                                                                                <?php echo $val["Destination"];?>
                                                                            </a>
                                                                        </li>
                                                                        <?php }?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <p><strong class="popular-colour"
                                                                            style="color: #363636 !important;">Popular
                                                                            Routes to
                                                                            <?php echo $country_details[0]["capital"];?></strong>
                                                                    </p>
                                                                    <ul class="source_links">
                                                                        <?php
                      foreach($route_details as $val){
                        $destination=str_replace(" ", "-", $val["Destination"]);
                        $Origin=str_replace(" ", "-", $val["Origin"]);
                        $route_url="/routes/bus-from-".$destination."-to-".$Origin;
                        
                    ?>
                                                                        <li>
                                                                            <a href="<?php echo $route_url;?>">
                                                                                <?php echo $val["Destination"];?> to
                                                                                <?php echo $val["Origin"];?>
                                                                            </a>
                                                                        </li>
                                                                        <?php }?>
                                                                    </ul>



                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->

                                    <?php
                        }
                        }
                            }
                          } 
                        ?>
                                </div>

                            </div>
                        </div>
                        <!--.post-content .page-content -->
                    </article>
                    <!-- end page content section -->
                </div>
            </div>
        </div>
        <div>
            <script>
            // toggle collapse
            jQuery('.pre-spin').css('display', 'flex');
            jQuery(document).ready(function() {
                jQuery('.pre-spin').css('display', 'none');
            });
            </script>
        </div>
        <?php if (logipro_get_ozy_data('hide_everything_but_content') < 1): ?>
        <div class="clear"></div>
    </div>
    <!--.container-->
    <div class="footer-tagline-container">
        <div id="custom-footer-tagline" class="no-sidebar has-title custom-footer">
            <?php dynamic_sidebar('Footer_Tagline'); ?>
        </div>
    </div>
    <?php
      /* footer widget bar and footer */
      include(get_stylesheet_directory() . '/include/footer-bars.php');
      ?>
    <!-- </div> -->
    <!--#main-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ClientJS/0.1.11/client.min.js"></script>
    <script type="text/javascript">
    var width = screen.width;

    function manageUIOnMobile() {
        jQuery(
                '<a href="https://easy.com" id="family-logo" target="_blank"><img id="family-logo-default" src="/wp-content/uploads/2019/01/part_of_the_easy_family_of_brands-300x42.png" alt="logo"></a>')
            .insertAfter("#top-menu #logo");
        var isMobile = '';
        //var client = new ClientJS();
        isMobile = (width < 768); //client.isMobile();
        if (isMobile == true && !jQuery('body').hasClass('home') && !jQuery('body').hasClass('page-id-13394')) {
            //jQuery('h5:contains("In This Section...")').parent().addClass('rightbar-section');
            //jQuery('h5:contains("In This Section...")').addClass('rightbar-toggle');
            jQuery('h5:contains("In this Section...")').addClass('rightbar-toggle');
            jQuery('h4:contains("In This Section...")').addClass('rightbar-toggle');
            jQuery("h5,h4").each(function(index, ele) {
                if (jQuery(this).html() === 'In This Section...') {
                    jQuery(this).addClass('rightbar-toggle');
                }
            });
            jQuery("li .custom-html-widget").closest("ul").parent().parent().parent().addClass('rightbar-section');
            if (jQuery('.rightbar-iframe').length == 0) {
                if (jQuery(".rightbar-toggle").length > 0) {
                    jQuery('<h5 style="color: #ff6600;text-align: left" class="rightbar-iframe">Search and Book</h5>')
                        .insertAfter(jQuery(".rightbar-toggle").next("#vmenu"));
                    //it doesn't exist
                } else {
                    jQuery(".rightbar-section").prepend(
                        '<h5 style="color: #ff6600;text-align: left" class="rightbar-iframe ">Search and Book</h5>');
                }
            }
            jQuery('.rightbar-iframe').next('.wpb_widgetised_column').addClass('bookingiframe');
            setTimeout(function() {
                jQuery('.rightbar-iframe').parent().find('.vc_custom_heading').addClass('rightbar-toggle');
            }, 1000);
            //jQuery( '<h5 style="color: #ff6600;text-align: left">Search and Book</h5>' ).find('.rightbar-section').insertAfter( "#vmenu" );
            //jQuery("li .custom-html-widget #iframeBookingEngine").closest("ul").parent().parent().parent().prepend('<h5 style="color: #ff6600;text-align: left">Search and Book</h5>');
            jQuery('#vmenu').css('display', 'none');
            jQuery('.page-share-buttons').css('display', 'none');
            if (jQuery('#headerShare').length == 0)
                jQuery('#header').append(
                    '<ul class="page-share-buttons" id="headerShare"><li><a href="https://www.facebook.com/sharer/sharer.php?u=https://www.easybus.com/en/" target="_blank" title="Facebook" class="symbol-facebook"><span class="symbol"></span></a></li><li><a href="https://twitter.com/intent/tweet?text=http://easybus.ibooking.com/en/routes/london-gatwick/gatwick-airport-heathrow-airport/" target="_blank" title="Twitter" class="symbol-twitter"><span class="symbol"></span></a></li><li><a href="https://plus.google.com/share?url=https://www.easybus.com/en/" target="_blank" title="Google+" class="symbol-googleplus"><span class="symbol"></span></a></li><li><a href="http://pinterest.com/pin/create/button/?url=http://easybus.ibooking.com/en/routes/london-gatwick/gatwick-airport-heathrow-airport/" target="_blank" title="Pinterest" class="symbol-pinterest"><span class="symbol"></span></a></li><li><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.easybus.com/en/" target="_blank" title="LinkedIn" class="symbol-linkedin"><span class="symbol"></span></a></li><li><a href="#" class="page-social-share"><span class="oic-share-outline"></span></a></li></ul>'
                    );
            else
                jQuery('#headerShare').css('display', 'block');
        } else {
            jQuery('.page-share-buttons').css('display', 'block');
            jQuery('#headerShare').css('display', 'none');
            jQuery('#iframeBookingEngine').parents('.wpb_widgetised_column').addClass('bookingiframe1');
            console.log(isMobile + 'mobile');
        }
    }

    jQuery(document).ready(function() {
        jQuery(window).resize(function() {
            width = screen.width;
            console.log("Current Device Width : ", width);
            manageUIOnMobile();
        });
        manageUIOnMobile();
        jQuery('.templatera-footer').parent().addClass('footerhide');
        var originCountryCode = $("#idOrigin").val();
        var destinationCountryCode = $("#idDestination").val();
        var selectedOriginName = $("#OriginName").val();
        var selectedDestinationName = $("#destinationName").val();
        if (originCountryCode && originCountryCode != '') {
            originCountryCode = originCountryCode.substring(0, 2);
        }
        if (destinationCountryCode && destinationCountryCode != '') {
            destinationCountryCode = destinationCountryCode.substring(0, 2);
        }
        var originRouteList = $('#originRouteList');
        $('#originRouteList').html('');
        var departureRouteList = $('#departureRouteList');
        $('#departureRouteList').html('');
    });
    jQuery(function() {
        /*jQuery('.rightbar-toggle').on('click', function () {
          jQuery('#vmenu').toggle();
        });*/
    });
    jQuery(function() {
        /*jQuery('.rightbar-iframe').on('click', function () {
          jQuery('.wpb_widgetised_column').toggle();
        });*/
    });
    </script>
    <?php
    endif;

    if (logipro_ozy_get_theme_mod('back_to_top_button') == '1' && logipro_get_ozy_data('hide_everything_but_content') <= 0) {
      ?>
    <div class="logipro-btt-container">
        <div class="top logipro-btt"><img src="<?php echo esc_url(LOGIPRO_OZY_BASE_URL) ?>images/up-arrow.svg"
                class="svg" alt="" /><img src="<?php echo esc_url(LOGIPRO_OZY_BASE_URL) ?>images/up-arrow-2.svg"
                class="svg" alt="" /></div>
    </div>
    <?php
    }

    // if (isset($ozyLogiProHelper->footer_html))
    //   echo $ozyLogiProHelper->footer_html;

    // echo '</div>'; //body bg wrapper end
    // if (logipro_get_ozy_data('is_animsition_active'))
    //   echo '</div><!--.animsition-->';
    wp_footer();
    ?>
    <script async="async" type="text/javascript">
    jQuery(window).load(function() {
        jQuery('.gm-style-iw').parent().parent().addClass('black-caption');
    });
    </script>
    <style>
    body.admin-bar #header {
        margin-top: 0 !important;
    }
    </style>
    <?php
get_footer();
?>
<script type="text/javascript">
/*jQuery(window).load(function() {
    jQuery('.rightbar-toggle').on('click', function() {
        console.log("links-bar");



  var x = document.getElementById("vmenu");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }



    });
});*/

function showhide(){
   var x = document.getElementById("vmenu");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  } 
}

jQuery(window).load(function() {
    jQuery('.rightbar-iframe').on('click', function() {

        console.log("search-bar");

        if ($('.wpb_widgetised_column:visible').length == 0) {
            jQuery('.wpb_widgetised_column').hide();
        } else {
            jQuery('.wpb_widgetised_column').show();
        }

    });
});
</script>