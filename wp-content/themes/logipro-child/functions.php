<?php 
function logipro_child_enqueue_styles() {	
	// enqueue parent style.css
	wp_enqueue_style('logipro-style', get_template_directory_uri() .'/style.css');	

	// enqueue parent styles from logipro.css
	wp_enqueue_style('logipro-main-style', get_template_directory_uri() .'/logipro.css', array('logipro-style'));	
	
	// enqueue child styles
	wp_enqueue_style('logipro-child', get_stylesheet_directory_uri() .'/style.css', array('logipro-main-style'));	
	
}
add_action('wp_enqueue_scripts', 'logipro_child_enqueue_styles');
/* custom PHP functions below this line */
//include(get_stylesheet_directory() . '/functions/functions.php');
//
///*function to add async to all scripts 07 feb 2019*/
/*function js_async_attr($tag){
# Add async to all remaining scripts
return str_replace( ' src', ' async="async" src', $tag );
}
add_filter( 'script_loader_tag', 'js_async_attr', 10 );*/
function gioga_add_async_attribute($tag, $handle) {
	if ( 'gmwd_map-js' !== $handle )
	return $tag;
	return str_replace( ' src', ' async="async" defer="defer" src', $tag );
}
add_filter('script_loader_tag', 'gioga_add_async_attribute', 10, 2);

add_filter( 'script_loader_tag', function ( $tag, $handle ) {

    if ( 'layerslider-greensock' !== $handle){
        return $tag;}

    return str_replace( ' src', ' async="async" defer="defer"  src', $tag );
}, 10, 2 );
/* custom theme call start*/
add_action('init', function() {

    $current_url=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    if(!empty($_GET["custmkid"])){
      global $wpdb;

      $word="contact?custmkid";
      $word2="unsubscribes?custmkid";
      if(strpos($current_url, $word) !== false){
         $url=get_home_url()."unsubscribes?custmkid=".$_GET["custmkid"];
         wp_redirect( $url );
         exit;
      }
      else if(strpos($current_url, $word2) !== false){
        $data_array=array('request_url'=>$current_url);
        $wpdb->insert("marketing_info",$data_array);
        $load = locate_template('page-unsubscribe.php', true);
        if ($load) {
            exit(); // just exit if template was found and loaded
        }
      }
      else{
        $data_array=array('request_url'=>$current_url);
        $wpdb->insert("marketing_info",$data_array);
      }
    }
    
    $url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/');
    $get_param = explode('/', $url_path);

    // echo "\n ".$url_path; 
    
    $argu ='';
    for($i=0; $i<count($get_param); $i++){
        $argu .= " " .$get_param[$i]; 
    }
    // echo "\n argu ".$argu; 

    if ((in_array('routes', $get_param) && (strpos($url_path, 'routes/bus-from-') !== FALSE)) ){
        // load the file if exists
        $load = locate_template('page-route-info.php', true);
        if ($load) {
            exit(); // just exit if template was found and loaded
        }
    }

    
    /*$mystring=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $current_url= explode('/', $mystring);
    $current_url=array_filter($current_url);
    if(strpos($mystring, "transfers") !== false){
      $current_url=array_filter($current_url);
      $counter=count($current_url);
      if($counter=="6"){
            $ifTemplateFound = locate_template('page-country-city-details.php', true);
            if ($ifTemplateFound) {
                 exit(); // just exit if template was found and loaded
            }
      }else{
                $ifTemplateFound = locate_template('page-country-details.php', true);
                if ($ifTemplateFound) {
                     exit(); // just exit if template was found and loaded
                }
      }
    }*/
    //echo count($current_url);die;
    
    $current_url=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $word="/routes/transfers/";
    if(strpos($current_url, $word) !== false){
       $current_url= explode('/', $current_url);
       $current_url=array_filter($current_url);
       $counter=count($current_url);
          if($counter=="6"){
            $ifTemplateFound = locate_template('page-country-city-details.php', true);
            if ($ifTemplateFound) {
                 exit(); // just exit if template was found and loaded
            }
          }else{
                    $ifTemplateFound = locate_template('page-country-details.php', true);
                    if ($ifTemplateFound) {
                         exit(); // just exit if template was found and loaded
                    }
          }
    }


    $current_url= explode('transfers', $current_url);
    /*$mystring=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    if(strpos($mystring, 'transfers') !== false){
      $current_url= explode('/', $mystring);
      if(!empty($current_url[7])){ 
         $ifTemplateFound = locate_template('page-country-city-details.php', true);
            if ($ifTemplateFound) {
                 exit(); // just exit if template was found and loaded
         }
      }else{
         $ifTemplateFound = locate_template('page-country-details.php', true);
                if ($ifTemplateFound) {
                     exit(); // just exit if template was found and loaded
                }       
      }
    }*/
    /*if(isset($current_url[1])){
        $count_current_url_param = explode('/', $current_url[1]);
        $count_param = count($count_current_url_param);
        
        if($count_param == 3 && isset($count_current_url_param[2]) && !empty($count_current_url_param[2])){
            $ifTemplateFound = locate_template('page-country-city-details.php', true);
            if ($ifTemplateFound) {
                 exit(); // just exit if template was found and loaded
            }
        }else{
            if($count_param == 3 && empty($count_current_url_param[2])){
                $ifTemplateFound = locate_template('page-country-details.php', true);
                if ($ifTemplateFound) {
                     exit(); // just exit if template was found and loaded
                } 
           }else{
                $current_url= explode('transfers', $current_url);
                         
           } 
        }
    }*/
});

/* custom theme call end*/

function shortcodeSearchWidget() {
    //$load =  get_template_directory_uri() . '-child/search-widget.php';
    //require $load;

    ob_start();
    get_template_part('search-widget');
    return ob_get_clean();   
}
add_shortcode('partner_search_widget', 'shortcodeSearchWidget');

add_action("wp_ajax_remove_email", "remove_email");
add_action("wp_ajax_nopriv_remove_email", "remove_email");
function remove_email(){
    $custmkid=$_POST["custmkid"];
    $email_address=$_POST["email_address"];
    global $wpdb;

    $unsubscribe_list = $wpdb->get_results( "SELECT * FROM unsubscribe_list where email='".$email_address."'");
    if(empty($unsubscribe_list)){
      $data_array=array('email'=>$email_address,'custmkid'=>$custmkid);
      $wpdb->insert("unsubscribe_list",$data_array);
      echo "success";
      die;
    }
}



?>

            
       
<?php  

?>