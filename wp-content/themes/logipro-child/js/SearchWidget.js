$(function () {
    $(document).click(function (e) {
        // check that your clicked
        // element has no id=info
        // and is not child of info
        if (e.target.id != "searchWidgetTicketTypeContainer" && !$("#searchWidgetTicketTypeContainer").find(e.target).length) {
            if (!$("#searchWidgetTicketTypeOptionContainer").hasClass("d-none"))
                $("#searchWidgetTicketTypeOptionContainer").addClass("d-none");
        }
        
        if (e.target.id != "eac-container-departFrom" && e.target.id != "eac-container-arrivalTo" && e.target.id != "departFrom" && e.target.id != "arrivalTo" && !$("#eac-container-departFrom").find(e.target).length && !$("#eac-container-arrivalTo").find(e.target).length) {
            if ($("#eac-container-departFrom > ul").attr('style') && $("#eac-container-departFrom > ul").attr('style').indexOf('block') > 0)
                $("#eac-container-departFrom > ul").attr('style', 'display:none;');

            if ($("#eac-container-arrivalTo > ul").attr('style') && $("#eac-container-arrivalTo > ul").attr('style').indexOf('block') > 0)
                $("#eac-container-arrivalTo > ul").attr('style', 'display:none;');
        }
        e.stopPropagation();
    });

    $("#searchWidgetTicketType").on("click", function (event) {
        event.preventDefault();
        if ($("#searchWidgetTicketTypeOptionContainer").hasClass("d-none"))
            $("#searchWidgetTicketTypeOptionContainer").removeClass("d-none");
        event.stopPropagation();
    });

    $(".select-step").selectStep({
        incrementLabel: "+",
        decrementLabel: "-",
        onChange: function (value) {
            $("#ticketCount").text(value.value);
        }
    });

    $("#departDate").datepicker({
        showOn: "both",
        buttonText: "<i class='fa fa-calendar' style='font-size:33px;'></i>",
        dateFormat: "dd/mm/yy",
        minDate: 0
    });
    $("#departDate").val(traveldate);

    $("#returnDate").datepicker({
        showOn: "both",
        buttonText: "<i class='fa fa-calendar' style='font-size:33px;'></i>",
        dateFormat: "dd/mm/yy",
        minDate: 0
    });
    $("#returnDate").val(returndate);

    $("#selectReturn").on("change", function () {
        if ($(this).is(":checked")) {
            if ($("#returnDateTimeContainer").hasClass("d-none"))
                $("#returnDateTimeContainer").removeClass("d-none");
        } else $("#returnDateTimeContainer").addClass("d-none");
    });

    $("#search").on("click", function () {
        var departureDate = $("#departDate").val();
        var departureTime = $("#departTime option:selected").text();
        var arrivalDate = $("#returnDate").val();
        var arrivalTime = $("#returnTime option:selected").text();
        var departureDateTime = moment(
            departureDate + " " + departureTime,
            "DD/MM/YYYY HH:mm"
        );
        var arrivalDateTime = moment(
            arrivalDate + " " + arrivalTime,
            "DD/MM/YYYY HH:mm"
        );

        if (!departureDateTime.isAfter(arrivalDateTime)) {
            //Proceed to complete booking....
        } else {
            //Show error
        }
    });
});
