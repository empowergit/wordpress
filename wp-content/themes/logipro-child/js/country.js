var AlphaCodeTwoCountryName = {
    AF: {
        Flag: '<div class="flag" id="AFG"></div>',
        Country: "Afghanistan",
        AlphaTwo: "AF",
        AlphaThree: "AFG",
        UNCode: "004"
    },
    AX: {
        Flag: '<div class="flag" id="ALA"></div>',
        Country: "Aland Islands",
        AlphaTwo: "AX",
        AlphaThree: "ALA",
        UNCode: "248"
    },
    AL: {
        Flag: '<div class="flag" id="ALB"></div>',
        Country: "Albania",
        AlphaTwo: "AL",
        AlphaThree: "ALB",
        UNCode: "008"
    },
    DZ: {
        Flag: '<div class="flag" id="DZA"></div>',
        Country: "Algeria",
        AlphaTwo: "DZ",
        AlphaThree: "DZA",
        UNCode: "012"
    },
    AS: {
        Flag: '<div class="flag" id="ASM"></div>',
        Country: "American Samoa",
        AlphaTwo: "AS",
        AlphaThree: "ASM",
        UNCode: "016"
    },
    AD: {
        Flag: '<div class="flag" id="AND"></div>',
        Country: "Andorra",
        AlphaTwo: "AD",
        AlphaThree: "AND",
        UNCode: "020"
    },
    AO: {
        Flag: '<div class="flag" id="AGO"></div>',
        Country: "Angola",
        AlphaTwo: "AO",
        AlphaThree: "AGO",
        UNCode: "024"
    },
    AI: {
        Flag: '<div class="flag" id="AIA"></div>',
        Country: "Anguilla",
        AlphaTwo: "AI",
        AlphaThree: "AIA",
        UNCode: "660"
    },
    AQ: {
        Flag: '<div class="flag" id="ATA"></div>',
        Country: "Antarctica",
        AlphaTwo: "AQ",
        AlphaThree: "ATA",
        UNCode: "010"
    },
    AG: {
        Flag: '<div class="flag" id="ATG"></div>',
        Country: "Antigua and Barbuda",
        AlphaTwo: "AG",
        AlphaThree: "ATG",
        UNCode: "028"
    },
    AR: {
        Flag: '<div class="flag" id="ARG"></div>',
        Country: "Argentina",
        AlphaTwo: "AR",
        AlphaThree: "ARG",
        UNCode: "032"
    },
    AM: {
        Flag: '<div class="flag" id="ARM"></div>',
        Country: "Armenia",
        AlphaTwo: "AM",
        AlphaThree: "ARM",
        UNCode: "051"
    },
    AW: {
        Flag: '<div class="flag" id="ABW"></div>',
        Country: "Aruba",
        AlphaTwo: "AW",
        AlphaThree: "ABW",
        UNCode: "533"
    },
    AU: {
        Flag: '<div class="flag" id="AUS"></div>',
        Country: "Australia",
        AlphaTwo: "AU",
        AlphaThree: "AUS",
        UNCode: "036"
    },
    AT: {
        Flag: '<div class="flag" id="AUT"></div>',
        Country: "Austria",
        AlphaTwo: "AT",
        AlphaThree: "AUT",
        UNCode: "040"
    },
    AZ: {
        Flag: '<div class="flag" id="AZE"></div>',
        Country: "Azerbaijan",
        AlphaTwo: "AZ",
        AlphaThree: "AZE",
        UNCode: "031"
    },
    BS: {
        Flag: '<div class="flag" id="BHS"></div>',
        Country: "Bahamas",
        AlphaTwo: "BS",
        AlphaThree: "BHS",
        UNCode: "044"
    },
    BH: {
        Flag: '<div class="flag" id="BHR"></div>',
        Country: "Bahrain",
        AlphaTwo: "BH",
        AlphaThree: "BHR",
        UNCode: "048"
    },
    BD: {
        Flag: '<div class="flag" id="BGD"></div>',
        Country: "Bangladesh",
        AlphaTwo: "BD",
        AlphaThree: "BGD",
        UNCode: "050"
    },
    BB: {
        Flag: '<div class="flag" id="BRB"></div>',
        Country: "Barbados",
        AlphaTwo: "BB",
        AlphaThree: "BRB",
        UNCode: "052"
    },
    BY: {
        Flag: '<div class="flag" id="BLR"></div>',
        Country: "Belarus",
        AlphaTwo: "BY",
        AlphaThree: "BLR",
        UNCode: "112"
    },
    BE: {
        Flag: '<div class="flag" id="BEL"></div>',
        Country: "Belgium",
        AlphaTwo: "BE",
        AlphaThree: "BEL",
        UNCode: "056"
    },
    BZ: {
        Flag: '<div class="flag" id="BLZ"></div>',
        Country: "Belize",
        AlphaTwo: "BZ",
        AlphaThree: "BLZ",
        UNCode: "084"
    },
    BJ: {
        Flag: '<div class="flag" id="BEN"></div>',
        Country: "Benin",
        AlphaTwo: "BJ",
        AlphaThree: "BEN",
        UNCode: "204"
    },
    BM: {
        Flag: '<div class="flag" id="BMU"></div>',
        Country: "Bermuda",
        AlphaTwo: "BM",
        AlphaThree: "BMU",
        UNCode: "060"
    },
    BT: {
        Flag: '<div class="flag" id="BTN"></div>',
        Country: "Bhutan",
        AlphaTwo: "BT",
        AlphaThree: "BTN",
        UNCode: "064"
    },
    BO: {
        Flag: '<div class="flag" id="BOL"></div>',
        Country: "Bolivia",
        AlphaTwo: "BO",
        AlphaThree: "BOL",
        UNCode: "068"
    },
    BA: {
        Flag: '<div class="flag" id="BIH"></div>',
        Country: "Bosnia and Herzegovina",
        AlphaTwo: "BA",
        AlphaThree: "BIH",
        UNCode: "070"
    },
    BW: {
        Flag: '<div class="flag" id="BWA"></div>',
        Country: "Botswana",
        AlphaTwo: "BW",
        AlphaThree: "BWA",
        UNCode: "072"
    },
    BV: {
        Flag: '<div class="flag" id="BVT"></div>',
        Country: "Bouvet Island",
        AlphaTwo: "BV",
        AlphaThree: "BVT",
        UNCode: "074"
    },
    BR: {
        Flag: '<div class="flag" id="BRA"></div>',
        Country: "Brazil",
        AlphaTwo: "BR",
        AlphaThree: "BRA",
        UNCode: "076"
    },
    VG: {
        Flag: '<div class="flag" id="VGB"></div>',
        Country: "British Virgin Islands",
        AlphaTwo: "VG",
        AlphaThree: "VGB",
        UNCode: "092"
    },
    IO: {
        Flag: '<div class="flag" id="IOT"></div>',
        Country: "British Indian Ocean Territory",
        AlphaTwo: "IO",
        AlphaThree: "IOT",
        UNCode: "086"
    },
    BN: {
        Flag: '<div class="flag" id="BRN"></div>',
        Country: "Brunei Darussalam",
        AlphaTwo: "BN",
        AlphaThree: "BRN",
        UNCode: "096"
    },
    BG: {
        Flag: '<div class="flag" id="BGR"></div>',
        Country: "Bulgaria",
        AlphaTwo: "BG",
        AlphaThree: "BGR",
        UNCode: "100"
    },
    BF: {
        Flag: '<div class="flag" id="BFA"></div>',
        Country: "Burkina Faso",
        AlphaTwo: "BF",
        AlphaThree: "BFA",
        UNCode: "854"
    },
    BI: {
        Flag: '<div class="flag" id="BDI"></div>',
        Country: "Burundi",
        AlphaTwo: "BI",
        AlphaThree: "BDI",
        UNCode: "108"
    },
    KH: {
        Flag: '<div class="flag" id="KHM"></div>',
        Country: "Cambodia",
        AlphaTwo: "KH",
        AlphaThree: "KHM",
        UNCode: "116"
    },
    CM: {
        Flag: '<div class="flag" id="CMR"></div>',
        Country: "Cameroon",
        AlphaTwo: "CM",
        AlphaThree: "CMR",
        UNCode: "120"
    },
    CA: {
        Flag: '<div class="flag" id="CAN"></div>',
        Country: "Canada",
        AlphaTwo: "CA",
        AlphaThree: "CAN",
        UNCode: "124"
    },
    CV: {
        Flag: '<div class="flag" id="CPV"></div>',
        Country: "Cape Verde",
        AlphaTwo: "CV",
        AlphaThree: "CPV",
        UNCode: "132"
    },
    KY: {
        Flag: '<div class="flag" id="CYM"></div>',
        Country: "Cayman Islands",
        AlphaTwo: "KY",
        AlphaThree: "CYM",
        UNCode: "136"
    },
    CF: {
        Flag: '<div class="flag" id="CAF"></div>',
        Country: "Central African Republic",
        AlphaTwo: "CF",
        AlphaThree: "CAF",
        UNCode: "140"
    },
    TD: {
        Flag: '<div class="flag" id="TCD"></div>',
        Country: "Chad",
        AlphaTwo: "TD",
        AlphaThree: "TCD",
        UNCode: "148"
    },
    CL: {
        Flag: '<div class="flag" id="CHL"></div>',
        Country: "Chile",
        AlphaTwo: "CL",
        AlphaThree: "CHL",
        UNCode: "152"
    },
    CN: {
        Flag: '<div class="flag" id="CHN"></div>',
        Country: "China",
        AlphaTwo: "CN",
        AlphaThree: "CHN",
        UNCode: "156"
    },
    HK: {
        Flag: '<div class="flag" id="HKG"></div>',
        Country: "Hong Kong, SAR China",
        AlphaTwo: "HK",
        AlphaThree: "HKG",
        UNCode: "344"
    },
    MO: {
        Flag: '<div class="flag" id="MAC"></div>',
        Country: "Macao, SAR China",
        AlphaTwo: "MO",
        AlphaThree: "MAC",
        UNCode: "446"
    },
    CX: {
        Flag: '<div class="flag" id="CXR"></div>',
        Country: "Christmas Island",
        AlphaTwo: "CX",
        AlphaThree: "CXR",
        UNCode: "162"
    },
    CC: {
        Flag: '<div class="flag" id="CCK"></div>',
        Country: "Cocos (Keeling) Islands",
        AlphaTwo: "CC",
        AlphaThree: "CCK",
        UNCode: "166"
    },
    CO: {
        Flag: '<div class="flag" id="COL"></div>',
        Country: "Colombia",
        AlphaTwo: "CO",
        AlphaThree: "COL",
        UNCode: "170"
    },
    KM: {
        Flag: '<div class="flag" id="COM"></div>',
        Country: "Comoros",
        AlphaTwo: "KM",
        AlphaThree: "COM",
        UNCode: "174"
    },
    CG: {
        Flag: '<div class="flag" id="COG"></div>',
        Country: "Congo (Brazzaville)",
        AlphaTwo: "CG",
        AlphaThree: "COG",
        UNCode: "178"
    },
    CD: {
        Flag: '<div class="flag" id="COD"></div>',
        Country: "Congo, (Kinshasa)",
        AlphaTwo: "CD",
        AlphaThree: "COD",
        UNCode: "180"
    },
    CK: {
        Flag: '<div class="flag" id="COK"></div>',
        Country: "Cook Islands",
        AlphaTwo: "CK",
        AlphaThree: "COK",
        UNCode: "184"
    },
    CR: {
        Flag: '<div class="flag" id="CRI"></div>',
        Country: "Costa Rica",
        AlphaTwo: "CR",
        AlphaThree: "CRI",
        UNCode: "188"
    },
    CI: {
        Flag: '<div class="flag" id="CIV"></div>',
        Country: "C�te d'Ivoire",
        AlphaTwo: "CI",
        AlphaThree: "CIV",
        UNCode: "384"
    },
    HR: {
        Flag: '<div class="flag" id="HRV"></div>',
        Country: "Croatia",
        AlphaTwo: "HR",
        AlphaThree: "HRV",
        UNCode: "191"
    },
    CU: {
        Flag: '<div class="flag" id="CUB"></div>',
        Country: "Cuba",
        AlphaTwo: "CU",
        AlphaThree: "CUB",
        UNCode: "192"
    },
    CY: {
        Flag: '<div class="flag" id="CYP"></div>',
        Country: "Cyprus",
        AlphaTwo: "CY",
        AlphaThree: "CYP",
        UNCode: "196"
    },
    CZ: {
        Flag: '<div class="flag" id="CZE"></div>',
        Country: "Czech Republic",
        AlphaTwo: "CZ",
        AlphaThree: "CZE",
        UNCode: "203"
    },
    DK: {
        Flag: '<div class="flag" id="DNK"></div>',
        Country: "Denmark",
        AlphaTwo: "DK",
        AlphaThree: "DNK",
        UNCode: "208"
    },
    DJ: {
        Flag: '<div class="flag" id="DJI"></div>',
        Country: "Djibouti",
        AlphaTwo: "DJ",
        AlphaThree: "DJI",
        UNCode: "262"
    },
    DM: {
        Flag: '<div class="flag" id="DMA"></div>',
        Country: "Dominica",
        AlphaTwo: "DM",
        AlphaThree: "DMA",
        UNCode: "212"
    },
    DO: {
        Flag: '<div class="flag" id="DOM"></div>',
        Country: "Dominican Republic",
        AlphaTwo: "DO",
        AlphaThree: "DOM",
        UNCode: "214"
    },
    EC: {
        Flag: '<div class="flag" id="ECU"></div>',
        Country: "Ecuador",
        AlphaTwo: "EC",
        AlphaThree: "ECU",
        UNCode: "218"
    },
    EG: {
        Flag: '<div class="flag" id="EGY"></div>',
        Country: "Egypt",
        AlphaTwo: "EG",
        AlphaThree: "EGY",
        UNCode: "818"
    },
    SV: {
        Flag: '<div class="flag" id="SLV"></div>',
        Country: "El Salvador",
        AlphaTwo: "SV",
        AlphaThree: "SLV",
        UNCode: "222"
    },
    GQ: {
        Flag: '<div class="flag" id="GNQ"></div>',
        Country: "Equatorial Guinea",
        AlphaTwo: "GQ",
        AlphaThree: "GNQ",
        UNCode: "226"
    },
    ER: {
        Flag: '<div class="flag" id="ERI"></div>',
        Country: "Eritrea",
        AlphaTwo: "ER",
        AlphaThree: "ERI",
        UNCode: "232"
    },
    EE: {
        Flag: '<div class="flag" id="EST"></div>',
        Country: "Estonia",
        AlphaTwo: "EE",
        AlphaThree: "EST",
        UNCode: "233"
    },
    ET: {
        Flag: '<div class="flag" id="ETH"></div>',
        Country: "Ethiopia",
        AlphaTwo: "ET",
        AlphaThree: "ETH",
        UNCode: "231"
    },
    FK: {
        Flag: '<div class="flag" id="FLK"></div>',
        Country: "Falkland Islands (Malvinas)",
        AlphaTwo: "FK",
        AlphaThree: "FLK",
        UNCode: "238"
    },
    FO: {
        Flag: '<div class="flag" id="FRO"></div>',
        Country: "Faroe Islands",
        AlphaTwo: "FO",
        AlphaThree: "FRO",
        UNCode: "234"
    },
    FJ: {
        Flag: '<div class="flag" id="FJI"></div>',
        Country: "Fiji",
        AlphaTwo: "FJ",
        AlphaThree: "FJI",
        UNCode: "242"
    },
    FI: {
        Flag: '<div class="flag" id="FIN"></div>',
        Country: "Finland",
        AlphaTwo: "FI",
        AlphaThree: "FIN",
        UNCode: "246"
    },
    FR: {
        Flag: '<div class="flag" id="FRA"></div>',
        Country: "France",
        AlphaTwo: "FR",
        AlphaThree: "FRA",
        UNCode: "250"
    },
    GF: {
        Flag: '<div class="flag" id="GUF"></div>',
        Country: "French Guiana",
        AlphaTwo: "GF",
        AlphaThree: "GUF",
        UNCode: "254"
    },
    PF: {
        Flag: '<div class="flag" id="PYF"></div>',
        Country: "French Polynesia",
        AlphaTwo: "PF",
        AlphaThree: "PYF",
        UNCode: "258"
    },
    TF: {
        Flag: '<div class="flag" id="ATF"></div>',
        Country: "French Southern Territories",
        AlphaTwo: "TF",
        AlphaThree: "ATF",
        UNCode: "260"
    },
    GA: {
        Flag: '<div class="flag" id="GAB"></div>',
        Country: "Gabon",
        AlphaTwo: "GA",
        AlphaThree: "GAB",
        UNCode: "266"
    },
    GM: {
        Flag: '<div class="flag" id="GMB"></div>',
        Country: "Gambia",
        AlphaTwo: "GM",
        AlphaThree: "GMB",
        UNCode: "270"
    },
    GE: {
        Flag: '<div class="flag" id="GEO"></div>',
        Country: "Georgia",
        AlphaTwo: "GE",
        AlphaThree: "GEO",
        UNCode: "268"
    },
    DE: {
        Flag: '<div class="flag" id="DEU"></div>',
        Country: "Germany",
        AlphaTwo: "DE",
        AlphaThree: "DEU",
        UNCode: "276"
    },
    GH: {
        Flag: '<div class="flag" id="GHA"></div>',
        Country: "Ghana",
        AlphaTwo: "GH",
        AlphaThree: "GHA",
        UNCode: "288"
    },
    GI: {
        Flag: '<div class="flag" id="GIB"></div>',
        Country: "Gibraltar",
        AlphaTwo: "GI",
        AlphaThree: "GIB",
        UNCode: "292"
    },
    GR: {
        Flag: '<div class="flag" id="GRC"></div>',
        Country: "Greece",
        AlphaTwo: "GR",
        AlphaThree: "GRC",
        UNCode: "300"
    },
    GL: {
        Flag: '<div class="flag" id="GRL"></div>',
        Country: "Greenland",
        AlphaTwo: "GL",
        AlphaThree: "GRL",
        UNCode: "304"
    },
    GD: {
        Flag: '<div class="flag" id="GRD"></div>',
        Country: "Grenada",
        AlphaTwo: "GD",
        AlphaThree: "GRD",
        UNCode: "308"
    },
    GP: {
        Flag: '<div class="flag" id="GLP"></div>',
        Country: "Guadeloupe",
        AlphaTwo: "GP",
        AlphaThree: "GLP",
        UNCode: "312"
    },
    GU: {
        Flag: '<div class="flag" id="GUM"></div>',
        Country: "Guam",
        AlphaTwo: "GU",
        AlphaThree: "GUM",
        UNCode: "316"
    },
    GT: {
        Flag: '<div class="flag" id="GTM"></div>',
        Country: "Guatemala",
        AlphaTwo: "GT",
        AlphaThree: "GTM",
        UNCode: "320"
    },
    GG: {
        Flag: '<div class="flag" id="GGY"></div>',
        Country: "Guernsey",
        AlphaTwo: "GG",
        AlphaThree: "GGY",
        UNCode: "831"
    },
    GN: {
        Flag: '<div class="flag" id="GIN"></div>',
        Country: "Guinea",
        AlphaTwo: "GN",
        AlphaThree: "GIN",
        UNCode: "324"
    },
    GW: {
        Flag: '<div class="flag" id="GNB"></div>',
        Country: "Guinea-Bissau",
        AlphaTwo: "GW",
        AlphaThree: "GNB",
        UNCode: "624"
    },
    GY: {
        Flag: '<div class="flag" id="GUY"></div>',
        Country: "Guyana",
        AlphaTwo: "GY",
        AlphaThree: "GUY",
        UNCode: "328"
    },
    HT: {
        Flag: '<div class="flag" id="HTI"></div>',
        Country: "Haiti",
        AlphaTwo: "HT",
        AlphaThree: "HTI",
        UNCode: "332"
    },
    HM: {
        Flag: '<div class="flag" id="HMD"></div>',
        Country: "Heard and Mcdonald Islands",
        AlphaTwo: "HM",
        AlphaThree: "HMD",
        UNCode: "334"
    },
    VA: {
        Flag: '<div class="flag" id="VAT"></div>',
        Country: "Holy See (Vatican City State)",
        AlphaTwo: "VA",
        AlphaThree: "VAT",
        UNCode: "336"
    },
    HN: {
        Flag: '<div class="flag" id="HND"></div>',
        Country: "Honduras",
        AlphaTwo: "HN",
        AlphaThree: "HND",
        UNCode: "340"
    },
    HU: {
        Flag: '<div class="flag" id="HUN"></div>',
        Country: "Hungary",
        AlphaTwo: "HU",
        AlphaThree: "HUN",
        UNCode: "348"
    },
    IS: {
        Flag: '<div class="flag" id="ISL"></div>',
        Country: "Iceland",
        AlphaTwo: "IS",
        AlphaThree: "ISL",
        UNCode: "352"
    },
    IN: {
        Flag: '<div class="flag" id="IND"></div>',
        Country: "India",
        AlphaTwo: "IN",
        AlphaThree: "IND",
        UNCode: "356"
    },
    ID: {
        Flag: '<div class="flag" id="IDN"></div>',
        Country: "Indonesia",
        AlphaTwo: "ID",
        AlphaThree: "IDN",
        UNCode: "360"
    },
    IR: {
        Flag: '<div class="flag" id="IRN"></div>',
        Country: "Iran, Islamic Republic of",
        AlphaTwo: "IR",
        AlphaThree: "IRN",
        UNCode: "364"
    },
    IQ: {
        Flag: '<div class="flag" id="IRQ"></div>',
        Country: "Iraq",
        AlphaTwo: "IQ",
        AlphaThree: "IRQ",
        UNCode: "368"
    },
    IE: {
        Flag: '<div class="flag" id="IRL"></div>',
        Country: "Ireland",
        AlphaTwo: "IE",
        AlphaThree: "IRL",
        UNCode: "372"
    },
    IM: {
        Flag: '<div class="flag" id="IMN"></div>',
        Country: "Isle of Man",
        AlphaTwo: "IM",
        AlphaThree: "IMN",
        UNCode: "833"
    },
    IL: {
        Flag: '<div class="flag" id="ISR"></div>',
        Country: "Israel",
        AlphaTwo: "IL",
        AlphaThree: "ISR",
        UNCode: "376"
    },
    IT: {
        Flag: '<div class="flag" id="ITA"></div>',
        Country: "Italy",
        AlphaTwo: "IT",
        AlphaThree: "ITA",
        UNCode: "380"
    },
    JM: {
        Flag: '<div class="flag" id="JAM"></div>',
        Country: "Jamaica",
        AlphaTwo: "JM",
        AlphaThree: "JAM",
        UNCode: "388"
    },
    JP: {
        Flag: '<div class="flag" id="JPN"></div>',
        Country: "Japan",
        AlphaTwo: "JP",
        AlphaThree: "JPN",
        UNCode: "392"
    },
    JE: {
        Flag: '<div class="flag" id="JEY"></div>',
        Country: "Jersey",
        AlphaTwo: "JE",
        AlphaThree: "JEY",
        UNCode: "832"
    },
    JO: {
        Flag: '<div class="flag" id="JOR"></div>',
        Country: "Jordan",
        AlphaTwo: "JO",
        AlphaThree: "JOR",
        UNCode: "400"
    },
    KZ: {
        Flag: '<div class="flag" id="KAZ"></div>',
        Country: "Kazakhstan",
        AlphaTwo: "KZ",
        AlphaThree: "KAZ",
        UNCode: "398"
    },
    KE: {
        Flag: '<div class="flag" id="KEN"></div>',
        Country: "Kenya",
        AlphaTwo: "KE",
        AlphaThree: "KEN",
        UNCode: "404"
    },
    KI: {
        Flag: '<div class="flag" id="KIR"></div>',
        Country: "Kiribati",
        AlphaTwo: "KI",
        AlphaThree: "KIR",
        UNCode: "296"
    },
    KP: {
        Flag: '<div class="flag" id="PRK"></div>',
        Country: "Korea (North)",
        AlphaTwo: "KP",
        AlphaThree: "PRK",
        UNCode: "408"
    },
    KR: {
        Flag: '<div class="flag" id="KOR"></div>',
        Country: "Korea (South)",
        AlphaTwo: "KR",
        AlphaThree: "KOR",
        UNCode: "410"
    },
    KW: {
        Flag: '<div class="flag" id="KWT"></div>',
        Country: "Kuwait",
        AlphaTwo: "KW",
        AlphaThree: "KWT",
        UNCode: "414"
    },
    KG: {
        Flag: '<div class="flag" id="KGZ"></div>',
        Country: "Kyrgyzstan",
        AlphaTwo: "KG",
        AlphaThree: "KGZ",
        UNCode: "417"
    },
    LA: {
        Flag: '<div class="flag" id="LAO"></div>',
        Country: "Lao PDR",
        AlphaTwo: "LA",
        AlphaThree: "LAO",
        UNCode: "418"
    },
    LV: {
        Flag: '<div class="flag" id="LVA"></div>',
        Country: "Latvia",
        AlphaTwo: "LV",
        AlphaThree: "LVA",
        UNCode: "428"
    },
    LB: {
        Flag: '<div class="flag" id="LBN"></div>',
        Country: "Lebanon",
        AlphaTwo: "LB",
        AlphaThree: "LBN",
        UNCode: "422"
    },
    LS: {
        Flag: '<div class="flag" id="LSO"></div>',
        Country: "Lesotho",
        AlphaTwo: "LS",
        AlphaThree: "LSO",
        UNCode: "426"
    },
    LR: {
        Flag: '<div class="flag" id="LBR"></div>',
        Country: "Liberia",
        AlphaTwo: "LR",
        AlphaThree: "LBR",
        UNCode: "430"
    },
    LY: {
        Flag: '<div class="flag" id="LBY"></div>',
        Country: "Libya",
        AlphaTwo: "LY",
        AlphaThree: "LBY",
        UNCode: "434"
    },
    LI: {
        Flag: '<div class="flag" id="LIE"></div>',
        Country: "Liechtenstein",
        AlphaTwo: "LI",
        AlphaThree: "LIE",
        UNCode: "438"
    },
    LT: {
        Flag: '<div class="flag" id="LTU"></div>',
        Country: "Lithuania",
        AlphaTwo: "LT",
        AlphaThree: "LTU",
        UNCode: "440"
    },
    LU: {
        Flag: '<div class="flag" id="LUX"></div>',
        Country: "Luxembourg",
        AlphaTwo: "LU",
        AlphaThree: "LUX",
        UNCode: "442"
    },
    MK: {
        Flag: '<div class="flag" id="MKD"></div>',
        Country: "Macedonia, Republic of",
        AlphaTwo: "MK",
        AlphaThree: "MKD",
        UNCode: "807"
    },
    MG: {
        Flag: '<div class="flag" id="MDG"></div>',
        Country: "Madagascar",
        AlphaTwo: "MG",
        AlphaThree: "MDG",
        UNCode: "450"
    },
    MW: {
        Flag: '<div class="flag" id="MWI"></div>',
        Country: "Malawi",
        AlphaTwo: "MW",
        AlphaThree: "MWI",
        UNCode: "454"
    },
    MY: {
        Flag: '<div class="flag" id="MYS"></div>',
        Country: "Malaysia",
        AlphaTwo: "MY",
        AlphaThree: "MYS",
        UNCode: "458"
    },
    MV: {
        Flag: '<div class="flag" id="MDV"></div>',
        Country: '<a href="maldives.htm">Maldives</a>',
        AlphaTwo: "MV",
        AlphaThree: "MDV",
        UNCode: "462"
    },
    ML: {
        Flag: '<div class="flag" id="MLI"></div>',
        Country: "Mali",
        AlphaTwo: "ML",
        AlphaThree: "MLI",
        UNCode: "466"
    },
    MT: {
        Flag: '<div class="flag" id="MLT"></div>',
        Country: "Malta",
        AlphaTwo: "MT",
        AlphaThree: "MLT",
        UNCode: "470"
    },
    MH: {
        Flag: '<div class="flag" id="MHL"></div>',
        Country: "Marshall Islands",
        AlphaTwo: "MH",
        AlphaThree: "MHL",
        UNCode: "584"
    },
    MQ: {
        Flag: '<div class="flag" id="MTQ"></div>',
        Country: "Martinique",
        AlphaTwo: "MQ",
        AlphaThree: "MTQ",
        UNCode: "474"
    },
    MR: {
        Flag: '<div class="flag" id="MRT"></div>',
        Country: "Mauritania",
        AlphaTwo: "MR",
        AlphaThree: "MRT",
        UNCode: "478"
    },
    MU: {
        Flag: '<div class="flag" id="MUS"></div>',
        Country: "Mauritius",
        AlphaTwo: "MU",
        AlphaThree: "MUS",
        UNCode: "480"
    },
    YT: {
        Flag: '<div class="flag" id="MYT"></div>',
        Country: "Mayotte",
        AlphaTwo: "YT",
        AlphaThree: "MYT",
        UNCode: "175"
    },
    MX: {
        Flag: '<div class="flag" id="MEX"></div>',
        Country: "Mexico",
        AlphaTwo: "MX",
        AlphaThree: "MEX",
        UNCode: "484"
    },
    FM: {
        Flag: '<div class="flag" id="FSM"></div>',
        Country: "Micronesia, Federated States of",
        AlphaTwo: "FM",
        AlphaThree: "FSM",
        UNCode: "583"
    },
    MD: {
        Flag: '<div class="flag" id="MDA"></div>',
        Country: "Moldova",
        AlphaTwo: "MD",
        AlphaThree: "MDA",
        UNCode: "498"
    },
    MC: {
        Flag: '<div class="flag" id="MCO"></div>',
        Country: "Monaco",
        AlphaTwo: "MC",
        AlphaThree: "MCO",
        UNCode: "492"
    },
    MN: {
        Flag: '<div class="flag" id="MNG"></div>',
        Country: "Mongolia",
        AlphaTwo: "MN",
        AlphaThree: "MNG",
        UNCode: "496"
    },
    ME: {
        Flag: '<div class="flag" id="MNE"></div>',
        Country: "Montenegro",
        AlphaTwo: "ME",
        AlphaThree: "MNE",
        UNCode: "499"
    },
    MS: {
        Flag: '<div class="flag" id="MSR"></div>',
        Country: "Montserrat",
        AlphaTwo: "MS",
        AlphaThree: "MSR",
        UNCode: "500"
    },
    MA: {
        Flag: '<div class="flag" id="MAR"></div>',
        Country: "Morocco",
        AlphaTwo: "MA",
        AlphaThree: "MAR",
        UNCode: "504"
    },
    MZ: {
        Flag: '<div class="flag" id="MOZ"></div>',
        Country: "Mozambique",
        AlphaTwo: "MZ",
        AlphaThree: "MOZ",
        UNCode: "508"
    },
    MM: {
        Flag: '<div class="flag" id="MMR"></div>',
        Country: "Myanmar",
        AlphaTwo: "MM",
        AlphaThree: "MMR",
        UNCode: "104"
    },
    NA: {
        Flag: '<div class="flag" id="NAM"></div>',
        Country: "Namibia",
        AlphaTwo: "NA",
        AlphaThree: "NAM",
        UNCode: "516"
    },
    NR: {
        Flag: '<div class="flag" id="NRU"></div>',
        Country: "Nauru",
        AlphaTwo: "NR",
        AlphaThree: "NRU",
        UNCode: "520"
    },
    NP: {
        Flag: '<div class="flag" id="NPL"></div>',
        Country: "Nepal",
        AlphaTwo: "NP",
        AlphaThree: "NPL",
        UNCode: "524"
    },
    NL: {
        Flag: '<div class="flag" id="NLD"></div>',
        Country: "Netherlands",
        AlphaTwo: "NL",
        AlphaThree: "NLD",
        UNCode: "528"
    },
    AN: {
        Flag: '<div class="flag" id="ANT"></div>',
        Country: "Netherlands Antilles",
        AlphaTwo: "AN",
        AlphaThree: "ANT",
        UNCode: "530"
    },
    NC: {
        Flag: '<div class="flag" id="NCL"></div>',
        Country: "New Caledonia",
        AlphaTwo: "NC",
        AlphaThree: "NCL",
        UNCode: "540"
    },
    NZ: {
        Flag: '<div class="flag" id="NZL"></div>',
        Country: "New Zealand",
        AlphaTwo: "NZ",
        AlphaThree: "NZL",
        UNCode: "554"
    },
    NI: {
        Flag: '<div class="flag" id="NIC"></div>',
        Country: "Nicaragua",
        AlphaTwo: "NI",
        AlphaThree: "NIC",
        UNCode: "558"
    },
    NE: {
        Flag: '<div class="flag" id="NER"></div>',
        Country: "Niger",
        AlphaTwo: "NE",
        AlphaThree: "NER",
        UNCode: "562"
    },
    NG: {
        Flag: '<div class="flag" id="NGA"></div>',
        Country: "Nigeria",
        AlphaTwo: "NG",
        AlphaThree: "NGA",
        UNCode: "566"
    },
    NU: {
        Flag: '<div class="flag" id="NIU"></div>',
        Country: "Niue",
        AlphaTwo: "NU",
        AlphaThree: "NIU",
        UNCode: "570"
    },
    NF: {
        Flag: '<div class="flag" id="NFK"></div>',
        Country: "Norfolk Island",
        AlphaTwo: "NF",
        AlphaThree: "NFK",
        UNCode: "574"
    },
    MP: {
        Flag: '<div class="flag" id="MNP"></div>',
        Country: "Northern Mariana Islands",
        AlphaTwo: "MP",
        AlphaThree: "MNP",
        UNCode: "580"
    },
    NO: {
        Flag: '<div class="flag" id="NOR"></div>',
        Country: "Norway",
        AlphaTwo: "NO",
        AlphaThree: "NOR",
        UNCode: "578"
    },
    OM: {
        Flag: '<div class="flag" id="OMN"></div>',
        Country: "Oman",
        AlphaTwo: "OM",
        AlphaThree: "OMN",
        UNCode: "512"
    },
    PK: {
        Flag: '<div class="flag" id="PAK"></div>',
        Country: "Pakistan",
        AlphaTwo: "PK",
        AlphaThree: "PAK",
        UNCode: "586"
    },
    PW: {
        Flag: '<div class="flag" id="PLW"></div>',
        Country: "Palau",
        AlphaTwo: "PW",
        AlphaThree: "PLW",
        UNCode: "585"
    },
    PS: {
        Flag: '<div class="flag" id="PSE"></div>',
        Country: "Palestinian Territory",
        AlphaTwo: "PS",
        AlphaThree: "PSE",
        UNCode: "275"
    },
    PA: {
        Flag: '<div class="flag" id="PAN"></div>',
        Country: "Panama",
        AlphaTwo: "PA",
        AlphaThree: "PAN",
        UNCode: "591"
    },
    PG: {
        Flag: '<div class="flag" id="PNG"></div>',
        Country: "Papua New Guinea",
        AlphaTwo: "PG",
        AlphaThree: "PNG",
        UNCode: "598"
    },
    PY: {
        Flag: '<div class="flag" id="PRY"></div>',
        Country: "Paraguay",
        AlphaTwo: "PY",
        AlphaThree: "PRY",
        UNCode: "600"
    },
    PE: {
        Flag: '<div class="flag" id="PER"></div>',
        Country: "Peru",
        AlphaTwo: "PE",
        AlphaThree: "PER",
        UNCode: "604"
    },
    PH: {
        Flag: '<div class="flag" id="PHL"></div>',
        Country: "Philippines",
        AlphaTwo: "PH",
        AlphaThree: "PHL",
        UNCode: "608"
    },
    PN: {
        Flag: '<div class="flag" id="PCN"></div>',
        Country: "Pitcairn",
        AlphaTwo: "PN",
        AlphaThree: "PCN",
        UNCode: "612"
    },
    PL: {
        Flag: '<div class="flag" id="POL"></div>',
        Country: "Poland",
        AlphaTwo: "PL",
        AlphaThree: "POL",
        UNCode: "616"
    },
    PT: {
        Flag: '<div class="flag" id="PRT"></div>',
        Country: "Portugal",
        AlphaTwo: "PT",
        AlphaThree: "PRT",
        UNCode: "620"
    },
    PR: {
        Flag: '<div class="flag" id="PRI"></div>',
        Country: "Puerto Rico",
        AlphaTwo: "PR",
        AlphaThree: "PRI",
        UNCode: "630"
    },
    QA: {
        Flag: '<div class="flag" id="QAT"></div>',
        Country: "Qatar",
        AlphaTwo: "QA",
        AlphaThree: "QAT",
        UNCode: "634"
    },
    RE: {
        Flag: '<div class="flag" id="REU"></div>',
        Country: "R�union",
        AlphaTwo: "RE",
        AlphaThree: "REU",
        UNCode: "638"
    },
    RO: {
        Flag: '<div class="flag" id="ROU"></div>',
        Country: "Romania",
        AlphaTwo: "RO",
        AlphaThree: "ROU",
        UNCode: "642"
    },
    RU: {
        Flag: '<div class="flag" id="RUS"></div>',
        Country: "Russian Federation",
        AlphaTwo: "RU",
        AlphaThree: "RUS",
        UNCode: "643"
    },
    RW: {
        Flag: '<div class="flag" id="RWA"></div>',
        Country: "Rwanda",
        AlphaTwo: "RW",
        AlphaThree: "RWA",
        UNCode: "646"
    },
    BL: {
        Flag: '<div class="flag" id="BLM"></div>',
        Country: "Saint-Barth�lemy",
        AlphaTwo: "BL",
        AlphaThree: "BLM",
        UNCode: "652"
    },
    SH: {
        Flag: '<div class="flag" id="SHN"></div>',
        Country: "Saint Helena",
        AlphaTwo: "SH",
        AlphaThree: "SHN",
        UNCode: "654"
    },
    KN: {
        Flag: '<div class="flag" id="KNA"></div>',
        Country: "Saint Kitts and Nevis",
        AlphaTwo: "KN",
        AlphaThree: "KNA",
        UNCode: "659"
    },
    LC: {
        Flag: '<div class="flag" id="LCA"></div>',
        Country: "Saint Lucia",
        AlphaTwo: "LC",
        AlphaThree: "LCA",
        UNCode: "662"
    },
    MF: {
        Flag: '<div class="flag" id="MAF"></div>',
        Country: "Saint-Martin (French part)",
        AlphaTwo: "MF",
        AlphaThree: "MAF",
        UNCode: "663"
    },
    PM: {
        Flag: '<div class="flag" id="SPM"></div>',
        Country: "Saint Pierre and Miquelon",
        AlphaTwo: "PM",
        AlphaThree: "SPM",
        UNCode: "666"
    },
    VC: {
        Flag: '<div class="flag" id="VCT"></div>',
        Country: "Saint Vincent and Grenadines",
        AlphaTwo: "VC",
        AlphaThree: "VCT",
        UNCode: "670"
    },
    WS: {
        Flag: '<div class="flag" id="WSM"></div>',
        Country: "Samoa",
        AlphaTwo: "WS",
        AlphaThree: "WSM",
        UNCode: "882"
    },
    SM: {
        Flag: '<div class="flag" id="SMR"></div>',
        Country: "San Marino",
        AlphaTwo: "SM",
        AlphaThree: "SMR",
        UNCode: "674"
    },
    ST: {
        Flag: '<div class="flag" id="STP"></div>',
        Country: "Sao Tome and Principe",
        AlphaTwo: "ST",
        AlphaThree: "STP",
        UNCode: "678"
    },
    SA: {
        Flag: '<div class="flag" id="SAU"></div>',
        Country: "Saudi Arabia",
        AlphaTwo: "SA",
        AlphaThree: "SAU",
        UNCode: "682"
    },
    SN: {
        Flag: '<div class="flag" id="SEN"></div>',
        Country: "Senegal",
        AlphaTwo: "SN",
        AlphaThree: "SEN",
        UNCode: "686"
    },
    RS: {
        Flag: '<div class="flag" id="SRB"></div>',
        Country: "Serbia",
        AlphaTwo: "RS",
        AlphaThree: "SRB",
        UNCode: "688"
    },
    SC: {
        Flag: '<div class="flag" id="SYC"></div>',
        Country: "Seychelles",
        AlphaTwo: "SC",
        AlphaThree: "SYC",
        UNCode: "690"
    },
    SL: {
        Flag: '<div class="flag" id="SLE"></div>',
        Country: "Sierra Leone",
        AlphaTwo: "SL",
        AlphaThree: "SLE",
        UNCode: "694"
    },
    SG: {
        Flag: '<div class="flag" id="SGP"></div>',
        Country: "Singapore",
        AlphaTwo: "SG",
        AlphaThree: "SGP",
        UNCode: "702"
    },
    SK: {
        Flag: '<div class="flag" id="SVK"></div>',
        Country: "Slovakia",
        AlphaTwo: "SK",
        AlphaThree: "SVK",
        UNCode: "703"
    },
    SI: {
        Flag: '<div class="flag" id="SVN"></div>',
        Country: "Slovenia",
        AlphaTwo: "SI",
        AlphaThree: "SVN",
        UNCode: "705"
    },
    SB: {
        Flag: '<div class="flag" id="SLB"></div>',
        Country: "Solomon Islands",
        AlphaTwo: "SB",
        AlphaThree: "SLB",
        UNCode: "090"
    },
    SO: {
        Flag: '<div class="flag" id="SOM"></div>',
        Country: "Somalia",
        AlphaTwo: "SO",
        AlphaThree: "SOM",
        UNCode: "706"
    },
    ZA: {
        Flag: '<div class="flag" id="ZAF"></div>',
        Country: "South Africa",
        AlphaTwo: "ZA",
        AlphaThree: "ZAF",
        UNCode: "710"
    },
    GS: {
        Flag: '<div class="flag" id="SGS"></div>',
        Country: "South Georgia and the South Sandwich Islands",
        AlphaTwo: "GS",
        AlphaThree: "SGS",
        UNCode: "239"
    },
    SS: {
        Flag: '<div class="flag" id="SSD"></div>',
        Country: "South Sudan",
        AlphaTwo: "SS",
        AlphaThree: "SSD",
        UNCode: "728"
    },
    ES: {
        Flag: '<div class="flag" id="ESP"></div>',
        Country: "Spain",
        AlphaTwo: "ES",
        AlphaThree: "ESP",
        UNCode: "724"
    },
    LK: {
        Flag: '<div class="flag" id="LKA"></div>',
        Country: "Sri Lanka",
        AlphaTwo: "LK",
        AlphaThree: "LKA",
        UNCode: "144"
    },
    SD: {
        Flag: '<div class="flag" id="SDN"></div>',
        Country: "Sudan",
        AlphaTwo: "SD",
        AlphaThree: "SDN",
        UNCode: "736"
    },
    SR: {
        Flag: '<div class="flag" id="SUR"></div>',
        Country: "Suriname",
        AlphaTwo: "SR",
        AlphaThree: "SUR",
        UNCode: "740"
    },
    SJ: {
        Flag: '<div class="flag" id="SJM"></div>',
        Country: "Svalbard and Jan Mayen Islands",
        AlphaTwo: "SJ",
        AlphaThree: "SJM",
        UNCode: "744"
    },
    SZ: {
        Flag: '<div class="flag" id="SWZ"></div>',
        Country: "Swaziland",
        AlphaTwo: "SZ",
        AlphaThree: "SWZ",
        UNCode: "748"
    },
    SE: {
        Flag: '<div class="flag" id="SWE"></div>',
        Country: "Sweden",
        AlphaTwo: "SE",
        AlphaThree: "SWE",
        UNCode: "752"
    },
    CH: {
        Flag: '<div class="flag" id="CHE"></div>',
        Country: "Switzerland",
        AlphaTwo: "CH",
        AlphaThree: "CHE",
        UNCode: "756"
    },
    SY: {
        Flag: '<div class="flag" id="SYR"></div>',
        Country: "Syrian Arab Republic (Syria)",
        AlphaTwo: "SY",
        AlphaThree: "SYR",
        UNCode: "760"
    },
    TW: {
        Flag: '<div class="flag" id="TWN"></div>',
        Country: "Taiwan, Republic of China",
        AlphaTwo: "TW",
        AlphaThree: "TWN",
        UNCode: "158"
    },
    TJ: {
        Flag: '<div class="flag" id="TJK"></div>',
        Country: "Tajikistan",
        AlphaTwo: "TJ",
        AlphaThree: "TJK",
        UNCode: "762"
    },
    TZ: {
        Flag: '<div class="flag" id="TZA"></div>',
        Country: "Tanzania, United Republic of",
        AlphaTwo: "TZ",
        AlphaThree: "TZA",
        UNCode: "834"
    },
    TH: {
        Flag: '<div class="flag" id="THA"></div>',
        Country: "Thailand",
        AlphaTwo: "TH",
        AlphaThree: "THA",
        UNCode: "764"
    },
    TL: {
        Flag: '<div class="flag" id="TLS"></div>',
        Country: "Timor-Leste",
        AlphaTwo: "TL",
        AlphaThree: "TLS",
        UNCode: "626"
    },
    TG: {
        Flag: '<div class="flag" id="TGO"></div>',
        Country: "Togo",
        AlphaTwo: "TG",
        AlphaThree: "TGO",
        UNCode: "768"
    },
    TK: {
        Flag: '<div class="flag" id="TKL"></div>',
        Country: "Tokelau",
        AlphaTwo: "TK",
        AlphaThree: "TKL",
        UNCode: "772"
    },
    TO: {
        Flag: '<div class="flag" id="TON"></div>',
        Country: "Tonga",
        AlphaTwo: "TO",
        AlphaThree: "TON",
        UNCode: "776"
    },
    TT: {
        Flag: '<div class="flag" id="TTO"></div>',
        Country: "Trinidad and Tobago",
        AlphaTwo: "TT",
        AlphaThree: "TTO",
        UNCode: "780"
    },
    TN: {
        Flag: '<div class="flag" id="TUN"></div>',
        Country: "Tunisia",
        AlphaTwo: "TN",
        AlphaThree: "TUN",
        UNCode: "788"
    },
    TR: {
        Flag: '<div class="flag" id="TUR"></div>',
        Country: "Turkey",
        AlphaTwo: "TR",
        AlphaThree: "TUR",
        UNCode: "792"
    },
    TM: {
        Flag: '<div class="flag" id="TKM"></div>',
        Country: "Turkmenistan",
        AlphaTwo: "TM",
        AlphaThree: "TKM",
        UNCode: "795"
    },
    TC: {
        Flag: '<div class="flag" id="TCA"></div>',
        Country: "Turks and Caicos Islands",
        AlphaTwo: "TC",
        AlphaThree: "TCA",
        UNCode: "796"
    },
    TV: {
        Flag: '<div class="flag" id="TUV"></div>',
        Country: "Tuvalu",
        AlphaTwo: "TV",
        AlphaThree: "TUV",
        UNCode: "798"
    },
    UG: {
        Flag: '<div class="flag" id="UGA"></div>',
        Country: "Uganda",
        AlphaTwo: "UG",
        AlphaThree: "UGA",
        UNCode: "800"
    },
    UA: {
        Flag: '<div class="flag" id="UKR"></div>',
        Country: "Ukraine",
        AlphaTwo: "UA",
        AlphaThree: "UKR",
        UNCode: "804"
    },
    AE: {
        Flag: '<div class="flag" id="ARE"></div>',
        Country: "United Arab Emirates",
        AlphaTwo: "AE",
        AlphaThree: "ARE",
        UNCode: "784"
    },
    GB: {
        Flag: '<div class="flag" id="GBR"></div>',
        Country: "United Kingdom",
        AlphaTwo: "GB",
        AlphaThree: "GBR",
        UNCode: "826"
    },
    US: {
        Flag: '<div class="flag" id="USA"></div>',
        Country: "United States of America",
        AlphaTwo: "US",
        AlphaThree: "USA",
        UNCode: "840"
    },
    UM: {
        Flag: '<div class="flag" id="UMI"></div>',
        Country: "US Minor Outlying Islands",
        AlphaTwo: "UM",
        AlphaThree: "UMI",
        UNCode: "581"
    },
    UY: {
        Flag: '<div class="flag" id="URY"></div>',
        Country: "Uruguay",
        AlphaTwo: "UY",
        AlphaThree: "URY",
        UNCode: "858"
    },
    UZ: {
        Flag: '<div class="flag" id="UZB"></div>',
        Country: "Uzbekistan",
        AlphaTwo: "UZ",
        AlphaThree: "UZB",
        UNCode: "860"
    },
    VU: {
        Flag: '<div class="flag" id="VUT"></div>',
        Country: "Vanuatu",
        AlphaTwo: "VU",
        AlphaThree: "VUT",
        UNCode: "548"
    },
    VE: {
        Flag: '<div class="flag" id="VEN"></div>',
        Country: "Venezuela (Bolivarian Republic)",
        AlphaTwo: "VE",
        AlphaThree: "VEN",
        UNCode: "862"
    },
    VN: {
        Flag: '<div class="flag" id="VNM"></div>',
        Country: "Viet Nam",
        AlphaTwo: "VN",
        AlphaThree: "VNM",
        UNCode: "704"
    },
    VI: {
        Flag: '<div class="flag" id="VIR"></div>',
        Country: "Virgin Islands, US",
        AlphaTwo: "VI",
        AlphaThree: "VIR",
        UNCode: "850"
    },
    WF: {
        Flag: '<div class="flag" id="WLF"></div>',
        Country: "Wallis and Futuna Islands",
        AlphaTwo: "WF",
        AlphaThree: "WLF",
        UNCode: "876"
    },
    EH: {
        Flag: '<div class="flag" id="ESH"></div>',
        Country: "Western Sahara",
        AlphaTwo: "EH",
        AlphaThree: "ESH",
        UNCode: "732"
    },
    YE: {
        Flag: '<div class="flag" id="YEM"></div>',
        Country: "Yemen",
        AlphaTwo: "YE",
        AlphaThree: "YEM",
        UNCode: "887"
    },
    ZM: {
        Flag: '<div class="flag" id="ZMB"></div>',
        Country: "Zambia",
        AlphaTwo: "ZM",
        AlphaThree: "ZMB",
        UNCode: "894"
    },
    ZW: {
        Flag: '<div class="flag" id="ZWE"></div>',
        Country: "Zimbabwe",
        AlphaTwo: "ZW",
        AlphaThree: "ZWE",
        UNCode: "716"
    }
};