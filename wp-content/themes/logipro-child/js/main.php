


function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function calculate() {
    var fixed_rates= jQuery("#fixed-rates :selected").val();
    var principal = jQuery('#finance-amount-slider').val();
    var interest = fixed_rates/100/12;
    if(fixed_rates == "1.54"){
       var payments = 2*12;
    }else{
       var payments = 5*12;
    }

      var mon = (principal*fixed_rates) / 100;
      
     if(fixed_rates == "1.54"){
      var new_monthly =  mon*2;
      var monthly =  new_monthly / payments;
     }else{
      var new_monthly =  mon*5;
      var monthly =  new_monthly / payments;
     } 



    // Check that the result is a finite number. If so, display the results.
    if (!isNaN(monthly) &&
        (monthly != Number.POSITIVE_INFINITY) &&
        (monthly != Number.NEGATIVE_INFINITY)) {

        monthly=round(monthly);
        var monthly_split = monthly.toString().split('.');

        jQuery('#calc-monthly-cost').html('<span class="sign">&pound;</span><span class="pounds">' + numberWithCommas(monthly_split[0]) + '</span><span class="pence">.' + monthly_split[1] + '</span>');
        var repayment = round(monthly * payments);

        jQuery('#calc-number-payments').html('<span>' + payments + ' MONTHLY PAYMENTS</span><br/>DURING FIXED RATE PERIOD<br/>ON AN INTEREST ONLY BASIS');
        jQuery("#fixed_rate_label").html(fixed_rates);
    }
}

// This simple method rounds a number to two decimal places.
function round(x) {
    return parseFloat(Math.round(x*100)/100).toFixed(2);
}

jQuery(document).ready(function() {



    calculate();
    

    var oldValue = '';

    jQuery('#finance-amount').on('change',function() {
        var amount = jQuery('#finance-amount').val().replace(',','');
        jQuery('#finance-amount').val(numberWithCommas(amount));
        jQuery('#finance-amount-slider').val(amount);
        jQuery('.quote-link').attr('href','apply.php?amount=' + jQuery('#finance-amount-slider').val() + '&term=' + jQuery('#finance-length-slider').val());
        calculate();
    });

    jQuery('#finance-amount-slider').on('change mousemove',function() {
        var amount = jQuery('#finance-amount-slider').val();
        jQuery('#finance-amount').val(numberWithCommas(amount));
        jQuery('.quote-link').attr('href','apply.php?amount=' + jQuery('#finance-amount-slider').val() + '&term=' + jQuery('#finance-length-slider').val());
        calculate();
    });

    jQuery('#finance-length-slider').on('change mousemove',function() {
        var text = '<span id="years">' + jQuery('#finance-length-slider').val()+ '</span> year';
        if(jQuery('#finance-length-slider').val()>1)
        {
            text = text + 's';
        }
        jQuery('#finance-length').html(text);
        jQuery('.quote-link').attr('href','apply.php?amount=' + jQuery('#finance-amount-slider').val() + '&term=' + jQuery('#finance-length-slider').val());
        calculate();
    });

    jQuery('#amount-not-sure').click(function()  {
        if(jQuery('#amount-not-sure').prop('checked'))  {
            oldValue = jQuery('#finance-amount').val()
            jQuery('#finance-amount').val('');
            jQuery('#finance-amount').attr('required',false);
        }
        else
        {
            jQuery('#finance-amount').val(oldValue);
            jQuery('#finance-amount').attr('required',true);
        }
    });

    jQuery('#finance-amount').click(function() {
        jQuery('#finance-amount').val(oldValue);
        if(jQuery('#amount-not-sure').prop('checked'))  {
            jQuery('#amount-not-sure').click();
        }
    });

    jQuery('#pay-over').on('change keyup',function() {
        var length = jQuery('#pay-over').val().length;
        jQuery('#pay-over').css('width',length + 'ch');
    });

    jQuery('#term-contain-quote').on('click',function() {
        jQuery('#pay-over').focus();
    });

    

    

    jQuery('#fixed-rates').on('change',function() {
        calculate();
    });
});