<?php
/*
    search.php

    MediaWiki API Demos
    Demo of `Search` module: Search for a text or title

    MIT License
*/

$endPoint = "https://en.wikivoyage.org/w/api.php";
$params = [
    "action" => "query",
    "format" => 'json',
    "prop" => 'extracts|pageprops',
    "exlimit" => 'max',
    "explaintext" => 'true',
    "exintro" => 'true',
    "rawcontinue" => '',
    "titles" => $locationName,
];

$url = $endPoint . "?" . http_build_query( $params );

$ch = curl_init( $url );
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
$output = curl_exec( $ch );
curl_close( $ch );

$result = json_decode( $output, true );
$page = $result['query']['pages'];
//echo('<pre>');
$Pixabay_API_KEY = '13936120-2239e8f0b4f1cdf976363f94d';
$Pixabay_URL = "https://pixabay.com/api/?key=" . $Pixabay_API_KEY . "&q=" . $locationName . "&image_type=photo&category=travel&per_page=3";

$Pixabay_ch = curl_init( $Pixabay_URL );
curl_setopt( $Pixabay_ch, CURLOPT_RETURNTRANSFER, true );
$Pixabay_output = curl_exec( $Pixabay_ch );
curl_close( $Pixabay_ch );

$PixabayResult = json_decode( $Pixabay_output, true );
$imageExist = false;
$image_src = '';
if (absint($PixabayResult['totalHits']) > 0)
{
    $imageExist = true;
    $image_src = $PixabayResult['hits'][0]['largeImageURL'];
}

if(!$imageExist)
{
    $Unsplash_API_KEY = '584a3c67e5d22a4b650133b7d4e08e742104ba3e8cf54f87b2674678e20b2da5';
    $Unsplash_URL = "https://api.unsplash.com/search/photos?client_id=584a3c67e5d22a4b650133b7d4e08e742104ba3e8cf54f87b2674678e20b2da5&query=" . $locationName . "&page=1&per_page=1";
    $Unsplash_request = wp_remote_get($Unsplash_URL);
    $Unsplash_output = wp_remote_retrieve_body($Unsplash_request);
    $UnsplashResult = json_decode($Unsplash_output);
    
    if (absint($UnsplashResult->total) > 0)
    {
        $imageExist = true;
        $image_src = $UnsplashResult->results[0]->urls->regular;
    }
}

foreach($page as $item){
?>
<div class="location-info-wrapper">
	<div class="location-img-row">
		<figure class="location-figure">
			<div class="location-img-box">
                <?php 
                if($imageExist)
                {
                ?>
                <div class="location-img-wrap">
                    <?php 
                    if($isOrigin){
                    ?>
                        <img width="800" src="<?php echo $image_src;?>" alt="Get on the bus in <?php echo $locationName;?>"/>
                    <?php 
                    }
                    else{
                    ?>
                        <img width="800" src="<?php echo $image_src;?>" alt="Get off the bus in <?php echo $locationName;?>"/>
                    <?php 
                    }
                    ?>
                </div>
                    <!-- <div class="location-img" style="background-image:url(<?php echo $image_src;?>)"></div> -->
                <?php 
                }
                ?>
            </div>
            <figcaption class="location-figure-caption"><?php echo $item['title'];?></figcaption>
		</figure>
	</div>
	<div class="location-info-row">
        <?php 
        if($isOrigin){
        ?>
            <?php 
            if(intval(strlen($item['extract']) > 0)){
            ?>
                <p class="show-read-more"><?php echo $defaultOriginText;?>. <?php echo $item['extract'];?></p>
            <?php 
            }
            else{
            ?>
                <p class="show-read-more"><?php echo $defaultOriginText;?>. <?php echo $defaultOriginTextWhenNoTextIsAvailable;?></p>
            <?php 
            }
        }
        else{
        ?>
            <p class="show-read-more"><?php echo $defaultDestinationText;?>. <?php echo $item['extract'];?></p>
        <?php 
        }
        ?>		
	</div>
</div>
<?php
}
?>