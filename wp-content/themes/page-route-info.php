<!DOCTYPE html>
<?php
/*
  Template Name: Page : Route Info Page Layout
 */
//get_header();
$full_query_param = add_query_arg();

$q_param = explode('bus-from-', urldecode($full_query_param));

$q_param_locate = explode('-to-', $q_param[1]);

$end_string = explode("?", $q_param_locate[1]);
$string_from = preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $q_param_locate[0]);
//$string_to = preg_replace('/[^\p{L}\p{N}\s]/u', ' ', htmlentities($end_string[0]));
$string_to = str_replace('-',' ',$end_string[0]);
//print_r($string_to);die;
$currency = (strlen($_GET["get_currency"])>0)?$_GET["get_currency"]:'GBP';

// Read JSON file
$json = file_get_contents(get_template_directory_uri().'-child/currencyCodes.json');

//Decode JSON
$json_data = json_decode($json,true);
$currency_symbol = '';
foreach ($json_data as $curr_data) {
    if (strpos($currency, $curr_data['code']) !== FALSE) {
        $currency_symbol = $curr_data['symbol'];
    }
}

global $wp;
$home_page = home_url(add_query_arg(array(), $wp->request));
$current_page_url = str_replace('/' . ICL_LANGUAGE_CODE . '/', '', home_url(add_query_arg(array(), $wp->request)));
$page_title = 'Bus from ' . ucwords($string_from) . ' to ' . ucwords($string_to);

$api_url1 = 'https://bookings.easybus.com/affiliate/api/search/source_destination_stations?source=' . ucwords($string_from) . '&destination=' . ucwords($string_to);
//print_r($api_url1);
$request1 = wp_remote_get($api_url1);
if (is_wp_error($request1)) {
    return false;
}
$body1 = wp_remote_retrieve_body($request1);
$data1 = json_decode($body1);
$source_code = $data1->source[0]->code;
$dest_code = $data1->destination[0]->code;

if (empty($data1->source[0]->code) || empty($data1->destination[0]->code)) {
    header('Location: '.home_url().'sorry-this-doesnt-exist-any-more');
    exit();
}


$searchDate = $_GET["searchDate"];

$current_date = strlen($searchDate)>0?date('Y-m-d', strtotime($searchDate)):date('Y-m-d', strtotime('+1 day'));
$current_start_time = date('H:i');
$current_end_time = date('H:i', strtotime('+4 hours'));
$mktime = mktime();
$api_url = 'https://bookings.easybus.com/affiliate/api/connections/find?DepartureStations=' . $source_code . '&ArrivalStations=' . $dest_code . '&DepartureDate=' . $current_date . '&DepatureStartTime=' . $current_start_time . '&DepatureEndTime=' . $current_end_time . '&ReturnDate=&ReturnStartTime=' . $current_start_time . '&ReturnEndTime=' . $current_start_time . '&Pax=1&Locale=en&Currency=' . $currency . '&TimeStamp=' . $mktime;
//print_r($api_url);die;
$request = wp_remote_get($api_url);
if (is_wp_error($request)) {
    return false;
}
$body = wp_remote_retrieve_body($request);
$data = json_decode($body);

$get_records = $data->OutboundData;
$noScheduleAvailable = count($data->OutboundData->data) <= 0;
// if (empty($get_records->included)) {

//     header('Location: '.home_url().'sorry-this-doesnt-exist-any-more');
//     exit();
// }
function datediff($date1, $date2)
{
    $diff = abs(strtotime($date1) - strtotime($date2));
    return sprintf(
        "%d Hours, %d Mins",
        intval(($diff % 86400) / 3600),
        intval(($diff / 60) % 60)
    );
    // "%d Days, %d Hours, %d Mins, %d Seconds", intval($diff / 86400), intval(( $diff % 86400 ) / 3600), intval(( $diff / 60 ) % 60), intval($diff % 60)
}

function secToHR($seconds)
{
    $hours = floor($seconds / 3600);
    $minutes = floor(($seconds / 60) % 60);
    $seconds = $seconds % 60;
    //  return "$hours:$minutes:$seconds";
    return $hours > 0 ? "$hours h $minutes min" : ($minutes > 0 ? "$minutes min" : "$seconds sec");
}

$min_price_array = array();
$duration_array = array();
for ($idx = 0; $idx < count($data->OutboundData->data); $idx++) {
    $book_out = $get_records->data[$idx]->attributes->booked_out; // if false then display
    //display "booked_out = false" records only
    if (empty($book_out)) {
        $cheapest_total_adult_price = number_format(($get_records->data[$idx]->attributes->cheapest_total_adult_price) / 100, 2);
        $min_price[] = array_push($min_price_array, $cheapest_total_adult_price);
        array_push($duration_array, $get_records->data[$idx]->attributes->duration);

        $for_relationships = $get_records->data[$idx]->relationships;

        $mkt_id_detail = $for_relationships->marketing_carrier->data->id; //code
        $marketting_carrier_details = mktCarrierNameByCode($mkt_id_detail, $data->MarketingCarrierDetails);
        $trade_name[] = $marketting_carrier_details->trade_name;
    }
}
$get_trade_name = array_unique($trade_name);


?>
<html <?php language_attributes(); ?>>
<head>
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta charset="<?php esc_attr(bloginfo( 'charset' )); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="format-detection" content="telephone=no">
    <?php 
    if($noScheduleAvailable)
    {
    ?>
    <title>easyBus offer low-cost tickets on this route</title>
    <?php 
    }
    else
    { 
    ?>
    <title><?php echo $page_title;?> from <?php echo $currency_symbol . min($min_price_array); ?> with easyBus.com</title>
    <?php 
    }
    ?>
    <meta name="description" content="easyBus.com provides access to cheap bus tickets from <?php echo ucwords($string_from);?> to <?php echo ucwords($string_to);?>. Fares from as low as <?php echo $currency_symbol . min($min_price_array);?>.">
    <meta name="keywords" content="bus <?php echo $string_from;?> to <?php echo $string_to;?>, <?php echo $string_from;?>, <?php echo $string_to;?>, bus times <?php echo $string_from;?> to <?php echo ucwords($string_to);?>">
	<link rel="profile" href="http://gmpg.org/xfn/11" />       
	<link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
	<?php remove_action( 'wp_head', '_wp_render_title_tag', 1 ); wp_head(); /* this is used by many Wordpress features and for plugins to work properly */ ?>
    
</head>

<body <?php body_class(); ?>>	
	
	<?php 
	/* Animsition */
		
	if(logipro_get_ozy_data('is_animsition_active')) echo '<div class="animsition">';
	echo '<div id="body-bg-wrapper">';	
	/* Include Primary Menu */
	logipro_ozy_include_primary_menu();
    ?>        
    <div class="none">
        <p><a href="#content"><?php esc_attr_e('Skip to Content', 'logipro'); ?></a></p><?php /* used for accessibility, particularly for screen reader applications */ ?>
    </div><!--.none-->
    <?php
		if(logipro_get_ozy_data('hide_everything_but_content') <= 0): 

		$header_slider_class = $footer_slider_class = '';
		logipro_ozy_header_footer_slider_class($header_slider_class, $footer_slider_class);
	?> 
    <div id="main" class="<?php echo esc_attr($footer_slider_class); echo esc_attr($header_slider_class); ?>">
        <?php
		logipro_ozy_custom_header();
        ?>
        
	<?php endif; ?>
            
<div class="pre-spin" style="display: none;"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/09/loader.gif"></div>
<script>
    jQuery("#toggleSearchWidget").click(function() {
        jQuery(".search-bar").toggleClass('show-widget');
    })


    var searchCount = 0;

    function scrollToResult() {
        var searchResultContent = document.getElementsByClassName('travel-info');
        if (searchResultContent.length > 0) {
            jQuery('html, body').animate({
                'scrollTop': jQuery(".travel-info").position().top
            });
            //searchResultContent[0].scrollIntoView(true);
        } else if (searchCount <= 50) {
            setTimeout(function() {
                scrollToResult();
                searchCount++;
            }, 200);
        }
    }

    jQuery(function() {
        setTimeout(function() {
            //scrollToResult();
        }, 500);
    });
</script>

<link href="<?php echo get_template_directory_uri(); ?>-child/css/datepicker.css" rel=stylesheet>
<link href="<?php echo get_template_directory_uri(); ?>-child/css/jquery-select-step.css" rel=stylesheet>
<link href="<?php echo get_template_directory_uri(); ?>-child/css/easy-autocomplete.min.css" rel=stylesheet>
<link href="<?php echo get_template_directory_uri(); ?>-child/css/easy-autocomplete.themes.min.css" rel=stylesheet>



<div id="page-title-wrapper">
    <div>
        <h1 class="page-title"><?php echo $page_title; ?></h1>
        <?php 
        if($noScheduleAvailable)
        {
        ?>
            <h4>easyBus offer low-cost tickets on this route</h4>
        <?php 
        }
        else
        { 
        ?>
            <h4>Bus Tickets from <?php echo $currency_symbol . min($min_price_array); ?> with up to <?php echo count($min_price); ?> Connections Per Day</h4>
        <?php 
        }
        ?>
        
    </div>
</div>

<div id="page-bread-crumbs-share-bar">
    <?php //logipro_ozy_the_breadcrumb(); 
    ?>
    <ul id="page-breadcrumbs" class="content-font">
        <li><a href="<?php echo $home_page; ?>"><i class="oic-home-2"></i></a></li>
        <li><a href="<?php echo $home_page; ?>routes/" title="Our Routes">Our Routes</a></li>
        <li><?php echo $page_title; ?></li>
    </ul>
    <span class="content-font"><?php esc_attr_e('Share', 'logipro') ?></span>
    <ul class="page-share-buttons">
        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_page_url . $full_query_param ?>" target="_blank" title="Facebook" class="symbol-facebook"><span class="symbol">&#xe027;</span></a></li>
        <li><a href="https://twitter.com/intent/tweet?text=<?php echo $current_page_url . $full_query_param ?>" target="_blank" title="Twitter" class="symbol-twitter"><span class="symbol">&#xe086;</span></a></li>
        <li><a href="https://plus.google.com/share?url=<?php echo $current_page_url . $full_query_param ?>" target="_blank" title="Google+" class="symbol-googleplus"><span class="symbol">&#xe039;</span></a></li>
        <li><a href="https://pinterest.com/pin/create/button/?url=<?php echo $current_page_url . $full_query_param ?>" target="_blank" title="Pinterest" class="symbol-pinterest"><span class="symbol">&#xe064;</span></a></li>
        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $current_page_url . $full_query_param ?>" target="_blank" title="LinkedIn" class="symbol-linkedin"><span class="symbol">&#xe052;</span></a></li>
        <li><a href="#" class="page-social-share"><span class="oic-share-outline"></span></a></li>
    </ul>
</div>
<div class="container <?php echo esc_attr(logipro_get_ozy_data('_page_content_css_name')); ?>">
    <div id="content" class="without-sidebar route-page">
    <div class="search-section">
        <div class="ticket-type-btn">
            <input type="radio" id="oneWay" checked="checked" name="way">
            <label for="oneWay">One-Way</label>

            <input type="radio" id="retrun" name="way">
            <label for="retrun">Return</label>
        </div>
        <div class="search-inner">
            <div class="mb-10">
                <label>From</label>
                <input type="text" id="departFrom" class="custom-input" onfocus="OnFocus(event)" value="<?php echo ucwords($string_from); ?>">
            </div>
            <div class="mb-10">
                <label>To</label>
                <input type="text" id="arrivalTo" class="custom-input" onfocus="OnFocus(event)" value="<?php echo ucwords($string_to); ?>">
            </div>
            <div id="departContainer" class="date-time-box">
                <div class="mb-10">
                    <label>Date</label>
                    <input type="text" id="departDate" class="custom-input" data-toggle="datepicker" name="from_date" readonly>
                </div>
                <div class="mb-10">
                    <label>Depart After</label>
                    <select id="departTime" class="custom-input">
                        <option selected="selected" value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </div>
            </div>
            <div id="returnContainer" class="date-time-box d-none">
                <div class="mb-10">
                    <label>Return</label>
                    <input type="text" id="returnDate" class="custom-input" data-toggle="datepicker" name="to_date" readonly>
                </div>
                <div class="mb-10">
                    <label>Depart After</label>
                    <select id="returnTime" class="custom-input">
                        <option selected="selected" value="00:00">00:00</option>
                        <option value="01:00">01:00</option>
                        <option value="02:00">02:00</option>
                        <option value="03:00">03:00</option>
                        <option value="04:00">04:00</option>
                        <option value="05:00">05:00</option>
                        <option value="06:00">06:00</option>
                        <option value="07:00">07:00</option>
                        <option value="08:00">08:00</option>
                        <option value="09:00">09:00</option>
                        <option value="10:00">10:00</option>
                        <option value="11:00">11:00</option>
                        <option value="12:00">12:00</option>
                        <option value="13:00">13:00</option>
                        <option value="14:00">14:00</option>
                        <option value="15:00">15:00</option>
                        <option value="16:00">16:00</option>
                        <option value="17:00">17:00</option>
                        <option value="18:00">18:00</option>
                        <option value="19:00">19:00</option>
                        <option value="20:00">20:00</option>
                        <option value="21:00">21:00</option>
                        <option value="22:00">22:00</option>
                        <option value="23:00">23:00</option>
                    </select>
                </div>
            </div>
            <div class="mb-10 seats">
                <label>Seats</label>
                <div class="position-relative">
                    <div id="searchWidgetTicketTypeContainer">
                        <button id="searchWidgetTicketType" class="ticket-type-dropdown" type="button">
                            <i class="fa fa-user user-icon"></i>
                            <span class="ticket-count" id="ticketCount">1</span><i class="fa fa-angle-down down-arrow-icon"></i>
                        </button>
                        <div id="searchWidgetTicketTypeOptionContainer" class="ticket-type-option d-none">
                            <span>Seats</span>
                            <div class="jquery-select-step">
                                <select name="selectTicketType" id="selectTicketType" class="select-step">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="submit" class="btn btn-black" value="SEARCH" onclick="Search()">

        </div>
    </div>
    <?php
    $api_redirect_url = 'https://bookings.easybus.com/affiliate/api/redirect/URL';
    $request_url = wp_remote_get($api_redirect_url);
    if (is_wp_error($request_url)) {
        return false;
    }
    $get_body = wp_remote_retrieve_body($request_url);
    $get_data = json_decode($get_body);
    $op = '?' . $get_data->redirectParams->operator . '=';
    $dep_st = '&' . $get_data->redirectParams->departureStation . '=';
    $arr_st = '&' . $get_data->redirectParams->arrivalStation . '=';
    $dep_tm = '&' . $get_data->redirectParams->departureTime . '=';
    $arr_tm = '&' . $get_data->redirectParams->arrivalTime . '=';
    $provider = $get_data->provider;
    $part_num = $get_data->retailerParterNumber;
    //$redirect_url = 'https://bookings.easybus.com/affiliate/api/redirect/save?redirectURL='.urlencode($get_data->redirectBaseUrl.'?'.$op.'=&'.$dep_st.'=&'.$arr_st.'=&'.$dep_tm.'=&'.$arr_tm.'=&retailer_partner_number='.$part_num.'&pax=1&currency=GBP&locale=en&provider='.$provider);


    //echo print_r($data->MarketingCarrierDetails);
    function mktCarrierNameByCode($code, $MarketingCarrierDetails)
    {
        foreach ($MarketingCarrierDetails as $mkt_data) {
            $mkt_code = $mkt_data->code;
            if ($code === $mkt_code) {
                return $mkt_data;
            }
        }
    }


    ?>
    <h4 class="sml-heading">Cheap Bus Tickets from <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?></h4>
    <p class="mb-35">easyBus can help you travel from <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?>
    <?php 
    if(!$noScheduleAvailable)
    {
    ?>
        with up to <strong><?php echo count($min_price); ?></strong> connections per day, and fares from as little
        as <strong><?php echo $currency_symbol . min($min_price_array); ?></strong>. We partner with more than 150 bus operators to bring you 1000s of locations
        you can travel to by bus, at simply great fares.    
    <?php 
    }
    ?>
    </p>
    <div class="main-section">
        <div class="search-result-section">            
            <div class="journey-section">        
                <!-- <div class="sep_line"></div> -->
                <br/>
                <h4 class="sml-heading">Journey Summary</h4>
                <?php 
                if($noScheduleAvailable)
                {
                ?>
                    <p>
                        Bus Departs - <strong><?php echo ucwords($string_from); ?></strong> |
                        Bus Arrives - <strong><?php echo ucwords($string_to); ?></strong>
                    </p>
                <?php 
                }
                else
                { 
                ?>
                    <p>
                        Typical Journey Time - <strong><?php echo secToHR(min($duration_array)); ?></strong> |
                        Bus Departs - <strong><?php echo ucwords($string_from); ?></strong> |
                        Bus Arrives - <strong><?php echo ucwords($string_to); ?></strong> |
                        Buses on This Route - <strong><?php echo implode(", ", $get_trade_name); ?></strong> 
                    </p>
                <?php 
                }
                ?>
                
            </div>
            <div class="sep_line"></div>
            <div class="alert alert-primary d-none" role="alert">
                <span>
                    <svg id="Capa_1" data-name="Capa 1" fill="#004085" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 395.39 792" class="m-ticket-holder-info">
                        <title>mobile-phone</title>
                        <path d="M521.81,0H270.19S198.3,0,198.3,72V720c0,72,71.89,72,71.89,72H521.81s71.89,0,71.89-72V72C593.69,0,521.81,0,521.81,0ZM342.08,43.89c0-4.46,2.12-8,4.78-8h98.28c2.62,0,4.78,3.56,4.78,8V46c0,4.46-2.16,7.94-4.78,7.94H346.86c-2.62,0-4.78-3.56-4.78-7.94ZM396,762a35.95,35.95,0,1,1,35.94-35.94A36,36,0,0,1,396,762Zm161.75-97H234.25V89.86h323.5Z" transform="translate(-198.3)"></path>
                        <path d="M383,267.06H256.3V393.72H383ZM357.62,368.19h-76v-75.8h76Z" transform="translate(-198.3)"></path>
                        <rect x="108.66" y="317.72" width="25.33" height="25.33"></rect>
                        <rect x="260.64" y="368.39" width="25.34" height="25.33"></rect>
                        <rect x="311.31" y="368.39" width="25.34" height="25.33"></rect>
                        <polygon points="235.31 317.72 260.64 317.72 260.64 343.06 336.64 343.06 336.64 267.06 311.31 267.06 311.31 292.39 285.98 292.39 285.98 267.06 209.98 267.06 209.98 393.72 235.31 393.72 235.31 317.72"></polygon>
                        <path d="M383,115.07H256.3V241.73H383ZM357.62,216.39h-76v-76h76Z" transform="translate(-198.3)"></path>
                        <rect x="108.66" y="165.73" width="25.33" height="25.33"></rect>
                        <path d="M535,115.07H408.29V241.73H535ZM509.62,216.39h-76v-76h76Z" transform="translate(-198.3)"></path>
                        <rect x="260.64" y="165.73" width="25.34" height="25.33"></rect>
                        <rect x="55.16" y="485.69" width="284.32" height="20.18"></rect>
                        <rect x="55.16" y="527.69" width="284.32" height="20.18"></rect>
                        <rect x="55.16" y="569.69" width="284.32" height="20.18"></rect>
                    </svg>
                </span> On the <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?> route there is no need to print your ticket. Just use your mobile device.
            </div>
            <?php
                 if(!empty($_GET["searchDate"])){
                   $dept_date = date('d F', strtotime($_GET["searchDate"])); 
                 }else{
                   $dept_date = date('d F', strtotime(date("Y-m-d"))); 
                 }
                 $dept_next_day = date('Y-m-d', strtotime($dept_date . ' +1 day'));
                ?>
                <div class="dept_date_main">
                    <span class="dept_date">Departures on <?php echo @$dept_date;?></span>
                    <span class="dept_next_day" onclick="change_day_fun('<?php echo $dept_next_day;?>');">Show Me the Next Day</span>
                    <span style="display: none;" id="searchDiffDate" data-toggle="datepicker"><input type="text"  name="searchDiffDate" readonly></span>
                    <span class="dept_diff_day" id="showSearchDiffDatePicker" data-method="show">Select a Different Date of Travel</span>
                    <span class="data_loader" style="display: none;">
                        <img id="" src='../wp-content/themes/logipro-child/ajax-loader.gif'/>
                    </span>
                </div>
            <div class="filter-date-sort">
                
                <div class="search-filter">
                    <div class="sort">
                        <i class="fa fa-money"></i>
                        <span>Currency</span>
                        <ul>
                            <li>
                                <button class="sort__btn">
                                    <div class="select-curr"><?php echo $currency; ?></div>
                                    <i class="fa fa-caret-down
                                    fa-w-10"></i>
                                </button>
                                <ul class="curr">
                                    <li><a>EUR</a></li>
                                    <li><a>HRK</a></li>
                                    <li><a>CZK</a></li>
                                    <li><a>NOK</a></li>
                                    <li><a>SEK</a></li>
                                    <li><a>CHF</a></li>
                                    <li><a>GBP</a></li>
                                    <li><a>USD</a></li>
                                    <li><a>BAM</a></li>
                                    <li><a>PLN</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <!--                <div class="sort">
                                        <i class="fa fa-sort-amount-asc"></i>
                                        <span>SORT BY</span>
                                        <ul>
                                            <li>
                                                <button class="sort__btn">
                                                    <div>Departure</div>
                                                    <i class="fa fa-caret-down
                                                    fa-w-10"></i>
                                                </button>
                                                <ul class="show">
                                                    <li><a class="sort_select">Departure</a></li>
                                                    <li><a class="sort_select">Rating</a></li>
                                                    <li><a class="sort_select">Price</a></li>
                                                    <li><a class="sort_select">Travel
                                                            time</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>-->
                </div>
            </div>
            <div class="search-result">
                <div class="accordion-container routes-list" id="schedules">
                    <?php

                    function readIncludedData($type, $id, $included)
                    {
                        foreach ($included as $data) {
                            if ($data->type === $type && $data->id === $id) {
                                return $data;
                            }
                        }
                    }

                    for ($idx = 0; $idx < count($data->OutboundData->data); $idx++) {
                        $book_out = $get_records->data[$idx]->attributes->booked_out; // if false then display
                        //display "booked_out = false" records only
                        if (empty($book_out)) {
                            $cheapest_total_adult_price = number_format(($get_records->data[$idx]->attributes->cheapest_total_adult_price) / 100, 2);
                            $min_price[] = array_push($min_price_array, $cheapest_total_adult_price);
                            $duration_in_hrs = secToHR($get_records->data[$idx]->attributes->duration); //show in hours and min
                            array_push($duration_array, $get_records->data[$idx]->attributes->duration);

                            $departure_time = $get_records->data[$idx]->attributes->departure_time;
                            $arrival_time = $get_records->data[$idx]->attributes->arrival_time;

                            $for_relationships = $get_records->data[$idx]->relationships;
                            $segments = $for_relationships->segments->data;

                            //                $mkt_type_detail = $get_records->data[$idx]->relationships->marketing_carrier->data->type;
                            $mkt_id_detail = $for_relationships->marketing_carrier->data->id; //code
                            $marketting_carrier_details = mktCarrierNameByCode($mkt_id_detail, $data->MarketingCarrierDetails);

                            $dept_details = readIncludedData($for_relationships->departure_station->data->type, $for_relationships->departure_station->data->id, $data->OutboundData->included);
                            $arrv_details = readIncludedData($for_relationships->arrival_station->data->type, $for_relationships->arrival_station->data->id, $data->OutboundData->included);

                            for ($rel_cnt = 0; $rel_cnt < count($segments); $rel_cnt++) {
                                $rel_type = $segments[$rel_cnt]->type;
                                $rel_id = $segments[$rel_cnt]->id;
                                // comapare with include objects type and id snd get attribute & relationship values
                                $segment_length = count($segments) - 1; // get the text value to be displayed
                            }
                            $op_val = $get_records->data[$idx]->relationships->marketing_carrier->data->id;
                            $dep_st_val = $get_records->data[$idx]->relationships->departure_station->data->id;
                            $arr_st_val = $get_records->data[$idx]->relationships->arrival_station->data->id;

                            $redirect_url = $get_data->redirectBaseUrl . $op . $op_val . $dep_st . $dep_st_val . $arr_st
                                . $arr_st_val . $dep_tm . gmdate('Y-m-d H:i', strtotime($departure_time)) . $arr_tm . gmdate('Y-m-d H:i', strtotime($arrival_time)) . '&retailer_partner_number=' . $part_num . '&pax=1&currency=' . $currency . '&locale=en';

                            ?>
                            <div class="set">
                                <div itemscope="" itemtype="http://schema.org/BusTrip">
                                    <meta itemprop="departureBusStop" content="<?php echo $dept_details->attributes->name;?>">
                                    <meta itemprop="departureTime" content="<?php echo gmdate('Y-m-d H:i:s', strtotime($departure_time));?>">
                                    <meta itemprop="arrivalBusStop" content="<?php echo $arrv_details->attributes->name; ?>">
                                    <meta itemprop="arrivalTime" content="<?php echo gmdate('Y-m-d H:i:s', strtotime($arrival_time));?>">
                                    <meta itemprop="provider" content="<?php echo $marketting_carrier_details->trade_name; ?>">
                                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <meta itemprop="priceCurrency" content="<?php echo $currency;?>">
                                        <meta itemprop="price" content="<?php echo $cheapest_total_adult_price;?>">
                                    </div>
                                </div>
                                <a href="javascript:void(0);">
                                    <i class="fa fa-plus"></i>
                                    <div class="routes-button api-route">
                                        <div class="routes-list__info routes-list__start">
                                            <div><span class="date"><?php echo gmdate('d M', strtotime($departure_time)); ?></span><span class="divider">&nbsp;|&nbsp;</span><span class="time"><?php echo gmdate('H:i', strtotime($departure_time)); ?></span><br>
                                                <div title="" class="departure-station" data-original-title="<?php echo $dept_details->attributes->name; ?>"><?php echo $dept_details->attributes->name; ?></div>
                                            </div>
                                        </div>
                                        <div class="routes-list__info routes-list__duration">
                                            <span class=""><?php echo $duration_in_hrs; ?></span><span class="divider"><span></span><span></span><span></span></span><span class="routes-list__changes">
                                                <?php if (count($segments) > 1) { ?>
                                                    <span class="routes-list__changes_icon"><?php echo count($segments) - 1; ?></span><span class="routes-list__changes_text"> Bus Change</span></span>
                                        <?php } else { ?>
                                            <span class="routes-list__changes_text"> Direct</span></span>
                                        <?php } ?>
                                        </div>
                                        <div class="routes-list__info routes-list__finish">
                                            <p><span class="date"><?php echo gmdate('d M', strtotime($arrival_time)); ?></span><span class="divider">&nbsp;|&nbsp;</span><span><?php echo gmdate('H:i', strtotime($arrival_time)); ?></span><br></p>
                                            <div class="destination-station">
                                                <div title="" class="destination-station-holder" data-original-title="<?php echo $arrv_details->attributes->name; ?>"><?php echo $arrv_details->attributes->name; ?></div>
                                            </div>
                                        </div>
                                        <div class="routes-list__info routes-list__extra routes-list__extra--1"><span class="routes-list__mobile">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M368.219,0H143.781c-21.823,0-39.577,17.755-39.577,39.577v432.845c0,21.823,17.755,39.577,39.577,39.577h224.438
                                                    c21.823,0,39.577-17.755,39.577-39.577V39.577C407.796,17.755,390.042,0,368.219,0z M392.767,472.423
                                                    c0,13.535-11.012,24.548-24.548,24.548H143.781c-13.536,0-24.548-11.013-24.548-24.548v-24.548h273.534V472.423z M392.767,48.094
                                                    H353.19c-4.151,0-7.515,3.365-7.515,7.515c0,4.15,3.364,7.515,7.515,7.515h39.577v369.722H119.233V63.123h197.386
                                                    c4.151,0,7.515-3.365,7.515-7.515c0-4.15-3.364-7.515-7.515-7.515H119.233v-8.517c0-13.535,11.012-24.548,24.548-24.548h224.438
                                                    c13.536,0,24.548,11.013,24.548,24.548V48.094z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M272.031,464.908h-32.063c-4.151,0-7.515,3.365-7.515,7.515c0,4.15,3.364,7.515,7.515,7.515h32.063
                                                    c4.151,0,7.515-3.365,7.515-7.515C279.546,468.273,276.182,464.908,272.031,464.908z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M257.002,24.047H256c-4.151,0-7.515,3.365-7.515,7.515s3.364,7.515,7.515,7.515h1.002c4.151,0,7.515-3.365,7.515-7.515
                                                    S261.153,24.047,257.002,24.047z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M289.065,24.047h-1.002c-4.151,0-7.515,3.365-7.515,7.515s3.364,7.515,7.515,7.515h1.002c4.151,0,7.515-3.365,7.515-7.515
                                                    S293.216,24.047,289.065,24.047z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M224.939,24.047h-1.002c-4.151,0-7.515,3.365-7.515,7.515s3.364,7.515,7.515,7.515h1.002c4.151,0,7.515-3.365,7.515-7.515
                                                    S229.09,24.047,224.939,24.047z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M239.969,144.282h-88.172c-4.151,0-7.515,3.365-7.515,7.515v88.172c0,4.15,3.364,7.515,7.515,7.515h88.172
                                                    c4.151,0,7.515-3.365,7.515-7.515v-88.172C247.483,147.646,244.12,144.282,239.969,144.282z M232.454,232.454h-73.143v-73.143
                                                    h73.143V232.454z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M215.922,168.329h-40.078c-4.151,0-7.515,3.365-7.515,7.515v40.078c0,4.15,3.364,7.515,7.515,7.515h40.078
                                                    c4.151,0,7.515-3.365,7.515-7.515v-40.078C223.436,171.693,220.073,168.329,215.922,168.329z M208.407,208.407h-25.049v-25.049
                                                    h25.049V208.407z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M239.969,264.517h-88.172c-4.151,0-7.515,3.365-7.515,7.515v88.172c0,4.15,3.364,7.515,7.515,7.515h88.172
                                                    c4.151,0,7.515-3.365,7.515-7.515v-88.172C247.483,267.881,244.12,264.517,239.969,264.517z M232.454,352.689h-73.143v-73.143
                                                    h73.143V352.689z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M215.922,288.564h-40.078c-4.151,0-7.515,3.365-7.515,7.515v40.078c0,4.15,3.364,7.515,7.515,7.515h40.078
                                                    c4.151,0,7.515-3.365,7.515-7.515v-40.078C223.436,291.928,220.073,288.564,215.922,288.564z M208.407,328.642h-25.049v-25.049
                                                    h25.049V328.642z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M360.204,264.517h-88.172c-4.151,0-7.515,3.365-7.515,7.515v88.172c0,4.15,3.364,7.515,7.515,7.515h88.172
                                                    c4.151,0,7.515-3.365,7.515-7.515v-88.172C367.718,267.881,364.355,264.517,360.204,264.517z M352.689,352.689h-73.143v-73.143
                                                    h73.143V352.689z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M336.157,288.564h-40.078c-4.151,0-7.515,3.365-7.515,7.515v40.078c0,4.15,3.364,7.515,7.515,7.515h40.078
                                                    c4.151,0,7.515-3.365,7.515-7.515v-40.078C343.671,291.928,340.308,288.564,336.157,288.564z M328.642,328.642h-25.049v-25.049
                                                    h25.049V328.642z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M360.204,200.391h-8.016H327.64v-17.033h16.532c4.151,0,7.515-3.365,7.515-7.515v-24.047c0-4.15-3.364-7.515-7.515-7.515
                                                    c-4.151,0-7.515,3.365-7.515,7.515v16.532h-16.532c-4.151,0-7.515,3.365-7.515,7.515v24.548h-9.018v-24.548
                                                    c0-4.15-3.364-7.515-7.515-7.515h-24.047c-4.151,0-7.515,3.365-7.515,7.515s3.364,7.515,7.515,7.515h16.532v24.548
                                                    c0,4.15,3.364,7.515,7.515,7.515h48.595v17.033h-33.065v-0.501c0-4.15-3.364-7.515-7.515-7.515s-7.515,3.365-7.515,7.515v8.016
                                                    c0,4.15,3.364,7.515,7.515,7.515h48.094c4.151,0,7.515-3.365,7.515-7.515v-24.548h0.501c4.151,0,7.515-3.365,7.515-7.515
                                                    S364.355,200.391,360.204,200.391z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M280.047,232.454h-8.016c-4.151,0-7.515,3.365-7.515,7.515s3.364,7.515,7.515,7.515h8.016
                                                    c4.151,0,7.515-3.365,7.515-7.515S284.198,232.454,280.047,232.454z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M272.031,200.391c-4.151,0-7.515,3.365-7.515,7.515v8.016c0,4.15,3.364,7.515,7.515,7.515s7.515-3.365,7.515-7.515v-8.016
                                                    C279.546,203.756,276.182,200.391,272.031,200.391z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M280.047,144.282h-8.016c-4.151,0-7.515,3.365-7.515,7.515c0,4.15,3.364,7.515,7.515,7.515h8.016
                                                    c4.151,0,7.515-3.365,7.515-7.515C287.562,147.646,284.198,144.282,280.047,144.282z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M305.096,144.282h-1.002c-4.151,0-7.515,3.365-7.515,7.515c0,4.15,3.364,7.515,7.515,7.515h1.002
                                                    c4.151,0,7.515-3.365,7.515-7.515C312.611,147.646,309.247,144.282,305.096,144.282z"></path>
                                                        </g>
                                                    </g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                    <g></g>
                                                </svg><span class="m-ticket-holder">
                                                    M-TICKET
                                                </span></span>
                                            <div class="routes-list__rating">
                                                <span>
                                                    <span class="company company-middle-row">
                                                        <span class="routes-list__company_rating">
                                                            <span class="company-letter-code" data-original-title="" title="" style="color: inherit;">
                                                                <?php echo $marketting_carrier_details->trade_name; ?>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="routes-list__info routes-list__price"><span class="route-price-amount">
                                                <?php echo $cheapest_total_adult_price; ?>
                                                <span class="currency"><?php echo $currency; ?></span>
                                                <span class="route-price-amount-group">
                                                    1 <span class="fa fa-user"></span>
                                                </span></span></div>
                                        <div class="buy-button-holder">
                                            <div>
                                                <button class="btn-inner btn route__item__return__buy routes-list__cta compact-button routes-list__cta--operator btn--buy-from-partner routes-list__cta compact-button" onclick="call_url('<?php echo $redirect_url; ?>');">Buy from Partner</button></div>
                                        </div>
                                    </div>


                                </a>
                                <div class="content card-body">
                                    <div class="panel-body__inner">
                                        <?php
                                                $counter = 0;
                                                $pre_arrival = '';
                                                foreach ($segments as $segment_data) {
                                                    $segment_details = readIncludedData($segment_data->type, $segment_data->id, $data->OutboundData->included);

                                                    $seg_departure_time = $segment_details->attributes->departure_time;
                                                    $seg_arrival_time = $segment_details->attributes->arrival_time;
                                                    $seg_relationships = $segment_details->relationships;

                                                    $seg_mkt_id_detail = $seg_relationships->operating_carrier->data->id; //code

                                                    $seg_marketting_carrier_details = mktCarrierNameByCode($seg_mkt_id_detail, $data->MarketingCarrierDetails);

                                                    $seg_dept_details = readIncludedData($seg_relationships->departure_station->data->type, $seg_relationships->departure_station->data->id, $data->OutboundData->included);
                                                    $seg_arrv_details = readIncludedData($seg_relationships->arrival_station->data->type, $seg_relationships->arrival_station->data->id, $data->OutboundData->included);
                                                    $seg_duration = datediff($seg_departure_time, $seg_arrival_time);
                                                    $layover_time = '';
                                                    if ($counter > 0 && $pre_arrival != '') {
                                                        $layover_time = datediff($pre_arrival, $seg_departure_time);
                                                    }
                                                    ?>
                                            <div>
                                                <div class="waiting-line" style="display:<?php echo ($counter == 0) ? 'none' : 'block'; ?>;">
                                                    <div class="waiting-block waiting-block-step">
                                                        <div class="line-waiting"></div>
                                                        <div class="boxes"><span class="box-waiting">
                                                                Change of bus |
                                                                <span><?php echo $layover_time; ?> layover in <?php echo $seg_dept_details->attributes->name; ?></span></span></div>
                                                    </div>
                                                </div>
                                                <table class="route-suggestion-additional">
                                                    <tbody>
                                                        <tr class="additional-content-top additional-content-top-search-results">
                                                            <td class="suggestion-col-1"><span class="suggestion-row-1"><?php echo gmdate('d M, H:i', strtotime($seg_departure_time)); ?></span><span class="route-suggestion-rounded-button suggestion-row-2">
                                                                    <i class="fa fa-clock-o"></i>

                                                                    <?php echo $seg_duration; ?>
                                                                </span><span class="suggestion-row-3"><?php echo gmdate('d M, H:i', strtotime($seg_arrival_time)); ?></span></td>
                                                            <td class="suggestion-col-2"><span class="route-suggestion-circle suggestion-row-1"></span>
                                                                <span class="route-suggestion-vertical-line line-green suggestion-row-1"></span>
                                                                <i aria-hidden="true" class="fa fa-bus route-suggestion-bus-icon suggestion-row-2"></i>
                                                                <span class="route-suggestion-vertical-line line-green suggestion-row-2"></span>
                                                                <span class="route-suggestion-circle suggestion-row-3"></span>
                                                                <span class="suggestion-row-1">
                                                                    <span><?php echo $seg_dept_details->attributes->name; ?></span>

                                                                </span><br>
                                                                <span class="suggestion-row-3"><span><?php echo $seg_arrv_details->attributes->name; ?></span></span></td>
                                                            <td class="suggestion-col-3">
                                                                <span class="route-suggestion-vertical-line line-gray"></span>
                                                                <span class="suggestion-row-1">
                                                                    <span class="company company-middle-row">
                                                                        <span class="routes-list__company_rating">
                                                                            <span class="operator-logo">
                                                                                <img src="https://bookings.easybus.com/affiliate/api/marketing-carrier/logo?code=<?php echo $seg_marketting_carrier_details->code; ?>" alt="<?php echo $seg_marketting_carrier_details->trade_name; ?>">
                                                                            </span>
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php $pre_arrival = $seg_arrival_time;
                                                    $counter++;
                                                } ?>
                                        <table class="route-suggestion-additional-2">
                                            <tbody>
                                                <tr class="suggestion-connection-row suggestion-connection-row-ticket-type">
                                                    <td class="ticket-print-info">
                                                        <div><span class="m-ticket-holder suggestion-row-1">
                                                                <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 395.39 792" class="m-ticket-holder-info">
                                                                    <title>mobile-phone</title>
                                                                    <path d="M521.81,0H270.19S198.3,0,198.3,72V720c0,72,71.89,72,71.89,72H521.81s71.89,0,71.89-72V72C593.69,0,521.81,0,521.81,0ZM342.08,43.89c0-4.46,2.12-8,4.78-8h98.28c2.62,0,4.78,3.56,4.78,8V46c0,4.46-2.16,7.94-4.78,7.94H346.86c-2.62,0-4.78-3.56-4.78-7.94ZM396,762a35.95,35.95,0,1,1,35.94-35.94A36,36,0,0,1,396,762Zm161.75-97H234.25V89.86h323.5Z" transform="translate(-198.3)"></path>
                                                                    <path d="M383,267.06H256.3V393.72H383ZM357.62,368.19h-76v-75.8h76Z" transform="translate(-198.3)"></path>
                                                                    <rect x="108.66" y="317.72" width="25.33" height="25.33"></rect>
                                                                    <rect x="260.64" y="368.39" width="25.34" height="25.33"></rect>
                                                                    <rect x="311.31" y="368.39" width="25.34" height="25.33"></rect>
                                                                    <polygon points="235.31 317.72 260.64 317.72 260.64 343.06 336.64 343.06 336.64 267.06 311.31 267.06 311.31 292.39 285.98 292.39 285.98 267.06 209.98 267.06 209.98 393.72 235.31 393.72 235.31 317.72"></polygon>
                                                                    <path d="M383,115.07H256.3V241.73H383ZM357.62,216.39h-76v-76h76Z" transform="translate(-198.3)"></path>
                                                                    <rect x="108.66" y="165.73" width="25.33" height="25.33"></rect>
                                                                    <path d="M535,115.07H408.29V241.73H535ZM509.62,216.39h-76v-76h76Z" transform="translate(-198.3)"></path>
                                                                    <rect x="260.64" y="165.73" width="25.34" height="25.33"></rect>
                                                                    <rect x="55.16" y="485.69" width="284.32" height="20.18"></rect>
                                                                    <rect x="55.16" y="527.69" width="284.32" height="20.18"></rect>
                                                                    <rect x="55.16" y="569.69" width="284.32" height="20.18"></rect>
                                                                </svg>
                                                                M-TICKET
                                                            </span><span class="text-capitalize">(Show QR code upon entering the bus, no need for printed ticket)</span></div>
                                                        <div class="route-price-amount">
                                                            <?php echo $cheapest_total_adult_price; ?>
                                                            <span class="currency"><?php echo $currency; ?></span></div>
                                                    </td>
                                                </tr>
                                                <tr class="show-on-mobile">
                                                    <td class="route-list-detail-button">
                                                        <!----><button class="btn-inner btn route__item__return__buy routes-list__cta compact-button routes-list__cta--operator btn--buy-from-partner routes-list__cta compact-button" onclick="call_url('<?php echo $redirect_url; ?>');">Buy from Partner</button></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    }

                    if(count($data->OutboundData->data) <= 0)
                    {
                    // }
                    ?>
                    <div class="no-schedule-container">
                        <div class="no-schedule-head">
                            <span>Sorry. We don't have any departures on the <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?> route departing tomorrow - <?php echo date('jS F, Y', strtotime($current_date))?></span>
                        </div>
                        <div class="no-schedule-body">
                            <span>
                            We may have departures on this route on a different date. Please 
                            <span class="no-schedule-body-ctrl"><input type="text" id="searchDate" class="custom-input" data-toggle="datepicker" name="searchDate" readonly> 
                            <button id="showSearchDatePicker" data-method="show">select your date of travel</button>
                            <span class="data_loader_main" style="display: none;">
                        <img id="" src='../wp-content/themes/logipro-child/ajax-loader.gif'/>
                    </span>
                          </span> and we will see what we can find.
                            </span>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>                
                <div class="pager-container">
                    <ul id="schedulePaging" class="pagination"></ul>
                </div>
            </div>
            <div class="why-travel-bus">
                <h4 class="sml-heading">Take the Bus Between <?php echo ucwords($string_from); ?> and <?php echo ucwords($string_to); ?>?</h4>
                <?php 
                if($noScheduleAvailable)
                {
                ?>                    
                    <p>We work with more than 150 operators to find the lowest fares. On this route you are guaranteed great value.</p> 
                    <p>Remember. Bus travel is low emission so you’re doing your bit for the environment as well as grabbing a super deal. All our bus operators maintain modern, well equipped fleets, offering comforts including wi-fi, electric outlets, WC, and reclining seats.</p>
                    <p>We recommend booking as far in advance as you can in order to secure your seat. Seats on the <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?> bus route sell quickly, so to secure a low fare book now.</p>
                <?php 
                }
                else
                { 
                ?>
                    <p>Looking to get from <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?>? Take the bus is a great choice and is often the cheapest method of transport. With tickets from as low as <strong><?php echo $currency_symbol . min($min_price_array); ?></strong> on this route you are guaranteed great value. We sell tickets on this route with <?php echo implode(", ", $get_trade_name); ?>.</p> 
                    <p>Remember. Bus travel is low emission so you’re doing your bit for the environment as well as grabbing a super deal. All our bus operators maintain modern, well equipped fleets, offering comforts including wi-fi, electric outlets, WC, and reclining seats.</p>
                    <p>We recommend booking as far in advance as you can in order to secure your seat. Seats on the <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?> bus route sell quickly, so to secure a fare as low as <strong><?php echo $currency_symbol . min($min_price_array); ?></strong> book now.</p>
                <?php 
                }
                ?>
            </div>
            <div class="sep_line"></div>
        </div>
        <div class="useful-information">            
            <section class="vc_section"><br/>
                <h4 style="color: #ff6600;text-align: left;margin: 6px 0;">Useful Information for <?php echo ucwords($string_from);?> and <?php echo ucwords($string_to);?></h4>
                <div class="location-info-section">
                    <div class="location-info-column">
                        <?php 
                        set_query_var('isOrigin',true);
                        set_query_var('locationName',ucwords($string_from));
                        set_query_var('tradeName',implode(", ", $get_trade_name));
                        set_query_var('defaultOriginText','Start your journey in ' . ucwords($string_from));
                        set_query_var('defaultOriginTextWhenNoTextIsAvailable',(!$noScheduleAvailable?('The shortest journey time from ' . ucwords($string_from) . ' is ' . secToHR(min($duration_array)) . ','):'') . ' Board the ' . implode(", ", $get_trade_name) . ' bus and we will have you on your way to ' .  ucwords($string_to) . ' in no time.' );
                        get_template_part('wikki');
                        ?>
                    </div>
                    <div class="location-info-column">
                        <?php
                        set_query_var('isOrigin',false);
                        set_query_var('locationName',ucwords($string_to));
                        if($noScheduleAvailable)
                        {
                            set_query_var('defaultDestinationText','Starting in ' .  ucwords($string_from) . ', you can enjoy low fares and a comfortable trip in a modern, well-equipped coach. <a href="https://www.easybus.com">easyBus.com</a>.');
                        }
                        else
                        {
                            set_query_var('defaultDestinationText','Starting in ' .  ucwords($string_from) . ', you can be in ' .  ucwords($string_to) . ' in just ' .  secToHR(min($duration_array)) . ' with ' . implode(", ", $get_trade_name) . '. Book today from just ' . $currency_symbol . min($min_price_array) . ' with <a href="https://www.easybus.com">easyBus.com</a>.');
                        }                      
                        get_template_part('wikki');
                        ?>
                    </div>
                </div>
                <div class="sep_line"></div>
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper"><br/>
                                <h4 style="color: #ff6600;text-align: left" class="vc_custom_heading">Where else can you travel?</h4>
                                <div class="popular-routes-links d-none">
                                    <div class="routes-links">
                                        <h4 class="sml-heading">Popular Routes from <span id="originCountry"></span></h4>
                                        <ul id="originRouteList">
                                            <li><a href="#">London to Manchester</a></li>
                                            <li><a href="#">London to Heathrow</a></li>
                                            <li><a href="#">London to Gatwick</a></li>
                                            <li><a href="#">London to Stansted</a></li>
                                        </ul>
                                    </div>
                                    <div class="routes-links">
                                        <h4 class="sml-heading">Popular Routes from <span id="departureCountry"></span></h4>
                                        <ul id="departureRouteList">
                                            <li><a href="#">Paris to Brussels</a></li>
                                            <li><a href="#">Paris to Lyon</a></li>
                                            <li><a href="#">Paris to Bordeaux</a></li>
                                            <li><a href="#">Paris to Amsterdam</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- <div class="sep_line"></div>
         -->

    <!--<div class="popular-routes-links">
            <div class="routes-links">
                <h4 class="sml-heading">Popular Routes from <?php echo ucwords($string_from); ?></h4>
                <ul>
                    <li><a href="#">London to Manchester</a></li>
                    <li><a href="#">London to Heathrow</a></li>
                    <li><a href="#">London to Gatwick</a></li>
                    <li><a href="#">London to Stansted</a></li>
                </ul>
            </div>
            <div class="routes-links">
                <h4 class="sml-heading">Popular Routes from <?php echo ucwords($string_to); ?></h4>
                <ul>
                    <li><a href="#">Paris to Brussels</a></li>
                    <li><a href="#">Paris to Lyon</a></li>
                    <li><a href="#">Paris to Bordeaux</a></li>
                    <li><a href="#">Paris to Amsterdam</a></li>
                </ul>
            </div>
        </div>-->        
        <div class="bus-logos">
            <img src="<?php echo get_site_url();?>/wp-content/uploads/2019/09/bus_co_logos.png" alt="easyBus offers routes from more than 150 bus operators">
        </div>
        
        <div class="easyBus-owner">
            <!-- <figure>
                <img src="<?php echo get_site_url();?>/wp-content/uploads/2019/09/steliosbus-1.jpg" alt="easyBus have been providing low cost bus routes since 2004">
            </figure> -->
            <div class="">
                <h4 class="sml-heading">easyBus – Convenient, flexible and great value bus tickets.</h4>
                <p>easyBus has been offering great value bus travel since 2004. Since mid-2019 we have
                    extended our network through partnerships with most of Europe’s leading bus companies. We now offer bus trip comparison, and online
                    booking, for more than 200 operators. This allows our customer to book thousands of possible routes in the UK and
                    throughout Europe. Wherever you are travelling to, easyBus offers you an amazing
                    range of destinations at great prices.</p>
            </div>
        </div>
        <div class="popular-routes">
            <h4 class="sml-heading">Popular Routes You can Book with easyBus.com</h4>
            <div class="route-boxes">
                <div class="route-box">
                    <img src="<?php echo get_site_url();?>/wp-content/uploads/2019/08/location_tag_gatwick.png" alt="Bus to London Gatwick from Just <?php echo $currency_symbol;?>2" />
                    <div class="route-title"><a href="/en/routes/london-gatwick/">London Gatwick from just <?php echo $currency_symbol;?>2</a> </div>
                </div>
                <div class="route-box">
                    <img src="<?php echo get_site_url();?>/wp-content/uploads/2019/08/location_tag_stansted.png" alt="Bus to London Stansted from Just <?php echo $currency_symbol;?>2" />
                    <div class="route-title"><a href="/en/routes/london-stansted/">London Stansted from just <?php echo $currency_symbol;?>2</a> </div>
                </div>
                <div class="route-box">
                    <img src="<?php echo get_site_url();?>/wp-content/uploads/2019/08/location_tag_luton.png" alt="Bus to London Luton from Just <?php echo $currency_symbol;?>2" />
                    <div class="route-title"><a href="/en/routes/london-luton/">London Luton from just <?php echo $currency_symbol;?>2</a> </div>
                </div>
                <div class="route-box">
                    <img src="<?php echo get_site_url();?>/wp-content/uploads/2019/08/location_tag_milan.jpg.png" alt="Bus to Milan to City Centre from Just <?php echo $currency_symbol;?>5" />
                    <div class="route-title"><a href="/en/routes/milan-airport-transfers/">Milan to City Centre from just <?php echo $currency_symbol;?>5</a> </div>
                </div>
            </div>
        </div>
        <div class="features-section">
            <div class="box">
                <!-- <div class="feature-icon"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/09/map-route-1.png" alt="Bus Tickets from just <?php echo $currency_symbol;?>1"></div>
                <h5 class="feature-title">Save Up To 60% <br />Bus Tickets From Just <?php echo $currency_symbol;?>1</h5> -->
                <div class="wpb_content_element title-with-icon clearfix top-style">
                    <span style="color:#ff6600 !important;background-color:#efefef !important;" class="span-icon oic-dollar rounded medium"></span>
                    <span style="color:#636363;">
                        <p style="text-align: center;">
                            <strong>Save up to 60%</strong><br>
                            Bus tickets from just £1
                        </p>
                        <p> </p>
                    </span>
                </div>
            </div>
            <div class="box">
                <!-- <div class="feature-icon"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/09/clock-1.png" alt="Bus Routes across the UK, Europe and Beyond"></div>
                <h5 class="feature-title">1000s Of Bus Routes To Choose From<br />
                    From More Than 150 Operators</h5> -->
                <div class="wpb_content_element title-with-icon clearfix top-style">
                    <span style="color:#ff6600 !important;background-color:#e8e8e8 !important;" class="span-icon oic-outlined-iconset-72 rounded medium"></span>
                    <span style="color:#636363;">
                        <p style="text-align: center;">
                            <strong>1000s of Bus Routes to choose from</strong><br>
                            From more than 150 operators
                        </p>
                        <p> </p>
                    </span>
                </div>
            </div>
            <div class="box">
                <!-- <div class="feature-icon"><img src="<?php echo get_site_url();?>/wp-content/uploads/2019/09/file-type-1.png" alt="Cheap Bus Tickets for <?php echo ucwords($string_from); ?> to <?php echo ucwords($string_to); ?> and 1000s of other bus routes at easyBus.com"></div>
                <h5 class="feature-title">Review Prices And Departures<br />
                    It’s Easy At EasyBus.Com</h5> -->
                <div class="wpb_content_element title-with-icon clearfix top-style">
                    <span style="color:#ff6600 !important;background-color:#e8e8e8 !important;" class="span-icon oic-clock-1 rounded medium "></span>
                    <span style="color:#636363;">
                        <p style="text-align: center;">
                            <strong>Review Prices and Departures</strong><br>
                            It’s easy at easyBus.com
                        </p>
                        <p> </p>
                    </span>
                </div>
            </div>
        </div>
        <!-- <div class="about-easyBus">
            <h4 class="sml-heading">About easyBus</h4>
            <p>easyBus, part of the easy Family of Brands, has been providing low-cost bus tickets since 2004, Founded by Stelios, creator of easyJet.com, we focus on great value bus tickets between 1000s of locations in the UK, Europe and beyond.</p>
            <div class="copyright-text">
                <p>© 2004-<?php echo gmdate('Y'); ?> easyBus Ltd. Registered No. 4671315 VAT No. 882 6179 86. Developed and Managed by <a href="https://www.ibooking.com">iBooking.com</a><br>
                    <a href="/privacy-policy/">Privacy Policy</a>&nbsp; <a href="/cookie-policy/">Cookie Policy</a>&nbsp; &nbsp;<a href="/about/about-us/">About easyBus</a>&nbsp; <a href="/about/partnership/">Bus Operator? – Sell with easyBus</a></p>
            </div>
        </div> -->
    </div>
	<input type="hidden" id="idOrigin" name="idOrigin" value="<?php echo ucwords($source_code); ?>">
    <input type="hidden" id="idDestination" name="idDestination" value="<?php echo ucwords($dest_code); ?>">
    <input type="hidden" id="OriginName" name="OriginName" value="<?php echo ucwords($string_from); ?>">
    <input type="hidden" id="destinationName" name="DestinationName" value="<?php echo ucwords($string_to); ?>">
</div>
</div>
<div>
    <!-- <div class="search-results">
        <noscript><strong>We're sorry but client-app doesn't work properly without JavaScript enabled. Please enable it to continue.</strong></noscript>
        <div id=app></div>
        <script src=https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/js/all.js> </script> <script src=/wp-content/js/chunk-vendors.2f327ad7.js> </script> <script src=/wp-content/js/app.207a4c02.js> </script> 
    </div> -->
    <script>
        var $ = jQuery;
        
        var tommorow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
        var day = tommorow.getDate()
        var month = tommorow.getMonth() + 1
        var year = tommorow.getFullYear()
        
        var dayAfterTommorow = new Date(new Date().getTime() + 48 * 60 * 60 * 1000);
        var dayT = dayAfterTommorow.getDate()
        var monthT = dayAfterTommorow.getMonth() + 1
        var yearT = dayAfterTommorow.getFullYear()
        

        var traveldate = year + "-" + month + "-" + day;
        var returndate = '';

        var ignoredList = [];
    </script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/slick.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/datepicker.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery-select-step.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/jquery.easy-autocomplete.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/SearchWidget.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/country.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/findIndex-pollyfill.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>-child/js/paging.js"></script>
    <script>
        function change_day_fun(id){
             $(".data_loader").show();
             window.location.href = setUrlParameter(window.location.href, 'searchDate',id);//window.location.href+''+queryString;
        }
        function call_url(url) {
            console.log('https://bookings.easybus.com/affiliate/api/redirect/save?redirectURL=' + encodeURIComponent(url) + '&provider=distribusion');
            // call api before link call
            jQuery.ajax({
                url: 'https://bookings.easybus.com/affiliate/api/redirect/save?redirectURL=' + encodeURIComponent(url) + '&provider=distribusion',
                type: 'GET',
                success: function(result) {
                    console.log(result);
                },
                error: function(error) {
                    console.log('error' + error);
                }
            });
            window.open(url);
        }

		function Search(){
			var departureDates = $("#departDate").val();
			var departuretime = $("#departTime").val();
			var st1 = departureDates.split("/");
			var strDepartureDates = st1[2] + "" + st1[1] + "" + st1[0];

			var returnDates = $("#returnDate").val();
			var returntime = $("#returnTime").val();
			var st2 = returnDates.split("/");
			var strReturnDates = st2[2] + "" + st2[1] + "" + st2[0];

			var seats = $("#selectTicketType").val();
			var baseLocation = window.location.protocol+'//'+window.location.host+'/en/routes'
			var url = "";
			url = baseLocation+"/easybus-offer-low-cost-airport-transfers-and-city-to-city-bus-routes-in-the-uk-and-europe/?seats=" + seats + "&idOrigin=" + $("#idOrigin").val()
					+ "&originlocation=" + $("#OriginName").val() + "&idDestination=" + $("#idDestination").val() + "&destinationlocation=" + $("#destinationName").val()
					+ "&departureDates=" + strDepartureDates + "&seatTypes=seat:1|students:0|Seniors:0||&departuretime=" + departuretime + "&id_prov=2&lng=english&isReferedByLanding=1";
			var chk = document.getElementById("retrun");
			if (chk.checked == true) {
				url = url + "&isReturn=1&returnDates=" + strReturnDates + "&returntime=" + returntime;
			}

			if(url !== ""){
				window.location.href = url;
			}
		}

		function OnFocus(event){
			event.target.select();
		}
        function setUrlParameter(url, key, value) {
            var key = encodeURIComponent(key),
                value = encodeURIComponent(value);

            var baseUrl = url.split('?')[0],
                newParam = key + '=' + value,
                params = '?' + newParam;

            if (url.split('?')[1] === undefined){ // if there are no query strings, make urlQueryString empty
                urlQueryString = '';
            } else {
                urlQueryString = '?' + url.split('?')[1];
            }

            // If the "search" string exists, then build params from it
            if (urlQueryString) {
                var updateRegex = new RegExp('([\?&])' + key + '[^&]*');
                var removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

                if (value === undefined || value === null || value === '') { // Remove param if value is empty
                    params = urlQueryString.replace(removeRegex, "$1");
                    params = params.replace(/[&;]$/, "");
                    
                } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
                    params = urlQueryString.replace(updateRegex, "$1" + newParam);
                    
                } else if (urlQueryString == '') { // If there are no query strings
                    params = '?' + newParam;
                } else { // Otherwise, add it to end of query string
                    params = urlQueryString + '&' + newParam;
                }
            }

            // no parameter was set so we don't need the question mark
            params = params === '?' ? '' : params;

            return baseUrl + params;
        }
        // datepicker slider
        jQuery(".slick-slider").slick({
            infinite: true,
            slidesToShow: 7,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        // toggle collapse
        jQuery('.pre-spin').css('display', 'flex');
        jQuery(document).ready(function() {
            jQuery('.pre-spin').css('display', 'none');
            jQuery(".set > a").on("click", function() {
                if (jQuery(this).hasClass("active")) {
                    jQuery(this).removeClass("active");
                    jQuery(this)
                        .siblings(".content")
                        .slideUp(200);
                    jQuery(".set > a >i")
                        .removeClass("fa-minus")
                        .addClass("fa-plus");
                } else {
                    jQuery(".set > a >i")
                        .removeClass("fa-minus")
                        .addClass("fa-plus");
                    jQuery(this)
                        .find("i")
                        .removeClass("fa-plus")
                        .addClass("fa-minus");
                    jQuery(".set > a").removeClass("active");
                    jQuery(this).addClass("active");
                    jQuery(".content").slideUp(200);
                    jQuery(this)
                        .siblings(".content")
                        .slideDown(200);
                }
            });
            // search date picker
            jQuery('#departDate').datepicker();
			jQuery('#departDate').datepicker('reset').datepicker('destroy').datepicker({
                autoHide: true,
                format: 'dd/mm/yyyy'
            });
			
			$('#departDate').datepicker('setDate', new Date(year, month-1, day));
			$('#departDate').datepicker('setStartDate', new Date());
			
            jQuery('#returnDate').datepicker();
			jQuery('#returnDate').datepicker('reset').datepicker('destroy').datepicker({
                autoHide: true,
                format: 'dd/mm/yyyy'
            });
			
			$('#returnDate').datepicker('setDate', new Date(yearT, monthT-1, dayT));
			$('#returnDate').datepicker('setStartDate', new Date());

            
            // $('#showSearchDatePicker').on('click',function(){
            //     $('#searchDate').datepicker('show');
            // });
            

            // search date picker
            // $('#showSearchDatePicker').on('click',function(){
            //     $('#searchDate').datepicker('show');
            // });

            jQuery('#searchDiffDate').datepicker();
            jQuery('#searchDiffDate').datepicker('reset').datepicker('destroy').datepicker({
                autoHide: true,
                format: 'yyyy-mm-dd',
                trigger: '#showSearchDiffDatePicker'
            });
            $('#searchDiffDate').datepicker('setStartDate', new Date(yearT, monthT-1, dayT));
            $('#searchDiffDate').on('pick.datepicker', function (e) {
                $(".data_loader").show();
                var queryString = '';
                if(window.location.href.indexOf('searchDiffDate=')>-1){
                }
                else if(window.location.href.indexOf('?')>-1){
                    queryString = '&searchDate='+$('#searchDiffDate').datepicker('getDate', true);
                }
                else{
                    queryString = '?searchDate='+$('#searchDiffDate').datepicker('getDate', true);
                }

                window.location.href = setUrlParameter(window.location.href, 'searchDate', $('#searchDiffDate').datepicker('getDate', true));//window.location.href+''+queryString;
            });

            


            jQuery('#searchDate').datepicker();
			jQuery('#searchDate').datepicker('reset').datepicker('destroy').datepicker({
                autoHide: true,
                format: 'yyyy-mm-dd',
                trigger: '#showSearchDatePicker'
            });
            
			$('#searchDate').datepicker('setStartDate', new Date(yearT, monthT-1, dayT));
            $('#searchDate').on('pick.datepicker', function (e) {
                $(".data_loader_main").show();
                var queryString = '';
                if(window.location.href.indexOf('searchDate=')>-1){
                }
                else if(window.location.href.indexOf('?')>-1){
                    queryString = '&searchDate='+$('#searchDate').datepicker('getDate', true);
                }
                else{
                    queryString = '?searchDate='+$('#searchDate').datepicker('getDate', true);
                }

                window.location.href = setUrlParameter(window.location.href, 'searchDate', $('#searchDate').datepicker('getDate', true));//window.location.href+''+queryString;
            });

            // currency selector
            jQuery('.sort__btn,.sort').on('click',function() {
                
				jQuery(".curr").addClass('show');
					jQuery('.curr li a').click(function() {
						jQuery('.curr li a').removeClass('sort_select');
						jQuery(this).addClass('sort_select');
						jQuery(".curr").removeClass('show');
						var get_currency = jQuery(".sort_select").text();
						jQuery(".select-curr").text(get_currency);
						jQuery('.pre-spin').css('display', 'flex');
						window.location.href = "?get_currency=" + get_currency;
					});
            });
			$(document).mouseup(function(e) 
			{
				var container = $(".search-filter");

				// if the target of the click isn't the container nor a descendant of the container
				if (!container.is(e.target) && container.has(e.target).length === 0) 
				{
					$('.curr').removeClass('show');
				}
			});
			jQuery.ajax({
                url: 'https://bookings.easybus.com/affiliate/api/search/ignored_stations',
                type: 'GET',
                success: function(result) {
                    ignoredList = result;
                },
                error: function(error) {
                    ignoredList = null;
                }
            });

			$('input[type=radio][name=way]').change(function() {
				$('#returnContainer').toggleClass('d-none');
			});

            //Read More Link Binding Logic
            var maxLength = 500;
            $(".show-read-more").each(function(){
                var myStr = $(this).text();
                $(this).removeClass('d-none');
                if($.trim(myStr).length > maxLength){
                    var newStr = myStr.substring(0, maxLength);
                    var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
                    $(this).empty().html(newStr);
                    $(this).append(' <a href="javascript:void(0);" class="read-more">Read more...</a>');
                    $(this).append('<span class="more-text">' + removedStr + '</span>');
                }
            });
            $(".read-more").click(function(){
                $(this).siblings(".more-text").contents().unwrap();
                $(this).remove();
            });

            $('#schedules').pageMe({pagerSelector:'#schedulePaging',childSelector:'.set',showPrevNext:true,hidePageNumbers:true,perPage:10});
        });

		var departOptions = {
			minCharNumber:3,
	        placeholder: "From: City, Station or Airport",
			url: function(phrase) {
				return "//bookings.easybus.com/affiliate/api/search/station?term=" + phrase;
			},
			ajaxCallback: function(stations){
				console.log("\n------------------------------------\nAutocomplete response data : \n", stations, "\n------------------------------------\n");
				var destinationCode = $("#idDestination").val();
				let ignoredStationCodeIndex = ignoredList.code.findIndex(
					(item, index) => {
						return item === destinationCode;
					}
				);
				if (ignoredStationCodeIndex === -1) {
					stations = stations.filter(function(item) {
						return destinationCode !== item.code;
					});
					return stations;
				} else {
					stations = stations.filter(function(item) {
						return !ignoredList.code.includes(item.code);
					});
					return stations;
				}
			},
			getValue: "name",
			template: {
				type: "custom",
				method: function(value, item) {
					return value + " - <span class=\"autocomplete-description\">"+ AlphaCodeTwoCountryName[item.code.substr(0,2)].Country +"</span>";
				}
			},
			list: {
				match: {
					enabled: true
				},
				onSelectItemEvent: function() {
					var data = $("#departFrom").getSelectedItemData();
					$("#idOrigin").val(data.code);
					$("#OriginName").val(data.name);
				}
			}
		};
		
        var isArrivalInputFocus = false;
		var arrivalOptions = {
			minCharNumber:3,
            parentInputSelector:"#departFrom",
	        placeholder: "To: City, Station or Airport",
            input: {
                /* Events */
                onFocusEvent: function(status) {
                    isArrivalInputFocus = status;
                }
            },
			url: function(phrase) {
				var url = isArrivalInputFocus?"//bookings.easybus.com/affiliate/api/search/linked_stations?stationCode="+$("#idOrigin").val():"//bookings.easybus.com/affiliate/api/search/station?term=" + phrase;
				return url;
			},
			ajaxCallback: function(stations){
				console.log("\n------------------------------------\nAutocomplete response data : \n", stations, "\n------------------------------------\n");
				var departureCode = $("#idOrigin").val();
					
				let ignoredStationCodeIndex = ignoredList.code.findIndex(
					(item, index) => {
						return item === departureCode;
					}
				);
				if (ignoredStationCodeIndex === -1) {
					stations = stations.filter(function(item) {
						return departureCode !== item.code;
					});
					return stations;
				} 
				else {
					stations = stations.filter(function(item) {
						return !ignoredList.code.includes(item.code);
					});
					return stations;
				}
			},
			getValue: "name",
			template: {
				type: "custom",
				method: function(value, item) {
					return value + " - <span class=\"autocomplete-description\">"+ AlphaCodeTwoCountryName[item.code.substr(0,2)].Country +"</span>";
				}
			},
			list: {
				match: {
					enabled: true
				},
                maxNumberOfElements: 10,
				onSelectItemEvent: function() {
					var data = $("#arrivalTo").getSelectedItemData();
					$("#idDestination").val(data.code);
					$("#destinationName").val(data.name);
				}
			},
            showCategories:function(){
                return isArrivalInputFocus && $("#arrivalTo").val().length <= 0;
            },
            categories: [{  
                header: "Popular destinations from ",
                maxNumberOfElements: 10
            }]
		};

		$("#departFrom").easyAutocomplete(departOptions);

		$("#arrivalTo").easyAutocomplete(arrivalOptions);

    </script>

</div>
		<?php 
			if(logipro_get_ozy_data('hide_everything_but_content') < 1): ?> 
        
        <div class="clear"></div>
        
        </div><!--.container-->  

		<?php 
			logipro_ozy_blog_pagination();
			?>

	<div class="footer-tagline-container">		
		<div id="custom-footer-tagline" class="no-sidebar has-title custom-footer">
		<?php dynamic_sidebar( 'Footer_Tagline' ); ?>
			</div>
		</div>
      <?php       /*footer widget bar and footer*/
            include(get_stylesheet_directory() . '/include/footer-bars.php');
        ?>
        
    </div><!--#main-->

	<script src="https://cdnjs.cloudflare.com/ajax/libs/ClientJS/0.1.11/client.min.js"></script>
    <script async="async" type="text/javascript">
        var width = screen.width;

        function manageUIOnMobile(){
            jQuery( '<a href="https://easy.com" id="family-logo" target="_blank"><img id="family-logo-default" src="/wp-content/uploads/2019/01/part_of_the_easy_family_of_brands-300x42.png" alt="logo"></a>' ).insertAfter( "#top-menu #logo" );
            
            var isMobile ='';
            //var client = new ClientJS();
            isMobile = (width < 768);//client.isMobile();
            if(isMobile==true && !jQuery('body').hasClass('home') && !jQuery('body').hasClass('page-id-13394')){
                //jQuery('h5:contains("In This Section...")').parent().addClass('rightbar-section');
                //jQuery('h5:contains("In This Section...")').addClass('rightbar-toggle');
                jQuery('h5:contains("In this Section...")').addClass('rightbar-toggle');
                jQuery('h4:contains("In This Section...")').addClass('rightbar-toggle');
                jQuery("h5,h4").each(function(index, ele){
                    if(jQuery(this).html() === 'In This Section...'){
                        jQuery(this).addClass('rightbar-toggle');
                    }
                });
                jQuery("li .custom-html-widget").closest("ul").parent().parent().parent().addClass('rightbar-section');
                if(jQuery('.rightbar-iframe').length == 0){
                    if(jQuery(".rightbar-toggle").length > 0) {
                        jQuery('<h5 style="color: #ff6600;text-align: left" class="rightbar-iframe">Search and Book</h5>').insertAfter(jQuery(".rightbar-toggle").next("#vmenu"));
                        //it doesn't exist
                    }else{
                        jQuery(".rightbar-section").prepend('<h5 style="color: #ff6600;text-align: left" class="rightbar-iframe ">Search and Book</h5>');					
                    }
                }
                jQuery('.rightbar-iframe').next('.wpb_widgetised_column').addClass('bookingiframe');
                setTimeout(function(){
                    jQuery('.rightbar-iframe').parent().find('.vc_custom_heading').addClass('rightbar-toggle');
                },1000);
                //jQuery( '<h5 style="color: #ff6600;text-align: left">Search and Book</h5>' ).find('.rightbar-section').insertAfter( "#vmenu" );
                //jQuery("li .custom-html-widget #iframeBookingEngine").closest("ul").parent().parent().parent().prepend('<h5 style="color: #ff6600;text-align: left">Search and Book</h5>');
                
                jQuery('#vmenu').css('display','none');
                
                jQuery('.page-share-buttons').css('display','none'); 
                if(jQuery('#headerShare').length == 0)
                    jQuery('#header').append('<ul class="page-share-buttons" id="headerShare"><li><a href="https://www.facebook.com/sharer/sharer.php?u=https://www.easybus.com/en/" target="_blank" title="Facebook" class="symbol-facebook"><span class="symbol"></span></a></li><li><a href="https://twitter.com/intent/tweet?text=http://easybus.ibooking.com/en/routes/london-gatwick/gatwick-airport-heathrow-airport/" target="_blank" title="Twitter" class="symbol-twitter"><span class="symbol"></span></a></li><li><a href="https://plus.google.com/share?url=https://www.easybus.com/en/" target="_blank" title="Google+" class="symbol-googleplus"><span class="symbol"></span></a></li><li><a href="http://pinterest.com/pin/create/button/?url=http://easybus.ibooking.com/en/routes/london-gatwick/gatwick-airport-heathrow-airport/" target="_blank" title="Pinterest" class="symbol-pinterest"><span class="symbol"></span></a></li><li><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.easybus.com/en/" target="_blank" title="LinkedIn" class="symbol-linkedin"><span class="symbol"></span></a></li><li><a href="#" class="page-social-share"><span class="oic-share-outline"></span></a></li></ul>');
                else
                    jQuery('#headerShare').css('display','block');
                
            }else{
                jQuery('.page-share-buttons').css('display','block');
                jQuery('#headerShare').css('display','none');  
                jQuery('#iframeBookingEngine').parents('.wpb_widgetised_column').addClass('bookingiframe1');
                console.log(isMobile+'mobile'); 
            }
        }

        jQuery(document).ready(function() {
            jQuery( window ).resize(function() {
                width = screen.width;
                console.log("Current Device Width : ",width);
                manageUIOnMobile();	
            });
            manageUIOnMobile();
            
            jQuery('.templatera-footer').parent().addClass('footerhide');

            
            var originCountryCode = $("#idOrigin").val();
            var destinationCountryCode = $("#idDestination").val();
            var selectedOriginName = $("#OriginName").val();
            var selectedDestinationName = $("#destinationName").val();


            if(originCountryCode && originCountryCode != ''){
                originCountryCode = originCountryCode.substring(0,2);
            }
            if(destinationCountryCode && destinationCountryCode != ''){
                destinationCountryCode = destinationCountryCode.substring(0,2);
            }

            var originRouteList = $('#originRouteList');

            $('#originRouteList').html('');
            if(originCountryCode.length == 2){
                $.get("https://bookings.easybus.com/affiliate/api/search/landing_page_links?countryCode="+originCountryCode+"&originStationName="+selectedOriginName, function(data, status){
                    console.log("Data: ", data);
                    var countryName = '';
                    $.each(data,function(index,route){
                        if(countryName == '')
                        countryName = route.Country ;
                        var li = $('<li/>')
                        .appendTo(originRouteList);
                        
                        var aaa = $('<a/>')
                        .text(route.Origin+' to '+route.Destination)
                        .attr('href','/en/routes/'+route.LandingPageUrl)
                        .appendTo(li);
                    });
                    if(countryName.length>0){
                        $('#originCountry').html(selectedOriginName);
                        $('.popular-routes-links').removeClass('d-none');
                    }
                    else
                        $('#originCountry').parent().parent().hide();
                });
            }

            var departureRouteList = $('#departureRouteList');
            $('#departureRouteList').html('');

console.log("country_code=="+destinationCountryCode);
console.log("==origin_code=="+selectedDestinationName);


            if(destinationCountryCode.length == 2){
                $.get("https://bookings.easybus.com/affiliate/api/search/landing_page_links?countryCode="+destinationCountryCode+"&originStationName="+selectedDestinationName, function(data, status){
                    console.log("Data: ", data);
                    var countryName = '';
                    $.each(data,function(index,route){
                        if(countryName == '')
                        countryName = route.Country ;
                        var li = $('<li/>')
                        .appendTo(departureRouteList);
                        
                        var aaa = $('<a/>')
                        .text(route.Origin+' to '+route.Destination)
                        .attr('href','/en/routes/'+route.LandingPageUrl)
                        .appendTo(li);
                    });
                    if(countryName.length>0){
                        $('#departureCountry').html(selectedDestinationName);
                        $('.popular-routes-links').removeClass('d-none');
                    }
                    else
                        $('#departureCountry').parent().parent().hide();

                });
            }
        });
        jQuery(function(){
            jQuery('.rightbar-toggle').on('click',function(){
                jQuery('#vmenu').toggle();
            });
        });
        jQuery(function(){
            jQuery('.rightbar-iframe').on('click',function(){
                jQuery('.wpb_widgetised_column').toggle();
            });
        });
    </script>
    
    <?php
		endif;	
		
		if(logipro_ozy_get_theme_mod('back_to_top_button') == '1' && logipro_get_ozy_data('hide_everything_but_content') <= 0 ) {	
	?>
            <div class="logipro-btt-container">
                <div class="top logipro-btt"><img src="<?php echo esc_url(LOGIPRO_OZY_BASE_URL) ?>images/up-arrow.svg" class="svg" alt=""/><img src="<?php echo esc_url(LOGIPRO_OZY_BASE_URL) ?>images/up-arrow-2.svg" class="svg" alt=""/></div>
</div>

    <?php
		}
		
		if(isset($ozyLogiProHelper->footer_html)) echo $ozyLogiProHelper->footer_html;		
		
		echo '</div>'; //body bg wrapper end		

		if(logipro_get_ozy_data('is_animsition_active')) echo '</div><!--.animsition-->';
		
		wp_footer();		
	?>
<script async="async" type="text/javascript">
	jQuery(window).load(function() {
		jQuery('.gm-style-iw').parent().parent().addClass('black-caption');
		});
</script>
<style>
.easy-autocomplete-container ul .eac-category {
    background: #ff6600;
    color: #fff;
}
body.admin-bar #header {
    margin-top: 0!important;
}
.no-schedule-container {
    background: #fff;
    padding: 40px 30px;
    min-height: 200px;
}
.no-schedule-head {
    color: #5F5F5F;
    font-weight: 700;
}
.no-schedule-body {
    padding-top: 20px;
    color: #5F5F5F;
    font-weight: 500;
}
.no-schedule-body button {
    border: 0;
    padding: 6px;
    min-width: 58px;
    text-align: center;
    background-color: #ff6600;
    color: #fff;
    border-radius: 4px;
}
.no-schedule-body-ctrl{
    position:relative;
}
.no-schedule-body #searchDate {
    width: 20px!important;
    height: 20px;
    position: absolute;
    top:5px;
    left:10px;
    z-index: -1;
}
</style>
<?php 
get_footer();
?>
