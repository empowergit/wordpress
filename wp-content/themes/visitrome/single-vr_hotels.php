<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package VisitRome
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :the_post();
           $post_id = get_the_ID();
           $hotel_address = get_post_meta($post_id, 'hotel_address', true);
           $facilities = get_the_terms($post_id, 'hotel_facilities');
           $hotel_popular_facilities = get_the_terms($post_id, 'hotel_popular_facilities');
           $hotel_price = get_post_meta($post_id, 'hotel_price', true);
           $hotel_policy = get_post_meta($post_id, 'hotel_policy', true);
           $hotel_gallery = get_post_meta($post_id, 'hotel_gallery', true);
           $location_on_map=get_post_meta($post_id, 'location_on_map', true);
           $latitude=  $location_on_map["lat"];
           $longitude= $location_on_map["lng"];
           //echo $booking_com_url = get_post_meta($post_id, 'booking_com_url', true);
		?>	
         <h1 class=""><?php echo get_the_title();?></h1>
         <div class="">
          Price:
          <?php 
                echo format_money($hotel_price);
          ?>
         </div>
         <div class=""><?php echo $hotel_address;?></div>
         <?php include("google_map.php");?>
         <div class=""><?php echo get_the_content();?></div>
         <div class="">
          <h2>Popular Facilities</h2>
          <?php
              if(!empty($hotel_popular_facilities)){
                foreach ($hotel_popular_facilities as $term) {
                  $terms = get_queried_object();
                  $icon = get_field('hotel_popular_facility_icon', "term_".$term->term_id);
                  echo "<div>".$icon.'</div>';
                  echo "<div>".$term->name.'</div>';  
                }
              } 
          ?>
         </div>
         <div class="">
          <h2>Facilities</h2>
         	<?php
              if(!empty($facilities)){
              	foreach ($facilities as $term) {
                  $icon = get_field('hotel_facility_icon', "term_".$term->term_id);
                  echo "<div>".$icon.'</div>';
                  echo "<div>".$term->name.'</div>'; 
                  $items = get_field('hotel_facility_items', "term_".$term->term_id);
                  echo $items;
              	}
              } 
         	?>
         </div>
         
         <div class="">
         	<?php echo $hotel_policy;?>
         </div>
         <div>
         	<?php
                foreach ($hotel_gallery as $value) {
                    ?>
                    <img src="<?php echo wp_get_attachment_image_url($value, [870, 555]) ?>">
                    <?php
                }
            ?>
         </div>
		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>
