<?php
// Include the Theme Custom posts
require_once dirname( __FILE__ ) . '/inc/theme-custom-post.php';

// Include Advanced Custom fields
require_once dirname( __FILE__ ) . '/inc/theme-custom-fields.php';


/**
 * VisitRome functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package VisitRome
 */

if ( ! function_exists( 'visitrome_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function visitrome_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on VisitRome, use a find and replace
		 * to change 'visitrome' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'visitrome', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'visitrome' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'visitrome_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'visitrome_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function visitrome_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'visitrome_content_width', 640 );
}
add_action( 'after_setup_theme', 'visitrome_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function visitrome_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'visitrome' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'visitrome' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'visitrome_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function visitrome_scripts() {
	wp_enqueue_style( 'visitrome-style', get_stylesheet_uri() );

	wp_enqueue_script( 'visitrome-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'visitrome-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'visitrome_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
// Admin CSS
add_action('admin_head', 'visitrome_admin_panel_css');
function visitrome_admin_panel_css() {
  $url = get_stylesheet_directory_uri() . '/admin/admin.css';	
  echo '<link href="'.$url.'" rel="stylesheet"/>';
}

// Admin JS
function visitrome_admin_js() {
	//global $post_type;;
    echo '<script>var ajaxUrl="'.admin_url("admin-ajax.php").'"</script>';
	//if( 'vr_hotels' == $post_type )
    $url = get_stylesheet_directory_uri() . '/admin/admin.js';
    echo '"<script type="text/javascript" src="'. $url . '"></script>"';
}
add_action('admin_footer', 'visitrome_admin_js');

// Apply Website colours changes from admin 
function theme_get_customizer_css() {
    ob_start();

    $text_color = get_theme_mod( 'text_color', '' );
    if ( ! empty( $text_color ) ) {
      ?>
      body {
        color: <?php echo $text_color; ?>;
      }
      <?php
    }


    $link_color = get_theme_mod( 'link_color', '' );
    if ( ! empty( $link_color ) ) {
      ?>
      a {
        color: <?php echo $link_color; ?>;
        border-bottom-color: <?php echo $link_color; ?>;
      }
      <?php
    }

    
    $border_color = get_theme_mod( 'border_color', '' );
    if ( ! empty( $border_color ) ) {
      ?>
      input,
      textarea {
        border-color: <?php echo $border_color; ?>;
      }
      <?php
    }

    
    $accent_color = get_theme_mod( 'accent_color', '' );
    if ( ! empty( $accent_color ) ) {
      ?>
      a:hover {
        color: <?php echo $accent_color; ?>;
        border-bottom-color: <?php echo $accent_color; ?>;
      }

      button,
      input[type="submit"] {
        background-color: <?php echo $accent_color; ?>;
      }
      <?php
    }

    
    $sidebar_background = get_theme_mod( 'sidebar_background', '' );
    if ( ! empty( $sidebar_background ) ) {
      ?>
      .sidebar {
        background-color: <?php echo $sidebar_background; ?>;
      }
      <?php
    }

    $css = ob_get_clean();
    return $css;
}
function theme_enqueue_styles() {
  wp_enqueue_style( 'theme-styles', get_stylesheet_uri() ); // This is where you enqueue your theme's main stylesheet
  $custom_css = theme_get_customizer_css();
  wp_add_inline_style( 'theme-styles', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

// Change theme colour from admin
function theme_customize_register( $wp_customize ) {
		// Text color
		$wp_customize->add_setting( 'text_color', array(
		  'default'   => '',
		  'transport' => 'refresh',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'text_color', array(
		  'section' => 'colors',
		  'label'   => esc_html__( 'Text color', 'theme' ),
		) ) );

		// Link color
		$wp_customize->add_setting( 'link_color', array(
		  'default'   => '',
		  'transport' => 'refresh',
		  'sanitize_callback' => 'sanitize_hex_color',
		) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
		  'section' => 'colors',
		  'label'   => esc_html__( 'Link color', 'theme' ),
		) ) );
}

add_action( 'customize_register', 'theme_customize_register' );

// Google Map API key for Advanced Custom fields
function my_acf_google_map_api( $api ){
  $key = ot_get_option("visit_domain_google_api_key");
  if(!empty($key)){
    $api['key'] = $key;
  }else{
    $api['key'] ="AIzaSyBNBkTy4fR0wc7UyYJrOPasx_5J8EPZ5M4";
  }
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// Add Theme Options in option tree
function custom_theme_options() {
  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
    return false;

  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array(
    'contextual_help' => array(
      'content'       => array( 
        array(
          'id'        => 'general_help',
          'title'     => 'General',
          'content'   => '<p>Help content goes here!</p>'
        )
      ),
      'sidebar'       => '<p>Sidebar content goes here!</p>',
    ),
    'sections'        => array(
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
      array(
        'id'          => 'visit_domain_social_link_section',
        'title'       => 'Social Links'
      ),
      array(
        'id'          => 'visit_domain_currency_section',
        'title'       => 'Currencies'
      ),
      array(
        'id'          => 'visit_domain_api_configure',
        'title'       => 'API Configure'
      )
    ),
    'settings'        => array(
       array(
         'id'          => 'visit_domain_google_api_key',
         'label'       => 'Google API key',
         'desc'        => '',
         'std'         => '',
         'type'        => 'text',
         'class'        => '',
         'operator'        => 'and',
         'section'        => 'visit_domain_api_configure'
       ),
       array(
         'id'          => 'visit_domain_contact_no',
         'label'       => 'Contact no',
         'desc'        => '',
         'std'         => '',
         'type'        => 'text',
         'rows'        => '',
         'post_type'   => '',
         'taxonomy'    => '',
         'min_max_step'        => '',
         'class'        => '',
         'condition'        => '',
         'operator'        => 'and',
         'section'        => 'general'
       ),    	
       array(
         'id'          => 'visit_domain_copyright',
         'label'       => 'Copyright',
         'desc'        => '',
         'std'         => '',
         'type'        => 'text',
         'rows'        => '',
         'post_type'   => '',
         'taxonomy'    => '',
         'min_max_step'        => '',
         'class'        => '',
         'condition'        => '',
         'operator'        => 'and',
         'section'        => 'general'
       ),
       array(
         'id'          => 'visit_domain_social_links',
         'label'       => 'Social Links',
         'desc'        => '',
         'std'         => '',
         'type'        => 'social-links',
         'rows'        => '',
         'post_type'   => '',
         'taxonomy'    => '',
         'min_max_step'        => '',
         'class'        => '',
         'condition'        => '',
         'operator'        => 'and',
         'section'        => 'visit_domain_social_link_section'
       ),
       array(
        'id'          => 'visit_domain_currency_list',
        'label'       => 'List of currencies',
        'desc'        => '',
        'std'         => array(
                            array(
                                  'title' => 'USD',
                                  'visit_domain_currency_name' => 'USD',
                                  'visit_domain_currency_symbol' => '$',
                                  'visit_domain_currency_rate' => 1,
                                  'visit_domain_currency_thousand_separator' => '.',
                                  'visit_domain_currency_decimal_separator' => ','
                            )
                         ),
        'type'        => 'list-item',
        'section'     => 'visit_domain_currency_section',
        'class'       => '',
        'choices'     => array(),
        'settings'    => array(
          array(
            'id'      => 'visit_domain_currency_name',
            'label'   => 'Currency Name',
            'desc'    => '',
            'std'     => '',
            'type'    => 'select',
            'class'   => 'unique_currency',
            'operator' => 'and',
            'choices' => getAllCurrency()
          ),
          array(
            'id'      => 'visit_domain_currency_symbol',
            'label'   => 'Currency Symbol',
            'desc'    => '',
            'std'     => '',
            'type'    => 'text',
            'class'   => '',
            'operator' => 'and',
            'choices' => array()
          ),
          array(
            'id'      => 'visit_domain_currency_rate',
            'label'   => 'Exchange rate',
            'desc'    => 'Exchange rate vs Primary Currency',
            'std'     => '',
            'type'    => 'text',
            'class'   => '',
            'operator' => 'and',
            'choices' => array()
          ),
          array(
            'id'      => 'visit_domain_currency_thousand_separator',
            'label'   => 'Thousand Separator',
            'desc'    => 'Optional. Specifies what string to use for thousands separator.',
            'std'     => '',
            'type'    => 'text',
            'class'   => '',
            'operator' => 'and',
            'choices' => array()
          ),
          array(
            'id'      => 'visit_domain_currency_decimal_separator',
            'label'   => 'Decimal Separator',
            'desc'    => 'Optional. Specifies what string to use for decimal point.',
            'std'     => '',
            'type'    => 'text',
            'class'   => '',
            'operator' => 'and',
            'choices' => array()
          )
        )
      ),
      array(
        'id'      => 'visit_domain_primary_currency',
        'label'   => 'Primary Currency',
        'desc'    => 'Select a unit of currency as main currency',
        'std'     => 'USD',
        'type'    => 'select',
        'class'   => '',
        'operator' => 'and',
        'choices' => getCurrencyList(),
        'section' => 'visit_domain_currency_section'    
      ), 
    )
  );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}
add_action( 'init', 'custom_theme_options', 1 );

// Get all currency list
function getAllCurrency(){
	            $all_currency = [
                'ALL' => 'Albania Lek',
                'DZD ' => 'Algeria',
                'AFN' => 'Afghanistan Afghani',
                'ARS' => 'Argentina Peso',
                'AWG' => 'Aruba Guilder',
                'AUD' => 'Australia Dollar',
                'AZN' => 'Azerbaijan New Manat',
                'BSD' => 'Bahamas Dollar',
                'BHD' => 'Bahraini Dinar',
                'BBD' => 'Barbados Dollar',
                'BDT' => 'Bangladeshi taka',
                'BYN' => 'Belarus Ruble',
                'BZD' => 'Belize Dollar',
                'BMD' => 'Bermuda Dollar',
                'BOB' => 'Bolivia Boliviano',
                'BAM' => 'Bosnia and Herzegovina Convertible Marka',
                'BWP' => 'Botswana Pula',
                'BGN' => 'Bulgaria Lev',
                'BRL' => 'Brazil Real',
                'BND' => 'Brunei Darussalam Dollar',
                'KHR' => 'Cambodia Riel',
                'CAD' => 'Canada Dollar',
                'KYD' => 'Cayman Islands Dollar',
                'CLP' => 'Chile Peso',
                'CNY' => 'China Yuan Renminbi',
                'COP' => 'Colombia Peso',
                'CRC' => 'Costa Rica Colon',
                'HRK' => 'Croatia Kuna',
                'CUP' => 'Cuba Peso',
                'CZK' => 'Czech Republic Koruna',
                'DKK' => 'Denmark Krone',
                'DOP' => 'Dominican Republic Peso',
                'XCD' => 'East Caribbean Dollar',
                'EGP' => 'Egypt Pound',
                'SVC' => 'El Salvador Colon',
                'EEK' => 'Estonia Kroon',
                'EUR' => 'Euro Member Countries',
                'FKP' => 'Falkland Islands (Malvinas) Pound',
                'FJD' => 'Fiji Dollar',
                'GHC' => 'Ghana Cedis',
                'GIP' => 'Gibraltar Pound',
                'GTQ' => 'Guatemala Quetzal',
                'GGP' => 'Guernsey Pound',
                'GYD' => 'Guyana Dollar',
                'GEL' => 'Georgia',
                'HNL' => 'Honduras Lempira',
                'HKD' => 'Hong Kong Dollar',
                'HUF' => 'Hungary Forint',
                'ISK' => 'Iceland Krona',
                'INR' => 'India Rupee',
                'IDR' => 'Indonesia Rupiah',
                'IRR' => 'Iran Rial',
                'IMP' => 'Isle of Man Pound',
                'ILS' => 'Israel Shekel',
                'JMD' => 'Jamaica Dollar',
                'JPY' => 'Japan Yen',
                'JEP' => 'Jersey Pound',
                'KZT' => 'Kazakhstan Tenge',
                'KPW' => 'Korea (North) Won',
                'KRW' => 'Korea (South) Won',
                'KGS' => 'Kyrgyzstan Som',
                'KDS' => 'Kenyan Shilling',
                'KSH' => 'Kenyan shilling',
                'LAK' => 'Laos Kip',
                'LVL' => 'Latvia Lat',
                'LBP' => 'Lebanon Pound',
                'LRD' => 'Liberia Dollar',
                'LTL' => 'Lithuania Litas',
                'MKD' => 'Macedonia Denar',
                'MYR' => 'Malaysia Ringgit',
                'MUR' => 'Mauritius Rupee',
                'MXN' => 'Mexico Peso',
                'MNT' => 'Mongolia Tughrik',
                'MMK' => 'Myanmar Kyats',
                'MAD' => 'Morocco Dirhams',
                'MZN' => 'Mozambique Metical',
                'MGA' => 'Malagasy ariary',
                'NAD' => 'Namibia Dollar',
                'NPR' => 'Nepal Rupee',
                'ANG' => 'Netherlands Antilles Guilder',
                'NZD' => 'New Zealand Dollar',
                'NIO' => 'Nicaragua Cordoba',
                'NGN' => 'Nigeria Naira',
                'NOK' => 'Norway Krone',
                'OMR' => 'Oman Rial',
                'PKR' => 'Pakistan Rupee',
                'PAB' => 'Panama Balboa',
                'PYG' => 'Paraguay Guarani',
                'PEN' => 'Peru Nuevo Sol',
                'SOL' => 'Peruvian Sol',
                'PHP' => 'Philippines Peso',
                'PLN' => 'Poland Zloty',
                'QAR' => 'Qatar Riyal',
                'RON' => 'Romania New Leu',
                'RUB' => 'Russia Ruble',
                'RWF' => 'Rwandan Frank',
                'SHP' => 'Saint Helena Pound',
                'SAR' => 'Saudi Arabia Riyal',
                'RSD' => 'Serbia Dinar',
                'SCR' => 'Seychelles Rupee',
                'SGD' => 'Singapore Dollar',
                'SBD' => 'Solomon Islands Dollar',
                'SOS' => 'Somalia Shilling',
                'ZAR' => 'South Africa Rand',
                'LKR' => 'Sri Lanka Rupee',
                'SEK' => 'Sweden Krona',
                'CHF' => 'Switzerland Franc',
                'SRD' => 'Suriname Dollar',
                'SYP' => 'Syria Pound',
                'TWD' => 'Taiwan New Dollar',
                'THB' => 'Thailand Baht',
                'TTD' => 'Trinidad and Tobago Dollar',
                'TRY' => 'Turkey Lira',
                'TRL' => 'Turkey Lira',
                'TVD' => 'Tuvalu Dollar',
                'TD' => 'Tunisian Dinar',
                'TZS' => 'Tanzanian Shilling',
                'UAH' => 'Ukraine Hryvna',
                'AED' => 'United Arab Emirates',
                'GBP' => 'United Kingdom Pound',
                'USD' => 'United States Dollar',
                'UYU' => 'Uruguay Peso',
                'UZS' => 'Uzbekistan Som',
                'UGX' => 'Ugandian Shilling',
                'VEF' => 'Venezuela Bolivar',
                'VND' => 'Viet Nam Dong',
                'YER' => 'Yemen Rial',
                'CFA' => 'West African Franc',
                'ZWD' => 'Zimbabwe Dollar',
                'ZMW' => 'Zambian Kwacha'
            ];
            $a = [];
            foreach ($all_currency as $key => $value) {
                $a[] = [
                    'value' => $key,
                    'label' => $value . '(' . $key . ' )'
                ];
            }
             return $a;
}

// Get Saved Currency list
function getCurrencyList(){
  $rows =array();
  $settings = get_option("option_tree");
  if(!empty($settings["visit_domain_currency_list"])){
    foreach($settings["visit_domain_currency_list"] as $value){
      $rows[]=$value;
    }
  }
  $choice = [];
  if(!empty($rows)){
  foreach ($rows as $key => $value) {
        $choice[] = [

            'label' => $value['title'],
            'value' => $value['visit_domain_currency_name']
        ];
    }
  }else{
        $choice[] = [

            'label' => "USD",
            'value' => "USD"
        ];    
  }    
  return $choice;
}

// Convert Currency
function format_money($price){
	    $money = (float)$price;
        $base = ot_get_option('visit_domain_primary_currency');
        if($_SESSION["current_currency"] == $base){
          $base = ot_get_option('visit_domain_primary_currency');  	
        }else{
           $base = $_SESSION["current_currency"];	
        }
	    $list = ot_get_option("visit_domain_currency_list");
	    foreach($list as $value){
	      if($value["visit_domain_currency_name"]==$base){
	      	$symbol = $value["visit_domain_currency_symbol"];
	      	$precision="2";
	      	$rate = $value["visit_domain_currency_rate"];
	      	$thousand_separator = $value["visit_domain_currency_thousand_separator"];
	      	$decimal_separator = $value["visit_domain_currency_decimal_separator"];
	      	if($_SESSION["current_currency"] != ot_get_option('visit_domain_primary_currency')){

                $money=$money*$rate; 	
             }
	      	$money = round($money, 2);
	      	$money = number_format((float)$money, (int)$precision, $decimal_separator, $thousand_separator);
	      	$money_string = $symbol . " " . $money;
	      }	
	    }
    return $money_string;
}

// Check Currency exists using AJAX
add_action("wp_ajax_checkCurrencyExists", "checkCurrencyExists");
add_action("wp_ajax_nopriv_checkCurrencyExists", "checkCurrencyExists");
function checkCurrencyExists(){
	$currency = $_POST["currency"];
	$settings = get_option("option_tree");
	if(!empty($settings)){
        foreach($settings["visit_domain_currency_list"] as $value){
        	$rows[]=$value["visit_domain_currency_name"];
        }

        if(in_array($currency, $rows)){
         	echo "0";
			wp_die();
		}else{
			echo "1";
		    wp_die();
		}
	}else{
            echo "1";
		    wp_die();
	}
}

// On theme activation show required plugins message
require_once dirname( __FILE__ ) . '/lib/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'visitrome_require_plugins' ); 
function visitrome_require_plugins() {
    $plugins = array(
        array(
            'name'               => 'OptionTree',
            'slug'               => 'OptionTree',
            'source'             => 'st-option-tree.zip', 
            'required'           => true, // this plugin is required
            'version'            => '2.7.3', // the user must use version 2.7.3 (or higher) of this plugin
            'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
        ),
        array(
            'name'               => 'Advanced Custom Fields PRO',
            'slug'               => 'Advanced-Custom-Fields-PRO',
            'source'             => 'advanced-custom-fields-pro-master.zip', 
            'required'           => true, // this plugin is required
            'version'            => '5.7.10', // the user must use version 5.7.10 (or higher) of this plugin
            'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
        ),
        array(
            'name'               => 'Easy Google Fonts',
            'slug'               => 'easy-google-fonts',
            'source'             => 'easy-google-fonts.zip', 
            'required'           => true, // this plugin is required
            'version'            => '1.4.4', // the user must use version 5.7.10 (or higher) of this plugin
            'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
        )
        
    );

    $config = array(
        'id'           => 'visitrome-tgmpa', 
        'default_path' => get_stylesheet_directory() . '/lib/plugins/', 
        'menu'         => 'visitrome-install-required-plugins', 
        'has_notices'  => true, 
        'dismissable'  => false, 
        'dismiss_msg'  => 'The following plugins are required for proper functioning of theme', 
        'is_automatic' => true, 
        'message'      => '<!--Hey there.-->', 
        'strings'      => array() 
    );

    tgmpa( $plugins, $config );
}

add_action('init','visitrome_init_function');
function visitrome_init_function(){
   if(!empty($_GET["currency"])){
      $_SESSION["current_currency"]=$_GET["currency"];
   }
}