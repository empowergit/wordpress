jQuery( ".hotel_address .acf-input-wrap input" ).on("keyup", function(){	
  var address = jQuery(this).val();
  jQuery(".location_on_map .search").val(address);
  jQuery( ".location_on_map .search" ).focus();
  jQuery( ".hotel_address .acf-input-wrap input" ).focus();
});

jQuery(".unique_currency").change(function(){
  var currency = jQuery(this).val();
  var id = jQuery(this).attr('id');
  jQuery('#'+id).after("");
  jQuery.ajax({
	    type: "POST",
	    data: {'action': 'checkCurrencyExists', 'currency': currency},
	    url: ajaxUrl,
	    success: function (result) {
	       if(result=='0'){
             jQuery('#setting_'+id).after("<div class='errorCurrency'>Already Exists.Please choose different.</div>");
	       }else{
	       	 jQuery('#'+id).after("");
	       }
	    }
  }); 
});


