<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'testwp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Z*W0PE*;-RR1p1!x8Nd;gw^4mRA%Goc:FbKU;y4JoY(7N5q5Ap]3dxnqE0AW?jG7' );
define( 'SECURE_AUTH_KEY',  '9W41g8u7tGMxZxSX;Cr<<gh+naG2Gx-:R|%%^1V+R_)`]m:qBc&u$1dX|!>G~l^o' );
define( 'LOGGED_IN_KEY',    '8J&5LfzS7mg,ov*]Q~ri|8yH?gsb[.Ozm3yEAu_@J;$od,5i&[L_wT(oPBe@lrD<' );
define( 'NONCE_KEY',        'Cr(<]UJ%WN&{}p5Ry(WF|[{uU@@J_KPz.:%(uUf}._Ngt.iOm$By]hg@|TA+|B}j' );
define( 'AUTH_SALT',        '(}<g;$1N P{wd!YwqBP(:Ul;9pZI#7m1G?o^od@H|4(Jc[8fIF:<B*bs?*!4#NX#' );
define( 'SECURE_AUTH_SALT', '-d0CmX6`jZ^|9FO(tTQu8V=3(+ICj@GFBjmQWdLW0L0OZAbwZBVZJhkF)oK4^bl`' );
define( 'LOGGED_IN_SALT',   'Q&[NV*c_,C2;#0hDZ,ZypC*yvrUywP/[5*}u8b Gp=6J[bOiS6Wd<|PAc};Dcz$u' );
define( 'NONCE_SALT',       'g-iLqlsolL+1~VtAG*O;r70%w4]~Rw /AiyF?0oemikAL:xKQf%}ab6,<HaPzq<O' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
